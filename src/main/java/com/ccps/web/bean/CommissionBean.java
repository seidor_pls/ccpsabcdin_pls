package com.ccps.web.bean;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.persistence.Column;
import javax.persistence.Id;

import org.primefaces.context.RequestContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.ccps.web.common.CommonMessages;
import com.ccps.web.model.BaseType;
import com.ccps.web.model.Commission;
import com.ccps.web.model.CommissionPlan;
import com.ccps.web.model.Company;
import com.ccps.web.model.Enterprise;
import com.ccps.web.model.repository.BaseTypeRepository;
import com.ccps.web.model.repository.CommissionPlanRepository;
import com.ccps.web.model.repository.CommissionRepository;
import com.ccps.web.model.repository.CompanyRepository;
import com.ccps.web.model.repository.EnterpriseRepository;

/**
 * author
 * plopezs
 */

@ManagedBean(name="commissionBean")
@SessionScoped
@Component(value="commissionBean")
@Scope("session")
public class CommissionBean implements Serializable{

	private static final long serialVersionUID = -3330873784361551084L;
	
	@Autowired
	private CompanyRepository companyRepository;
	
	@Autowired
	private CommissionRepository commissionRepository;
	
	@Autowired
	private CommissionPlanRepository commissionPlanRepository;
	
	@Autowired
	private BaseTypeRepository baseTypeRepository;
	
	@Autowired
	private EnterpriseRepository enterpriseRepository;

	private List<Commission> colCommissions;
	
	private List<CommissionPlan> colCommissionPlans;
	
	private List<BaseType> colBaseTypes;
	
	private List<Company> colCompanies;
	
	private List<Enterprise> colEnterprises;
	
	//Que tipo de guardado se está realizando actualmente (new, update, comision o plan de comision)
	private String currentPersistenceMode = "";
	
	//Campos de Commission
	private int idCommission;
	
	//@Column(nullable=false)
	private int companyId;
	
	//@Column(nullable=false)
	private int enterpriseId;
	
	//@Column(nullable=false)
	private Integer codProducto;
	
	//@Column(nullable=false)
	private Integer rubro;
	
	//@Column(nullable=false)
	private Double comFijaAnticipada;
	
	//@Column(nullable=false)
	private Double comCorredora;
	
	//@Column(nullable=false)
	private Double comRecCobranza;
	
	//@Column(nullable=true)
	private Double primaNeta;
	
	//@Column(nullable=true)
	private Double primaBruta;
	
	//@Column(nullable=true)
	private Double porcExenta;

	//@Column(nullable=true)
	private Double porcAfecta;
	
	//@Column(nullable=true)
	private Double porcNeta;
	
	//@Column(nullable=true)
	private Double porcIva;
	
	//@Column(nullable=true)
	private Double porcBruta;
	
	//Campos de CommissionPlan
	private int idCommissionPlan;
	
	//@Column(nullable=false)
	private String tipoPlan;
	
	//@Column(nullable=true)
	private Double primaFinalSeguroRobo;
	
	//@Column(nullable=true)
	private Double comInterPrimaNeta;
	
	//@Column(nullable=true)
	private Integer rangoInferior;
	
	//@Column(nullable=true)
	private Integer rangoSuperior;
	
	private Boolean cpEnabled;
	
	private Boolean comEnabled;
	
	//@Column(nullable=false)
	private int idBaseType;
	
	//@Column(nullable=false)
	private String poliza;
	
	//@Column(nullable=false)
	private String nombrePoliza;
	
	//@Column(nullable=false)
	private String tipo;
	
	//@Column(nullable=false)
	private String nombreInterno;
	
	//@Column(nullable=false)
	private Double comIntermediacion;
	
	//@Column(nullable=false)
	private Double tasaTecnica;
	
	private Boolean baseEnabled;
	
	public List<Commission> getColCommissions() {
		return colCommissions;
	}

	public void setColCommissions(List<Commission> colCommissions) {
		this.colCommissions = colCommissions;
	}

	public List<CommissionPlan> getColCommissionPlans() {
		return colCommissionPlans;
	}

	public void setColCommissionPlans(List<CommissionPlan> colCommissionPlans) {
		this.colCommissionPlans = colCommissionPlans;
	}

	public List<BaseType> getColBaseTypes() {
		return colBaseTypes;
	}

	public void setColBaseTypes(List<BaseType> colBaseTypes) {
		this.colBaseTypes = colBaseTypes;
	}

	public List<Company> getColCompanies() {
		return colCompanies;
	}

	public void setColCompanies(List<Company> colCompanies) {
		this.colCompanies = colCompanies;
	}

	public List<Enterprise> getColEnterprises() {
		return colEnterprises;
	}

	public void setColEnterprises(List<Enterprise> colEnterprises) {
		this.colEnterprises = colEnterprises;
	}

	public String getCurrentPersistenceMode() {
		return currentPersistenceMode;
	}

	public void setCurrentPersistenceMode(String currentPersistenceMode) {
		this.currentPersistenceMode = currentPersistenceMode;
	}

	public int getIdCommission() {
		return idCommission;
	}

	public void setIdCommission(int idCommission) {
		this.idCommission = idCommission;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	public int getEnterpriseId() {
		return enterpriseId;
	}

	public void setEnterpriseId(int enterpriseId) {
		this.enterpriseId = enterpriseId;
	}

	public Integer getCodProducto() {
		return codProducto;
	}

	public void setCodProducto(Integer codProducto) {
		this.codProducto = codProducto;
	}

	public Integer getRubro() {
		return rubro;
	}

	public void setRubro(Integer rubro) {
		this.rubro = rubro;
	}

	public Double getComFijaAnticipada() {
		return comFijaAnticipada;
	}

	public void setComFijaAnticipada(Double comFijaAnticipada) {
		this.comFijaAnticipada = comFijaAnticipada;
	}

	public Double getComCorredora() {
		return comCorredora;
	}

	public void setComCorredora(Double comCorredora) {
		this.comCorredora = comCorredora;
	}

	public Double getComRecCobranza() {
		return comRecCobranza;
	}

	public void setComRecCobranza(Double comRecCobranza) {
		this.comRecCobranza = comRecCobranza;
	}

	public Double getPrimaNeta() {
		return primaNeta;
	}

	public void setPrimaNeta(Double primaNeta) {
		this.primaNeta = primaNeta;
	}

	public Double getPrimaBruta() {
		return primaBruta;
	}

	public void setPrimaBruta(Double primaBruta) {
		this.primaBruta = primaBruta;
	}

	public Double getPorcExenta() {
		return porcExenta;
	}

	public void setPorcExenta(Double porcExenta) {
		this.porcExenta = porcExenta;
	}

	public Double getPorcAfecta() {
		return porcAfecta;
	}

	public void setPorcAfecta(Double porcAfecta) {
		this.porcAfecta = porcAfecta;
	}

	public Double getPorcNeta() {
		return porcNeta;
	}

	public void setPorcNeta(Double porcNeta) {
		this.porcNeta = porcNeta;
	}

	public Double getPorcIva() {
		return porcIva;
	}

	public void setPorcIva(Double porcIva) {
		this.porcIva = porcIva;
	}

	public Double getPorcBruta() {
		return porcBruta;
	}

	public void setPorcBruta(Double porcBruta) {
		this.porcBruta = porcBruta;
	}

	public int getIdCommissionPlan() {
		return idCommissionPlan;
	}

	public void setIdCommissionPlan(int idCommissionPlan) {
		this.idCommissionPlan = idCommissionPlan;
	}

	public String getTipoPlan() {
		return tipoPlan;
	}

	public void setTipoPlan(String tipoPlan) {
		this.tipoPlan = tipoPlan;
	}

	public Double getPrimaFinalSeguroRobo() {
		return primaFinalSeguroRobo;
	}

	public void setPrimaFinalSeguroRobo(Double primaFinalSeguroRobo) {
		this.primaFinalSeguroRobo = primaFinalSeguroRobo;
	}

	public Double getComInterPrimaNeta() {
		return comInterPrimaNeta;
	}

	public void setComInterPrimaNeta(Double comInterPrimaNeta) {
		this.comInterPrimaNeta = comInterPrimaNeta;
	}

	public Integer getRangoInferior() {
		return rangoInferior;
	}

	public void setRangoInferior(Integer rangoInferior) {
		this.rangoInferior = rangoInferior;
	}

	public Integer getRangoSuperior() {
		return rangoSuperior;
	}

	public void setRangoSuperior(Integer rangoSuperior) {
		this.rangoSuperior = rangoSuperior;
	}

	public Boolean getCpEnabled() {
		return cpEnabled;
	}

	public void setCpEnabled(Boolean cpEnabled) {
		this.cpEnabled = cpEnabled;
	}

	public Boolean getComEnabled() {
		return comEnabled;
	}

	public void setComEnabled(Boolean comEnabled) {
		this.comEnabled = comEnabled;
	}

	public int getIdBaseType() {
		return idBaseType;
	}

	public void setIdBaseType(int idBaseType) {
		this.idBaseType = idBaseType;
	}

	public String getPoliza() {
		return poliza;
	}

	public void setPoliza(String poliza) {
		this.poliza = poliza;
	}

	public String getNombrePoliza() {
		return nombrePoliza;
	}

	public void setNombrePoliza(String nombrePoliza) {
		this.nombrePoliza = nombrePoliza;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getNombreInterno() {
		return nombreInterno;
	}

	public void setNombreInterno(String nombreInterno) {
		this.nombreInterno = nombreInterno;
	}

	public Double getComIntermediacion() {
		return comIntermediacion;
	}

	public void setComIntermediacion(Double comIntermediacion) {
		this.comIntermediacion = comIntermediacion;
	}

	public Double getTasaTecnica() {
		return tasaTecnica;
	}

	public void setTasaTecnica(Double tasaTecnica) {
		this.tasaTecnica = tasaTecnica;
	}

	public Boolean getBaseEnabled() {
		return baseEnabled;
	}

	public void setBaseEnabled(Boolean baseEnabled) {
		this.baseEnabled = baseEnabled;
	}

	public String getTextoTituloEdicion() {
		if (this.currentPersistenceMode == "newcom" || this.currentPersistenceMode == "newcomplan")
			return "Nueva Comisión";
		
		if (this.currentPersistenceMode == "updatecom" || this.currentPersistenceMode == "updatecomplan")
			return "Modificar Comisión";
		
		if (this.currentPersistenceMode == "newbase")
			return "Nuevo Tipo Base";
		
		if (this.currentPersistenceMode == "updatebase")
			return "Modificar Tipo Base";
		
		return "";
	}

	public void setTextoTituloEdicion(String textoTituloEdicion) {
	}

	//METHODS
	@PostConstruct
	public void PageLoad(){
		this.colCommissions = commissionRepository.findAll();
		this.colCommissionPlans = commissionPlanRepository.findAll();
		this.colBaseTypes = baseTypeRepository.findAll();
		this.colCompanies = companyRepository.findAll();
		this.colEnterprises = enterpriseRepository.findAll();
	}
	
	public void NewCommission()	{
		try
		{
			//Setea tipo de guardado a nueva comision.
			this.currentPersistenceMode = "newcom";
			
			CleanAllFields();
			
			FacesContext.getCurrentInstance().getExternalContext().redirect(FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath() + "/pages/comisiones/updatecom.jsf");
		}
		catch (IOException ex)
		{
			//TODO: Excepcion
		}
	}
	
	public void NewCommissionPlan()	{
		try
		{
			//Setea tipo de guardado a nuevo plan de comision.
			this.currentPersistenceMode = "newcomplan";
			
			CleanAllFields();
			
			FacesContext.getCurrentInstance().getExternalContext().redirect(FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath() + "/pages/comisiones/updatecp.jsf");
		}
		catch (IOException ex)
		{
			//TODO: Excepcion
		}
	}
	
	public void NewBaseType()	{
		try
		{
			//Setea tipo de guardado a nueva comision.
			this.currentPersistenceMode = "newbase";
			
			CleanAllFields();
			
			FacesContext.getCurrentInstance().getExternalContext().redirect(FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath() + "/pages/comisiones/updatebase.jsf");
		}
		catch (IOException ex)
		{
			//TODO: Excepcion
		}
	}
	
	public void UpdateCommission(int id) {
		try
		{
			//Setea tipo de guardado a edición de  comision.
			this.currentPersistenceMode = "updatecom";
			
			this.idCommission = id;
			
			FillUpdateFields(id);
			
			FacesContext.getCurrentInstance().getExternalContext().redirect(FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath() + "/pages/comisiones/updatecom.jsf");
		}
		catch (IOException ex)
		{
			//TODO: Excepcion
		}
	}
	
	public void UpdateCommissionPlan(int id) {
		try
		{
			//Setea tipo de guardado a edición de plan de comision.
			this.currentPersistenceMode = "updatecomplan";
			
			this.idCommissionPlan  = id;
			
			FillUpdateFields(id);
			
			FacesContext.getCurrentInstance().getExternalContext().redirect(FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath() + "/pages/comisiones/updatecp.jsf");
		}
		catch (IOException ex)
		{
			//TODO: Excepcion
		}
	}
	
	public void UpdateBaseType(int id) {
		try
		{
			//Setea tipo de guardado a edición de  comision.
			this.currentPersistenceMode = "updatebase";
			
			this.idBaseType = id;
			
			FillUpdateFields(id);
			
			FacesContext.getCurrentInstance().getExternalContext().redirect(FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath() + "/pages/comisiones/updatebase.jsf");
		}
		catch (IOException ex)
		{
			//TODO: Excepcion
		}
	}
	
	public void CleanAllFields() {
		this.companyId = 0;
		this.enterpriseId = 0;
		this.codProducto = 0;
		this.rubro = 0;
		this.comFijaAnticipada = 0d;
		this.comCorredora = 0d;
		this.comRecCobranza = 0d;
		this.primaNeta = 0d;
		this.primaBruta = 0d;
		this.porcAfecta = 0d;
		this.porcBruta = 0d;
		this.porcExenta = 0d;
		this.porcIva = 0d;
		this.porcNeta = 0d;
		this.tipoPlan = "";
		this.primaFinalSeguroRobo = 0d;
		this.comInterPrimaNeta = 0d;
		this.rangoInferior = 0;
		this.rangoSuperior = 0;
		this.comEnabled = false;
		this.cpEnabled = false;
		this.idBaseType = 0;
		this.poliza = "";
		this.nombrePoliza = "";
		this.tipo = "";
		this.nombreInterno = "";
		this.comIntermediacion = 0d;
		this.tasaTecnica = 0d;
		this.baseEnabled = false;
	}
	
	public void FillUpdateFields(int id) {
		if (this.currentPersistenceMode.equals("updatecom")) {
			
			Commission objCom = commissionRepository.findOne(id);
			
			this.companyId = objCom.getCompany().getId();
			try {
				this.enterpriseId = objCom.getEnterprise().getId();
			}
			catch (Exception ex){
				this.enterpriseId = 0;
			}
			this.codProducto = objCom.getCodProducto();
			this.rubro = objCom.getRubro();
			this.comFijaAnticipada = objCom.getComFijaAnticipada();
			this.comCorredora = objCom.getComCorredora();
			this.comRecCobranza = objCom.getComRecCobranza();
			this.primaNeta = objCom.getPrimaNeta();
			this.primaBruta = objCom.getPrimaBruta();
			this.porcAfecta = objCom.getPorcAfecta();
			this.porcBruta = objCom.getPorcBruta();
			this.porcExenta = objCom.getPorcExenta();
			this.porcIva = objCom.getPorcIva();
			this.porcNeta = objCom.getPorcNeta();
			this.comEnabled = objCom.getEnabled();
		}
		
		if (this.currentPersistenceMode.equals("updatecomplan")) {
			CommissionPlan objCP = commissionPlanRepository.findOne(id);
			
			this.tipoPlan = objCP.getTipoPlan();
			this.primaFinalSeguroRobo = objCP.getPrimaFinalSeguroRobo();
			this.comInterPrimaNeta = objCP.getComInterPrimaNeta();
			this.rangoInferior = objCP.getRangoInferior();
			this.rangoSuperior = objCP.getRangoSuperior();
			this.cpEnabled = objCP.getEnabled();
		}
		
		if(this.currentPersistenceMode.equals("updatebase")) {
			BaseType objBT = baseTypeRepository.findOne(id);
			
			this.poliza = objBT.getPoliza();
			this.nombrePoliza = objBT.getNombrePoliza();
			this.tipo = objBT.getTipo();
			this.nombreInterno = objBT.getNombreInterno();
			this.comIntermediacion = objBT.getComIntermediacion();
			this.tasaTecnica = objBT.getTasaTecnica();
			this.baseEnabled = objBT.getEnabled();
		}
	}
	
	public void CancelEditCommission() {
		try
		{
			//Despues de Guardar Limpia el tipo de guardado actual.
			this.currentPersistenceMode = "";
			this.idCommission = 0;
			this.idCommissionPlan = 0;
			this.idBaseType = 0;
			
			FacesContext.getCurrentInstance().getExternalContext().redirect(FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath() + "/pages/comisiones/list.jsf");
		}
		catch (IOException ex)
		{
			//TODO: Excepcion
		}
	}
	
	public void SaveCommission() {
		try
		{
			if (this.currentPersistenceMode.equals("newcom")) {
				Commission objCom = new Commission();
				Company objCompany = companyRepository.findOne(this.companyId);
				Enterprise objEnterprise = enterpriseRepository.findOne(this.enterpriseId);
				
				objCom.setCodProducto(codProducto);
				objCom.setComCorredora(this.comCorredora);
				objCom.setComFijaAnticipada(this.comFijaAnticipada);
				objCom.setCompany(objCompany);
				objCom.setEnterprise(objEnterprise);
				objCom.setComRecCobranza(this.comRecCobranza);
				objCom.setPrimaBruta(this.primaBruta);
				objCom.setPrimaNeta(this.primaNeta);
				objCom.setRubro(this.rubro);
				objCom.setEnabled(this.comEnabled);
				objCom.setPorcAfecta(this.porcAfecta);
				objCom.setPorcBruta(this.porcBruta);
				objCom.setPorcExenta(this.porcExenta);
				objCom.setPorcIva(this.porcIva);
				objCom.setPorcNeta(this.porcNeta);
				objCom.setEnabled(this.comEnabled);
				
				commissionRepository.save(objCom);
			}
			
			if (this.currentPersistenceMode.equals("updatecom")) {
				Commission objCom = commissionRepository.findOne(this.idCommission);
				Company objCompany = companyRepository.findOne(this.companyId);
				Enterprise objEnterprise = enterpriseRepository.findOne(this.enterpriseId);
				
				objCom.setCodProducto(codProducto);
				objCom.setComCorredora(this.comCorredora);
				objCom.setComFijaAnticipada(this.comFijaAnticipada);
				objCom.setCompany(objCompany);
				objCom.setEnterprise(objEnterprise);
				objCom.setComRecCobranza(this.comRecCobranza);
				objCom.setPrimaBruta(this.primaBruta);
				objCom.setPrimaNeta(this.primaNeta);
				objCom.setRubro(this.rubro);
				objCom.setPorcAfecta(this.porcAfecta);
				objCom.setPorcBruta(this.porcBruta);
				objCom.setPorcExenta(this.porcExenta);
				objCom.setPorcIva(this.porcIva);
				objCom.setPorcNeta(this.porcNeta);
				objCom.setEnabled(this.comEnabled);
				
				commissionRepository.save(objCom);
			}
			
			//Después de Guardar Limpia el tipo de guardado actual.
			this.currentPersistenceMode = "";
			this.idCommission = 0;
			this.idCommissionPlan = 0;
			
			this.colCommissions = commissionRepository.findAll();
			
			FacesContext.getCurrentInstance().getExternalContext().redirect(FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath() + "/pages/comisiones/list.jsf");
			
			CommonMessages.ShowInfoMessage("Info:", "Comisión guardada correctamente.");
		}
		catch (IOException ex)
		{
			//TODO: Excepcion
		}
	}
	
	public void SaveCommissionPlan() {
		try
		{
			if (this.currentPersistenceMode.equals("newcomplan")) {
				CommissionPlan objCom = new CommissionPlan();
				
				objCom.setComInterPrimaNeta(this.comInterPrimaNeta);
				objCom.setPrimaFinalSeguroRobo(this.primaFinalSeguroRobo);
				objCom.setRangoInferior(this.rangoInferior);
				objCom.setRangoSuperior(this.rangoSuperior);
				objCom.setTipoPlan(this.tipoPlan);
				objCom.setEnabled(this.cpEnabled);
				
				commissionPlanRepository.save(objCom);
			}
			
			if (this.currentPersistenceMode.equals("updatecomplan")) {
				CommissionPlan objCom = commissionPlanRepository.findOne(this.idCommissionPlan);
				
				objCom.setComInterPrimaNeta(this.comInterPrimaNeta);
				objCom.setPrimaFinalSeguroRobo(this.primaFinalSeguroRobo);
				objCom.setRangoInferior(this.rangoInferior);
				objCom.setRangoSuperior(this.rangoSuperior);
				objCom.setTipoPlan(this.tipoPlan);
				objCom.setEnabled(this.cpEnabled);
				
				commissionPlanRepository.save(objCom);
			}
			
			//Después de Guardar Limpia el tipo de guardado actual.
			this.currentPersistenceMode = "";
			this.idCommission = 0;
			this.idCommissionPlan = 0;
			
			this.colCommissionPlans = commissionPlanRepository.findAll();
			
			FacesContext.getCurrentInstance().getExternalContext().redirect(FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath() + "/pages/comisiones/list.jsf");
			
			CommonMessages.ShowInfoMessage("Info:", "Comisión guardada correctamente.");
		}
		catch (IOException ex)
		{
			//TODO: Excepcion
		}
	}
	
	public void SaveBaseType() {
		try
		{
			if (this.currentPersistenceMode.equals("newbase")) {
				BaseType objBT = new BaseType();
				
				objBT.setComIntermediacion(this.comIntermediacion);
				objBT.setEnabled(this.baseEnabled);
				objBT.setNombreInterno(this.nombreInterno);
				objBT.setNombrePoliza(this.nombrePoliza);
				objBT.setPoliza(this.poliza);
				objBT.setTasaTecnica(this.tasaTecnica);
				objBT.setTipo(this.tipo);
				
				baseTypeRepository.save(objBT);
			}
			
			if (this.currentPersistenceMode.equals("updatebase")) {
				BaseType objBT = baseTypeRepository.findOne(this.idBaseType);
				
				objBT.setComIntermediacion(this.comIntermediacion);
				objBT.setEnabled(this.baseEnabled);
				objBT.setNombreInterno(this.nombreInterno);
				objBT.setNombrePoliza(this.nombrePoliza);
				objBT.setPoliza(this.poliza);
				objBT.setTasaTecnica(this.tasaTecnica);
				objBT.setTipo(this.tipo);
				
				baseTypeRepository.save(objBT);
			}
			
			//Después de Guardar Limpia el tipo de guardado actual.
			this.currentPersistenceMode = "";
			this.idCommission = 0;
			this.idCommissionPlan = 0;
			this.idBaseType = 0;
			
			this.colBaseTypes = baseTypeRepository.findAll();
			
			FacesContext.getCurrentInstance().getExternalContext().redirect(FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath() + "/pages/comisiones/list.jsf");
			
			CommonMessages.ShowInfoMessage("Info:", "Tipo Base guardado correctamente.");
		}
		catch (IOException ex)
		{
			//TODO: Excepcion
		}
	}

	public void ChangeCompany(AjaxBehaviorEvent event)
	{
		
	}
	
	public void ChangeEnterprise(AjaxBehaviorEvent event)
	{
		
	}

	public void HandleDeleteCommission(int id) {
		try
		{
			//Setea tipo de guardado a edición de  comision.
			this.currentPersistenceMode = "updatecom";
			
			this.idCommission = id;
			
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("$('#dialogConfirmEliminaCom').show();");
		}
		catch (Exception ex)
		{
			//TODO: Excepcion
		}
	}
	
	public void HandleDeleteCommissionPlan(int id) {
		try
		{
			//Setea tipo de guardado a edición de plan de comision.
			this.currentPersistenceMode = "updatecomplan";
			
			this.idCommissionPlan  = id;
			
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("$('#dialogConfirmEliminaComPlan').show();");
		}
		catch (Exception ex)
		{
			//TODO: Excepcion
		}
	}

	public void DeleteCommission() {
		try
		{
			Commission objCom = commissionRepository.findOne(this.idCommission);
			
			commissionRepository.delete(objCom);
			
			this.currentPersistenceMode = "";
			this.idCommission = 0;
			
			this.colCommissions = commissionRepository.findAll();
			
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("$('#dialogConfirmEliminaCom').hide();");
			
			CommonMessages.ShowInfoMessage("Info:", "Comisión eliminada correctamente.");
		}
		catch (Exception ex)
		{
			//TODO: Excepcion
		}
	}
	
	public void DeleteCommissionPlan() {
		try
		{
			CommissionPlan objCom = commissionPlanRepository.findOne(this.idCommissionPlan);
			
			commissionPlanRepository.delete(objCom);
			
			this.currentPersistenceMode = "";
			this.idCommissionPlan = 0;
			
			this.colCommissionPlans = commissionPlanRepository.findAll();
			
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("$('#dialogConfirmEliminaComPlan').hide();");
			
			CommonMessages.ShowInfoMessage("Info:", "Comisión eliminada correctamente.");
		}
		catch (Exception ex)
		{
			//TODO: Excepcion
		}
	}

	public void CancelDeleteCommission()
	{
		this.currentPersistenceMode = "";
		this.idCommission = 0;
		this.idCommissionPlan = 0;
		
		RequestContext context = RequestContext.getCurrentInstance();
		context.execute("$('#dialogConfirmEliminaCom').hide();");
		context.execute("$('#dialogConfirmEliminaComPlan').hide();");
	}
	
	public void FilterByCompany() {
		this.colCommissions = commissionRepository.findByCompany(this.companyId);
	}
}
