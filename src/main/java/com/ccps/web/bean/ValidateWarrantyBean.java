package com.ccps.web.bean;

import static javax.faces.context.FacesContext.getCurrentInstance;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIOutput;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.event.ValueChangeEvent;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.ccps.web.common.CommonMessages;
import com.ccps.web.model.Binnacle;
import com.ccps.web.model.FileType;
import com.ccps.web.model.Period;
import com.ccps.web.model.User;
import com.ccps.web.model.Warranty;
import com.ccps.web.model.repository.BinnacleRepository;
import com.ccps.web.model.repository.FileTypeRepository;
import com.ccps.web.model.repository.PeriodRepository;
import com.ccps.web.model.repository.WarrantyRepository;

/**
 * author
 * plopezs
 */

@ManagedBean(name="valWarrantyBean")
@SessionScoped
@Component(value="valWarrantyBean")
@Scope("session")
public class ValidateWarrantyBean implements Serializable{
	private static final long serialVersionUID = 159756707456511525L;

	@Autowired
	private WarrantyRepository warrantyRepository;
	
	@Autowired
	private FileTypeRepository fileTypeRepository;
	
	@Autowired
	private PeriodRepository periodRepository;
	
	@Autowired
	private BinnacleRepository binnacleRepository;
	
	List<Warranty> colWarrantyRobosErrors = null;
	List<Warranty> colWarrantyCellErrors = null;
	private Map<Integer, String> colYears;
	private Map<Integer, String> colMonths;
	private int year = 0;
	private int month = 0;
	private String strMonth = "";
	private FileType ftRobo  = null;
	private FileType ftCell  = null;
	List<String> glosasCelulares = new ArrayList<String>();
	
	public List<Warranty> getColWarrantyRobosErrors() {
		return colWarrantyRobosErrors;
	}

	public void setColWarrantyRobosErrors(List<Warranty> colWarrantyRobosErrors) {
		this.colWarrantyRobosErrors = colWarrantyRobosErrors;
	}

	public List<Warranty> getColWarrantyCellErrors() {
		return colWarrantyCellErrors;
	}

	public void setColWarrantyCellErrors(List<Warranty> colWarrantyCellErrors) {
		this.colWarrantyCellErrors = colWarrantyCellErrors;
	}

	public Map<Integer, String> getColYears() {
		return colYears;
	}

	public void setColYears(Map<Integer, String> colYears) {
		this.colYears = colYears;
	}

	public Map<Integer, String> getColMonths() {
		return colMonths;
	}

	public void setColMonths(Map<Integer, String> colMonths) {
		this.colMonths = colMonths;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		this.month = month;
	}

	public String getStrMonth() {
		return strMonth;
	}

	public void setStrMonth(String strMonth) {
		this.strMonth = strMonth;
	}

	public FileType getFtRobo() {
		return ftRobo;
	}

	public void setFtRobo(FileType ftRobo) {
		this.ftRobo = ftRobo;
	}

	public FileType getFtCell() {
		return ftCell;
	}

	public void setFtCell(FileType ftCell) {
		this.ftCell = ftCell;
	}
	
	public Boolean getExistenRobos() {
		return colWarrantyRobosErrors.size() > 0;
	}

	public void setExistenRobos(Boolean existenRobos) {
	}

	public Boolean getExistenCelulares() {
		return colWarrantyCellErrors.size() > 0;
	}

	public void setExistenCelulares(Boolean existenCelulares) {
	}

	//METHODS
	//PAGE LOAD
	@PostConstruct
	public void PageLoad()
	{
		InitYearsMonths();
		InitPeriod();
		FillGlosasCelulares();
		
		//Si existe un periodo abierto
		if (this.month > 0)
			GetWarrantyErrors();
	}
	
	public void FillGlosasCelulares()
	{
		this.glosasCelulares.add("CELULAR");
		this.glosasCelulares.add("LG L1 E4");
		this.glosasCelulares.add("CEL SAM");
		this.glosasCelulares.add("CONTR MO");
		this.glosasCelulares.add("CONTR ENT");
		this.glosasCelulares.add("CONTRATO");
		this.glosasCelulares.add("CEL ALCA");
		this.glosasCelulares.add("TELEFONI");
		this.glosasCelulares.add("CELUL SA");
		this.glosasCelulares.add("TELEFONO");
		this.glosasCelulares.add("CONT MOV");
		this.glosasCelulares.add("CEL SON");
		this.glosasCelulares.add("CEL SAMS");
		this.glosasCelulares.add("APPLE IPH");
		this.glosasCelulares.add("ALCATEL");
		this.glosasCelulares.add("PLAN ENT");
		this.glosasCelulares.add("PLAN MOV");
		this.glosasCelulares.add("MI PRIME");
	}
	
	public void InitYearsMonths() {
		try
		{
			Date today = new Date();
			
			this.colYears = new HashMap<Integer, String>();
			this.colMonths = new HashMap<Integer, String>();
			
			//Agrega el año actual y los 4 siguientes.
			for (Integer i = today.getYear(); i < today.getYear() + 5; i++)
			{
				this.colYears.put(i, i.toString());
			}
			
			this.colMonths.put(1, "Enero");
			this.colMonths.put(2, "Febrero");
			this.colMonths.put(3, "Marzo");
			this.colMonths.put(4, "Abril");
			this.colMonths.put(5, "Mayo");
			this.colMonths.put(6, "Junio");
			this.colMonths.put(7, "Julio");
			this.colMonths.put(8, "Agosto");
			this.colMonths.put(9, "Septiembre");
			this.colMonths.put(10, "Octubre");
			this.colMonths.put(11, "Noviembre");
			this.colMonths.put(12, "Diciembre");
		}
		catch (Exception e) 
		{
			CommonMessages.ShowErrorMessage("Error:", "No fue posible  inicializar los meses y años. " + e.getMessage());
	    }
	}
	
	public void InitPeriod() {
		try
		{
			this.ftRobo = fileTypeRepository.findByType("GarantiaRobo");
			this.ftCell = fileTypeRepository.findByType("GarantiaCelular");
			
			if (ftRobo != null) {
				int maxYear = periodRepository.findMaxYearClosedPeriodsByType(ftRobo.getId());
				int maxMonth = 0;
				
				List<Period> colPeriods = periodRepository.findAllClosedPeriodsByYearType(maxYear, ftRobo.getId());
				
				for (Period period:colPeriods){
					if (period.getMes() > maxMonth)
						maxMonth = period.getMes();
				}
				
				if (maxMonth < 12) {
					maxMonth++;
				}
				else {
					maxYear++;
					maxMonth = 1;
				}
				
				colPeriods = periodRepository.findByYearMonthType(maxYear, maxMonth, ftRobo.getId());
				
				if (colPeriods.size() <= 0)
				{
					//Si no existe informo por pantalla que no existe periodo.
					CommonMessages.ShowWarningMessage("Advertencia:", "No existe un periodo abierto. ");
				}
				else
				{
					this.month = maxMonth;
					this.year = maxYear;
					this.strMonth = this.colMonths.get(maxMonth);
				}
			}
		}
		catch (Exception e)
		{
			CommonMessages.ShowErrorMessage("Error:", "No fue posible obtener el periodo actual. " + e.getMessage());
	    }
	}

	public void GetWarrantyErrors() {
		int previousCantRobo = 0;
		int postCantRobo = 0;
		int previousCantCell = 0;
		int postCantCell = 0;
		
		if (this.colWarrantyCellErrors != null)
			previousCantCell += this.colWarrantyCellErrors.size();
		
		if (this.colWarrantyRobosErrors != null)
			previousCantRobo += this.colWarrantyRobosErrors.size();
		
		this.colWarrantyRobosErrors = warrantyRepository.findByPeriodStatus(this.month, this.year, ftRobo.getId(), "E");
		this.colWarrantyCellErrors = warrantyRepository.findByPeriodStatus(this.month, this.year, ftCell.getId(), "E");
		
		if (this.colWarrantyCellErrors != null)
			postCantCell += this.colWarrantyCellErrors.size();
		
		if (this.colWarrantyRobosErrors != null)
			postCantRobo += this.colWarrantyRobosErrors.size();
		
		if (previousCantCell <= postCantCell && previousCantRobo <= postCantRobo)
			CommonMessages.ShowWarningMessage("Advertencia:", "No se encontraron nuevos registros para validar");
	}
	
	public void GetWarrantyErrorsNoMsg() {
		int previousCant = 0;
		int postCant = 0;
		
		if (this.colWarrantyCellErrors != null)
			previousCant += this.colWarrantyCellErrors.size();
		
		if (this.colWarrantyRobosErrors != null)
			previousCant += this.colWarrantyRobosErrors.size();
		
		this.colWarrantyRobosErrors = warrantyRepository.findByPeriodStatus(this.month, this.year, ftRobo.getId(), "E");
		this.colWarrantyCellErrors = warrantyRepository.findByPeriodStatus(this.month, this.year, ftCell.getId(), "E");
		
		if (this.colWarrantyCellErrors != null)
			postCant += this.colWarrantyCellErrors.size();
		
		if (this.colWarrantyRobosErrors != null)
			postCant += this.colWarrantyRobosErrors.size();
	}
	
	public void SaveInformation() {
		List<Warranty> colWarrantyRobosCorrected = GetCorrectedRobos();
		List<Warranty> colWarrantyCellCorrected = GetCorrectedCells();
		
		//Guarda cada registro de robos
		for (Warranty war : colWarrantyRobosCorrected)
		{
			FileType ft = war.getFileType();
			
			ft = fileTypeRepository.findByType(ft.getType());
			
			war.setFileType(ft);
			war.setStatus("C");
			
			warrantyRepository.save(war);
		}
		
		//Guarda cada registro de celulares
		for (Warranty war : colWarrantyCellCorrected)
		{
			FileType ft = war.getFileType();
			
			ft = fileTypeRepository.findByType(ft.getType());
			
			war.setFileType(ft);
			war.setStatus("C");
			
			warrantyRepository.save(war);
		}
		
		if (this.colWarrantyRobosErrors.size() > 0)
			InsertBinnacle(ftRobo, 3, "Corrección de datos de Robos.");
		
		if (this.colWarrantyCellErrors.size() > 0)
			InsertBinnacle(ftCell, 3, "Corrección de datos de Celulares.");

		//Si existe un periodo abierto
		if (this.month > 0)
			GetWarrantyErrorsNoMsg();
		
		CommonMessages.ShowInfoMessage("Info:", "Correcciones guardadas con éxito.");
	}
	
	public List<Warranty> GetCorrectedRobos()
	{
		List<Warranty> colWar = new ArrayList<Warranty>();
		
		for (Warranty war : this.colWarrantyRobosErrors)
		{
			Boolean isValid = true;
			
			if (war.getPrecioVenta() < 1000 || war.getPrecioVenta() > 5000000)
			{
				continue;
			}
			
			if (war.getGlosaArticulo().trim().isEmpty())
			{
				continue;
			}
			
			if (war.getGlosaArticulo().trim().length() < 2)
			{
				continue;
			}
			
			if (war.getGlosaArticulo().trim().toUpperCase().equals("NO DEFINIDO"))
			{
				continue;
			}
			
			for (String glosa : this.glosasCelulares)
			{
				if (war.getGlosaArticulo().trim().toUpperCase().indexOf(glosa) == 0)
				{
					isValid = false;
					break;
				}
			}
			
			if (!isValid)
				continue;
			
			colWar.add(war);
		}
		
		return colWar;
	}
	
	public List<Warranty> GetCorrectedCells()
	{
		List<Warranty> colWar = new ArrayList<Warranty>();
		
		for (Warranty war : this.colWarrantyCellErrors)
		{
			if (war.getPrecioVenta() < 1000 || war.getPrecioVenta() > 2000000)
			{
				continue;
			}
			
			if (war.getGlosaArticulo().trim().isEmpty())
			{
				continue;
			}
			
			if (war.getGlosaArticulo().trim().length() < 2)
			{
				continue;
			}
			
			if (war.getGlosaArticulo().trim().toUpperCase().equals("NO DEFINIDO"))
			{
				continue;
			}
			
			for (String glosa : this.glosasCelulares)
			{
				if (war.getGlosaArticulo().trim().toUpperCase().indexOf(glosa) == 0)
				{
					colWar.add(war);
					break;
				}
			}
			
			continue;
		}
		
		return colWar;
	}
	
	public void ChangeWarrantyType(AjaxBehaviorEvent event) {
		Integer warId= (Integer) ((UIOutput) event.getSource()).getValue();
		
		if (warId < 0) { //ROBO
			warId = warId * -1;
			
			Warranty war = warrantyRepository.findOne(warId);
			
			FileType ft = fileTypeRepository.findByType("GarantiaRobo");
			
			war.setFileType(ft);
			
			warrantyRepository.save(war);
			
			//Si existe un periodo abierto
			if (this.month > 0)
				GetWarrantyErrors();
		}
		else if (warId > 0) { //CELULAR
			Warranty war = warrantyRepository.findOne(warId);
			
			FileType ft = fileTypeRepository.findByType("GarantiaCelular");
			
			war.setFileType(ft);
			
			warrantyRepository.save(war);
			
			//Si existe un periodo abierto
			if (this.month > 0)
				GetWarrantyErrors();
		}
	}
	
	public void InsertBinnacle(FileType fileType, Integer binType, String mensaje){
		
		List<Period> colPeriods = periodRepository.findByYearMonthType(this.year, this.month, fileType.getId());
		
		if (colPeriods.size() > 0)
		{
			Binnacle binnacle = new Binnacle();
			java.util.Date fecha = new Date();
			binnacle.setBinDate(fecha);
			binnacle.setFileType(fileType);
			binnacle.setBinType(binType);
			User userSession = (User) getCurrentInstance().getExternalContext().getSessionMap().get("user"); 
			binnacle.setUser(userSession);
			binnacle.setPeriod(colPeriods.get(0));
			binnacle.setDetails2(mensaje);
			binnacleRepository.save(binnacle);
		}
	}
}
