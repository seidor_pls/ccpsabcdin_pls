package com.ccps.web.bean;

import static javax.faces.context.FacesContext.getCurrentInstance;

import java.io.ByteArrayOutputStream;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.persistence.Tuple;

import org.primefaces.context.RequestContext;
import org.primefaces.model.StreamedContent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.apache.poi.hssf.util.CellRangeAddress;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.ccps.web.common.CommonMessages;
import com.ccps.web.common.CommonReporting;
import com.ccps.web.model.Authority;
import com.ccps.web.model.Binnacle;
import com.ccps.web.model.Collection;
import com.ccps.web.model.Commission;
import com.ccps.web.model.CommissionPlan;
import com.ccps.web.model.Company;
import com.ccps.web.model.FileType;
import com.ccps.web.model.FileTypeCompany;
import com.ccps.web.model.Period;
import com.ccps.web.model.Role;
import com.ccps.web.model.Sale;
import com.ccps.web.model.User;
import com.ccps.web.model.Warranty;
import com.ccps.web.model.repository.*;

/**
 * author
 * plopezs
 */

@ManagedBean(name="gpReportBean")
@SessionScoped
@Component(value="gpReportBean")
@Scope("session")
public class WarrantyReportBean implements Serializable{
	private static final long serialVersionUID = 6260459998331769648L;

	private Boolean error = false;
	private List<Company> colCompanies;
	private Map<Integer, String> colYears;
	private Map<Integer, String> colMonths;
	private int year = 0;
	private int month = 0;
	private int companyId = 0;
	private String tipoArchivo = "";
	private StreamedContent file;
	List<FileType> fileTypesReportesGP;
	private List<Binnacle> binnacles;
	private String UFValue = "";
	private Boolean validUF = true;
	private String UFMessage = "";
	private List<CommissionPlan> colCommissionPlans;
	private User loggedUser;
	
	@Autowired
	private CompanyRepository companyRepository;
	
	@Autowired
	private WarrantyRepository warrantyRepository;
	
	@Autowired
	private PeriodRepository periodRepository;
	
	@Autowired
	private FileTypeRepository fileTypeRepository;
	
	@Autowired
	private FileTypeCompanyRepository fileTypeCompanyRepository;
	
	@Autowired
	private BinnacleRepository binnacleRepository;
	
	@Autowired
	private CommissionPlanRepository commissionPlanRepository;
	
	@Autowired
	private AuthorityRepository authorityRepository;
	
	//PROPERTIES
	public StreamedContent getFile() {
        return file;
    }
	
	public Boolean getError() {
		return error;
	}

	public void setError(Boolean error) {
		this.error = error;
	}

	public List<Company> getColCompanies() {
		return colCompanies;
	}

	public void setColCompanies(List<Company> colCompanies) {
		this.colCompanies = colCompanies;
	}

	public Map<Integer, String> getColYears() {
		return colYears;
	}

	public void setColYears(Map<Integer, String> colYears) {
		this.colYears = colYears;
	}

	public Map<Integer, String> getColMonths() {
		return colMonths;
	}

	public void setColMonths(Map<Integer, String> colMonths) {
		this.colMonths = colMonths;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		this.month = month;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}
	
	public String getTipoArchivo() {
		return tipoArchivo;
	}

	public void setTipoArchivo(String tipoArchivo) {
		this.tipoArchivo = tipoArchivo;
	}
	
	public List<FileType> getFileTypesReportesGP() {
		return fileTypesReportesGP;
	}

	public void setFileTypesReportesGP(List<FileType> fileTypesReportesGP) {
		this.fileTypesReportesGP = fileTypesReportesGP;
	}
	
	public List<Binnacle> getBinnacles() {
		return binnacles;
	}

	public void setBinnacles(List<Binnacle> binnacles) {
		this.binnacles = binnacles;
	}

	public String getUFValue() {
		return UFValue;
	}

	public void setUFValue(String uFValue) {
		UFValue = uFValue;
	}

	public Boolean getValidUF() {
		return validUF;
	}

	public void setValidUF(Boolean validUF) {
		this.validUF = validUF;
	}

	public String getUFMessage() {
		return UFMessage;
	}

	public void setUFMessage(String uFMessage) {
		UFMessage = uFMessage;
	}

	//METHODS
	@PostConstruct
	public void PageLoad(){
		FacesContext context = FacesContext.getCurrentInstance();
		
		this.fileTypesReportesGP = fileTypeRepository.findByCategory("reporteGP");
		this.colCommissionPlans = commissionPlanRepository.findAll();
		
		this.loggedUser = (User) context.getExternalContext().getSessionMap().get("user");
		
		if (this.loggedUser == null)
			System.out.println("******************************************************NO HAY USUARIO");
		else
			System.out.println("******************************************************USER LOGIN: " + this.loggedUser.getId());
		
		GetWarrantyCompanies();
		
		InitYearsMonths();
	}

	public void InitYearsMonths()
	{
		try
		{
			Calendar cal = Calendar.getInstance();
			int currentYear = cal.get(Calendar.YEAR);
			
			this.colYears = new HashMap<Integer, String>();
			this.colMonths = new HashMap<Integer, String>();
			
			//Agrega el año actual y los 4 siguientes.
			for (Integer i = currentYear; i < currentYear + 5; i++)
			{
				this.colYears.put(i, i.toString());
			}
			
			this.colMonths.put(1, "Enero");
			this.colMonths.put(2, "Febrero");
			this.colMonths.put(3, "Marzo");
			this.colMonths.put(4, "Abril");
			this.colMonths.put(5, "Mayo");
			this.colMonths.put(6, "Junio");
			this.colMonths.put(7, "Julio");
			this.colMonths.put(8, "Agosto");
			this.colMonths.put(9, "Septiembre");
			this.colMonths.put(10, "Octubre");
			this.colMonths.put(11, "Noviembre");
			this.colMonths.put(12, "Diciembre");
		}
		catch (Exception e) 
		{
			CommonMessages.ShowErrorMessage("Error", "Error al inicializar los meses y años. " + e.getMessage());
	    }
	}
	
	
	public void GetWarrantyCompanies()
	{
		this.colCompanies = new ArrayList<Company>();
		
		List<FileTypeCompany> ftCompanyColl = fileTypeCompanyRepository.findByFileType(this.fileTypesReportesGP.get(0).getId());
		
		for (FileTypeCompany ftc : ftCompanyColl)
		{
			Boolean contains = false;
			
			for (Company com : this.colCompanies)
			{
				if (ftc.getCompany().getId() == com.getId())
					contains = true;
			}
			
			if (!contains)
				this.colCompanies.add(ftc.getCompany());
		}
		
		if (ftCompanyColl.size() == 1)
		{
			this.companyId = ftCompanyColl.get(0).getCompany().getId();
		}
	}
	
	
	public void HandleGenerateReportButton(){
		if (ValidateForm() && ValidateClosedPeriod()) {
			ReadBinnacle();
				
			if (this.UFValue.isEmpty())
			{
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("$('#dialogConfirmReport').show();");
			}
			else
			{
				GenerateExcelReport();
			}
		}
	}
	
	
	public void CloseDialogReport(){
		this.UFMessage = "";
		this.UFValue = "";
		this.validUF = true;
		RequestContext context = RequestContext.getCurrentInstance();
		context.execute("$('#dialogConfirmReport').hide();");
	}
	
	
	public void GenerateReport(){
		if (ValidateUF()) {
			
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("$('#dialogConfirmReport').hide();");
			
			GenerateExcelReport();
		}
	}
	
	
	public void GenerateExcelReport(){
		RequestContext context = RequestContext.getCurrentInstance();
		
		String reportName = "";
		
		if (this.tipoArchivo.contains("Robo"))
			reportName = "Robo Plus";
		else if (this.tipoArchivo.contains("Celu"))
			reportName = "Cel Plus";
		else
			reportName = this.tipoArchivo;
		
		String companyName = companyRepository.findOne(this.companyId).getName();
		String fileName = "Cierre " + companyName + " - " + reportName + " " + this.colMonths.get(month) + " " + this.year + ".xlsx";
		
        file = CommonReporting.StreamExcelReport(GenerateExcelFileFromDB(this.month, this.year, this.companyId, reportName), fileName);
        
        if (file != null)
        {
        	context.execute("$('#divDownloadReport').show();");
        	FileType ftSel = fileTypeRepository.findByType(this.tipoArchivo);
        	InsertBinnacle(ftSel, 2, fileName, this.UFValue);
        	this.UFValue = "";
        }
	}
	
	
	private static Map<String, CellStyle> createStyles(Workbook wb){
        Map<String, CellStyle> styles = new HashMap<String, CellStyle>();
        
        CellStyle style;
        
        //FONTS
        Font blackFont = wb.createFont();
        blackFont.setFontHeightInPoints((short)11);
        blackFont.setFontName("Calibri");
        blackFont.setColor(IndexedColors.BLACK.getIndex());
        
        Font blackFontBold = wb.createFont();
        blackFontBold.setFontHeightInPoints((short)11);
        blackFontBold.setFontName("Calibri");
        blackFontBold.setBold(true);
        blackFontBold.setColor(IndexedColors.BLACK.getIndex());
        
        Font whiteFont = wb.createFont();
        whiteFont.setFontHeightInPoints((short)11);
        whiteFont.setFontName("Calibri");
        whiteFont.setColor(IndexedColors.WHITE.getIndex());
        
        Font whiteFontBold = wb.createFont();
        whiteFontBold.setFontHeightInPoints((short)11);
        whiteFontBold.setFontName("Calibri");
        whiteFontBold.setBold(true);
        whiteFontBold.setColor(IndexedColors.WHITE.getIndex());
        
        Font redFont = wb.createFont();
        redFont.setFontHeightInPoints((short)11);
        redFont.setFontName("Calibri");
        redFont.setColor(IndexedColors.RED.getIndex());
        
        
        style = wb.createCellStyle();
        style.setAlignment(CellStyle.ALIGN_RIGHT);
        style.setFont(blackFont);
        style.setDataFormat(wb.createDataFormat().getFormat("_-$ * #,##0_-;-$ * #,##0_-;_-$ * \"-\"_-;_-@_-"));
        style.setFillForegroundColor(IndexedColors.WHITE.getIndex());
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        styles.put("pesos_white_centered_sin_cero", style);
        
        style = wb.createCellStyle();
        style.setAlignment(CellStyle.ALIGN_RIGHT);
        style.setFont(blackFont);
        style.setDataFormat(wb.createDataFormat().getFormat("_-$ * #,##0_-;-$ * #,##0_-;_-$ * \"-\"_-;_-@_-"));
        style.setFillForegroundColor(IndexedColors.WHITE.getIndex());
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        styles.put("pesos_white", style);
        
        style = wb.createCellStyle();
        style.setAlignment(CellStyle.ALIGN_RIGHT);
        style.setFont(whiteFont);
        style.setDataFormat(wb.createDataFormat().getFormat("_-$ * #,##0_-;-$ * #,##0_-;_-$ * \"-\"_-;_-@_-"));
        style.setFillForegroundColor(IndexedColors.BLACK.getIndex());
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        styles.put("pesos_black", style);
        
        style = wb.createCellStyle();
        style.setAlignment(CellStyle.ALIGN_CENTER);
        style.setFont(whiteFont);
        style.setDataFormat(wb.createDataFormat().getFormat("_-$ * #,##0_-;-$ * #,##0_-;_-$ * \"-\"_-;_-@_-"));
        style.setFillForegroundColor(IndexedColors.BLACK.getIndex());
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        styles.put("pesos_black_centered", style);
        
        style = wb.createCellStyle();
        style.setAlignment(CellStyle.ALIGN_CENTER);
        style.setFont(whiteFont);
        style.setDataFormat(wb.createDataFormat().getFormat("_-$ * #,##0_-;-$ * #,##0_-;_-$ * \"-\"_-;_-@_-"));
        style.setFillForegroundColor(IndexedColors.BLACK.getIndex());
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        styles.put("pesos_black_centered_sin_cero", style);
        
        style = wb.createCellStyle();
        style.setAlignment(CellStyle.ALIGN_RIGHT);
        style.setFont(blackFont);
        style.setDataFormat(wb.createDataFormat().getFormat("_-* #,##0_-;-* #,##0_-;_-* \"-\"_-;_-@_-"));
        style.setFillForegroundColor(IndexedColors.WHITE.getIndex());
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        styles.put("entero_white", style);
        
        style = wb.createCellStyle();
        style.setAlignment(CellStyle.ALIGN_RIGHT);
        style.setFont(whiteFont);
        style.setDataFormat(wb.createDataFormat().getFormat("_-* #,##0_-;-* #,##0_-;_-* \"-\"_-;_-@_-"));
        style.setFillForegroundColor(IndexedColors.BLACK.getIndex());
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        styles.put("entero_black", style);
        
        style = wb.createCellStyle();
        style.setAlignment(CellStyle.ALIGN_RIGHT);
        style.setFont(blackFont);
        style.setDataFormat(wb.createDataFormat().getFormat("_-* 0.000000_-;-*0.000000_-;_-* \"-\"??_-;_-@_-"));
        style.setFillForegroundColor(IndexedColors.WHITE.getIndex());
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        styles.put("decimal_white", style);
        
        style = wb.createCellStyle();
        style.setAlignment(CellStyle.ALIGN_RIGHT);
        style.setFont(whiteFont);
        style.setDataFormat(wb.createDataFormat().getFormat("_-* 0.000000_-;-*0.000000_-;_-* \"-\"??_-;_-@_-"));
        style.setFillForegroundColor(IndexedColors.GREY_40_PERCENT.getIndex());
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        styles.put("decimal_grey", style);
        
        style = wb.createCellStyle();
        style.setAlignment(CellStyle.ALIGN_RIGHT);
        style.setFont(whiteFont);
        style.setDataFormat(wb.createDataFormat().getFormat("_-* 0.000000_-;-*0.000000_-;_-* \"-\"??_-;_-@_-"));
        style.setFillForegroundColor(IndexedColors.BLACK.getIndex());
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        styles.put("decimal_black", style);
        
        style = wb.createCellStyle();
        style.setAlignment(CellStyle.ALIGN_CENTER);
        style.setFont(whiteFont);
        style.setDataFormat(wb.createDataFormat().getFormat("_-* 0.000000_-;-*0.000000_-;_-* \"-\"??_-;_-@_-"));
        style.setFillForegroundColor(IndexedColors.BLACK.getIndex());
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        styles.put("decimal_black_centered", style);
        
        style = wb.createCellStyle();
        style.setAlignment(CellStyle.ALIGN_CENTER);
        style.setFont(whiteFont);
        style.setDataFormat(wb.createDataFormat().getFormat("0.#0%"));
        style.setFillForegroundColor(IndexedColors.BLACK.getIndex());
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        styles.put("porcentaje_black_centered_dos_dec", style);

        style = wb.createCellStyle();
        style.setAlignment(CellStyle.ALIGN_CENTER);
        style.setFont(whiteFont);
        style.setDataFormat(wb.createDataFormat().getFormat("0.#0%"));
        style.setFillForegroundColor(IndexedColors.BLACK.getIndex());
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        styles.put("porcentaje_black_centered_un_dec", style);
        
        style = wb.createCellStyle();
        style.setAlignment(CellStyle.ALIGN_LEFT);
        style.setFont(blackFont);
        style.setFillForegroundColor(IndexedColors.WHITE.getIndex());
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        styles.put("texto_white", style);
        
        style = wb.createCellStyle();
        style.setAlignment(CellStyle.ALIGN_CENTER);
        style.setFont(blackFont);
        style.setFillForegroundColor(IndexedColors.WHITE.getIndex());
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        styles.put("texto_white_centered", style);
        
        style = wb.createCellStyle();
        style.setAlignment(CellStyle.ALIGN_LEFT);
        style.setFont(blackFontBold);
        style.setFillForegroundColor(IndexedColors.WHITE.getIndex());
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        styles.put("texto_white_bold", style);
        
        style = wb.createCellStyle();
        style.setAlignment(CellStyle.ALIGN_CENTER);
        style.setFont(blackFontBold);
        style.setFillForegroundColor(IndexedColors.WHITE.getIndex());
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        styles.put("texto_white_bold_centered", style);
        
        style = wb.createCellStyle();
        style.setAlignment(CellStyle.ALIGN_LEFT);
        style.setFont(blackFont);
        style.setFillForegroundColor(IndexedColors.WHITE.getIndex());
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        style.setWrapText(true);
        styles.put("texto_white_wrapped", style);
        
        style = wb.createCellStyle();
        style.setAlignment(CellStyle.ALIGN_LEFT);
        style.setFont(whiteFont);
        style.setFillForegroundColor(IndexedColors.GREY_40_PERCENT.getIndex());
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        styles.put("texto_grey", style);
        
        style = wb.createCellStyle();
        style.setAlignment(CellStyle.ALIGN_LEFT);
        style.setFont(whiteFont);
        style.setFillForegroundColor(IndexedColors.GREY_40_PERCENT.getIndex());
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        style.setWrapText(true);
        styles.put("texto_grey_wrapped", style);
        
        style = wb.createCellStyle();
        style.setAlignment(CellStyle.ALIGN_LEFT);
        style.setFont(whiteFont);
        style.setFillForegroundColor(IndexedColors.BLACK.getIndex());
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        styles.put("texto_black", style);
        
        style = wb.createCellStyle();
        style.setAlignment(CellStyle.ALIGN_CENTER);
        style.setFont(whiteFont);
        style.setFillForegroundColor(IndexedColors.BLACK.getIndex());
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        styles.put("texto_black_centered", style);
        
        style = wb.createCellStyle();
        style.setAlignment(CellStyle.ALIGN_LEFT);
        style.setFont(whiteFontBold);
        style.setFillForegroundColor(IndexedColors.BLACK.getIndex());
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        styles.put("texto_black_bold", style);
        
        style = wb.createCellStyle();
        style.setAlignment(CellStyle.ALIGN_LEFT);
        style.setFont(whiteFont);
        style.setFillForegroundColor(IndexedColors.BLACK.getIndex());
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        style.setWrapText(true);
        styles.put("texto_black_wrapped", style);
        
        style = wb.createCellStyle();
        style.setAlignment(CellStyle.ALIGN_CENTER);
        style.setFont(whiteFontBold);
        style.setFillForegroundColor(IndexedColors.BLACK.getIndex());
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        style.setWrapText(true);
        styles.put("texto_black_wrapped_bold_centered", style);
        
        style = wb.createCellStyle();
        style.setAlignment(CellStyle.ALIGN_LEFT);
        style.setFont(redFont);
        style.setFillForegroundColor(IndexedColors.BLACK.getIndex());
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        style.setWrapText(true);
        styles.put("texto_black_wrapped_red", style);

        return styles;
    }
	
	private CommissionPlan GetTipoPlanByPrecio(Integer precioVenta)
	{
		for (CommissionPlan cp : this.colCommissionPlans)
		{
			if (precioVenta >= cp.getRangoInferior() && precioVenta <= cp.getRangoSuperior())
				return cp;
		}
		
		return new CommissionPlan();
	}

	public ByteArrayOutputStream GenerateExcelFileFromDB(Integer month, Integer year, int companyId, String reportName) {
		ByteArrayOutputStream returnStream = new ByteArrayOutputStream();
		
		try
		{
			XSSFWorkbook workbook = new XSSFWorkbook();
			XSSFSheet sheetProduccion = workbook.createSheet("Produccion " + this.colMonths.get(month) + " " + year.toString());
			XSSFSheet sheetResumen = workbook.createSheet("Resumen");
			XSSFSheet sheetCuadroCom = workbook.createSheet("Cuadro Comisiones");
			
			Map<String, CellStyle> styles = createStyles(workbook);
			
			DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
					    
		    Calendar cal = Calendar.getInstance();
		    cal.clear();
		    cal.set(this.year, this.month - 1, 1);
			int maxMonthDay = cal.getActualMaximum(cal.DAY_OF_MONTH);
			int rowCount = 0;
			
			FileType fileTypeId = null;
			List<FileType> colGplusFT = fileTypeRepository.findByCategory("gplus");
			
			for (FileType ft : colGplusFT)
			{
				if (this.tipoArchivo.contains(ft.getType()))
				{
					fileTypeId = ft;
					break;
				}
			}
			
			//DATASOURCES
			//Total Produccion
	        List<Warranty> colTotalProd = warrantyRepository.findByPeriod(month, year, fileTypeId.getId());
	        
	        Map<String, Double> sumaPrima = new HashMap<String, Double>();
	        
	        for (CommissionPlan cp : this.colCommissionPlans){
	        	sumaPrima.put(cp.getTipoPlan(), 0d);
	        }
	        
	        Map<String, Integer> sumaCuenta = new HashMap<String, Integer>();
	        
	        for (CommissionPlan cp : this.colCommissionPlans){
	        	sumaCuenta.put(cp.getTipoPlan(), 0);
	        }
	       
	        Row row = null;
	        Cell cell = null;
	        
	        Double totalSumaPrima = 0d;
	        Integer totalFolio = 0;
	        Integer totalPrimaBrutaPesos = 0;
	        Integer totalPrimaNetaPesos = 0;
	        
	        //Se calcula multiplicando el valor totalPrimaNetaPesos * comisionIntPrimaNeta
	        Integer totalComIntNeta = 0;
	        Integer totalComBrutaPesos = 0;
	        
	        sheetProduccion.setColumnWidth(0, 9*256);
	        sheetProduccion.setColumnWidth(1, 12*256);
	        sheetProduccion.setColumnWidth(2, 12*256);
	        sheetProduccion.setColumnWidth(3, 12*256);
	        sheetProduccion.setColumnWidth(4, 12*256);
	        sheetProduccion.setColumnWidth(5, 18*256);
	        sheetProduccion.setColumnWidth(6, 13*256);
	        sheetProduccion.setColumnWidth(7, 13*256);
	        sheetProduccion.setColumnWidth(8, 13*256);
	        sheetProduccion.setColumnWidth(9, 8*256);
	        sheetProduccion.setColumnWidth(10, 23*256);
	        sheetProduccion.setColumnWidth(11, 12*256);
	        sheetProduccion.setColumnWidth(12, 12*256);
	        sheetProduccion.setColumnWidth(13, 5*256);
	        sheetProduccion.setColumnWidth(14, 9*256);
	        sheetProduccion.setColumnWidth(15, 19*256);

            //Ventas Totales
            rowCount = 0;
	        //HEADER
	        row = sheetProduccion.createRow(rowCount);
	        
	        cell = row.createCell(0);
            cell.setCellValue("Folio");
            cell = row.createCell(1);
            cell.setCellValue("RutCliente");
            cell = row.createCell(2);
            cell.setCellValue("Ap Paterno");
            cell = row.createCell(3);
            cell.setCellValue("Ap Materno");
            cell = row.createCell(4);
            cell.setCellValue("Nombres");
            cell = row.createCell(5);
            cell.setCellValue("Dirección");
            cell = row.createCell(6);
            cell.setCellValue("Comuna");
            cell = row.createCell(7);
            cell.setCellValue("Ciudad");
            cell = row.createCell(8);
            cell.setCellValue("Teléfono");
            cell = row.createCell(9);
            cell.setCellValue("Cod Suc");
            cell = row.createCell(10);
            cell.setCellValue("Glosa Sucursal");
            cell = row.createCell(11);
            cell.setCellValue("Fch Vta Gtia");
            cell = row.createCell(12);
            cell.setCellValue("Precio Venta");
            cell = row.createCell(13);
            cell.setCellValue("Tipo");
            cell = row.createCell(14);
            cell.setCellValue("Prima");
            cell = row.createCell(15);
            cell.setCellValue("Gls Articulo");
            
            String tipoPlan = "";
            Double prima = 0d;
            
            if (reportName.equals("Robo Plus"))
            {
            	tipoPlan = "Robo";
	            prima = commissionPlanRepository.findByTipoPlan("Robo").getPrimaFinalSeguroRobo();
            }
	        
	        for (Warranty item : colTotalProd) {
	        	//Creación de Fila en SheetProduccion
	            row = sheetProduccion.createRow(++rowCount);
	            
	            if (reportName.equals("Cel Plus"))
	            {
		            tipoPlan = GetTipoPlanByPrecio(item.getPrecioVenta()).getTipoPlan();
		            prima = GetTipoPlanByPrecio(item.getPrecioVenta()).getPrimaFinalSeguroRobo();
	            }
	            
	            cell = row.createCell(0);
	            cell.setCellValue(item.getFolio());
	            cell = row.createCell(1);
	            cell.setCellValue(item.getRutCliente());
	            cell = row.createCell(2);
	            cell.setCellValue(item.getApPaterno());
	            cell = row.createCell(3);
	            cell.setCellValue(item.getApMaterno());
	            cell = row.createCell(4);
	            cell.setCellValue(item.getNombres());
	            cell = row.createCell(5);
	            cell.setCellValue(item.getDireccion());
	            cell = row.createCell(6);
	            cell.setCellValue(item.getComuna());
	            cell = row.createCell(7);
	            cell.setCellValue(item.getCiudad());
	            cell = row.createCell(8);
	            cell.setCellValue(item.getTelefono());
	            cell = row.createCell(9);
	            cell.setCellValue(item.getCodSucursal());
	            cell = row.createCell(10);
	            cell.setCellValue(item.getGlosaSucursal());
	            cell = row.createCell(11);
	            cell.setCellValue(item.getFecVentaGarantia() == null ? df.format(cal.getTime()) : df.format(item.getFecVentaGarantia()));
	            cell = row.createCell(12);
	            cell.setCellValue(item.getPrecioVenta());
	            cell = row.createCell(13);
	            cell.setCellValue(tipoPlan);
	            cell = row.createCell(14);
	            cell.setCellValue(prima);
	            cell = row.createCell(15);
	            cell.setCellValue(item.getGlosaArticulo());
	            
	            //Cálculo de sumatorias de valores
	            
	            int valSumaCuenta = sumaCuenta.get(tipoPlan);
	            Double valSumaPrima = sumaPrima.get(tipoPlan);
	            
	            sumaCuenta.remove(tipoPlan);
	            sumaPrima.remove(tipoPlan);
	            
	            sumaCuenta.put(tipoPlan, valSumaCuenta + 1);
	            sumaPrima.put(tipoPlan, valSumaPrima + prima);
	        }
	        
	        //Resumen
	        rowCount = 0;
	        
	        //HEADER
        	sheetResumen.setColumnWidth(0, 15*256);
        	sheetResumen.setColumnWidth(1, 15*256);
        	sheetResumen.setColumnWidth(2, 15*256);
        	sheetResumen.setColumnWidth(3, 15*256);
        	sheetResumen.setColumnWidth(4, 15*256);
        	sheetResumen.setColumnWidth(5, 15*256);
        	sheetResumen.setColumnWidth(6, 15*256);
        	
	        row = sheetResumen.createRow(++rowCount);
	        
	        cell = row.createCell(0);
	        cell.setCellStyle(styles.get("texto_black_bold"));
	        cell.setCellValue("Cierre " + reportName.toUpperCase() + " " + this.colMonths.get(month) + " " + this.year);
	        cell = row.createCell(1);
	        cell.setCellStyle(styles.get("texto_black_bold"));
	        cell = row.createCell(2);
	        cell.setCellStyle(styles.get("texto_black_bold"));
	        cell = row.createCell(3);
	        cell.setCellStyle(styles.get("texto_black"));
	        cell = row.createCell(4);
	        cell.setCellStyle(styles.get("texto_black"));
	        cell = row.createCell(5);
	        cell.setCellStyle(styles.get("texto_black"));
	        cell = row.createCell(6);
	        cell.setCellStyle(styles.get("texto_black"));
	        
	        row = sheetResumen.createRow(++rowCount);
	        
	        cell = row.createCell(0);
	        cell.setCellStyle(styles.get("texto_black_wrapped"));
	        cell.setCellValue("Tipo");
	        cell = row.createCell(1);
	        cell.setCellStyle(styles.get("texto_black_wrapped"));
	        cell.setCellValue("Suma de Prima");
	        cell = row.createCell(2);
	        cell.setCellStyle(styles.get("texto_black_wrapped"));
	        cell.setCellValue("Cuenta de Folio");
	        cell = row.createCell(3);
	        cell.setCellStyle(styles.get("texto_black_wrapped"));
	        cell.setCellValue("Prima Bruta en $");
	        cell = row.createCell(4);
	        cell.setCellStyle(styles.get("texto_black_wrapped"));
	        cell.setCellValue("Prima Neta en $");
	        cell = row.createCell(5);
	        cell.setCellStyle(styles.get("texto_black_wrapped_red"));
	        cell.setCellValue("Comisión int. NETA por confirmar");
	        cell = row.createCell(6);
	        cell.setCellStyle(styles.get("texto_black_wrapped_red"));
	        cell.setCellValue("Comisión Bruta en $ por confirmar");
	        
	        Double valUFDouble = Double.parseDouble(this.UFValue.replace(",", "."));
	        
	        for (CommissionPlan cp : this.colCommissionPlans)
	        {
	        	if ((reportName.equals("Cel Plus") && !cp.getTipoPlan().equals("Robo")) || (reportName.equals("Robo Plus") && cp.getTipoPlan().equals("Robo")))
	        	{
		        	int sumaPrimaBrutaPesos = (int) Math.round(sumaPrima.get(cp.getTipoPlan()) * valUFDouble);
		        	int sumaPrimaNetaPesos = (int) Math.round(sumaPrimaBrutaPesos / 1.19);
		        	int sumaComIntNeta = (int) Math.round(sumaPrimaNetaPesos * cp.getComInterPrimaNeta());
		        	int sumaComBrutaPesos = (int) Math.round(sumaComIntNeta * 1.19);
		        	
		        	totalSumaPrima = totalSumaPrima + sumaPrima.get(cp.getTipoPlan());
		        	totalFolio = totalFolio + sumaCuenta.get(cp.getTipoPlan());
		        	totalPrimaBrutaPesos = totalPrimaBrutaPesos + sumaPrimaBrutaPesos;
		        	totalPrimaNetaPesos = totalPrimaNetaPesos + sumaPrimaNetaPesos;
		        	totalComIntNeta = totalComIntNeta + sumaComIntNeta;
		        	totalComBrutaPesos = totalComBrutaPesos + sumaComBrutaPesos;
		        	
			        row = sheetResumen.createRow(++rowCount);
			        cell = row.createCell(0);
			        cell.setCellStyle(styles.get("texto_white"));
			        cell.setCellValue(cp.getTipoPlan());
			        cell = row.createCell(1);
			        cell.setCellStyle(styles.get("decimal_white"));
			        cell.setCellValue(sumaPrima.get(cp.getTipoPlan()));
			        cell = row.createCell(2);
			        cell.setCellStyle(styles.get("entero_white"));
			        cell.setCellValue(sumaCuenta.get(cp.getTipoPlan()));
			        cell = row.createCell(3);
			        cell.setCellStyle(styles.get("pesos_white"));
			        cell.setCellValue(sumaPrimaBrutaPesos);
			        cell = row.createCell(4);
			        cell.setCellStyle(styles.get("pesos_white"));
			        cell.setCellValue(sumaPrimaNetaPesos);
			        cell = row.createCell(5);
			        cell.setCellStyle(styles.get("pesos_white"));
			        cell.setCellValue(sumaComIntNeta);
			        cell = row.createCell(6);
			        cell.setCellStyle(styles.get("pesos_white"));
			        cell.setCellValue(sumaComBrutaPesos);
	        	}
	        }
	        
	        row = sheetResumen.createRow(++rowCount);
	        cell = row.createCell(0);
	        cell.setCellStyle(styles.get("texto_black"));
	        cell.setCellValue("Total general");
	        cell = row.createCell(1);
	        cell.setCellStyle(styles.get("decimal_black"));
	        cell.setCellValue(totalSumaPrima);
	        cell = row.createCell(2);
	        cell.setCellStyle(styles.get("entero_black"));
	        cell.setCellValue(totalFolio);
	        cell = row.createCell(3);
	        cell.setCellStyle(styles.get("pesos_black"));
	        cell.setCellValue(totalPrimaBrutaPesos);
	        cell = row.createCell(4);
	        cell.setCellStyle(styles.get("pesos_black"));
	        cell.setCellValue(totalPrimaNetaPesos);
	        cell = row.createCell(5);
	        cell.setCellStyle(styles.get("pesos_black"));
	        cell.setCellValue(totalComIntNeta);
	        cell = row.createCell(6);
	        cell.setCellStyle(styles.get("pesos_black"));
	        cell.setCellValue(totalComBrutaPesos);
	        
	        row = sheetResumen.createRow(++rowCount);
	        cell = row.createCell(0);
        	cell.setCellStyle(styles.get("texto_white"));
        	cell.setCellValue("UF al " + maxMonthDay + " de " + this.colMonths.get(month));
        	cell = row.createCell(1);
        	cell.setCellStyle(styles.get("decimal_white"));
        	cell.setCellValue(this.UFValue);
	        
	        
	        //Cuadro Comisiones
            rowCount = 0;
	        
	        //HEADER
            sheetCuadroCom.setColumnWidth(0, 11*256);
            sheetCuadroCom.setColumnWidth(1, 11*256);
            sheetCuadroCom.setColumnWidth(2, 11*256);
            sheetCuadroCom.setColumnWidth(3, 11*256);
            sheetCuadroCom.setColumnWidth(4, 11*256);
            sheetCuadroCom.setColumnWidth(5, 11*256);
	        
            row = sheetCuadroCom.createRow(rowCount);
	        
            cell = row.createCell(0);
            cell.setCellStyle(styles.get("texto_black_wrapped_bold_centered"));
            cell.setCellValue("Tipo PLAN");
            cell = row.createCell(1);
            cell.setCellStyle(styles.get("texto_black_wrapped_bold_centered"));
            cell.setCellValue("Inferior");
            cell = row.createCell(2);
            cell.setCellStyle(styles.get("texto_black_wrapped_bold_centered"));
            cell.setCellValue("Superior");
            cell = row.createCell(3);
            cell.setCellStyle(styles.get("texto_black_wrapped_bold_centered"));
            cell.setCellValue("Prima Final Seguro Robo +");
            cell = row.createCell(4);
            cell.setCellStyle(styles.get("texto_black_wrapped_bold_centered"));
            cell.setCellValue("Comision Intermediación Sobre prima NETA");
            cell = row.createCell(5);
            cell.setCellStyle(styles.get("texto_black_wrapped_bold_centered"));
            cell.setCellValue("Comision Intermediación Sobre prima NETA");
	        
            for (CommissionPlan item : this.colCommissionPlans) {
            	if ((reportName.equals("Cel Plus") && !item.getTipoPlan().equals("Robo")) || (reportName.equals("Robo Plus") && item.getTipoPlan().equals("Robo")))
            	{
		            row = sheetCuadroCom.createRow(++rowCount);
		            
		            cell = row.createCell(0);
		            cell.setCellStyle(styles.get("texto_black_centered"));
		            cell.setCellValue(item.getTipoPlan());
		            cell = row.createCell(1);
		            
		            if (item.getRangoSuperior() > 2000000000)
		            {
		            	cell.setCellStyle(styles.get("texto_black_centered"));
		            	cell.setCellValue("Superior a");
		            }
		            else
		            {
		            	cell.setCellStyle(styles.get("pesos_black_centered_sin_cero"));
		            	cell.setCellValue(item.getRangoInferior() < 0 ? 0 : item.getRangoInferior());
		            }
		            
		            cell = row.createCell(2);
		            cell.setCellStyle(styles.get("pesos_black_centered_sin_cero"));
		            
		            if (item.getRangoSuperior() > 2000000000)
		            {
		            	cell.setCellValue(item.getRangoInferior() < 0 ? 0 : (item.getRangoInferior() - 1));
		            }
		            else
		            {
		            	cell.setCellValue(item.getRangoSuperior() < 0 ? 0 : item.getRangoSuperior());
		            }
		            
		            cell = row.createCell(3);
		            cell.setCellStyle(styles.get("decimal_black_centered"));
		            cell.setCellValue(item.getPrimaFinalSeguroRobo());
		            cell = row.createCell(4);
		            cell.setCellStyle(styles.get("decimal_black_centered"));
		            cell.setCellValue(item.getComInterPrimaNeta());
		            cell = row.createCell(5);
		            cell.setCellStyle(styles.get("porcentaje_black_centered_dos_dec"));
		            cell.setCellValue(item.getComInterPrimaNeta());
            	}
	        }
            
            //Se comenta a solicitud de Karen Vilaza
            /*if (reportName.equals("Cel Plus"))
            {
	            //Cuadro Prima
    			XSSFSheet sheetCuadroPrima = workbook.createSheet("Cuadro Prima");
            	
	            rowCount = 0;
		        
		        //HEADER
	            sheetCuadroPrima.setColumnWidth(0, 11*256);
	            sheetCuadroPrima.setColumnWidth(1, 11*256);
	            sheetCuadroPrima.setColumnWidth(2, 11*256);
	            sheetCuadroPrima.setColumnWidth(3, 15*256);
	            sheetCuadroPrima.addMergedRegion(new CellRangeAddress(0,0,0,3));
	            sheetCuadroPrima.addMergedRegion(new CellRangeAddress(1,1,1,2));
	            sheetCuadroPrima.addMergedRegion(new CellRangeAddress((this.colCommissionPlans.size() + 2),(this.colCommissionPlans.size() + 2),0,3));
	            sheetCuadroPrima.addMergedRegion(new CellRangeAddress((this.colCommissionPlans.size() + 3),(this.colCommissionPlans.size() + 3),0,2));
	            
	            row = sheetCuadroPrima.createRow(rowCount);
	            
	            cell = row.createCell(0);
		        cell.setCellStyle(styles.get("texto_white_bold_centered"));
	            cell.setCellValue("Celular Plus");
	            
	            row = sheetCuadroPrima.createRow(++rowCount);
		        
		        cell = row.createCell(0);
		        cell.setCellStyle(styles.get("texto_white_bold"));
	            cell.setCellValue("Tipo");
	            cell = row.createCell(1);
	            cell.setCellStyle(styles.get("texto_white_bold_centered"));
	            cell.setCellValue("Rango");
	            cell = row.createCell(3);
	            cell.setCellStyle(styles.get("texto_white_bold"));
	            cell.setCellValue("Valor Prima UF");
	            
	            for (CommissionPlan item : this.colCommissionPlans) {
	            	if (item.getTipoPlan() != "Robo")
	            	{
			            row = sheetCuadroPrima.createRow(++rowCount);
			            
			            cell = row.createCell(0);
				        cell.setCellStyle(styles.get("texto_white"));
			            cell.setCellValue(item.getTipoPlan());
			            cell = row.createCell(1);
			            
			            if (item.getRangoSuperior() > 2000000000)
			            {
			            	cell.setCellStyle(styles.get("texto_white_centered"));
			            	cell.setCellValue("Superior a");
			            }
			            else
			            {
			            	cell.setCellStyle(styles.get("pesos_white_centered_sin_cero"));
			            	cell.setCellValue(item.getRangoInferior() < 0 ? 0 : item.getRangoInferior());
			            }
			            
			            cell = row.createCell(2);
			            cell.setCellStyle(styles.get("pesos_white_centered_sin_cero"));
			            
			            if (item.getRangoSuperior() > 2000000000)
			            {
			            	cell.setCellValue(item.getRangoInferior() < 0 ? 0 : (item.getRangoInferior() - 1));
			            }
			            else
			            {
			            	cell.setCellValue(item.getRangoSuperior() < 0 ? 0 : item.getRangoSuperior());
			            }
		
			            cell = row.createCell(3);
			            cell.setCellStyle(styles.get("decimal_white"));
			            cell.setCellValue(item.getPrimaFinalSeguroRobo());
	            	}
		        }
	            
	            CommissionPlan cp = commissionPlanRepository.findByTipoPlan("Robo");
	            row = sheetCuadroPrima.createRow(++rowCount);
	            
	            cell = row.createCell(0);
		        cell.setCellStyle(styles.get("texto_white_bold_centered"));
	            cell.setCellValue("Robo Plus");
	            
	            row = sheetCuadroPrima.createRow(++rowCount);
	            
	            cell = row.createCell(0);
		        cell.setCellStyle(styles.get("texto_white_centered"));
	            cell.setCellValue("TODOS los productos:");
	            cell = row.createCell(3);
		        cell.setCellStyle(styles.get("decimal_white"));
	            cell.setCellValue(cp.getPrimaFinalSeguroRobo());
            }*/
	        
	        workbook.write(returnStream);
	        
	        workbook.close();
		}
		catch(Exception ex)
		{
			CommonMessages.ShowErrorMessage("Error", "No fue posible generar el reporte.");
		}
		
		return returnStream;
	}
		
	public Boolean ValidateForm()
	{
		if (this.year <= 0)	{
			CommonMessages.ShowWarningMessage("Datos Incompletos", "Debe seleccionar un año.");
			return false;
		}
		
		if (this.month <= 0) {
			CommonMessages.ShowWarningMessage("Datos Incompletos", "Debe seleccionar un mes.");
			return false;
		}
		
		if (this.companyId <= 0) {
			CommonMessages.ShowWarningMessage("Datos Incompletos", "Debe seleccionar una compañia.");
			return false;
		}
		
		if (this.tipoArchivo == null || this.tipoArchivo.isEmpty())	{
			CommonMessages.ShowWarningMessage("Datos Incompletos", "Debe seleccionar un tipo de archivo.");
			return false;
		}
		
		if (!ValidateGenerationPermissions()) {
			CommonMessages.ShowWarningMessage("No Autorizado:", "Aún no se ha generado el reporte de cierre del periodo actual y usted no tiene los permisos necesarios para generarlo.");
			return false;
		}
		
		return true;
	}

	public Boolean ValidateClosedPeriod()
	{
		List<Period> colPeriods = periodRepository.findByYearMonthType(this.year, this.month, 1);
		
		if (colPeriods.size() <= 0)
		{
			CommonMessages.ShowWarningMessage("Periodo Inválido", "El periodo seleccionado no está cerrado. Debe cerrarse para generar el reporte.");
			return false;
		}
		
		for (Period per : colPeriods)
			if (!per.getCerrado()) {
				CommonMessages.ShowWarningMessage("Periodo Inválido", "El periodo seleccionado no está cerrado. Debe cerrarse para generar el reporte.");
				return false;
			}
		
		return true;
	}
	
	public Boolean ValidateGenerationPermissions() {
		Boolean canGenerate = false;
		
		if (this.loggedUser != null)
		{
			List<Authority> authorities = authorityRepository.findByUserId(this.loggedUser.getId());
			
			for (Authority auth : authorities) {
				//Role rol = roleRepository.findOne(auth.getId());
				//System.out.println("************************************* ROL: " + auth.getId());
				if (auth.getRole().getName().equals("ROLE_ADMIN")){
					canGenerate = true;
				}
			}
			
			ReadBinnacle();
			
			if (!canGenerate)
				canGenerate = this.binnacles.size() > 0;
		}
		
		return canGenerate;
	}
	
	public Boolean ValidateUF()
	{
		this.UFMessage = "";
		this.validUF = true;
		
		if (this.UFValue.contains(".") || this.UFValue.contains("-")){
			this.validUF = false;
			this.UFMessage  = "El formato del valor U.F. es 000000,000000. Ejemplo: 25345,334345";
			return false;
		}
		
		try {
			Double numberUF = Double.parseDouble(this.UFValue.replace(",", "."));
		} catch (NumberFormatException e) {
			this.validUF = false;
			this.UFMessage  = "El formato del valor U.F. es 000000,000000. Ejemplo: 25345,334345";
			return false;
		}
		
		return true;
	}
	
	public void InsertBinnacle(FileType fileType, Integer binType, String fileName, String valorUF){		
		if (fileType != null)
		{
			List<Period> colPeriods = periodRepository.findByYearMonthType(this.year, this.month, fileType.getId());
			
			if (colPeriods.size() > 0)
			{
				Binnacle binnacle = new Binnacle();
				java.util.Date fecha = new Date();
				binnacle.setBinDate(fecha);
				binnacle.setFileType(fileType);
				binnacle.setBinType(binType);
				User userSession = (User) getCurrentInstance().getExternalContext().getSessionMap().get("user"); 
				binnacle.setUser(userSession);
				binnacle.setPeriod(colPeriods.get(0));
				binnacle.setDetails(valorUF);
				binnacle.setDetails2(fileName);
				binnacleRepository.save(binnacle);
			}
		}
		else
		{
			CommonMessages.ShowErrorMessage("Error:", "No se cargaron los tipos de archivo de Garantía Plus.");
		}
	}
	
	public void ReadBinnacle(){
    	try{
    		FileType ftReporte = fileTypeRepository.findByType(this.tipoArchivo);

    		if (ftReporte != null)
    		{
	    		if (binnacles == null)
	    			binnacles = new ArrayList<Binnacle>();
	    		
	    		binnacles.clear();
	    		
	    		List<Binnacle> colBin = binnacleRepository.findLastClosedByTypeAndPeriod(this.year, this.month, ftReporte.getId(), 2);
	    		
	    		if (colBin.size() > 0)
	    		{
	        		binnacles.add(colBin.get(0));
	        		this.UFValue = colBin.get(0).getDetails();
	    		}
    		}
			
    	}catch(Exception e){
    		e.printStackTrace();
    	}
	}
}