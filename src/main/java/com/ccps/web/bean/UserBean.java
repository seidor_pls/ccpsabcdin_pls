package com.ccps.web.bean;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import com.ccps.web.common.CommonMessages;
import com.ccps.web.model.Authority;
import com.ccps.web.model.User;
import com.ccps.web.model.repository.AuthorityRepository;
import com.ccps.web.model.repository.RoleRepository;
import com.ccps.web.model.repository.UserRepository;

/**
 * @author jDuchens, plopezs
 *
 */

@ManagedBean
@SessionScoped
@Component(value="userBean")
@Scope("session")
public class UserBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LoggerFactory.getLogger(User.class);
	private UploadedFile file;
	private String destination="/home/images/";
	private String contentType;
	private StreamedContent profileImage;
	
	private User user;
	private Integer idSeleccionado;
	private List<User> users;
	
	private User userSession;
	
	private String isAdmin;
	
	//Que tipo de guardado se está realizando actualmente (new, update)
	private String currentPersistenceMode = "";
	
	private String password;
	
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private AuthorityRepository authorityRepository;
	@Autowired
	private RoleRepository roleRepository;
	
	private static Random random = new Random((new Date()).getTime());
	
	public String getMessage() {
		logger.debug("Returning message from task home bean");
		return "Task message";
	}
	
	public User getUser() {

		return user;
	}
	
	public Integer getIdSeleccionado() {
		return idSeleccionado;
	}
	
	public UploadedFile getFile() {
        return file;
    }
 
    public void setFile(UploadedFile file) {
        this.file = file;
    }
    
    public String getCurrentPersistenceMode() {
		return currentPersistenceMode;
	}

	public void setCurrentPersistenceMode(String currentPersistenceMode) {
		this.currentPersistenceMode = currentPersistenceMode;
	}
	
	public String getTextoTituloEdicion() {
		if (this.currentPersistenceMode == "new")
			return "Nuevo Registro de Usuario";
		
		if (this.currentPersistenceMode == "update")
			return "Modificar Registro de Usuario";
		
		return "";
	}

	public String getIsAdmin() {
		return isAdmin;
	}

	public void setIsAdmin(String isAdmin) {
		this.isAdmin = isAdmin;
	}
	
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	//METHODS
	public String SaveUser() {
		
		if (this.password.trim().length() > 0) {
			this.password = getEncryptedPassword(this.password);
			user.setPassword(this.password);
		}
		
		User nuevoUser = userRepository.save(user);
		
		List<Authority> colAuth = authorityRepository.findByUserId(this.user.getId());
		
		for (Authority auth : colAuth)
		{
			authorityRepository.delete(auth);
		}
		
		Authority autho = new Authority();
		
		if (this.isAdmin.equals("1")){
			autho.setRole(roleRepository.findOne(1));
			autho.setUser(nuevoUser);
			authorityRepository.save(autho);
		}
		else
		{
			autho.setRole(roleRepository.findOne(2));
			autho.setUser(nuevoUser);
			authorityRepository.save(autho);
		}
		
		CleanAllFields();
		getUsers();
		
		try {
			FacesContext.getCurrentInstance().getExternalContext().redirect(FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath() + "/pages/user/list.jsf");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return "success";
	}
	
	public User getUserSession() {
			
			org.springframework.security.core.userdetails.User user = (org.springframework.security.core.userdetails.User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			userSession = userRepository.findByEmail(user.getUsername());
			FacesContext context = FacesContext.getCurrentInstance();
			userSession.setPassword(null);
			context.getExternalContext().getSessionMap().put("user", userSession);
			
		return userSession;
		
	}
	
	public void invlidateUserSession(){
		userSession = null;
	}
	
	public void invalidateUsers(){
		users = null;
	}
	
	public List<User> getUsers() {
		users = (List<User>) userRepository.findAll();
		return users;
	}
	
	public StreamedContent getProfileImage(){
		try{
			FacesContext context = FacesContext.getCurrentInstance();
	    	String id = context.getExternalContext().getRequestParameterMap().get("id");
	    	Integer idInt;
	    	if(id==null){
	    		User userSession = (User) context.getExternalContext().getSessionMap().get("user");
	    		idInt = userSession.getId();
	    	}
	    	else{
	    		idInt = Integer.parseInt(id);
	    	}
	    	User userImage = userRepository.findOne(idInt);
	    	String fileString = destination+"profile/"+idInt+"/"+userImage.getPhoto();
			File file = new File(fileString);
			
			if(file.isFile()){
				profileImage = new DefaultStreamedContent(new FileInputStream(fileString), contentType);	
			}
			else{
				String filePath = context.getExternalContext().getRealPath("/resources/images/avatar_default.jpg");
				profileImage = new DefaultStreamedContent(new FileInputStream(filePath), contentType);
			}
		}catch(Exception e){ e.printStackTrace(); }
		
		return profileImage;
        
    }
	
	public StreamedContent getImageId() throws IOException{
		FacesContext context = FacesContext.getCurrentInstance();
    	String id = context.getExternalContext().getRequestParameterMap().get("idUsuario");
    	
		return null;
	}
	
	public void NewUser()	{
		try
		{
			//Setea tipo de guardado a nuevo usuario.
			this.currentPersistenceMode = "new";
			
			user = new User();
			
			FacesContext.getCurrentInstance().getExternalContext().redirect(FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath() + "/pages/user/update.jsf");
		}
		catch (IOException ex)
		{
			//TODO: Excepcion
		}
	}
	
	public void UpdateUser(int id) {
		try
		{
			//Setea tipo de guardado a edición.
			this.currentPersistenceMode = "update";
			
			this.idSeleccionado = id;
			
			user = userRepository.findOne(this.idSeleccionado);
			this.password = "";
			
			SetIsAdminFromUser();
			
			FacesContext.getCurrentInstance().getExternalContext().redirect(FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath() + "/pages/user/update.jsf");
		}
		catch (IOException ex)
		{
			//TODO: Excepcion
		}
	}

	public void CancelEditUser() {
		try
		{
			//Despues de Guardar Limpia el tipo de guardado actual.
			this.currentPersistenceMode = "";
			this.idSeleccionado = 0;
			CleanAllFields();
			
			FacesContext.getCurrentInstance().getExternalContext().redirect(FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath() + "/pages/user/list.jsf");
		}
		catch (IOException ex)
		{
			//TODO: Excepcion
		}
	}
	
	
	public void CleanAllFields() {
		this.user = null;
		this.password = "";
	}
	
	
	public void HandleDeleteUser(int id) {
		try
		{
			//Setea tipo de guardado a edición.
			this.currentPersistenceMode = "update";
			
			this.idSeleccionado = id;
			
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("$('#dialogConfirmEliminaUser').show();");
		}
		catch (Exception ex)
		{
			//TODO: Excepcion
		}
	}

	public void DeleteUser() {
		try
		{
			this.user = userRepository.findOne(this.idSeleccionado);
			
			List<Authority> colAuth = authorityRepository.findByUserId(this.user.getId());
			
			for (Authority auth : colAuth)
			{
				authorityRepository.delete(auth);
			}
			
			userRepository.delete(this.user);
			
			CleanAllFields();
			
			getUsers();
			
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("$('#dialogConfirmEliminaUser').hide();");
			
			CommonMessages.ShowInfoMessage("Info:", "Usuario eliminado correctamente.");
		}
		catch (Exception ex)
		{
			//TODO: Excepcion
		}
	}
	
	public void CancelDeleteUser()
	{
		this.currentPersistenceMode = "";
		this.idSeleccionado = 0;
		CleanAllFields();
		
		RequestContext context = RequestContext.getCurrentInstance();
		context.execute("$('#dialogConfirmEliminaUser').hide();");
	}

	public static String getEncryptedPassword(String clearTextPassword)   {
	  return org.apache.commons.codec.digest.DigestUtils.sha256Hex(clearTextPassword);
	}
	
	public void SetIsAdminFromUser()
	{
		this.isAdmin = "0";
		
		List<Authority> colAuth = authorityRepository.findByUserId(this.user.getId());
		for (Authority auth : colAuth) {
			if (auth.getRole().getName().equals("ROLE_ADMIN"))
				this.isAdmin = "1";
		}
	}
}
