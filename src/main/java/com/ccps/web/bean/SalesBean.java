package com.ccps.web.bean;

import static java.lang.System.out;
import static javax.faces.context.FacesContext.getCurrentInstance;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.event.ValueChangeEvent;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.ccps.web.common.CommonMessages;
import com.ccps.web.model.Binnacle;
import com.ccps.web.model.Collection;
import com.ccps.web.model.Company;
import com.ccps.web.model.Enterprise;
import com.ccps.web.model.FileType;
import com.ccps.web.model.FileTypeCompany;
import com.ccps.web.model.Period;
import com.ccps.web.model.Sale;
import com.ccps.web.model.repository.*;
import com.ccps.web.model.User;

/**
 * @author jDuchens
 *
 */


@ManagedBean
@SessionScoped
@Component(value="salesBean")
@Scope("session")
public class SalesBean implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LoggerFactory.getLogger(SalesBean.class);
	
	private String individualfileType;
	private UploadedFile file;
	private HSSFWorkbook workbook;
	private XSSFWorkbook workBook2;
	private Boolean error = false;
	private String company;
	private int companyId;
	private List<Company> companies;
	private String enterprise;
	private int enterpriseId;
	private List<Enterprise> enterprises;
	private Sale sale;
	private List<Sale> sales;
	List<FileType> fileTypesVentas;
	List<FileType> fileTypesRemesa;
	Period period;
	List<Period> periodList;
	private Integer valueCertificado;          
	private Integer valueSucursal;               
	private String valueRutVendedor;            
	private String valueNombreVendedor;         
	private String valueCargoVendedor;          
	private String valueRutReferente;           
	private String valueNombreReferente;        
	private String valueCargoReferente;         
	private String valueIndCruce;               
	private Integer valueNroRemesa;               
	private Integer valueCodigoProducto;         
	private Integer valuePuntosPorVenta;         
	private Date valueFechaEmision;           
	private Integer valueTarjeta;                
	private Double valuePrimaAnualizada;        
	private Double valueComisionRecaudacion;    
	private Double valueComisionRecaudacion2;   
	private String valueRutAsegurado;           
	private String valueNombreAsegurado;        
	private Date valueFechaNacimiento;        
	private String valueDireccionParticular;    
	private String valueComuna;                 
	private String valueTelParticular;          
	private String valueTelLaboral;             
	private String valueTelCelular;             
	private String valueEstado;                 
	private String valueUsuarioDesafiliacion;   
	private Date valueFechaDesafiliacion;     
	private String valueMotivoDesafiliacion;    
	private Integer valueIdentifGrabacion;
	private int closingYear = 0;
	private int closingMonth = 0;
	private int year = 0;
	private int month = 0;
	private String strMonth = "";
	private Map<Integer, String> colYears;
	private Map<Integer, String> colMonths;
	private String valueRutEmp;	
	private Integer valueRubro;	
	private Integer valueCorrelativo;	
	private String valueRutCli;	
	private Date valueFechaAplicacion;	
	private Integer valueNumeroRemesa;	
	private Date valueFechaProceso;	
	private Integer valueMonto;
	private List<Binnacle> binnacles;
	private Boolean isFileRecaudacion = true;
	@Autowired
	private SalesRepository salesRepository;
	@Autowired
	private FileTypeRepository fileTypeRepository;
	@Autowired
	private PeriodRepository periodRepository;
	@Autowired
	private CollectionRepository collectionRepository;
	@Autowired
	private CompanyRepository companyRepository;
	@Autowired
	private EnterpriseRepository enterpriseRepository;
	@Autowired
	private BinnacleRepository binnacleRepository;
	@Autowired
	private FileTypeCompanyRepository fileTypeCompanyRepository;
	
	public Boolean getCanClosePeriod() {
		return binnacles.size() == (fileTypesVentas.size() + fileTypesRemesa.size());
	}
	
	public String getIndividualfileType() {
		return individualfileType;
	}

	public void setIndividualfileType(String individualfileType) {
		this.individualfileType = individualfileType;
	}

	public UploadedFile getFile() {
		return file;
	}

	public void setFile(UploadedFile file) {
		this.file = file;
	}

	public HSSFWorkbook getWorkbook() {
		return workbook;
	}

	public void setWorkbook(HSSFWorkbook workbook) {
		this.workbook = workbook;
	}

	public Boolean getError() {
		return error;
	}

	public void setError(Boolean error) {
		this.error = error;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getEnterprise() {
		return enterprise;
	}

	public void setEnterprise(String enterprise) {
		this.enterprise = enterprise;
	}

	public List<Company> getCompanies() {
		return companies;
	}

	public void setCompanies(List<Company> companies) {
		this.companies = companies;
	}

	public List<Enterprise> getEnterprises() {
		return enterprises;
	}

	public void setEnterprises(List<Enterprise> enterprises) {
		this.enterprises = enterprises;
	}

	public Sale getSale() {
		sale = salesRepository.findById(1);
		return sale;
	}

	public void setSale(Sale sale) {
		this.sale = sale;
	}
	
	public List<Sale> getSales() {
		sales = salesRepository.findAll();  
		return sales;
	}

	public int getClosingYear() {
		return closingYear;
	}

	public void setClosingYear(int closingYear) {
		this.closingYear = closingYear;
	}

	public int getClosingMonth() {
		return closingMonth;
	}

	public void setClosingMonth(int closingMonth) {
		this.closingMonth = closingMonth;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		this.month = month;
	}

	public String getStrMonth() {
		return strMonth;
	}

	public void setStrMonth(String strMonth) {
		this.strMonth = strMonth;
	}

	public Map<Integer, String> getColYears() {
		return colYears;
	}

	public void setColYears(Map<Integer, String> colYears) {
		this.colYears = colYears;
	}

	public Map<Integer, String> getColMonths() {
		return colMonths;
	}

	public void setColMonths(Map<Integer, String> colMonths) {
		this.colMonths = colMonths;
	}
	
	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	public int getEnterpriseId() {
		return enterpriseId;
	}

	public void setEnterpriseId(int enterpriseId) {
		this.enterpriseId = enterpriseId;
	}

	public List<Period> getPeriodList() {
		return periodList;
	}

	public void setPeriodList(List<Period> periodList) {
		this.periodList = periodList;
	}

	public List<Binnacle> getBinnacles() {
		return binnacles;
	}

	public void setBinnacles(List<Binnacle> binnacles) {
		this.binnacles = binnacles;
	}

	public Boolean getIsFileRecaudacion() {
		return isFileRecaudacion;
	}

	public void setIsFileRecaudacion(Boolean isFileRecaudacion) {
		this.isFileRecaudacion = isFileRecaudacion;
	}
	
	public Boolean getCanRollbackPeriod() {
		if (this.binnacles.size() > 0)
				return false;
		
		return true;
	}
	
	//METHODS
	@PostConstruct
    public void init() {
		//LOAD ENTERPRISE
		this.enterprises = new ArrayList<Enterprise>();
		//ABC
		this.enterprises.add(enterpriseRepository.findOne(1));
		//DIN
		this.enterprises.add(enterpriseRepository.findOne(2));
		
		InitYearsMonths();
		InitPeriod();
		GetSalesCompanies();
		ReadBinnacle();
	}
	
	public void GetSalesCompanies()
	{
		this.companies = new ArrayList<Company>();
		
		for (FileType ft : fileTypesVentas)
		{
			List<FileTypeCompany> ftCompanyColl = fileTypeCompanyRepository.findByFileType(ft.getId());
			
			for (FileTypeCompany ftc : ftCompanyColl)
			{
				Boolean contains = false;
				
				for (Company com : this.companies)
				{
					if (ftc.getCompany().getId() == com.getId())
						contains = true;
				}
				
				if (!contains)
					this.companies.add(ftc.getCompany());
			}
		}
		
		//Se llenan las compañias relacionadas SOLO CON VENTAS, las de remesa no son necesarias. En caso de requerirlo, se debe descomentar el código siguiente:
		/*
		for (FileType ft : fileTypesRemesa)
		{
			List<FileTypeCompany> ftCompanyColl = fileTypeCompanyRepository.findByFileType(ft.getId());
			
			for (FileTypeCompany ftc : ftCompanyColl)
			{
				Boolean contains = false;
				
				for (Company com : this.companies)
				{
					if (ftc.getCompany().getId() == com.getId())
						contains = true;
				}
				
				if (!contains)
					this.companies.add(ftc.getCompany());
			}
		}*/
		
		if (this.companies.size() == 1)
		{
			this.companyId = this.companies.get(0).getId();
		}
	}

	@SuppressWarnings("deprecation")
	public void InitYearsMonths(){
		try{
			Date today = new Date();
			this.colYears = new HashMap<Integer, String>();
			this.colMonths = new HashMap<Integer, String>();
			for (Integer i = today.getYear(); i < today.getYear() + 5; i++){this.colYears.put(i, i.toString());}
			this.colMonths.put(1, "Enero");this.colMonths.put(2, "Febrero");this.colMonths.put(3, "Marzo");this.colMonths.put(4, "Abril");this.colMonths.put(5, "Mayo");
			this.colMonths.put(6, "Junio");this.colMonths.put(7, "Julio");this.colMonths.put(8, "Agosto");this.colMonths.put(9, "Septiembre");this.colMonths.put(10, "Octubre");
			this.colMonths.put(11, "Noviembre");this.colMonths.put(12, "Diciembre");
		}
		catch (Exception e){
			this.error = true;
			FacesMessage message = new FacesMessage();
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			message.setSummary("Error al inicializar los meses y años. " + e.getMessage());
			message.setDetail(e.getMessage());
			FacesContext.getCurrentInstance().addMessage(null, message);
	    }
	}

	public void InitPeriod(){
		try{
			this.fileTypesVentas = fileTypeRepository.findByCategory("ventas");
			this.fileTypesRemesa = fileTypeRepository.findByCategory("remesa");
		
			if (fileTypesVentas.size() > 0) {
				int maxYear  = periodRepository.findMaxYearClosedPeriodsByType(fileTypesVentas.get(0).getId());
				int maxMonth = 0;
				
				List<Period> colPeriods = periodRepository.findAllClosedPeriodsByYearType(maxYear, fileTypesVentas.get(0).getId());
				
				for (Period period:colPeriods) {
					if (period.getMes() > maxMonth)maxMonth = period.getMes();
				}
				if (maxMonth < 12) {
					maxMonth++;
				}
				else {
					maxYear++;maxMonth = 1;
				}
				
				colPeriods = periodRepository.findByYearMonthType(maxYear, maxMonth, fileTypesVentas.get(0).getId());
				
				//Si no existe creo un nuevo periodo
				if (colPeriods.size() <= 0){
					
					for (FileType ft : fileTypesVentas)
					{
						Period periodo = new Period(ft, maxMonth, maxYear, false);
						periodRepository.save(periodo);
					}
					
					for (FileType ft : fileTypesRemesa)
					{
						Period periodo = new Period(ft, maxMonth, maxYear, false);
						periodRepository.save(periodo);
					}
				}
				
				this.month    = maxMonth;
				this.year     = maxYear;
				this.strMonth = this.colMonths.get(maxMonth);
			}
		}
		catch (Exception e){
			this.error = true;
			FacesMessage message = new FacesMessage();
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			message.setSummary("Error al tratar de inicializar el periodo. " + e.getMessage());
			message.setDetail(e.getMessage());
			FacesContext.getCurrentInstance().addMessage(null, message);
	    }
	}
	
	public void CloseMonth() {
		try{
			for (FileType ft : fileTypesVentas)
			{
				List<Period> colPeriods = periodRepository.findByYearMonthType(this.year, this.month, ft.getId());
				for (Period per: colPeriods){
					per.setCerrado(true);
					periodRepository.save(per);
					InsertBinnacle(ft, 4, "Ventas: Cierre de Mes");
				}
			}
			
			for (FileType ft : fileTypesRemesa)
			{
				List<Period> colPeriods = periodRepository.findByYearMonthType(this.year, this.month, ft.getId());
				for (Period per: colPeriods){
					per.setCerrado(true);
					periodRepository.save(per);
					InsertBinnacle(ft, 4, "Remesas: Cierre de Mes");
				}
			}
			
			InitPeriod();
			ReadBinnacle();
			
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("$('#dialogConfirmCierre').hide();");
			CommonMessages.ShowInfoMessage("Info: ", "Mes cerrado correctamente.");
		}catch (Exception e){
			this.error = true;
			FacesMessage message = new FacesMessage();
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			message.setSummary("Error al intentar cerrar el mes. " + e.getMessage());
			message.setDetail(e.getMessage());
			FacesContext.getCurrentInstance().addMessage(null, message);
	    }
	}
	
	public void HandleUploadExcel(FileUploadEvent event){
		this.file = event.getFile();
	}
	
	public void UploadExcel(){
		RequestContext context = RequestContext.getCurrentInstance();
		
		if (ValidateIndividualFileType())
		{
			if (this.individualfileType.equals("ventas"))
			{		
				if (ValidateFormVentas())
				{
			        List<Sale> colSales = salesRepository.findByPeriodCompanyEnterprise(this.month, this.year, this.companyId, this.enterpriseId);
			        
			        if (colSales.size() > 0)
			        {
			        	context.execute("PF('statusDialog').hide();");
			        	context.execute("$('#dialogConfirmRecarga').show();");
			        }
			        else
			        {
			        	ProcessUploadExcel();
			        	context.execute("PF('statusDialog').hide();");
			        }
		        }
				else
				{
					context.execute("PF('statusDialog').hide();");
				}
			}
			else
			{
				UploadExcelRemesa();
			}
		}
		else
		{
			context.execute("PF('statusDialog').hide();");
		}
	}
	
	public void AcceptReload(){
        try
        {
        	if (this.individualfileType.equals("ventas"))
        	{
				List<Sale> colSales = salesRepository.findByPeriodCompanyEnterprise(this.month, this.year, this.companyId, this.enterpriseId);
				
				for (Sale sale : colSales)
				{
					salesRepository.delete(sale);
				}
				
				ProcessUploadExcel();
        	}
        	else
        	{
        		AcceptReloadRecaudacion();
        	}
        }
        catch (Exception ex)
        {
        	CommonMessages.ShowErrorMessage("Error", "No fue posible limpiar los registros para el periodo solicitado");
        }
        finally {
        	RequestContext context = RequestContext.getCurrentInstance();
			context.execute("$('#dialogConfirmRecarga').hide();");			
        	context.execute("PF('statusDialog').hide();");
		}
	}
	
	public void ProcessUploadExcel() {
		FacesMessage message = new FacesMessage();
		try{
	        if(this.file != null) {
	            String formatSuffix = file.getFileName();
	            formatSuffix = formatSuffix.toLowerCase();
	            out.println("upload file != a null in basicBean!!!!!!!!!!! " + file.getFileName() + " - " + formatSuffix.endsWith(".xls"));
	            /*######################################:READ EXCEL:#######################################*/
		        //Read from memory
		        FileInputStream fis = (FileInputStream) file.getInputstream();
		        //Read from directory
		        workbook          = new HSSFWorkbook (fis);
		        HSSFSheet sheet   = workbook.getSheetAt(0);
		        Iterator<Row> ite = sheet.rowIterator(); 
		        FileType objFileTypeToSave = null;
		        
		        List<FileTypeCompany> colFtcToSave = fileTypeCompanyRepository.findByCompany(this.companyId);
		        Enterprise objEnterprise = enterpriseRepository.findOne(this.enterpriseId);
		        
		        for (FileTypeCompany ftc : colFtcToSave)
		        {
		        	if (ftc.getFileType().getType().contains(objEnterprise.getName()))
		        	{
		        		objFileTypeToSave = ftc.getFileType();
		        		break;
		        	}
		        }
		        
		        try{
	        		 this.periodList = periodRepository.findByYearMonthType(this.year, this.month, objFileTypeToSave.getId());
	        		 this.period = periodList.get(0);
	        	}catch(NullPointerException e){
	        		 e.printStackTrace();
	        	}catch (Exception e) {
	        		 e.printStackTrace();
				}
		        Enterprise empresa = enterpriseRepository.findOne(this.enterpriseId);
		        Company compa = companyRepository.findOne(this.companyId);
		        SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy");
		        String temp = "";
		        int i = 0;
		        while(ite.hasNext()){ 
					Row row = ite.next();  
					if(i>0){
						HSSFCell cellCertificado           = (HSSFCell) row.getCell(0);
						HSSFCell cellSucursal              = (HSSFCell) row.getCell(1);
						HSSFCell cellRutVendedor           = (HSSFCell) row.getCell(2);
						HSSFCell cellNombreVendedor        = (HSSFCell) row.getCell(3);
						HSSFCell cellCargoVendedor         = (HSSFCell) row.getCell(4);
						HSSFCell cellRutReferente          = (HSSFCell) row.getCell(5);
						HSSFCell cellNombreReferente       = (HSSFCell) row.getCell(6);
						HSSFCell cellCargoReferente        = (HSSFCell) row.getCell(7);
						HSSFCell cellIndCruce              = (HSSFCell) row.getCell(8);
						HSSFCell cellNroRemesa             = (HSSFCell) row.getCell(9);
						HSSFCell cellCodigoProducto        = (HSSFCell) row.getCell(10);
						HSSFCell cellPuntosPorVenta        = (HSSFCell) row.getCell(11);
						HSSFCell cellFechaEmision          = (HSSFCell) row.getCell(12);
						HSSFCell cellTarjeta               = (HSSFCell) row.getCell(13);
						HSSFCell cellPrimaAnualizada       = (HSSFCell) row.getCell(14);
						HSSFCell cellComisionRecaudacion   = (HSSFCell) row.getCell(15);
						HSSFCell cellComisionRecaudacion2  = (HSSFCell) row.getCell(16);
						HSSFCell cellRutAsegurado          = (HSSFCell) row.getCell(17);
						HSSFCell cellNombreAsegurado       = (HSSFCell) row.getCell(18);
						HSSFCell cellFechaNacimiento       = (HSSFCell) row.getCell(19);
						HSSFCell cellDireccionParticular   = (HSSFCell) row.getCell(20);
						HSSFCell cellComuna                = (HSSFCell) row.getCell(21);
						HSSFCell cellTelParticular         = (HSSFCell) row.getCell(22);
						HSSFCell cellTelLaboral            = (HSSFCell) row.getCell(23);
						HSSFCell cellTelCelular            = (HSSFCell) row.getCell(24);
						HSSFCell cellEstado                = (HSSFCell) row.getCell(25);
						HSSFCell cellUsuarioDesafiliacion  = (HSSFCell) row.getCell(26);
						HSSFCell cellFechaDesafiliacion    = (HSSFCell) row.getCell(27);
						HSSFCell cellMotivoDesafiliacion   = (HSSFCell) row.getCell(28);
						HSSFCell cellIdentifGrabacion      = (HSSFCell) row.getCell(29);
//						out.println(cellCertificado);out.println(cellSucursal);out.println(cellRutVendedor);out.println(cellNombreVendedor);out.println(cellCargoVendedor);
//						out.println(cellRutReferente);out.println(cellNombreReferente);out.println(cellCargoReferente);out.println(cellIndCruce);out.println(cellNroRemesa);
//						out.println(cellCodigoProducto);out.println(cellPuntosPorVenta);out.println(cellFechaEmision);out.println(cellTarjeta);out.println(cellPrimaAnualizada);
//						out.println(cellComisionRecaudacion);out.println(cellComisionRecaudacion2);out.println(cellRutAsegurado);out.println(cellNombreAsegurado);
//						out.println(cellFechaNacimiento);out.println(cellDireccionParticular);out.println(cellComuna);out.println(cellTelParticular);out.println(cellTelLaboral);
//						out.println(cellTelCelular);out.println(cellEstado);out.println(cellUsuarioDesafiliacion);out.println(cellFechaDesafiliacion);
//						out.println(cellMotivoDesafiliacion);out.println(cellIdentifGrabacion);
					    try{temp = (cellCertificado.getStringCellValue() != "")?cellCertificado.getStringCellValue():"0"; 
					    	valueCertificado           = Integer.parseInt(temp);}catch(IllegalStateException e){out.println("1");}catch(NumberFormatException e){out.println("1"); }
					    try{temp = (cellSucursal.getStringCellValue() != "")?cellSucursal.getStringCellValue():"0";
					    	valueSucursal              = Integer.parseInt(temp);}catch(IllegalStateException e){out.println("2");}catch(NumberFormatException e){out.println("2"); }
					    try{valueRutVendedor           = cellRutVendedor.getStringCellValue();}catch(IllegalStateException e){out.println("3");}
					    try{valueNombreVendedor        = cellNombreVendedor.getStringCellValue();}catch(IllegalStateException e){out.println("4");}
					    try{valueCargoVendedor         = cellCargoVendedor.getStringCellValue();}catch(IllegalStateException e){out.println("5");}
					    try{valueRutReferente          = cellRutReferente.getStringCellValue();}catch(IllegalStateException e){out.println("6");}
					    try{valueNombreReferente       = cellNombreReferente.getStringCellValue();}catch(IllegalStateException e){out.println("7");}
					    try{valueCargoReferente        = cellCargoReferente.getStringCellValue();}catch(IllegalStateException e){out.println("8");}
					    try{valueIndCruce              = cellIndCruce.getStringCellValue();}catch(IllegalStateException e){out.println("9");}
					    try{temp = (cellNroRemesa.getStringCellValue() != "")?cellNroRemesa.getStringCellValue():"0";
					    	valueNroRemesa             = Integer.parseInt(temp);}catch(IllegalStateException e){out.println("10");}catch(NumberFormatException e){out.println("10"); }
					    try{temp = (cellCodigoProducto.getStringCellValue() != "")?cellCodigoProducto.getStringCellValue():"0";
					    	valueCodigoProducto        = Integer.parseInt(temp);}catch(IllegalStateException e){out.println("11");}catch(NumberFormatException e){out.println("11"); }
					    try{temp = (cellPuntosPorVenta.getStringCellValue() != "")?cellPuntosPorVenta.getStringCellValue():"0";
					    	valuePuntosPorVenta        = Integer.parseInt(temp);}catch(IllegalStateException e){out.println("12");}catch(NumberFormatException e){out.println("12"); }
					    try{valueFechaEmision          = dateFormatter.parse(cellFechaEmision.getStringCellValue());}catch(IllegalStateException e){out.println("13");}catch (ParseException e) {
					    	out.println("13"); e.printStackTrace();
						}
					    try{valueTarjeta               = (Integer)(int) cellTarjeta.getNumericCellValue();}catch(IllegalStateException e){out.println("14");}catch(NumberFormatException e){out.println("14"); }
					    try{temp = (cellPrimaAnualizada.getStringCellValue() != "")?cellPrimaAnualizada.getStringCellValue():"0";
					    	valuePrimaAnualizada       = Double.parseDouble(temp);}catch(IllegalStateException e){out.println("15"); e.printStackTrace();}catch(NumberFormatException e){out.println("15"); }
					    try{temp = (cellComisionRecaudacion.getStringCellValue() != "")?cellComisionRecaudacion.getStringCellValue():"0";
					    	valueComisionRecaudacion   = Double.parseDouble(temp);}catch(IllegalStateException e){out.println("16"); e.printStackTrace();}catch(NumberFormatException e){out.println("16"); }
					    try{temp = (cellComisionRecaudacion2.getStringCellValue() != "")?cellComisionRecaudacion2.getStringCellValue():"0";
					    	valueComisionRecaudacion2  = Double.parseDouble(temp);}catch(IllegalStateException e){out.println("17"); e.printStackTrace();}catch(NumberFormatException e){out.println("17"); }
					    try{valueRutAsegurado          = cellRutAsegurado.getStringCellValue();}catch(IllegalStateException e){out.println("18");}
					    try{valueNombreAsegurado       = cellNombreAsegurado.getStringCellValue();}catch(IllegalStateException e){out.println("19");}
					    try{valueFechaNacimiento       = dateFormatter.parse(cellFechaNacimiento.getStringCellValue());}catch(IllegalStateException e){out.println("20"); e.printStackTrace();}catch (ParseException e) {
					    	out.println("20"); e.printStackTrace();
						}
					    try{valueDireccionParticular   = cellDireccionParticular.getStringCellValue();}catch(IllegalStateException e){out.println("21");}
					    try{valueComuna                = cellComuna.getStringCellValue();}catch(IllegalStateException e){out.println("22");}
					    try{valueTelParticular         = cellTelParticular.getStringCellValue();}catch(IllegalStateException e){out.println("23");}
					    try{valueTelLaboral            = cellTelLaboral.getStringCellValue();}catch(IllegalStateException e){out.println("24");}
					    try{valueTelCelular            = cellTelCelular.getStringCellValue();}catch(IllegalStateException e){out.println("25");}
					    try{valueEstado                = cellEstado.getStringCellValue();}catch(IllegalStateException e){out.println("26");}
					    try{valueUsuarioDesafiliacion  = cellUsuarioDesafiliacion.getStringCellValue();}catch(IllegalStateException e){out.println("27");}
					    try{valueFechaDesafiliacion    = dateFormatter.parse(cellFechaDesafiliacion.getStringCellValue()); }catch(IllegalStateException e){out.println("28");}catch (ParseException e) {
					    	out.println("28"); e.printStackTrace();
						}
					    try{valueMotivoDesafiliacion   = cellMotivoDesafiliacion.getStringCellValue();}catch(IllegalStateException e){out.println("29");}
					    try{temp = (cellIdentifGrabacion.getStringCellValue() != "")?cellIdentifGrabacion.getStringCellValue():"0";out.println("temp!!!! " + temp);
					    	valueIdentifGrabacion      = Integer.parseInt(temp);}catch(IllegalStateException e){out.println("30-1");valueIdentifGrabacion = (Integer)(int)cellIdentifGrabacion.getNumericCellValue();}catch(NumberFormatException e){out.println("30-2");}

						Sale sSales = new Sale(objFileTypeToSave,
								                this.period,
								                compa,
								                empresa,
												valueCertificado, 
												valueSucursal, 
												valueRutVendedor,
												valueNombreVendedor, 
												valueCargoVendedor, 
												valueRutReferente, 
												valueNombreReferente, 
												valueCargoReferente, 
												valueIndCruce,
												valueNroRemesa, 
												valueCodigoProducto,
												valuePuntosPorVenta, 
												valueFechaEmision, 
												valueTarjeta,
												valuePrimaAnualizada, 
												valueComisionRecaudacion, 
												valueComisionRecaudacion2, 
												valueRutAsegurado, 
												valueNombreAsegurado,
												valueFechaNacimiento, 
												valueDireccionParticular, 
												valueComuna, 
												valueTelParticular,
												valueTelLaboral, 
												valueTelCelular, 
												valueEstado, 
												valueUsuarioDesafiliacion, 
												valueFechaDesafiliacion,
												valueMotivoDesafiliacion, 
												valueIdentifGrabacion);
			
					   salesRepository.save(sSales);

					}
					i++;//if(i==3)break;
					
				}
				fis.close();	
				InsertBinnacle(objFileTypeToSave, 1, this.file.getFileName());
				workbook.close();
		        /*####################################:END READ EXCEL:######################################*/

				/*###############################:Message Succesful Upload:##################################*/
				message.setSeverity(FacesMessage.SEVERITY_INFO);
				message.setSummary("Info:");
				message.setDetail("Archivo " + "\"" + file.getFileName() + "\"" + " procesado correctamente.");
	            FacesContext.getCurrentInstance().addMessage(null, message);
	            this.file = null;
	            ReadBinnacle();
	            /*#############################:END Message Succesful Upload:################################*/
	        }else{
	        	logger.info("upload file = a null in SalesBean!!!!!!!!!!!");
	        	/*###############################:Message Succesful Upload:##################################*/
	        	this.error = true;
				message.setSeverity(FacesMessage.SEVERITY_WARN);
				message.setSummary("Warning:");
				message.setDetail("El archivo esta vacio.");
	            FacesContext.getCurrentInstance().addMessage(null, message);
	            /*#############################:END Message Succesful Upload:################################*/
	        }
	       
		}catch(IOException e){
			e.printStackTrace();
			this.error = true;
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			message.setSummary("Error:");
			message.setDetail("No fue posible leer el archivo. Es probable que se encuentre bloqueado por otro proceso, o fue eliminado.");
			FacesContext.getCurrentInstance().addMessage(null, message);
		
		}catch(Exception e){
			e.printStackTrace();
			this.error = true;
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			message.setSummary("Error:");
			message.setDetail(e.getMessage());
			FacesContext.getCurrentInstance().addMessage(null, message);
		}
	}
	
	public void HandleUploadExcelRemesa(FileUploadEvent event){
		this.file = event.getFile();
	}
	
	public void UploadExcelRemesa(){
		RequestContext context = RequestContext.getCurrentInstance();
		if (ValidateFormRemesas())
		{
	        List<Collection> colRecaudacion = collectionRepository.findByPeriod(this.month, this.year);
	        
	        if (colRecaudacion.size() > 0)
	        {
	        	context.execute("PF('statusDialog').hide();");
	        	context.execute("$('#dialogConfirmRecarga').show();");
	        }
	        else
	        {
	        	ProcessUploadExcelRemesa();
	        	context.execute("PF('statusDialog').hide();");
	        }
		}
		else
		{
			context.execute("PF('statusDialog').hide();");
		}
	}
	
	public void AcceptReloadRecaudacion(){
        try
        {
        	List<Collection> colRecaudacion = collectionRepository.findByPeriod(this.month, this.year);
			
			for (Collection col : colRecaudacion)
			{
				collectionRepository.delete(col);
			}
			
			ProcessUploadExcelRemesa();
        }
        catch (Exception ex)
        {
        	CommonMessages.ShowErrorMessage("Error", "No fue posible limpiar los registros para el periodo solicitado");
        }
        finally {
        	RequestContext context = RequestContext.getCurrentInstance();
			context.execute("$('#dialogConfirmRecarga').hide();");
			context.execute("PF('statusDialog').hide();");
		}
	}
	
	public void ProcessUploadExcelRemesa(){
		
		try{
	        if(this.file != null) {
	        	/*#####################################:READ EXCEL:#######################################*/
	        	 FileInputStream fis = (FileInputStream) file.getInputstream(); 
	        	 workBook2 = new XSSFWorkbook(fis);
	        	 int sheetsSize = workBook2.getNumberOfSheets();
	        	 Integer temp2;
	        	 SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyyMMdd");
	        	 FileType objFileTypeToSave = fileTypeRepository.findByType("Remesas");
		         Enterprise empresa = null;
		         Company compania = null;
	        	 try{
	        		 this.periodList = periodRepository.findByYearMonthType(this.year, this.month, objFileTypeToSave.getId());
	        		 this.period = periodList.get(0);
	        	 }catch(NullPointerException e){
	        		 e.printStackTrace();
	        	 }catch (Exception e) {
	        		 e.printStackTrace();
				 }
	        	 for(int currentSheet=1; currentSheet<sheetsSize; currentSheet++){ 
	        		 XSSFSheet sheet = workBook2.getSheetAt(currentSheet);
	        		 Iterator<Row> ite = sheet.iterator();
	        		 
	        		 switch (currentSheet) {
				        case 1:
				        	 compania = companyRepository.findByName("OHIO");
				        	 empresa  = enterpriseRepository.findByName("DIN");
				        break;
				        case 2:
				        	 compania = companyRepository.findByName("HDI");
				        	 empresa  = enterpriseRepository.findByName("DIN");
				        break;
				        case 3:
				        	continue;
				        case 4:
				        	 compania = companyRepository.findByName("BCI");
				        	 empresa  = enterpriseRepository.findByName("DIN");
					    break;
				        case 5:
				        	 compania = companyRepository.findByName("BICE");
				        	 empresa  = enterpriseRepository.findByName("DIN");
					    break;
				        case 6:
				        	 compania = companyRepository.findByName("CARDIF");
				        	 empresa  = enterpriseRepository.findByName("DIN");
					    break;
				        case 7:
				        	 compania = companyRepository.findByName("OHIO");
				        	 empresa  = enterpriseRepository.findByName("ABC");
					    break;
				        case 8:
				        	 compania = companyRepository.findByName("HDI");
				        	 empresa  = enterpriseRepository.findByName("ABC");
					    break;
				        case 9:
				        	 compania = companyRepository.findByName("METLIFE");
				        	 empresa  = enterpriseRepository.findByName("ABC");
					    break;
				        case 10:
				        	 compania = companyRepository.findByName("BCI");
				        	 empresa  = enterpriseRepository.findByName("ABC");
					    break;
				        case 11:
				        	 compania = companyRepository.findByName("BICE");
				        	 empresa  = enterpriseRepository.findByName("ABC");
					    break;
				        case 12:
				        	 compania = companyRepository.findByName("CARDIF");
				        	 empresa  = enterpriseRepository.findByName("ABC");
					    break;

				        }
	        		 
	        		 int x = 0, i = 0;
	        		 for(int k=0; k<5; k++){
					     while(ite.hasNext()){ 
							Row row = ite.next();  
							if(i>7){
								XSSFCell cellRutEmp           = (XSSFCell) row.getCell(0+x);	
								XSSFCell cellRubro            = (XSSFCell) row.getCell(1+x);	
								XSSFCell cellCorrelativo      = (XSSFCell) row.getCell(2+x);	
								XSSFCell cellRutCli           = (XSSFCell) row.getCell(3+x);	
								XSSFCell cellFechaAplicacion  = (XSSFCell) row.getCell(4+x);	
								XSSFCell cellNumeroRemesa     = (XSSFCell) row.getCell(5+x);	
								XSSFCell cellFechaProceso     = (XSSFCell) row.getCell(6+x);	
								XSSFCell cellMonto            = (XSSFCell) row.getCell(7+x);	

							    try{out.println("cellRutEmp.getCellType()!!" + cellRutEmp.getCellType());
							    	if(cellRutEmp.getCellType() == 0){
							    		temp2 = (Integer)(int) cellRutEmp.getNumericCellValue();
							    		valueRutEmp = temp2.toString();
							    	}else{valueRutEmp = cellRutEmp.getStringCellValue();}
							    }catch(IllegalStateException e){out.println("1");}catch(NumberFormatException e){out.println("1"); e.printStackTrace();
								}catch(NullPointerException e){valueRutEmp = "0"; out.println("1-1");}
								try{valueRubro = (Integer)(int) cellRubro.getNumericCellValue();
								}catch(IllegalStateException e){out.println("2");}catch(NumberFormatException e){out.println("2"); e.printStackTrace();
								}catch(NullPointerException e){valueRubro = 0; out.println("2-2");}
								try{valueCorrelativo = (Integer)(int) cellCorrelativo.getNumericCellValue();
								}catch(IllegalStateException e){out.println("3");}catch(NumberFormatException e){out.println("3"); e.printStackTrace();
								}catch(NullPointerException e){valueCorrelativo = 0; out.println("3-3");}
							    try{out.println("cellRutCli.getCellType()!!" + cellRutCli.getCellType());
							    	if(cellRutCli.getCellType() == 0){
							    		temp2 = (Integer)(int) cellRutCli.getNumericCellValue();
							    		valueRutCli = temp2.toString();
							    	}else{valueRutCli = cellRutCli.getStringCellValue();}
							    }catch(IllegalStateException e){out.println("4");}catch(NumberFormatException e){out.println("4-4"); e.printStackTrace();
								}catch(NullPointerException e){valueRutCli = "0"; out.println("4-4");}
							    try{temp2 = (Integer)(int) cellFechaAplicacion.getNumericCellValue();valueFechaAplicacion  = dateFormatter.parse(temp2.toString()); 
							    }catch(IllegalStateException e){out.println("5");}catch(NumberFormatException e){out.println("5"); e.printStackTrace();
							    }catch(NullPointerException e){out.println("5-5"); }
								try{valueNumeroRemesa = (Integer)(int) cellNumeroRemesa.getNumericCellValue();
								}catch(IllegalStateException e){out.println("6");}catch(NumberFormatException e){out.println("6"); e.printStackTrace();
								}catch(NullPointerException e){valueNumeroRemesa = 0; out.println("6-6");}
								try{temp2 = (Integer)(int) cellFechaProceso.getNumericCellValue();valueFechaProceso  = dateFormatter.parse(temp2.toString()); 
								}catch(IllegalStateException e){out.println("7");}catch(NumberFormatException e){out.println("7"); e.printStackTrace();
								}catch(NullPointerException e){out.println("7-7");}
								try{valueMonto = (Integer)(int) cellMonto.getNumericCellValue();
								}catch(IllegalStateException e){out.println("8");}catch(NumberFormatException e){out.println("8"); e.printStackTrace();
								}catch(NullPointerException e){valueMonto = 0; out.println("8-8");}
								
								if(valueRubro != 0) {
									com.ccps.web.model.Collection collection = new com.ccps.web.model.Collection(objFileTypeToSave, 
																												 this.period, 
																												 compania, 
											                                                                     empresa,
											                                                                     valueRutEmp, 
											                                                                     valueRubro, 
											                                                                     valueCorrelativo, 
											                                                                     valueRutCli, 
											                                                                     valueFechaAplicacion,
											                                                                     valueNumeroRemesa, 
											                                                                     valueFechaProceso, 
											                                                                     valueMonto);
									collectionRepository.save(collection);
								}
								//out.println(cellMonto);
								//if(i==20)break;
							}
							i++;
				         }
					   i = 0;
					   x = x + 9;
					   ite = sheet.iterator();
	        		 }
	        	 }
			     fis.close();
			     InsertBinnacle(objFileTypeToSave, 1, this.file.getFileName());
			     CommonMessages.ShowInfoMessage("Info:", "Archivo " + "\"" + this.file.getFileName() + "\"" + " procesado correctamente.");
			     this.file = null;
			     ReadBinnacle();
			     /*####################################:END READ EXCEL:######################################*/
	        }
        }catch (Exception e) {
			CommonMessages.ShowErrorMessage("Error :", "El formato del archivo es incorrecto.");
		}
	}
	
	public void changeCompanyEnterprise(ValueChangeEvent event){
		if(event.getComponent().getClientId().toString().equals("mainForm:company")){
			try
			{
				this.companyId = Integer.parseInt(event.getNewValue().toString());
			}
			catch (Exception ex)
			{
			}
		}else if(event.getComponent().getClientId().toString().equals("mainForm:enterprise")){
			try
			{
				this.enterpriseId = Integer.parseInt(event.getNewValue().toString());
			}
			catch (Exception ex)
			{
			}
		}
	}
	
	public void ChangeIndividualFileType(AjaxBehaviorEvent event)
	{
		if (this.individualfileType.equals("ventas"))
		{
			this.isFileRecaudacion = false;
		}
		else
		{
			this.isFileRecaudacion = true;
			this.companyId = 0;
			this.enterpriseId = 0;
		}
	}
	
	public void InsertBinnacle(FileType fileType, Integer binType, String fileName){

		this.periodList = periodRepository.findByYearMonthType(this.year, this.month, fileType.getId());
    		 
		Binnacle binnacle = new Binnacle();
		java.util.Date fecha = new Date();
		binnacle.setBinDate(fecha);
		binnacle.setFileType(fileType);
		binnacle.setBinType(binType);
		User userSession = (User) getCurrentInstance().getExternalContext().getSessionMap().get("user"); 
		binnacle.setUser(userSession);
		binnacle.setPeriod(periodList.get(0));
		binnacle.setDetails2(fileName);
		binnacleRepository.save(binnacle);
	}
	
    public void ReadBinnacle(){
    	try{
    		if (binnacles == null)
    			binnacles = new ArrayList<Binnacle>();
    		
    		binnacles.clear();
    		
    		for (FileType ft : fileTypesVentas)
			{
    			List<Binnacle> colBin = binnacleRepository.findLastByTypeAndPeriod(this.year, this.month, ft.getId(), 1);
    			if (colBin.size() > 0)
        			binnacles.add(colBin.get(0));
			}
			
			for (FileType ft : fileTypesRemesa)
			{
				List<Binnacle> colBin = binnacleRepository.findLastByTypeAndPeriod(this.year, this.month, ft.getId(), 1);
    			if (colBin.size() > 0)
        			binnacles.add(colBin.get(0));
			}
    	}catch(Exception e){
    		e.printStackTrace();
    	}
	}
    
    public Boolean ValidateFormVentas()
	{
		if (this.companyId <= 0)
		{
			CommonMessages.ShowWarningMessage("Datos Incompletos", "Debe seleccionar una compañía.");
			return false;
		}
		
		if (this.enterpriseId <= 0)
		{
			CommonMessages.ShowWarningMessage("Datos Incompletos", "Debe seleccionar una empresa.");
			return false;
		}
		
		if (this.file == null)
		{
			CommonMessages.ShowWarningMessage("Datos Incompletos", "Debe seleccionar un archivo a cargar.");
			return false;
		}
		
		return true;
	}
    
    public Boolean ValidateFormRemesas()
	{
		if (this.file == null)
		{
			CommonMessages.ShowWarningMessage("Datos Incompletos", "Debe seleccionar un archivo a cargar.");
			return false;
		}
		
		return true;
	}
    
    public Boolean ValidateIndividualFileType()
	{
		if (this.individualfileType.isEmpty())
		{
			CommonMessages.ShowWarningMessage("Datos Incompletos", "Debe seleccionar el tipo de archivo que desea procesar.");
			return false;
		}
		
		return true;
	}
    
    public void RollBackClosePeriod() {
		
		//Elimino los periodos actuales
		for (FileType ft : fileTypesVentas) {
			List<Period> colActualPeriods = periodRepository.findByYearMonthType(this.year, this.month, ft.getId());
			
			for (Period p : colActualPeriods) {
				periodRepository.delete(p);
			}
		}
		for (FileType ft : this.fileTypesRemesa) {
			List<Period> colActualPeriods = periodRepository.findByYearMonthType(this.year, this.month, ft.getId());
			
			for (Period p : colActualPeriods) {
				periodRepository.delete(p);
			}
		}
		
		//Cambio estado de cierre de periodo anterior
		for (FileType ft : fileTypesVentas) {
			int maxYear = periodRepository.findMaxYearClosedPeriodsByType(ft.getId());
			int maxMonth = 0;
			
			List<Period> colPeriods = periodRepository.findAllClosedPeriodsByYearType(maxYear, ft.getId());
			
			for (Period period:colPeriods){
				if (period.getMes() > maxMonth)
					maxMonth = period.getMes();
			}
			
			colPeriods = periodRepository.findByYearMonthType(maxYear, maxMonth, ft.getId());
			
			for (Period p : colPeriods) {
				p.setCerrado(false);
				periodRepository.save(p);
			}
			
			List<Binnacle> colBinFT = binnacleRepository.findByTypeAndAllPeriods(maxYear, maxMonth, ft.getId(), 4);
			
			for (Binnacle b : colBinFT) {
				binnacleRepository.delete(b);
			}
			
			FileType ftRep = fileTypeRepository.findByType("ReporteIndividual-BCI");
			
			List<Binnacle> colBinFTRep = binnacleRepository.findByTypeAndAllPeriods(maxYear, maxMonth, ftRep.getId(), 4);
			
			for (Binnacle b : colBinFTRep) {
				binnacleRepository.delete(b);
			}
			
			colBinFTRep = binnacleRepository.findByTypeAndAllPeriods(maxYear, maxMonth, ftRep.getId(), 2);
			
			for (Binnacle b : colBinFTRep) {
				binnacleRepository.delete(b);
			}
			
			ftRep = fileTypeRepository.findByType("ReporteIndividual-OHIO");
			
			colBinFTRep = binnacleRepository.findByTypeAndAllPeriods(maxYear, maxMonth, ftRep.getId(), 4);
			
			for (Binnacle b : colBinFTRep) {
				binnacleRepository.delete(b);
			}
			
			colBinFTRep = binnacleRepository.findByTypeAndAllPeriods(maxYear, maxMonth, ftRep.getId(), 2);
			
			for (Binnacle b : colBinFTRep) {
				binnacleRepository.delete(b);
			}
			
			ftRep = fileTypeRepository.findByType("ReporteIndividual-BICE");
			
			colBinFTRep = binnacleRepository.findByTypeAndAllPeriods(maxYear, maxMonth, ftRep.getId(), 4);
			
			for (Binnacle b : colBinFTRep) {
				binnacleRepository.delete(b);
			}
			
			colBinFTRep = binnacleRepository.findByTypeAndAllPeriods(maxYear, maxMonth, ftRep.getId(), 2);
			
			for (Binnacle b : colBinFTRep) {
				binnacleRepository.delete(b);
			}
			
			ftRep = fileTypeRepository.findByType("ReporteIndividual-ISE");
			
			colBinFTRep = binnacleRepository.findByTypeAndAllPeriods(maxYear, maxMonth, ftRep.getId(), 4);
			
			for (Binnacle b : colBinFTRep) {
				binnacleRepository.delete(b);
			}
			
			colBinFTRep = binnacleRepository.findByTypeAndAllPeriods(maxYear, maxMonth, ftRep.getId(), 2);
			
			for (Binnacle b : colBinFTRep) {
				binnacleRepository.delete(b);
			}
			
			ftRep = fileTypeRepository.findByType("ReporteIndividual-CARD");
			
			colBinFTRep = binnacleRepository.findByTypeAndAllPeriods(maxYear, maxMonth, ftRep.getId(), 4);
			
			for (Binnacle b : colBinFTRep) {
				binnacleRepository.delete(b);
			}
			
			colBinFTRep = binnacleRepository.findByTypeAndAllPeriods(maxYear, maxMonth, ftRep.getId(), 2);
			
			for (Binnacle b : colBinFTRep) {
				binnacleRepository.delete(b);
			}
			
			ftRep = fileTypeRepository.findByType("ReporteIndividual-METLIFE");
			
			colBinFTRep = binnacleRepository.findByTypeAndAllPeriods(maxYear, maxMonth, ftRep.getId(), 4);
			
			for (Binnacle b : colBinFTRep) {
				binnacleRepository.delete(b);
			}
			
			colBinFTRep = binnacleRepository.findByTypeAndAllPeriods(maxYear, maxMonth, ftRep.getId(), 2);
			
			for (Binnacle b : colBinFTRep) {
				binnacleRepository.delete(b);
			}
		}
		
		for (FileType ft : this.fileTypesRemesa) {
			int maxYear = periodRepository.findMaxYearClosedPeriodsByType(ft.getId());
			int maxMonth = 0;
			
			List<Period> colPeriods = periodRepository.findAllClosedPeriodsByYearType(maxYear, ft.getId());
			
			for (Period period:colPeriods){
				if (period.getMes() > maxMonth)
					maxMonth = period.getMes();
			}
			
			colPeriods = periodRepository.findByYearMonthType(maxYear, maxMonth, ft.getId());
			
			for (Period p : colPeriods) {
				p.setCerrado(false);
				periodRepository.save(p);
			}
			
			List<Binnacle> colBinFT = binnacleRepository.findByTypeAndAllPeriods(maxYear, maxMonth, ft.getId(), 2);
			
			for (Binnacle b : colBinFT) {
				binnacleRepository.delete(b);
			}
			
			colBinFT = binnacleRepository.findByTypeAndAllPeriods(maxYear, maxMonth, ft.getId(), 4);
			
			for (Binnacle b : colBinFT) {
				binnacleRepository.delete(b);
			}
		}
		
		InitPeriod();
		ReadBinnacle();
		getCanClosePeriod();
		getCanRollbackPeriod();
		
		CommonMessages.ShowInfoMessage("Info:", "Se ha realizado el Rollback del cierre de periodo correctamente.");
		
		RequestContext context = RequestContext.getCurrentInstance();
		context.execute("$('#dialogConfirmRollback').hide();");
	}
}
