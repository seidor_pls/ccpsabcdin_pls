package com.ccps.web.bean;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import javax.activation.MimetypesFileTypeMap;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.apache.commons.codec.Charsets;
import org.apache.commons.io.IOUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.ccps.web.common.CommonMessages;
import com.ccps.web.common.CommonReporting;
import com.ccps.web.model.Binnacle;
import com.ccps.web.model.CommissionPlan;
import com.ccps.web.model.Company;
import com.ccps.web.model.Enterprise;
import com.ccps.web.model.FileType;
import com.ccps.web.model.FileTypeCompany;
import com.ccps.web.model.Period;
import com.ccps.web.model.User;
import com.ccps.web.model.Warranty;
import com.ccps.web.model.repository.BinnacleRepository;
import com.ccps.web.model.repository.CommissionPlanRepository;
import com.ccps.web.model.repository.CommissionRepository;
import com.ccps.web.model.repository.EnterpriseRepository;
import com.ccps.web.model.repository.FileTypeCompanyRepository;
import com.ccps.web.model.repository.FileTypeRepository;
import com.ccps.web.model.repository.PeriodRepository;
import com.ccps.web.model.repository.WarrantyRepository;

import static javax.faces.context.FacesContext.getCurrentInstance;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;

/**
 * author
 * plopezs
 */

@ManagedBean(name="warrantyBean")
@SessionScoped
@Component(value="warrantyBean")
@Scope("session")
public class WarrantyBean implements Serializable{
	private static final long serialVersionUID = 6669156456591022392L;
	
	private Boolean error = false;
	
	@Autowired
	private WarrantyRepository warrantyRepository;
	
	@Autowired
	private FileTypeRepository fileTypeRepository;
	
	@Autowired
	private PeriodRepository periodRepository;
	
	@Autowired
	private EnterpriseRepository enterpriseRepository;
	
	@Autowired
	private BinnacleRepository binnacleRepository;
	
	@Autowired
	private FileTypeCompanyRepository fileTypeCompanyRepository;
	
	@Autowired
	private CommissionRepository commissionRepository;
	
	@Autowired
	private CommissionPlanRepository commissionPlanRepository;
	
	private UploadedFile file;
	
	private StreamedContent csvFile;
	
	private int closingYear = 0;
	private int closingMonth = 0;
	private String tipoArchivo = "";
	private Integer year = 0;
	private Integer month = 0;
	private String strMonth = "";
	
	private Date fecInicio;
	private Date fecFin;
	
	private Map<Integer, String> colYears;
	private Map<Integer, String> colMonths;
	
	private List<Binnacle> binnacles;
	
	List<FileType> fileTypesGP;
	List<FileType> ftGPReports;

	//PROPERTIES
	public StreamedContent getCsvFile() {
        return csvFile;
    }
	
	public Boolean getError() {
		return error;
	}	

	public void setError(Boolean error) {
		this.error = error;
	}

	public int getClosingYear() {
		return closingYear;
	}

	public void setClosingYear(int closingYear) {
		this.closingYear = closingYear;
	}

	public int getClosingMonth() {
		return closingMonth;
	}

	public void setClosingMonth(int closingMonth) {
		this.closingMonth = closingMonth;
	}

	public Date getFecInicio() {
		return fecInicio;
	}

	public void setFecInicio(Date fecInicioRobo) {
		this.fecInicio = fecInicioRobo;
	}

	public Date getFecFin() {
		return fecFin;
	}

	public void setFecFin(Date fecFin) {
		this.fecFin = fecFin;
	}

	public UploadedFile getFile() {
		return file;
	}

	public void setFile(UploadedFile file) {
		this.file = file;
	}

	public String getTipoArchivo() {
		return tipoArchivo;
	}

	public void setTipoArchivo(String tipoArchivo) {
		this.tipoArchivo = tipoArchivo;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		this.month = month;
	}

	public String getStrMonth() {
		return strMonth;
	}

	public void setStrMonth(String strMonth) {
		this.strMonth = strMonth;
	}

	public Map<Integer, String> getColYears() {
		return colYears;
	}

	public void setColYears(Map<Integer, String> colYears) {
		this.colYears = colYears;
	}

	public Map<Integer, String> getColMonths() {
		return colMonths;
	}

	public void setColMonths(Map<Integer, String> colMonths) {
		this.colMonths = colMonths;
	}
	
	public List<Binnacle> getBinnacles() {
		return binnacles;
	}

	public void setBinnacles(List<Binnacle> binnacles) {
		this.binnacles = binnacles;
	}
	
	public Boolean getCanClosePeriod() {
		
		Calendar calendar = Calendar.getInstance();
		calendar.clear();
		calendar.set(this.year, this.month - 1, 1);
		Integer maxDay = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
				
		//System.out.println("CantTotalDiasMES: " + maxDay.toString());
		
		String strMonth = "";
		String strYear = this.year.toString().substring(2,4);
		
		if (this.month > 9)
			strMonth = this.month.toString();
		else
			strMonth = "0" + this.month.toString();
		
		String strDateReplace = "/" + strMonth + "/" + strYear + " 00:00:00,000000";
		
		//Verifica que la cantidad de días ingresados en garantías correspondan a los N días del Mes del periodo.
		for (FileType ft : fileTypesGP) {
			
			List<String> colDays = warrantyRepository.findDaysIniFinByPeriod(this.month, this.year, ft.getId());
			Integer cantDias = 0;
			
			for (String d : colDays) {
				String dayOnly = d.replace(strDateReplace, "");
				String[] daySpan = dayOnly.split("&");
				
				if (daySpan.length > 1) {
					cantDias += (Integer.parseInt(daySpan[1]) - Integer.parseInt(daySpan[0])) + 1;
				}
				
				//System.out.println("CantDias: " + cantDias.toString());
			}
			
			//Si no se han cubierto todos los días del mes, éste no se puede cerrar.
			if (cantDias < maxDay)
				return false;
		}
		
		//Busca Garantías con Errores
		List<Warranty> colWarranty = new ArrayList<Warranty>();
		
		for (FileType ft : fileTypesGP) {
			colWarranty.addAll(warrantyRepository.findByPeriodStatus(this.month, this.year, ft.getId(), "E"));
		}
		
		//Si hay errores, no se puede cerrar mes
		if (colWarranty.size() > 0)
			return false;
		
		return true;
	}
	
	public Boolean getCanRollbackPeriod() {
		if (this.binnacles.size() > 0)
				return false;
		
		return true;
	}

	public List<FileType> getFileTypesGP() {
		return fileTypesGP;
	}

	public void setFileTypesGP(List<FileType> fileTypesGP) {
		this.fileTypesGP = fileTypesGP;
	}

	//METHODS
	@PostConstruct
	public void PageLoad(){		
		InitYearsMonths();
		InitPeriod();
		ReadBinnacle();
	}
	
	public void InitYearsMonths()
	{
		try
		{
			Date today = new Date();
			
			this.colYears = new HashMap<Integer, String>();
			this.colMonths = new HashMap<Integer, String>();
			
			//Agrega el año actual y los 4 siguientes.
			for (Integer i = today.getYear(); i < today.getYear() + 5; i++)
			{
				this.colYears.put(i, i.toString());
			}
			
			this.colMonths.put(1, "Enero");
			this.colMonths.put(2, "Febrero");
			this.colMonths.put(3, "Marzo");
			this.colMonths.put(4, "Abril");
			this.colMonths.put(5, "Mayo");
			this.colMonths.put(6, "Junio");
			this.colMonths.put(7, "Julio");
			this.colMonths.put(8, "Agosto");
			this.colMonths.put(9, "Septiembre");
			this.colMonths.put(10, "Octubre");
			this.colMonths.put(11, "Noviembre");
			this.colMonths.put(12, "Diciembre");
		}
		catch (Exception e) 
		{
			CommonMessages.ShowErrorMessage("Error:", "No fue posible inicializar los meses y años. " + e.getMessage());
	    }
	}
	
	public void InitPeriod(){
		try
		{
			this.fileTypesGP = fileTypeRepository.findByCategory("gplus");
			this.ftGPReports = fileTypeRepository.findByCategory("reporteGP");
			
			if (this.fileTypesGP.size() > 0) {
				int maxYear = periodRepository.findMaxYearClosedPeriodsByType(this.fileTypesGP.get(0).getId());
				int maxMonth = 0;
				
				List<Period> colPeriods = periodRepository.findAllClosedPeriodsByYearType(maxYear, this.fileTypesGP.get(0).getId());
				
				for (Period period:colPeriods){
					if (period.getMes() > maxMonth)
						maxMonth = period.getMes();
				}
				
				if (maxMonth < 12) {
					maxMonth++;
				}
				else {
					maxYear++;
					maxMonth = 1;
				}
				
				colPeriods = periodRepository.findByYearMonthType(maxYear, maxMonth, this.fileTypesGP.get(0).getId());
				
				//Si no existe creo un nuevo periodo
				if (colPeriods.size() <= 0)
				{
					for (FileType ft : fileTypesGP)
					{
						Period periodo = new Period(ft, maxMonth, maxYear, false);
						periodRepository.save(periodo);
					}
					
					for (FileType ft : ftGPReports)
					{
						Period periodo = new Period(ft, maxMonth, maxYear, false);
						periodRepository.save(periodo);
					}
				}
				
				this.month = maxMonth;
				this.year = maxYear;
				this.strMonth = this.colMonths.get(maxMonth);
			}
		}
		catch (Exception e)
		{
			CommonMessages.ShowErrorMessage("Error:", "No fue posible inicializar el periodo. " + e.getMessage());
	    }
	}
	
	public void HandleFileUpload(FileUploadEvent event) {
		this.file = event.getFile();
	}
	
	public void Upload(){
		RequestContext context = RequestContext.getCurrentInstance();
		
		try
		{
			FileType fileType = new FileType();
			
			if (!ValidateForm(false))
			{
				context.execute("PF('statusDialog').hide();");
				return;
			}
			
			for (FileType ft : this.fileTypesGP)
			{
				if (this.tipoArchivo.equals(ft.getType()))
				{
					fileType = ft;
					break;
				}
			}
			
			//VALIDA SI YA SE HAN CARGADO DATOS EN EL PERIODO DE FECHAS
			this.fecInicio.setHours(0);
			this.fecInicio.setMinutes(0);
			this.fecInicio.setSeconds(0);
			
			this.fecFin.setHours(0);
			this.fecFin.setMinutes(0);
			this.fecFin.setSeconds(0);
			
			List<Warranty> colGuardadasPeriodo = warrantyRepository.findByDateRange(this.fecInicio, this.fecFin, fileType.getId());
			
			//Existen datos ya cargados para el periodo de fechas.
			if (colGuardadasPeriodo.size() > 0)
			{
				context.execute("PF('statusDialog').hide();");
				context.execute("$('#dialogConfirmRecarga').show();");
				return;
			}
			
			List<Period> colPeriods = periodRepository.findAllOpenPeriodsByMonthYearType(this.month, this.year, fileType.getId());
			
			List<Warranty> colGarantias = ReadGarantiasFromCSV(this.file.getInputstream(), this.fecInicio, this.fecFin, fileType, colPeriods.get(0));
				
			SaveWarranties(colGarantias, fileType);
			
			CommonMessages.ShowInfoMessage("Info:", "Archivo procesado correctamente.");
			
			this.file = null;
		}
		catch (IOException e) 
		{
			CommonMessages.ShowErrorMessage("Error:", "No fue posible leer el archivo. Es probable que se encuentre bloqueado por otro proceso, o fue eliminado.");
			this.file = null;
	    }
		catch (Exception e) 
		{
			CommonMessages.ShowErrorMessage("Error:", "No se pudo procesar archivo. " + e.getMessage());
			this.file = null;
	    }
		finally {
			context.execute("PF('statusDialog').hide();");
		}
	}
	
	public void AcceptReload(){
		RequestContext context = RequestContext.getCurrentInstance();
		
		try
		{
			FileType fileType = new FileType();
			
			for (FileType ft : this.fileTypesGP)
			{
				if (this.tipoArchivo.equals(ft.getType()))
				{
					fileType = ft;
					break;
				}
			}
			
			//OBTIENE EL ARCHIVO CARGADO Y BORRA SUS REGISTROS
			List<Warranty> colWarranty = warrantyRepository.findByDateRange(this.fecInicio, this.fecFin, fileType.getId());
			
			for (Warranty war:colWarranty)
			{
				warrantyRepository.delete(war);
			}
			
			List<Period> colPeriods = periodRepository.findAllOpenPeriodsByMonthYearType(this.month, this.year, fileType.getId());
			
			List<Warranty> colGarantias = ReadGarantiasFromCSV(this.file.getInputstream(), this.fecInicio, this.fecFin, fileType, colPeriods.get(0));
			
			SaveWarranties(colGarantias, fileType);
			
			CommonMessages.ShowInfoMessage("Info:", "Archivo procesado correctamente.");
			
			context.execute("$('#dialogConfirmRecarga').hide();");
			context.execute("PF('statusDialog').hide();");
		}
		catch (IOException e) 
		{
			CommonMessages.ShowErrorMessage("Error:", "No fue posible leer el archivo. Es probable que se encuentre bloqueado por otro proceso, o fue eliminado.");
	    }
		catch (Exception e) 
		{
			CommonMessages.ShowErrorMessage("Error:", "No fue posible leer el archivo. " + e.getMessage());
	    }
		finally {
			context.execute("PF('statusDialog').hide();");
			this.file = null;
		}
	}
	
	public String GetGPRowValidationStatus(String[] row)
	{
		int precioVenta = Integer.parseInt(row[17].replace(".", ""));
		String glosaArticulo = row[21];
		
		List<String> glosasCelulares = new ArrayList<String>();
		
		glosasCelulares.add("CELULAR");
		glosasCelulares.add("LG L1 E4");
		glosasCelulares.add("CEL SAM");
		glosasCelulares.add("CONTR MO");
		glosasCelulares.add("CONTR ENT");
		glosasCelulares.add("CONTRATO");
		glosasCelulares.add("CEL ALCA");
		glosasCelulares.add("TELEFONI");
		glosasCelulares.add("CELUL SA");
		glosasCelulares.add("TELEFONO");
		glosasCelulares.add("CONT MOV");
		glosasCelulares.add("CEL SON");
		glosasCelulares.add("CEL SAMS");
		glosasCelulares.add("APPLE IPH");
		glosasCelulares.add("ALCATEL");
		glosasCelulares.add("PLAN ENT");
		glosasCelulares.add("PLAN MOV");
		glosasCelulares.add("MI PRIME");
		
		if (this.tipoArchivo.equals("GarantiaCelular"))
		{
			if (precioVenta < 1000 || precioVenta > 2000000)
			{
				return "E";
			}
			
			if (glosaArticulo.trim().isEmpty())
			{
				return "E";
			}
			
			if (glosaArticulo.trim().length() < 2)
			{
				return "E";
			}
			
			if (glosaArticulo.trim().toUpperCase().equals("NO DEFINIDO"))
			{
				return "E";
			}
			
			for (String glosa : glosasCelulares)
			{
				if (glosaArticulo.trim().toUpperCase().indexOf(glosa) == 0)
				{
					return "I";
				}
			}
			
			return "E";
		}
		else if(this.tipoArchivo.equals("GarantiaRobo"))
		{
			if (precioVenta < 1000 || precioVenta > 5000000)
			{
				return "E";
			}
			
			if (glosaArticulo.trim().isEmpty())
			{
				return "E";
			}
			
			if (glosaArticulo.trim().length() < 2)
			{
				return "E";
			}
			
			if (glosaArticulo.trim().toUpperCase().equals("NO DEFINIDO"))
			{
				return "E";
			}
			
			for (String glosa : glosasCelulares)
			{
				if (glosaArticulo.trim().toUpperCase().indexOf(glosa) == 0)
				{
					return "E";
				}
			}
		}
		
		return "I";
	}
	
	public List<Warranty> ReadGarantiasFromCSV(InputStream input, Date fecInicio, Date fecFin, FileType type, Period period) throws Exception {
		
		List<Warranty> colGarantias = new ArrayList();
		
		try
		{
	    	Scanner scanner = new Scanner(input);
	    	
	    	Integer lineNum = 0;
	    	String line = scanner.nextLine();
	    	
	    	while (scanner.hasNextLine()) {
	    		lineNum++;
	    		
	    		line = scanner.nextLine();
	    		
	    		String[] attributes = line.split(";");
	    		
	    		Warranty garantia;
				
				try
				{
					if (attributes.length > 0)
					{
						String status = GetGPRowValidationStatus(attributes);
						
						if (attributes[1].trim().toUpperCase().equals("ABC"))
						{
							//ABC
							Enterprise empresa = enterpriseRepository.findOne(1);
							garantia = new Warranty(attributes, fecInicio, fecFin, type, period, empresa, status);
							colGarantias.add(garantia);
						}
						
						if (attributes[1].trim().toUpperCase().equals("DIN"))
						{
							//DIN
							Enterprise empresa = enterpriseRepository.findOne(2);
							garantia = new Warranty(attributes, fecInicio, fecFin, type, period, empresa, status);
							colGarantias.add(garantia);
						}
					}
				}
				catch (Exception e)
				{
					scanner.close();
					throw new Exception("Columna o valor no esperado en linea " + lineNum + ": " + e.getMessage() + ".");
				}
	    	}
	    	
	    	scanner.close();
	    }
		catch (Exception e) {
			throw new Exception("No se pudo analizar el archivo. El formato del mismo es incorrecto.");
	    }
		
		return colGarantias; 
	}

	public void CloseMonth() {
		try
		{
			for (FileType ft : fileTypesGP)
			{
				List<Period> colPeriods = periodRepository.findByYearMonthType(this.year, this.month, ft.getId());
				
				for (Period per: colPeriods){
					per.setCerrado(true);
					periodRepository.save(per);
					InsertBinnacle(ft, 4, "Garantías Plus: Cierre de Mes");
				}
			}
			
			for (FileType ft : this.ftGPReports)
			{
				List<Period> colPeriods = periodRepository.findByYearMonthType(this.year, this.month, ft.getId());
				
				for (Period per: colPeriods){
					per.setCerrado(true);
					periodRepository.save(per);
					InsertBinnacle(ft, 4, "Garantías Plus: Cierre de Mes");
				}
			}
			
			InitPeriod();
			ReadBinnacle();
			getCanClosePeriod();
			
			CommonMessages.ShowInfoMessage("Info:", "Mes cerrado correctamente.");
			
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("$('#dialogConfirmCierre').hide();");
		}
		catch (Exception e) 
		{
			CommonMessages.ShowErrorMessage("Error:", "No fue posible cerrar el mes. " + e.getMessage());
	    }
	}

	public void SaveWarranties(List<Warranty> colGarantias, FileType ft){
		List<FileTypeCompany> ftcColl = fileTypeCompanyRepository.findByFileType(ft.getId());
		
		for(Warranty war: colGarantias)
		{
			if (ftcColl.size() > 0)
			{
				war.setCompany(ftcColl.get(0).getCompany());
			}
			
			warrantyRepository.save(war);
		}
		
		InsertBinnacle(ft, 1, this.file.getFileName());
		ReadBinnacle();
	}

	public Boolean ValidateForm(Boolean csv)
	{
		Calendar calendar = Calendar.getInstance();
		calendar.clear();
		calendar.set(this.year, this.month - 1, 1);
		Date periodInitDate = calendar.getTime();
		periodInitDate.setHours(0);
		periodInitDate.setMinutes(0);
		periodInitDate.setSeconds(0);
		Integer maxDay = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
		calendar.clear();
		calendar.set(this.year, this.month - 1, maxDay);
		Date periodEndDate = calendar.getTime();
		
		if (this.tipoArchivo.trim().equals(""))
		{
			CommonMessages.ShowWarningMessage("Datos Incompletos", "Debe seleccionar un tipo de archivo.");
			return false;
		}
		
		if (this.fecInicio == null)
		{
			CommonMessages.ShowWarningMessage("Datos Incompletos", "Debe seleccionar una fecha de inicio.");
			return false;
		}
		else {
			this.fecInicio.setHours(1);
			this.fecInicio.setMinutes(1);
			this.fecInicio.setSeconds(1);
		}
		
		if (this.fecFin == null)
		{
			CommonMessages.ShowWarningMessage("Datos Incompletos", "Debe seleccionar una fecha de fin.");
			return false;
		}
		
		if (this.fecFin.equals(this.fecInicio))
		{
			CommonMessages.ShowErrorMessage("Datos Incorrectos", "La fecha de inicio y la fecha de fin deben ser distintas.");
			return false;
		}
		
		if (this.fecFin.before(this.fecInicio))
		{
			CommonMessages.ShowErrorMessage("Datos Incorrectos", "La fecha de inicio debe ser anterior a la fecha de fin.");
			return false;
		}
		
		if (this.fecInicio.before(periodInitDate))
		{
			CommonMessages.ShowErrorMessage("Datos Incorrectos", "La fecha de inicio seleccionada corresponde a un periodo ya cerrado.");
			return false;
		}
		
		if (this.fecInicio.after(periodEndDate))
		{
			CommonMessages.ShowErrorMessage("Datos Incorrectos", "La fecha de inicio seleccionada corresponde a un periodo aún no abierto.");
			return false;
		}
		
		if (this.fecFin.before(periodInitDate))
		{
			CommonMessages.ShowErrorMessage("Datos Incorrectos", "La fecha de fin seleccionada corresponde a un periodo ya cerrado.");
			return false;
		}
		
		if (this.fecFin.after(periodEndDate))
		{
			CommonMessages.ShowErrorMessage("Datos Incorrectos", "La fecha de fin seleccionada corresponde a un periodo aún no abierto.");
			return false;
		}
		
		if (!csv && this.file == null)
		{
			CommonMessages.ShowWarningMessage("Datos Incompletos", "Debe seleccionar un archivo a cargar.");
			return false;
		}
		
		return true;
	}

	public void InsertBinnacle(FileType fileType, Integer binType, String fileName){
		
		List<Period> colPeriods = periodRepository.findByYearMonthType(this.year, this.month, fileType.getId());
		
		if (colPeriods.size() > 0)
		{
			Binnacle binnacle = new Binnacle();
			java.util.Date fecha = new Date();
			binnacle.setBinDate(fecha);
			binnacle.setFileType(fileType);
			binnacle.setBinType(binType);
			User userSession = (User) getCurrentInstance().getExternalContext().getSessionMap().get("user"); 
			binnacle.setUser(userSession);
			binnacle.setPeriod(colPeriods.get(0));
			binnacle.setDetails2(fileName);
			binnacleRepository.save(binnacle);
		}
	}
	
	public void ReadBinnacle(){
    	try{
    		if (binnacles == null)
    			binnacles = new ArrayList<Binnacle>();
    		
    		binnacles.clear();
    		
    		for (FileType ft : fileTypesGP)
			{
    			List<Binnacle> colBin = binnacleRepository.findByTypeAndPeriod(this.year, this.month, ft.getId(), ft.getId(), ft.getId(), 1);
    			
    			for (Binnacle b : colBin) {
        			binnacles.add(b);
    			}
			}
    	}catch(Exception e){
    		e.printStackTrace();
    	}
	}
	
	public void RollBackClosePeriod() {
		
		//Elimino los periodos actuales
		for (FileType ft : fileTypesGP) {
			List<Period> colActualPeriods = periodRepository.findByYearMonthType(this.year, this.month, ft.getId());
			
			for (Period p : colActualPeriods) {
				periodRepository.delete(p);
			}
		}
		for (FileType ft : this.ftGPReports) {
			List<Period> colActualPeriods = periodRepository.findByYearMonthType(this.year, this.month, ft.getId());
			
			for (Period p : colActualPeriods) {
				periodRepository.delete(p);
			}
		}
		
		//Cambio estado de cierre de periodo anterior
		for (FileType ft : fileTypesGP) {
			int maxYear = periodRepository.findMaxYearClosedPeriodsByType(ft.getId());
			int maxMonth = 0;
			
			List<Period> colPeriods = periodRepository.findAllClosedPeriodsByYearType(maxYear, ft.getId());
			
			for (Period period:colPeriods){
				if (period.getMes() > maxMonth)
					maxMonth = period.getMes();
			}
			
			colPeriods = periodRepository.findByYearMonthType(maxYear, maxMonth, ft.getId());
			
			for (Period p : colPeriods) {
				p.setCerrado(false);
				periodRepository.save(p);
			}
			
			List<Binnacle> colBinFT = binnacleRepository.findByTypeAndAllPeriods(maxYear, maxMonth, ft.getId(), 4);
			
			for (Binnacle b : colBinFT) {
				binnacleRepository.delete(b);
			}
			
			FileType ftRep = fileTypeRepository.findByType("Reporte-GarantiaRobo");
			
			List<Binnacle> colBinFTRep = binnacleRepository.findByTypeAndAllPeriods(maxYear, maxMonth, ftRep.getId(), 4);
			
			for (Binnacle b : colBinFTRep) {
				binnacleRepository.delete(b);
			}
			
			colBinFTRep = binnacleRepository.findByTypeAndAllPeriods(maxYear, maxMonth, ftRep.getId(), 2);
			
			for (Binnacle b : colBinFTRep) {
				binnacleRepository.delete(b);
			}
			
			ftRep = fileTypeRepository.findByType("Reporte-GarantiaCelular");
			
			colBinFTRep = binnacleRepository.findByTypeAndAllPeriods(maxYear, maxMonth, ftRep.getId(), 4);
			
			for (Binnacle b : colBinFTRep) {
				binnacleRepository.delete(b);
			}
			
			colBinFTRep = binnacleRepository.findByTypeAndAllPeriods(maxYear, maxMonth, ftRep.getId(), 2);
			
			for (Binnacle b : colBinFTRep) {
				binnacleRepository.delete(b);
			}
		}
		
		for (FileType ft : this.ftGPReports) {
			int maxYear = periodRepository.findMaxYearClosedPeriodsByType(ft.getId());
			int maxMonth = 0;
			
			List<Period> colPeriods = periodRepository.findAllClosedPeriodsByYearType(maxYear, ft.getId());
			
			for (Period period:colPeriods){
				if (period.getMes() > maxMonth)
					maxMonth = period.getMes();
			}
			
			colPeriods = periodRepository.findByYearMonthType(maxYear, maxMonth, ft.getId());
			
			for (Period p : colPeriods) {
				p.setCerrado(false);
				periodRepository.save(p);
			}
			
			List<Binnacle> colBinFT = binnacleRepository.findByTypeAndPeriod(maxYear, maxMonth, ft.getId(), ft.getId(), ft.getId(), 4);
			
			for (Binnacle b : colBinFT) {
				binnacleRepository.delete(b);
			}
			
			colBinFT = binnacleRepository.findByTypeAndPeriod(maxYear, maxMonth, ft.getId(), ft.getId(), ft.getId(), 2);
			
			for (Binnacle b : colBinFT) {
				binnacleRepository.delete(b);
			}
		}
		
		InitPeriod();
		ReadBinnacle();
		getCanClosePeriod();
		getCanRollbackPeriod();
		
		CommonMessages.ShowInfoMessage("Info:", "Se ha realizado el Rollback del cierre de periodo correctamente.");
		
		RequestContext context = RequestContext.getCurrentInstance();
		context.execute("$('#dialogConfirmRollback').hide();");
	}
	
	public void HandleGenerateCSVButton(){
		if (ValidateForm(true))
			GenerateCSVFile();
	}
	
	public void GenerateCSVFile() {
		try {
			RequestContext context = RequestContext.getCurrentInstance();
			
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			List<String> lines = GetGPlusLines();
			IOUtils.writeLines(lines, "\r\n", out, Charsets.UTF_8.displayName());
			InputStream in = new ByteArrayInputStream(out.toByteArray());
			
			DateFormat df = new SimpleDateFormat("ddMMyy");
			
			String fileName = "";

			if (this.tipoArchivo.equals("GarantiaRobo"))
				fileName = "PDCCION" +  df.format(this.fecInicio) + "AL" + df.format(this.fecFin) + ".txt";
			else
				fileName = "CPPDCCION" +  df.format(this.fecInicio) + "AL" + df.format(this.fecFin) + ".txt";
			
			csvFile = new DefaultStreamedContent(in, "text/plain", fileName, Charsets.UTF_8.name());
	        
	        if (csvFile != null)
	        {
	        	context.execute("$('#divDownloadReport').show();");
	        }
		} catch (IOException e) {
			CommonMessages.ShowErrorMessage("Error:", "No fue posible generar el archivo.");
            e.printStackTrace();
        }
	}
	
	public List<String> GetGPlusLines(){
		List<String> lines = new ArrayList<String>();
		
		FileType fileType = new FileType();

		for (FileType ft : this.fileTypesGP)
		{
			if (this.tipoArchivo.equals(ft.getType()))
			{
				fileType = ft;
				break;
			}
		}
		
		List<Warranty> colWarranty = warrantyRepository.findByDateRange(this.fecInicio, this.fecFin, fileType.getId());
		
		StringBuilder sbLine = new StringBuilder();
		
		sbLine.append("Folio;");
		sbLine.append("RutCliente;");
		sbLine.append("Ap Paterno;");
		sbLine.append("Ap Materno;");
		sbLine.append("Nombres;");
		sbLine.append("Dirección;");
		sbLine.append("Comuna;");
		sbLine.append("Ciudad;");
		sbLine.append("Teléfono;");
		sbLine.append("Cod Suc;");
		sbLine.append("Glosa Sucursal;");
		sbLine.append("Fch Vta Gtia;");
		sbLine.append("Precio Venta;");
		sbLine.append("Tipo;");
		sbLine.append("Prima;");
		sbLine.append("Gls Articulo");
		
		lines.add(sbLine.toString());
		
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		
		CommissionPlan com = null;
		
		if (fileType.getType().equals("GarantiaRobo"))
			com = commissionPlanRepository.findByTipoPlan("Robo");
		
		for (Warranty war : colWarranty){
			sbLine = new StringBuilder();
			
			if (!fileType.getType().equals("GarantiaRobo"))
				com = commissionPlanRepository.findByRange(war.getPrecioVenta());
			
			sbLine.append(war.getFolio().toString());
			sbLine.append(";");
			sbLine.append(war.getRutCliente());
			sbLine.append(";");
			sbLine.append(war.getApPaterno());
			sbLine.append(";");
			sbLine.append(war.getApMaterno());
			sbLine.append(";");
			sbLine.append(war.getNombres());
			sbLine.append(";");
			sbLine.append(war.getDireccion());
			sbLine.append(";");
			sbLine.append(war.getComuna());
			sbLine.append(";");
			sbLine.append(war.getCiudad());
			sbLine.append(";");
			sbLine.append(war.getTelefono());
			sbLine.append(";");
			sbLine.append(war.getCodSucursal().toString());
			sbLine.append(";");
			sbLine.append(war.getGlosaSucursal());
			sbLine.append(";");
			sbLine.append(war.getFecVentaGarantia() == null ? "00/00/0000" : df.format(war.getFecVentaGarantia()));
			sbLine.append(";");
			sbLine.append(String.format("%,d", war.getPrecioVenta()).replace(",", "."));
			sbLine.append(";");
			
			if (com == null) {
				sbLine.append("N/A;0,000000;");
			}
			else {
				sbLine.append(com.getTipoPlan());
				sbLine.append(";");
				sbLine.append(com.getPrimaFinalSeguroRobo().toString().replace(".", ","));
				sbLine.append(";");
			}
			sbLine.append(war.getGlosaArticulo());
			
			lines.add(sbLine.toString());
		}
		
		return lines;
	}
}