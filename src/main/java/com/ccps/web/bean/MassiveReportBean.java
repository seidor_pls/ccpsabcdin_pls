package com.ccps.web.bean;

import static java.lang.System.out;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.activation.MimetypesFileTypeMap;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.primefaces.context.RequestContext;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.ccps.web.common.CommonMessages;
import com.ccps.web.model.BaseType;
import com.ccps.web.model.FileType;
import com.ccps.web.model.Massive;
import com.ccps.web.model.Period;
import com.ccps.web.model.repository.FileTypeRepository;
import com.ccps.web.model.repository.MassiveRepository;
import com.ccps.web.model.repository.PeriodRepository;



/**
 * @author jDuchens
 *
 */

@ManagedBean(name="massiveReportBean")
@SessionScoped
@Component(value="massiveReportBean")
@Scope("session")
public class MassiveReportBean implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private Map<Integer, String> colYears;
	private Map<Integer, String> colMonths;
	private int year = 0;
	private int month = 0;
	private Integer cantRegistrosValue;
	private Long totalMontoAseguradoValue;
	private Long totalValorPrimaValue;
	private BaseType baseTypeValue;
	private Double comIntermediacionValue;
	private Double tasaTecnicaValue;
	private Integer cantRegistrosValue2;
	private Long totalMontoAseguradoValue2;
	private BaseType baseTypeValue2;
	private Double comIntermediacionValue2;
	private Double tasaTecnicaValue2;
	String destination="home/formatos_cierre/";
	String formato_cierre="Cierre_MASIVOS.xls";
	String absoluteFilePath;
	String filePath;
	private StreamedContent file;
	@Autowired
	private PeriodRepository periodRepository;
	@Autowired
	private MassiveRepository massiveRepository;
	@Autowired
	private FileTypeRepository fileTypeRepository;
	FileType fileTypeCesantia;
	FileType fileTypeDesgravamen;
	
	
	public MassiveReportBean() {
		super();
	}

	@PostConstruct
	public void InitPage(){
		this.fileTypeCesantia     = fileTypeRepository.findByType("MasivoCesantia");
		this.fileTypeDesgravamen  = fileTypeRepository.findByType("MasivoDesgravamen");
		InitYearsMonths();
	}
	public Map<Integer, String> getColYears() {
		return colYears;
	}
	public void setColYears(Map<Integer, String> colYears) {
		this.colYears = colYears;
	}
	public Map<Integer, String> getColMonths() {
		return colMonths;
	}
	public void setColMonths(Map<Integer, String> colMonths) {
		this.colMonths = colMonths;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public int getMonth() {
		return month;
	}
	public void setMonth(int month) {
		this.month = month;
	}
	public BaseType getBaseTypeValue() {
		return baseTypeValue;
	}
	public void setBaseTypeValue(BaseType baseTypeValue) {
		this.baseTypeValue = baseTypeValue;
	}
	public Double getComIntermediacionValue() {
		return comIntermediacionValue;
	}
	public void setComIntermediacionValue(Double comIntermediacionValue) {
		this.comIntermediacionValue = comIntermediacionValue;
	}
	public Double getTasaTecnicaValue() {
		return tasaTecnicaValue;
	}
	public void setTasaTecnicaValue(Double tasaTecnicaValue) {
		this.tasaTecnicaValue = tasaTecnicaValue;
	}
	public void setCantRegistrosValue(Integer cantRegistrosValue) {
		this.cantRegistrosValue = cantRegistrosValue;
	}
	public void setTotalMontoAseguradoValue(Long totalMontoAseguradoValue) {
		this.totalMontoAseguradoValue = totalMontoAseguradoValue;
	}
	public void setTotalValorPrimaValue(Long totalValorPrimaValue) {
		this.totalValorPrimaValue = totalValorPrimaValue;
	}
	public Integer getCantRegistrosValue2() {
		return cantRegistrosValue2;
	}
	public void setCantRegistrosValue2(Integer cantRegistrosValue2) {
		this.cantRegistrosValue2 = cantRegistrosValue2;
	}
	public Long getTotalMontoAseguradoValue2() {
		return totalMontoAseguradoValue2;
	}
	public void setTotalMontoAseguradoValue2(Long totalMontoAseguradoValue2) {
		this.totalMontoAseguradoValue2 = totalMontoAseguradoValue2;
	}
	public BaseType getBaseTypeValue2() {
		return baseTypeValue2;
	}
	public void setBaseTypeValue2(BaseType baseTypeValue2) {
		this.baseTypeValue2 = baseTypeValue2;
	}
	public Double getComIntermediacionValue2() {
		return comIntermediacionValue2;
	}
	public void setComIntermediacionValue2(Double comIntermediacionValue2) {
		this.comIntermediacionValue2 = comIntermediacionValue2;
	}
	public Double getTasaTecnicaValue2() {
		return tasaTecnicaValue2;
	}
	public void setTasaTecnicaValue2(Double tasaTecnicaValue2) {
		this.tasaTecnicaValue2 = tasaTecnicaValue2;
	}
	public StreamedContent getFile() {
		return file;
	}
	
	public void InitYearsMonths(){
		try{
			Calendar cal    = Calendar.getInstance();
			int currentYear = cal.get(Calendar.YEAR);
			this.colYears   = new HashMap<Integer, String>();
			this.colMonths  = new HashMap<Integer, String>();
			for (Integer i = currentYear; i < currentYear + 5; i++){this.colYears.put(i, i.toString());}
			this.colMonths.put(1, "Enero");this.colMonths.put(2, "Febrero");this.colMonths.put(3, "Marzo");this.colMonths.put(4, "Abril");this.colMonths.put(5, "Mayo");
			this.colMonths.put(6, "Junio");this.colMonths.put(7, "Julio");this.colMonths.put(8, "Agosto");this.colMonths.put(9, "Septiembre");this.colMonths.put(10, "Octubre");
			this.colMonths.put(11, "Noviembre");this.colMonths.put(12, "Diciembre");
		}
		catch (Exception e){CommonMessages.ShowErrorMessage("Error", "Error al inicializar los meses y años. " + e.getMessage());}
	}
	
	public void CloseDialogReport(){
		RequestContext context = RequestContext.getCurrentInstance();
		context.execute("$('#dialogConfirmReport').hide();");
	}
	
	public Boolean ValidateForm(){
		if (this.year <= 0){
			CommonMessages.ShowWarningMessage("Datos Incompletos", "Debe seleccionar un año.");
			return false;
		}
		if (this.month <= 0){
			CommonMessages.ShowWarningMessage("Datos Incompletos", "Debe seleccionar un mes.");
			return false;
		}
		return true;
	}
	
	public Boolean ValidateClosedPeriod(){
		//Desgravamen
		List<Period> colPeriods  = periodRepository.findByYearMonthType(this.year, this.month, this.fileTypeDesgravamen.getId());
		//Cesantia
		List<Period> colPeriods2 = periodRepository.findByYearMonthType(this.year, this.month, this.fileTypeCesantia.getId());
		for (Period per : colPeriods){
			if (!per.getCerrado()) {
				CommonMessages.ShowWarningMessage("Periodo Inválido", "El periodo seleccionado no está cerrado. Debe cerrarse para generar el reporte.");
				return false;
			}else{
				return true;
			}
		}
		CommonMessages.ShowWarningMessage("Periodo Inválido", "El periodo seleccionado no está cerrado. Debe cerrarse para generar el reporte.");
		return false;
	}
	
	public void HandleGenerateReportButton(){
		if (ValidateForm() && ValidateClosedPeriod())
			GenerateExcelReport();
		
		if (this.file != null){
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("$('#divDownloadReport').show();");
		}
	}
	
	public void GenerateExcelReport(){
		List <Massive> colDesgravamen = massiveRepository.findByFileTypePeriod(this.fileTypeDesgravamen.getId(), this.month, this.year);
		List <Massive> colCesantia    = massiveRepository.findByFileTypePeriod(this.fileTypeCesantia.getId(), this.month, this.year);
		
		if (colDesgravamen.size() == 0 && colCesantia.size() == 0)
		{
			CommonMessages.ShowWarningMessage("Periodo Inválido", "El periodo seleccionado no tiene datos para generar el reporte.");
			return;
		}
		
		FacesContext context = FacesContext.getCurrentInstance();
		this.absoluteFilePath = "C:\\PLS\\PEGA\\PROYECTOS\\ABCDIN\\ccpsabcdin_pls\\home\\formatos_cierre\\" + this.formato_cierre;//context.getExternalContext().getRealPath(this.destination+this.formato_cierre);
		this.filePath = absoluteFilePath;//this.destination+this.formato_cierre;
	    try {
            FileInputStream fileIn = new FileInputStream(new File(this.absoluteFilePath));
            HSSFWorkbook workbook = new HSSFWorkbook(fileIn);
            //Desgravamen
            HSSFSheet sheet = workbook.getSheetAt(0);
            Cell cell = null;
            
            //Titulo Cuadro Mes
            cell = sheet.getRow(0).getCell(0);
            cell.setCellValue("Desgravamen " + this.colMonths.get(this.month) + " " + this.year);
            
            for(int i=1;i<6;i++){
               if (i > colDesgravamen.size())
               {
                   //N de clientes
                   this.cantRegistrosValue = 0;
       	           cell = sheet.getRow(2).getCell(i);
       	           cell.setCellValue(this.cantRegistrosValue);
                   //Saldo insoluto
       	           this.totalMontoAseguradoValue = 0l;
       	           cell = sheet.getRow(3).getCell(i);
       	           cell.setCellFormula(this.totalMontoAseguradoValue.toString());
                   //Prima mensual
                   this.totalValorPrimaValue = 0l;
                   cell = sheet.getRow(4).getCell(i);
                   cell.setCellFormula(this.totalValorPrimaValue.toString());
                   //Tasa
                   this.tasaTecnicaValue = 0d;
                   Double tasaTecnicaValue = Double.parseDouble(this.tasaTecnicaValue.toString());
                   cell = sheet.getRow(6).getCell(i);
                   cell.setCellValue(tasaTecnicaValue);
                   //Comisión
                   this.comIntermediacionValue = 0d;
                   Double comIntermediacionValue = this.comIntermediacionValue;
                   cell = sheet.getRow(7).getCell(i);
                   cell.setCellValue(comIntermediacionValue);
                   //CellStyle style = workbook.createCellStyle();
                   //style.setDataFormat(workbook.createDataFormat().getFormat("0.#000000%"));
                   //cell.setCellStyle(style);
               }
               else
               {
	               this.baseTypeValue = colDesgravamen.get(i-1).getBaseType();
	               //N de clientes
	               this.cantRegistrosValue = colDesgravamen.get(i-1).getCantRegistros();
		           cell = sheet.getRow(2).getCell(i);
		           cell.setCellValue(this.cantRegistrosValue);
		           //Saldo insoluto
		           this.totalMontoAseguradoValue = colDesgravamen.get(i-1).getTotalMontoAsegurado();
		           cell = sheet.getRow(3).getCell(i);
	               cell.setCellFormula(this.totalMontoAseguradoValue.toString());
	               //Prima mensual
	               this.totalValorPrimaValue = colDesgravamen.get(i-1).getTotalValorPrima();
	         	   cell = sheet.getRow(4).getCell(i);
	               cell.setCellFormula(this.totalValorPrimaValue.toString());
	               //Tasa
	               this.tasaTecnicaValue = this.baseTypeValue.getTasaTecnica();
	               Double tasaTecnicaValue = Double.parseDouble(this.tasaTecnicaValue.toString());
	         	   cell = sheet.getRow(6).getCell(i);
	               cell.setCellValue(tasaTecnicaValue);
	               //Comisión
	               this.comIntermediacionValue = this.baseTypeValue.getComIntermediacion();
	               Double comIntermediacionValue = this.comIntermediacionValue;
	         	   cell = sheet.getRow(7).getCell(i);
	         	   cell.setCellValue(comIntermediacionValue);
	               //CellStyle style = workbook.createCellStyle();
	               //style.setDataFormat(workbook.createDataFormat().getFormat("0.#000000%"));
	               //cell.setCellStyle(style);
               }
            }
            
            //Cesantia
            HSSFSheet sheet2 = workbook.getSheetAt(1);
            Cell cell2 = null;
            
            //Titulo Cuadro Mes
            cell2 = sheet2.getRow(0).getCell(0);
            cell2.setCellValue("Cesantía " + this.colMonths.get(this.month) + " " + this.year);
            
            for(int i=1;i<6;i++){
            	if (i > colDesgravamen.size())
                {
            		//N de clientes
            		this.cantRegistrosValue2 = 0;
            		cell2 = sheet2.getRow(2).getCell(i);
            		cell2.setCellValue(this.cantRegistrosValue2);
            		//Saldo insoluto
            		this.totalMontoAseguradoValue2 = 0l;
            		cell2 = sheet2.getRow(3).getCell(i);
            		cell2.setCellFormula(this.totalMontoAseguradoValue2.toString());
            		//Valor Prima Tec. 
            		//Valor Prima Tec. Neta
            		//Tasa
            		this.tasaTecnicaValue2 = 0d;
            		Double tasaTecnicaValue2 = Double.parseDouble(this.tasaTecnicaValue2.toString());
            		cell2 = sheet2.getRow(7).getCell(i);
            		cell2.setCellValue(tasaTecnicaValue2);
            		//Comisión
            		this.comIntermediacionValue2 = 0d;
            		Double comIntermediacionValue2 = this.comIntermediacionValue2; 
            		cell2 = sheet2.getRow(8).getCell(i);
            		cell2.setCellValue(comIntermediacionValue2);
            		//CellStyle style2 = workbook.createCellStyle();
            		//style2.setDataFormat(workbook.createDataFormat().getFormat("0.#000000%"));
            		//cell2.setCellStyle(style2);
                }
            	else
            	{
            		this.baseTypeValue2 = colCesantia.get(i-1).getBaseType();
            		//N de clientes
            		this.cantRegistrosValue2 = colCesantia.get(i-1).getCantRegistros();
            		cell2 = sheet2.getRow(2).getCell(i);
            		cell2.setCellValue(this.cantRegistrosValue2);
            		//Saldo insoluto
            		this.totalMontoAseguradoValue2 = colCesantia.get(i-1).getTotalMontoAsegurado();
            		cell2 = sheet2.getRow(3).getCell(i);
            		cell2.setCellFormula(this.totalMontoAseguradoValue2.toString());
            		//Valor Prima Tec. 
            		//Valor Prima Tec. Neta
            		//Tasa
            		this.tasaTecnicaValue2 = this.baseTypeValue2.getTasaTecnica();
            		Double tasaTecnicaValue2 = Double.parseDouble(this.tasaTecnicaValue2.toString());
            		cell2 = sheet2.getRow(7).getCell(i);
            		cell2.setCellValue(tasaTecnicaValue2);
            		//Comisión
            		this.comIntermediacionValue2 = this.baseTypeValue2.getComIntermediacion();
            		Double comIntermediacionValue2 = this.comIntermediacionValue2; 
            		cell2 = sheet2.getRow(8).getCell(i);
            		cell2.setCellValue(comIntermediacionValue2);
            		//CellStyle style2 = workbook.createCellStyle();
            		//style2.setDataFormat(workbook.createDataFormat().getFormat("0.#000000%"));
            		//cell2.setCellStyle(style2);
            	}
             }
            FormulaEvaluator evaluator = workbook.getCreationHelper().createFormulaEvaluator();
            evaluator.evaluateAll();
            fileIn.close();
            FileOutputStream outFile =new FileOutputStream(new File(this.filePath));
            workbook.write(outFile);
            File fileToStreamed = new File(this.filePath); 
            this.file = new DefaultStreamedContent(new FileInputStream(fileToStreamed), new MimetypesFileTypeMap().getContentType(fileToStreamed), "Cierre_MASIVOS_"+
            this.colMonths.get(this.month)+"_"+this.year+".xls");
            outFile.close();
            workbook.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
		
	}
	
}
