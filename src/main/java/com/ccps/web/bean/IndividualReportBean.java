package com.ccps.web.bean;

import static javax.faces.context.FacesContext.getCurrentInstance;

import java.io.ByteArrayOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.primefaces.model.StreamedContent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.ccps.web.common.CommonMessages;
import com.ccps.web.common.CommonReporting;
import com.ccps.web.model.Authority;
import com.ccps.web.model.Binnacle;
import com.ccps.web.model.Collection;
import com.ccps.web.model.Commission;
import com.ccps.web.model.Company;
import com.ccps.web.model.FileType;
import com.ccps.web.model.FileTypeCompany;
import com.ccps.web.model.Period;
import com.ccps.web.model.Sale;
import com.ccps.web.model.User;
import com.ccps.web.model.repository.*;

/**
 * author
 * plopezs
 */

@ManagedBean(name="individualReportBean")
@SessionScoped
@Component(value="individualReportBean")
@Scope("session")
public class IndividualReportBean implements Serializable{
	private static final long serialVersionUID = 6260459998331769648L;

	private Boolean error = false;
	private List<Company> colCompanies;
	private Map<Integer, String> colYears;
	private Map<Integer, String> colMonths;
	private int year = 0;
	private int month = 0;
	private int companyId = 0;
	private String UFValue = "";
	private Boolean validUF = true;
	private String UFMessage = "";
	private StreamedContent file;
	List<FileType> fileTypesVentas;
	List<FileType> fileTypesRemesa;
	private List<Binnacle> binnacles;
	private User loggedUser;
	
	@Autowired
	private CompanyRepository companyRepository;
	
	@Autowired
	private SalesRepository salesRepository;
	
	@Autowired
	private CollectionRepository collectionRepository;
	
	@Autowired
	private CommissionRepository commissionRepository;
	
	@Autowired
	private PeriodRepository periodRepository;
	
	@Autowired
	private FileTypeRepository fileTypeRepository;
	
	@Autowired
	private FileTypeCompanyRepository fileTypeCompanyRepository;
	
	@Autowired
	private BinnacleRepository binnacleRepository;
	
	@Autowired
	private AuthorityRepository authorityRepository;
	
	//PROPERTIES
	public StreamedContent getFile() {
        return file;
    }
	
	public void setFile(StreamedContent file) {
		this.file = file;
	}
	
	public Boolean getError() {
		return error;
	}

	public void setError(Boolean error) {
		this.error = error;
	}

	public List<Company> getColCompanies() {
		return colCompanies;
	}

	public void setColCompanies(List<Company> colCompanies) {
		this.colCompanies = colCompanies;
	}

	public Map<Integer, String> getColYears() {
		return colYears;
	}

	public void setColYears(Map<Integer, String> colYears) {
		this.colYears = colYears;
	}

	public Map<Integer, String> getColMonths() {
		return colMonths;
	}

	public void setColMonths(Map<Integer, String> colMonths) {
		this.colMonths = colMonths;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		this.month = month;
	}

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	public String getUFValue() {
		return UFValue;
	}

	public void setUFValue(String uFValue) {
		UFValue = uFValue;
	}
	
	public Boolean getValidUF() {
		return validUF;
	}

	public void setValidUF(Boolean validUF) {
		this.validUF = validUF;
	}

	public String getUFMessage() {
		return UFMessage;
	}

	public void setUFMessage(String uFMessage) {
		UFMessage = uFMessage;
	}
	
	public List<Binnacle> getBinnacles() {
		return binnacles;
	}

	public void setBinnacles(List<Binnacle> binnacles) {
		this.binnacles = binnacles;
	}

	//METHODS
	@PostConstruct
	public void PageLoad(){
		FacesContext context = FacesContext.getCurrentInstance();
		
		this.fileTypesVentas = fileTypeRepository.findByCategory("ventas");
		this.fileTypesRemesa = fileTypeRepository.findByCategory("remesa");
		
		this.loggedUser = (User) context.getExternalContext().getSessionMap().get("user");
		
		if (this.loggedUser == null)
			System.out.println("NO HAY USUARIO");
		else
			System.out.println("USER LOGIN: " + this.loggedUser.getEmail());
		
		GetSalesCompanies();
		
		InitYearsMonths();
	}
	
	public void GetSalesCompanies()
	{
		this.colCompanies = new ArrayList<Company>();
		
		for (FileType ft : fileTypesVentas)
		{
			List<FileTypeCompany> ftCompanyColl = fileTypeCompanyRepository.findByFileType(ft.getId());
			
			for (FileTypeCompany ftc : ftCompanyColl)
			{
				Boolean contains = false;
				
				for (Company com : this.colCompanies)
				{
					if (ftc.getCompany().getId() == com.getId())
						contains = true;
				}
				
				if (!contains)
					this.colCompanies.add(ftc.getCompany());
			}
		}
		
		for (FileType ft : fileTypesRemesa)
		{
			List<FileTypeCompany> ftCompanyColl = fileTypeCompanyRepository.findByFileType(ft.getId());
			
			for (FileTypeCompany ftc : ftCompanyColl)
			{
				Boolean contains = false;
				
				for (Company com : this.colCompanies)
				{
					if (ftc.getCompany().getId() == com.getId())
						contains = true;
				}
				
				if (!contains)
					this.colCompanies.add(ftc.getCompany());
			}
		}
		
		if (this.colCompanies.size() == 1)
		{
			this.companyId = this.colCompanies.get(0).getId();
		}
	}

	public void InitYearsMonths()
	{
		try
		{
			Calendar cal = Calendar.getInstance();
			int currentYear = cal.get(Calendar.YEAR);
			
			this.colYears = new HashMap<Integer, String>();
			this.colMonths = new HashMap<Integer, String>();
			
			//Agrega el año actual y los 4 siguientes.
			for (Integer i = currentYear; i < currentYear + 5; i++)
			{
				this.colYears.put(i, i.toString());
			}
			
			this.colMonths.put(1, "Enero");
			this.colMonths.put(2, "Febrero");
			this.colMonths.put(3, "Marzo");
			this.colMonths.put(4, "Abril");
			this.colMonths.put(5, "Mayo");
			this.colMonths.put(6, "Junio");
			this.colMonths.put(7, "Julio");
			this.colMonths.put(8, "Agosto");
			this.colMonths.put(9, "Septiembre");
			this.colMonths.put(10, "Octubre");
			this.colMonths.put(11, "Noviembre");
			this.colMonths.put(12, "Diciembre");
		}
		catch (Exception e) 
		{
			CommonMessages.ShowErrorMessage("Error", "Error al inicializar los meses y años. " + e.getMessage());
	    }
	}
	
	public void HandleGenerateReportButton(){
		//GenerateExcelReport();
		
		if (ValidateForm() && ValidateClosedPeriod()) {
			if (this.companyId == 1 || this.companyId == 2 || this.companyId == 4)
			{
				ReadBinnacle();
				
				if (this.UFValue.isEmpty())
				{
					RequestContext context = RequestContext.getCurrentInstance();
					context.execute("$('#dialogConfirmReport').show();");
				}
				else
				{
					GenerateExcelReport();
				}
			}
			else
			{
				GenerateExcelReport();
			}
		}
	}
	
	public void GenerateReport(){
		if (ValidateUF()) {
			
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("$('#dialogConfirmReport').hide();");
			
			GenerateExcelReport();
		}
	}
	
	public void GenerateExcelReport(){
		RequestContext context = RequestContext.getCurrentInstance();
		List<FileTypeCompany> ftCollReporte = fileTypeCompanyRepository.findByCompanyCategory(this.companyId, "reporte");
		
		if (ftCollReporte.size() > 0)
		{
			String companyName = companyRepository.findOne(this.companyId).getName();
			String fileName = "Cierre " + companyName + " " + this.colMonths.get(month) + " " + this.year + ".xlsx";
			
	        file = CommonReporting.StreamExcelReport(GenerateExcelFileFromDB(this.month, this.year, this.companyId), fileName);
			
			//String fileName = "prueba.xlsx";
			//file = CommonReporting.StreamExcelReport(DemoExcel(), fileName);
	        
	        if (file != null)
	        {
	        	context.execute("$('#divDownloadReport').show();");
	        }
	        
	        InsertBinnacle(ftCollReporte.get(0).getFileType(), 2, fileName, this.UFValue);
	        this.UFValue = "";
		}
	}
	
	private static Map<String, CellStyle> createStyles(Workbook wb){
        Map<String, CellStyle> styles = new HashMap<String, CellStyle>();
        
        CellStyle style;
        
        //FONTS
        Font blackFont = wb.createFont();
        blackFont.setFontHeightInPoints((short)11);
        blackFont.setFontName("Calibri");
        blackFont.setColor(IndexedColors.BLACK.getIndex());
        
        Font blackFontBold = wb.createFont();
        blackFontBold.setFontHeightInPoints((short)11);
        blackFontBold.setFontName("Calibri");
        blackFontBold.setBold(true);
        blackFontBold.setColor(IndexedColors.BLACK.getIndex());
        
        Font whiteFont = wb.createFont();
        whiteFont.setFontHeightInPoints((short)11);
        whiteFont.setFontName("Calibri");
        whiteFont.setColor(IndexedColors.WHITE.getIndex());
        
        Font whiteFontBold = wb.createFont();
        whiteFontBold.setFontHeightInPoints((short)11);
        whiteFontBold.setFontName("Calibri");
        whiteFontBold.setBold(true);
        whiteFontBold.setColor(IndexedColors.WHITE.getIndex());
        
        Font redFont = wb.createFont();
        redFont.setFontHeightInPoints((short)11);
        redFont.setFontName("Calibri");
        redFont.setColor(IndexedColors.RED.getIndex());
        
        
        style = wb.createCellStyle();
        style.setAlignment(CellStyle.ALIGN_RIGHT);
        style.setFont(blackFont);
        style.setDataFormat(wb.createDataFormat().getFormat("_-$ * #,##0_-;-$ * #,##0_-;_-$ * \"-\"_-;_-@_-"));
        style.setFillForegroundColor(IndexedColors.WHITE.getIndex());
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        styles.put("pesos_white_centered_sin_cero", style);
        
        style = wb.createCellStyle();
        style.setAlignment(CellStyle.ALIGN_RIGHT);
        style.setFont(blackFont);
        style.setDataFormat(wb.createDataFormat().getFormat("_-$ * #,##0_-;-$ * #,##0_-;_-$ * \"-\"_-;_-@_-"));
        style.setFillForegroundColor(IndexedColors.WHITE.getIndex());
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        styles.put("pesos_white", style);
        
        style = wb.createCellStyle();
        style.setAlignment(CellStyle.ALIGN_RIGHT);
        style.setFont(whiteFont);
        style.setDataFormat(wb.createDataFormat().getFormat("_-$ * #,##0_-;-$ * #,##0_-;_-$ * \"-\"_-;_-@_-"));
        style.setFillForegroundColor(IndexedColors.BLACK.getIndex());
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        styles.put("pesos_black", style);
        
        style = wb.createCellStyle();
        style.setAlignment(CellStyle.ALIGN_CENTER);
        style.setFont(whiteFont);
        style.setDataFormat(wb.createDataFormat().getFormat("_-$ * #,##0_-;-$ * #,##0_-;_-$ * \"-\"_-;_-@_-"));
        style.setFillForegroundColor(IndexedColors.BLACK.getIndex());
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        styles.put("pesos_black_centered", style);
        
        style = wb.createCellStyle();
        style.setAlignment(CellStyle.ALIGN_CENTER);
        style.setFont(whiteFont);
        style.setDataFormat(wb.createDataFormat().getFormat("_-$ * #,##0_-;-$ * #,##0_-;_-$ * \"-\"_-;_-@_-"));
        style.setFillForegroundColor(IndexedColors.BLACK.getIndex());
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        styles.put("pesos_black_centered_sin_cero", style);
        
        style = wb.createCellStyle();
        style.setAlignment(CellStyle.ALIGN_RIGHT);
        style.setFont(blackFont);
        style.setDataFormat(wb.createDataFormat().getFormat("_-* #,##0_-;-* #,##0_-;_-* \"-\"_-;_-@_-"));
        style.setFillForegroundColor(IndexedColors.WHITE.getIndex());
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        styles.put("entero_white", style);
        
        style = wb.createCellStyle();
        style.setAlignment(CellStyle.ALIGN_RIGHT);
        style.setFont(whiteFont);
        style.setDataFormat(wb.createDataFormat().getFormat("_-* #,##0_-;-* #,##0_-;_-* \"-\"_-;_-@_-"));
        style.setFillForegroundColor(IndexedColors.BLACK.getIndex());
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        styles.put("entero_black", style);
        
        style = wb.createCellStyle();
        style.setAlignment(CellStyle.ALIGN_RIGHT);
        style.setFont(blackFont);
        style.setDataFormat(wb.createDataFormat().getFormat("_-* 0.000_-;-*0.000_-;_-* \"-\"??_-;_-@_-"));
        style.setFillForegroundColor(IndexedColors.WHITE.getIndex());
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        styles.put("decimal_white", style);
        
        style = wb.createCellStyle();
        style.setAlignment(CellStyle.ALIGN_RIGHT);
        style.setFont(whiteFont);
        style.setDataFormat(wb.createDataFormat().getFormat("_-* 0.000_-;-*0.000_-;_-* \"-\"??_-;_-@_-"));
        style.setFillForegroundColor(IndexedColors.GREY_40_PERCENT.getIndex());
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        styles.put("decimal_grey", style);
        
        style = wb.createCellStyle();
        style.setAlignment(CellStyle.ALIGN_RIGHT);
        style.setFont(whiteFont);
        style.setDataFormat(wb.createDataFormat().getFormat("_-* 0.000_-;-*0.000_-;_-* \"-\"??_-;_-@_-"));
        style.setFillForegroundColor(IndexedColors.BLACK.getIndex());
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        styles.put("decimal_black", style);
        
        style = wb.createCellStyle();
        style.setAlignment(CellStyle.ALIGN_CENTER);
        style.setFont(whiteFont);
        style.setDataFormat(wb.createDataFormat().getFormat("_-* 0.000_-;-*0.000_-;_-* \"-\"??_-;_-@_-"));
        style.setFillForegroundColor(IndexedColors.BLACK.getIndex());
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        styles.put("decimal_black_centered", style);
        
        style = wb.createCellStyle();
        style.setAlignment(CellStyle.ALIGN_CENTER);
        style.setFont(whiteFont);
        style.setDataFormat(wb.createDataFormat().getFormat("0.#0%"));
        style.setFillForegroundColor(IndexedColors.BLACK.getIndex());
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        styles.put("porcentaje_black_centered_dos_dec", style);

        style = wb.createCellStyle();
        style.setAlignment(CellStyle.ALIGN_CENTER);
        style.setFont(whiteFont);
        style.setDataFormat(wb.createDataFormat().getFormat("0.#0%"));
        style.setFillForegroundColor(IndexedColors.BLACK.getIndex());
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        styles.put("porcentaje_black_centered_un_dec", style);
        
        style = wb.createCellStyle();
        style.setAlignment(CellStyle.ALIGN_LEFT);
        style.setFont(blackFont);
        style.setFillForegroundColor(IndexedColors.WHITE.getIndex());
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        styles.put("texto_white", style);
        
        style = wb.createCellStyle();
        style.setAlignment(CellStyle.ALIGN_CENTER);
        style.setFont(blackFont);
        style.setFillForegroundColor(IndexedColors.WHITE.getIndex());
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        styles.put("texto_white_centered", style);
        
        style = wb.createCellStyle();
        style.setAlignment(CellStyle.ALIGN_LEFT);
        style.setFont(blackFontBold);
        style.setFillForegroundColor(IndexedColors.WHITE.getIndex());
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        styles.put("texto_white_bold", style);
        
        style = wb.createCellStyle();
        style.setAlignment(CellStyle.ALIGN_CENTER);
        style.setFont(blackFontBold);
        style.setFillForegroundColor(IndexedColors.WHITE.getIndex());
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        styles.put("texto_white_bold_centered", style);
        
        style = wb.createCellStyle();
        style.setAlignment(CellStyle.ALIGN_LEFT);
        style.setFont(blackFont);
        style.setFillForegroundColor(IndexedColors.WHITE.getIndex());
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        style.setWrapText(true);
        styles.put("texto_white_wrapped", style);
        
        style = wb.createCellStyle();
        style.setAlignment(CellStyle.ALIGN_LEFT);
        style.setFont(whiteFont);
        style.setFillForegroundColor(IndexedColors.GREY_40_PERCENT.getIndex());
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        styles.put("texto_grey", style);
        
        style = wb.createCellStyle();
        style.setAlignment(CellStyle.ALIGN_LEFT);
        style.setFont(whiteFont);
        style.setFillForegroundColor(IndexedColors.GREY_40_PERCENT.getIndex());
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        style.setWrapText(true);
        styles.put("texto_grey_wrapped", style);
        
        style = wb.createCellStyle();
        style.setAlignment(CellStyle.ALIGN_LEFT);
        style.setFont(whiteFont);
        style.setFillForegroundColor(IndexedColors.BLACK.getIndex());
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        styles.put("texto_black", style);
        
        style = wb.createCellStyle();
        style.setAlignment(CellStyle.ALIGN_CENTER);
        style.setFont(whiteFont);
        style.setFillForegroundColor(IndexedColors.BLACK.getIndex());
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        styles.put("texto_black_centered", style);
        
        style = wb.createCellStyle();
        style.setAlignment(CellStyle.ALIGN_LEFT);
        style.setFont(whiteFontBold);
        style.setFillForegroundColor(IndexedColors.BLACK.getIndex());
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        styles.put("texto_black_bold", style);
        
        style = wb.createCellStyle();
        style.setAlignment(CellStyle.ALIGN_LEFT);
        style.setFont(whiteFont);
        style.setFillForegroundColor(IndexedColors.BLACK.getIndex());
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        style.setWrapText(true);
        styles.put("texto_black_wrapped", style);
        
        style = wb.createCellStyle();
        style.setAlignment(CellStyle.ALIGN_CENTER);
        style.setFont(whiteFontBold);
        style.setFillForegroundColor(IndexedColors.BLACK.getIndex());
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        style.setWrapText(true);
        styles.put("texto_black_wrapped_bold_centered", style);
        
        style = wb.createCellStyle();
        style.setAlignment(CellStyle.ALIGN_LEFT);
        style.setFont(redFont);
        style.setFillForegroundColor(IndexedColors.BLACK.getIndex());
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        style.setWrapText(true);
        styles.put("texto_black_wrapped_red", style);
        
        style = wb.createCellStyle();
        style.setAlignment(CellStyle.ALIGN_LEFT);
        style.setFont(blackFont);
        style.setFillForegroundColor(IndexedColors.LAVENDER.getIndex());
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        style.setWrapText(true);
        styles.put("texto_lavender_wrapped", style);

        return styles;
    }
	
	public ByteArrayOutputStream DemoExcel() {
		ByteArrayOutputStream returnStream = new ByteArrayOutputStream();
		
		try
		{
			XSSFWorkbook workbook = new XSSFWorkbook();
			XSSFSheet sheet = workbook.createSheet("PRUEBA");
			Map<String, CellStyle> styles = createStyles(workbook);
			
			Row row = sheet.createRow(0);
	        Cell cell = row.createCell(0);
	        
	        cell.setCellStyle(styles.get("formula_$"));
	        cell.setCellValue(41350);
	        
	        workbook.write(returnStream);
	        
	        workbook.close();
		}
		catch(Exception ex)
		{
			CommonMessages.ShowErrorMessage("Error", "No fue posible generar el reporte.");
		}
		
		return returnStream;
	}
	
	public ByteArrayOutputStream GenerateExcelFileFromDB(Integer month, Integer year, int companyId) {
		ByteArrayOutputStream returnStream = new ByteArrayOutputStream();
		
		try
		{
			XSSFSheet sheetVT = null;
			XSSFSheet sheetVSin = null;
			
			XSSFWorkbook workbook = new XSSFWorkbook();
			XSSFSheet sheetResumen = workbook.createSheet("RESUMEN");
			XSSFSheet sheetCalcCom = workbook.createSheet("Calculo comisiones");
			
			if (this.companyId == 1)
			{
				sheetVT = workbook.createSheet("Ventas totales");
				sheetVSin = workbook.createSheet("Ventas sin desafil y prima 0");
			}
			
			if (this.companyId == 2)
			{
				sheetVT = workbook.createSheet("Ventas totales");
				sheetVSin = workbook.createSheet("Ventas sin desafil y prima 0");
			}
			
			XSSFSheet sheetRemesa = workbook.createSheet("Remesa " + this.colMonths.get(month) + " " + year.toString());
			
			Map<String, CellStyle> styles = createStyles(workbook);
					    
		    Calendar cal = Calendar.getInstance();
		    cal.clear();
		    cal.set(this.year, this.month - 1, 1);
			int maxMonthDay = cal.getActualMaximum(cal.DAY_OF_MONTH);
			int rowCount = 0;
			
			//DATASOURCES
			//Ventas Totales
	        List<Sale> colTotalSales = salesRepository.findByPeriodCompany(this.month, this.year, this.companyId);
	        //Ventas sin desafil y prima 0
	        List<Sale> colSalesSinDesa = salesRepository.findByPeriodCompanySinEstado(this.month, this.year, this.companyId, "Desafiliada");
	        //Remesas
	        List<Collection> colRecaudacion = collectionRepository.findByPeriodCompany(this.month, this.year, this.companyId);
	        //Cuadro Comisiones
	        List<Commission> colComision = commissionRepository.findByCompany(this.companyId);
	        
	        //Cuadro Comisiones
	        List<Commission> colComisionABC = commissionRepository.findByCompanyEnterprise(this.companyId, 1);
	        
	        //Cuadro Comisiones
	        List<Commission> colComisionDIN = commissionRepository.findByCompanyEnterprise(this.companyId, 2);
	        
	        Row row = null;
	        Cell cell = null;
	        String Empresa = "ABC";
	        Integer totalCantAbc = 0;
	        Double totalSumaPrimaAbc = 0d;
	        Integer totalComisionPesosAbc = 0;
	        Double totalComisionDecimalAbc = 0d;
	        Integer totalCantDin = 0;
	        Double totalSumaPrimaDin = 0d;
	        Integer totalComisionPesosDin = 0;
	        Double totalComisionDecimalDin = 0d;
	        
	        sheetCalcCom.setColumnWidth(0, 19*256);
        	sheetCalcCom.setColumnWidth(1, 11*256);
        	sheetCalcCom.setColumnWidth(2, 11*256);
        	sheetCalcCom.setColumnWidth(3, 15*256);
        	sheetCalcCom.setColumnWidth(4, 15*256);
        	sheetCalcCom.setColumnWidth(5, 19*256);
        	sheetCalcCom.setColumnWidth(6, 20*256);
        	sheetCalcCom.setColumnWidth(7, 20*256);
        	sheetCalcCom.setColumnWidth(8, 20*256);
        	sheetCalcCom.setColumnWidth(9, 20*256);
        	sheetCalcCom.setColumnWidth(10, 20*256);
	        
	        //Cálculo de Comisiones
	        rowCount = 0;
	        if (this.companyId == 1 || this.companyId == 2)
	        {
		        //HEADER VENTAS
		        row = sheetCalcCom.createRow(++rowCount);
		        cell = row.createCell(0);
		        cell.setCellStyle(styles.get("texto_black"));
		        cell.setCellValue("Comisión de recaudación sobre la venta " + this.colMonths.get(month) + " " + this.year);
		        cell = row.createCell(1);
		        cell.setCellStyle(styles.get("texto_black"));
		        cell = row.createCell(2);
		        cell.setCellStyle(styles.get("texto_black"));
		        cell = row.createCell(3);
		        cell.setCellStyle(styles.get("texto_black"));
		        cell = row.createCell(4);
		        cell.setCellStyle(styles.get("texto_black"));
		        cell = row.createCell(5);
		        cell.setCellStyle(styles.get("texto_black"));
		        cell = row.createCell(6);
		        cell.setCellStyle(styles.get("texto_black"));
		        row = sheetCalcCom.createRow(++rowCount);
		        cell = row.createCell(0);
		        cell.setCellStyle(styles.get("texto_grey_wrapped"));
	            cell.setCellValue("Empresa");
	            cell = row.createCell(1);
	            cell.setCellStyle(styles.get("texto_grey_wrapped"));
	            cell.setCellValue("Código Producto");
	            cell = row.createCell(2);
	            cell.setCellStyle(styles.get("texto_grey_wrapped"));
	            cell.setCellValue("Cuenta de Certificado");
	            cell = row.createCell(3);
	            cell.setCellStyle(styles.get("texto_grey_wrapped"));
	            cell.setCellValue("Suma de Prima Anualizada");
	            cell = row.createCell(4);
	            cell.setCellStyle(styles.get("texto_grey_wrapped"));
	            cell.setCellValue("Comisión Fija anticipada por Acceso Preferente UF");
	            cell = row.createCell(5);
	            cell.setCellStyle(styles.get("texto_grey_wrapped"));
	            cell.setCellValue("Comisión Fija anticipada por Acceso Preferente en pesos");
	            cell = row.createCell(6);
	            cell.setCellStyle(styles.get("texto_grey_wrapped"));
	            cell.setCellValue("Comisión Anticipada Acceso Preferente TOTAL");
		        
		        //ABC *****************************************************
		        //LISTADO DE CODIGOS DE PRODUCTOS PARA EL PERIODO
		        List<Integer> colProductos = salesRepository.findColProductoByPeriodCompanyEnterprise(this.month, this.year, this.companyId, 1);
	            
		        if (colProductos.size() > 0) {
		            //FILAS SUMADAS POR CODIGO PRODUCTO
			        for (Integer codProd: colProductos)
			        {
			        	row = sheetCalcCom.createRow(++rowCount);
			            
			        	cell = row.createCell(0);
			            cell.setCellValue(Empresa);
			            
			        	List<Sale> colSalesCodProducto = salesRepository.findByPeriodCompanyEnterpriseCodProducto(this.month, this.year, this.companyId, 1, codProd, "Desafiliada");
			        	Double sumaPrima = 0d;
			        	Double ufDoubleValue = Double.parseDouble(this.UFValue.replace(",", "."));
			        	
			        	for (Sale sale : colSalesCodProducto)
			        		sumaPrima += sale.getPrimaAnualizada();
			        	
			        	List<Commission> colCommisionCodProducto = commissionRepository.findByCompanyCodProducto(this.companyId, codProd);
			        	Double comPrimaAnt = colCommisionCodProducto.size() > 0 ? colCommisionCodProducto.get(0).getComFijaAnticipada() : 0d;
			        	Integer comPrimaAntPesos = (int) Math.round(ufDoubleValue * comPrimaAnt);
			        	Double sumaComDecimal = (ufDoubleValue * comPrimaAnt) * colSalesCodProducto.size();
			        	Integer sumaComPesos = (int) Math.round(sumaComDecimal);
			        	
			        	cell = row.createCell(1);
			        	cell.setCellValue(codProd);
			        	cell = row.createCell(2);
			        	cell.setCellValue(colSalesCodProducto.size());
			        	cell = row.createCell(3);
			        	cell.setCellValue(sumaPrima);
			        	cell = row.createCell(4);
			        	cell.setCellValue(comPrimaAnt);
			        	cell = row.createCell(5);
			        	cell.setCellStyle(styles.get("pesos_white"));
			        	cell.setCellValue(comPrimaAntPesos);
			        	cell = row.createCell(6);
			        	cell.setCellStyle(styles.get("pesos_white"));
			        	cell.setCellValue(sumaComPesos);
			        	
			        	Empresa = "";
			        	totalCantAbc += colSalesCodProducto.size();
			        	totalSumaPrimaAbc += sumaPrima;
			        	totalComisionDecimalAbc += sumaComDecimal;
			        	//totalComisionPesosAbc += sumaComPesos;
			        }
		        }
		        else
		        {
		        	//NO HAY REGISTROS
			        row = sheetCalcCom.createRow(++rowCount);
			        cell = row.createCell(0);
		        	cell.setCellValue(Empresa);
		        	cell = row.createCell(1);
		        	cell.setCellValue(0);
		        	cell = row.createCell(2);
		        	cell.setCellValue(0);
		        	cell = row.createCell(3);
		        	cell.setCellValue(0d);
		        	cell = row.createCell(4);
		        	cell.setCellValue(0d);
		        	cell = row.createCell(5);
		        	cell.setCellValue(0);
		        	cell = row.createCell(6);
		        	cell.setCellValue(0);
		        	Empresa = "";
		        }
		        
		        //TOTAL ABC
		        
		        row = sheetCalcCom.createRow(++rowCount);
		        cell = row.createCell(0);
		        cell.setCellStyle(styles.get("texto_black"));
	        	cell.setCellValue("Total ABC");
		        cell = row.createCell(1);
		        cell.setCellStyle(styles.get("texto_black"));
	        	cell = row.createCell(2);
	        	cell.setCellStyle(styles.get("entero_black"));
	        	cell.setCellValue(totalCantAbc);
	        	cell = row.createCell(3);
	        	cell.setCellStyle(styles.get("decimal_black"));
	        	cell.setCellValue(totalSumaPrimaAbc);
	        	cell = row.createCell(4);
		        cell.setCellStyle(styles.get("texto_black"));
		        cell = row.createCell(5);
		        cell.setCellStyle(styles.get("texto_black"));
	        	cell = row.createCell(6);
	        	cell.setCellStyle(styles.get("pesos_black"));
	        	cell.setCellValue((int) Math.round(totalComisionDecimalAbc));
		        
		        //ABC *****************************************************	
		        
		        //DIN *****************************************************
		        //LISTADO DE CODIGOS DE PRODUCTOS PARA EL PERIODO
		        colProductos = salesRepository.findColProductoByPeriodCompanyEnterprise(this.month, this.year, this.companyId, 2);
		        Empresa = "DIN";
	            
		        if (colProductos.size() > 0) {
		            //FILAS SUMADAS POR CODIGO PRODUCTO
			        for (Integer codProd: colProductos)
			        {
			        	row = sheetCalcCom.createRow(++rowCount);
			            
			        	cell = row.createCell(0);
			            cell.setCellValue(Empresa);
			            
			        	List<Sale> colSalesCodProducto = salesRepository.findByPeriodCompanyEnterpriseCodProducto(this.month, this.year, this.companyId, 2, codProd, "Desafiliada");
			        	Double sumaPrima = 0d;
			        	Double ufDoubleValue = Double.parseDouble(this.UFValue.replace(",", "."));
			        	
			        	for (Sale sale : colSalesCodProducto)
			        		sumaPrima += sale.getPrimaAnualizada();
			        	
			        	List<Commission> colCommisionCodProducto = commissionRepository.findByCompanyCodProducto(this.companyId, codProd);
			        	Double comPrimaAnt = colCommisionCodProducto.size() > 0 ? colCommisionCodProducto.get(0).getComFijaAnticipada() : 0d;
			        	Integer comPrimaAntPesos = (int) Math.round(ufDoubleValue * comPrimaAnt);
			        	Double sumaComDecimal = (ufDoubleValue * comPrimaAnt) * colSalesCodProducto.size();
			        	Integer sumaComPesos = (int) Math.round(sumaComDecimal);
			        	
			        	cell = row.createCell(1);
			        	cell.setCellValue(codProd);
			        	cell = row.createCell(2);
			        	cell.setCellValue(colSalesCodProducto.size());
			        	cell = row.createCell(3);
			        	cell.setCellValue(sumaPrima);
			        	cell = row.createCell(4);
			        	cell.setCellValue(comPrimaAnt);
			        	cell = row.createCell(5);
			        	cell.setCellStyle(styles.get("pesos_white"));
			        	cell.setCellValue(comPrimaAntPesos);
			        	cell = row.createCell(6);
			        	cell.setCellStyle(styles.get("pesos_white"));
			        	cell.setCellValue(sumaComPesos);
			        	
			        	Empresa = "";
			        	totalCantDin += colSalesCodProducto.size();
			        	totalSumaPrimaDin += sumaPrima;
			        	totalComisionDecimalDin += sumaComDecimal;
			        	//totalComisionPesosDin += sumaComPesos;
			        }
		        }
		        else
		        {
		        	//NO HAY REGISTROS
			        row = sheetCalcCom.createRow(++rowCount);
			        cell = row.createCell(0);
		        	cell.setCellValue(Empresa);
		        	cell = row.createCell(1);
		        	cell.setCellValue(0);
		        	cell = row.createCell(2);
		        	cell.setCellValue(0);
		        	cell = row.createCell(3);
		        	cell.setCellValue(0d);
		        	cell = row.createCell(4);
		        	cell.setCellValue(0d);
		        	cell = row.createCell(5);
		        	cell.setCellValue(0);
		        	cell = row.createCell(6);
		        	cell.setCellValue(0);
		        	Empresa = "";
		        }
		        
		        //TOTAL DIN
		        
		        row = sheetCalcCom.createRow(++rowCount);
		        cell = row.createCell(0);
		        cell.setCellStyle(styles.get("texto_black"));
	        	cell.setCellValue("Total DIN");
	        	cell = row.createCell(1);
	        	cell.setCellStyle(styles.get("texto_black"));
	        	cell = row.createCell(2);
	        	cell.setCellStyle(styles.get("entero_black"));
	        	cell.setCellValue(totalCantDin);
	        	cell = row.createCell(3);
	        	cell.setCellStyle(styles.get("decimal_black"));
	        	cell.setCellValue(totalSumaPrimaDin);
	        	cell = row.createCell(4);
	        	cell.setCellStyle(styles.get("texto_black"));
	        	cell = row.createCell(5);
	        	cell.setCellStyle(styles.get("texto_black"));
	        	cell = row.createCell(6);
	        	cell.setCellStyle(styles.get("pesos_black"));
	        	cell.setCellValue((int) Math.round(totalComisionDecimalDin));
		        
		        //DIN *****************************************************
		        
		        //TOTAL GENERAL
	        	
	        	row = sheetCalcCom.createRow(++rowCount);
		        cell = row.createCell(0);
	        	cell.setCellValue("Total general");
	        	cell = row.createCell(1);
	        	cell.setCellStyle(styles.get("texto_black"));
	        	cell = row.createCell(2);
	        	cell.setCellStyle(styles.get("entero_black"));
	        	cell.setCellValue(totalCantAbc + totalCantDin);
	        	cell = row.createCell(3);
	        	cell.setCellStyle(styles.get("decimal_black"));
	        	cell.setCellValue(totalSumaPrimaAbc + totalSumaPrimaDin);
	        	cell = row.createCell(4);
	        	cell.setCellStyle(styles.get("texto_black"));
	        	cell = row.createCell(5);
	        	cell.setCellStyle(styles.get("texto_black"));
	        	cell = row.createCell(6);
	        	cell.setCellStyle(styles.get("pesos_black"));
	        	cell.setCellValue((int) Math.round(totalComisionDecimalAbc + totalComisionDecimalDin));
	        }
	        
	        if (this.companyId == 1 || this.companyId == 2)
	        {
	        	rowCount = rowCount + 2;
	        	
	        	//UF
	        	row = sheetCalcCom.createRow(rowCount);
	        	cell = row.createCell(0);
	        	cell.setCellStyle(styles.get("texto_grey"));
	        	cell.setCellValue("UF al " + maxMonthDay + " de " + this.colMonths.get(month));
	        	cell = row.createCell(1);
	        	cell.setCellStyle(styles.get("decimal_grey"));
	        	cell.setCellValue(this.UFValue);
	        	
	        	rowCount = rowCount + 3;
	        }
            
	        //RECAUDACION*****************************************************************
	        
	        Integer totalCantRecAbc = 0;
	        Integer totalSumaMontoAbc = 0;
	        Double totalSumaMontoPrimaNetaAbc = 0d;
	        Double totalComisionIntNetoAbc = 0d;
	        Double totalComisionIntBrutoAbc = 0d;
	        Double totalComisionRecaudaAbc = 0d;
	        Double totalComisionRecaudaBrutoAbc = 0d;
	        
	        Integer totalCantRecDin = 0;
	        Integer totalSumaMontoDin = 0;
	        Double totalSumaMontoPrimaNetaDin = 0d;
	        Double totalComisionIntNetoDin = 0d;
	        Double totalComisionIntBrutoDin = 0d;
	        Double totalComisionRecaudaDin = 0d;
	        Double totalComisionRecaudaBrutoDin = 0d;
	        
	        Integer totalCantRecHDIAbc = 0;
	        Integer totalSumaMontoHDIAbc = 0;
	        Double totalSumaMontoPrimaNetaHDIAbc = 0d;
	        Double totalComisionIntNetoHDIAbc = 0d;
	        Double totalComisionIntBrutoHDIAbc = 0d;
	        Double totalComisionRecaudaHDIAbc = 0d;
	        
	        Integer totalCantRecHDIDin = 0;
	        Integer totalSumaMontoHDIDin = 0;
	        Double totalSumaMontoPrimaNetaHDIDin = 0d;
	        Double totalComisionIntNetoHDIDin = 0d;
	        Double totalComisionIntBrutoHDIDin = 0d;
	        Double totalComisionRecaudaHDIDin = 0d;
	        
	        Integer totalCantRecTele = 0;
	        Integer totalSumaMontoTele = 0;
	        Double totalSumaMontoPrimaNetaTele = 0d;
	        Double totalComisionIntNetoTele = 0d;
	        Double totalComisionIntBrutoTele = 0d;
	        Double totalComisionRecaudaTele = 0d;
	        
	        Integer totalCantRecPres = 0;
	        Integer totalSumaMontoPres = 0;
	        Double totalSumaMontoPrimaNetaPres = 0d;
	        Double totalComisionIntNetoPres = 0d;
	        Double totalComisionIntBrutoPres = 0d;
	        Double totalComisionRecaudaPres = 0d;
	        
	        Double sumaMontoPrimaNetaDin = 0d;
	        Double comisionIntNetoDin = 0d;
	        Double comisionIntBrutoDin = 0d;
	        Double comisionRecaudaDin = 0d;
	        Double comisionRecaudaBrutoDin = 0d;
	        
	        Double sumaMontoPrimaNetaAbc = 0d;
	        Double comisionIntNetoAbc = 0d;
	        Double comisionIntBrutoAbc = 0d;
	        Double comisionRecaudaAbc = 0d;
	        Double comisionRecaudaBrutoAbc = 0d;
	        
	        Double sumaMontoPrimaNetaUFAbc = 0d;
	        Double sumaMontoPrimaBrutaUFAbc = 0d;
	        Double sumaPrimaNetaPesosBICEAbc = 0d;
	        
	        Double sumaMontoPrimaNetaUFDin = 0d;
	        Double sumaMontoPrimaBrutaUFDin = 0d;
	        Double sumaPrimaNetaPesosBICEDin = 0d;
	        
	        Double totalSumaMontoPrimaNetaUFAbc = 0d;
	        Double totalSumaMontoPrimaBrutaUFAbc = 0d;
	        Double totalSumaPrimaNetaPesosBICEAbc = 0d;
	        
	        Double totalSumaMontoPrimaNetaUFDin = 0d;
	        Double totalSumaMontoPrimaBrutaUFDin = 0d;
	        Double totalSumaPrimaNetaPesosBICEDin = 0d;
	        
	        List<Commission> colCommisionCodProducto = null;
	        
	        Double ufDoubleValue = 0d;
	        
	        if (this.UFValue != null && this.UFValue.trim().length() > 0)
	        	ufDoubleValue = Double.parseDouble(this.UFValue.replace(",", "."));
        	
        	//HEADER RECAUDACION
	        row = sheetCalcCom.createRow(++rowCount);
	        cell = row.createCell(0);
	        cell.setCellStyle(styles.get("texto_black"));
	        cell.setCellValue("Comisión sobre la Prima Recaudada " + this.colMonths.get(month) + " " + this.year);
	        cell = row.createCell(1);
	        cell.setCellStyle(styles.get("texto_black"));
	        cell = row.createCell(2);
	        cell.setCellStyle(styles.get("texto_black"));
	        cell = row.createCell(3);
	        cell.setCellStyle(styles.get("texto_black"));
	        cell = row.createCell(4);
	        cell.setCellStyle(styles.get("texto_black"));
	        cell = row.createCell(5);
	        cell.setCellStyle(styles.get("texto_black"));
	        cell = row.createCell(6);
	        cell.setCellStyle(styles.get("texto_black"));
	        cell = row.createCell(7);
	        cell.setCellStyle(styles.get("texto_black"));
	        
	        if (this.companyId == 4) { //BICE
		        cell = row.createCell(8);
		        cell.setCellStyle(styles.get("texto_black"));
		        cell = row.createCell(9);
		        cell.setCellStyle(styles.get("texto_black"));
		        cell = row.createCell(10);
		        cell.setCellStyle(styles.get("texto_black"));
	        }
	        
	        if (this.companyId == 5) { //CARDIF
		        cell = row.createCell(8);
		        cell.setCellStyle(styles.get("texto_black"));
	        }
	        
	        if (this.companyId == 2) { //OHIO
	        	row = sheetCalcCom.createRow(++rowCount);
    	        cell = row.createCell(0);
    	        cell.setCellStyle(styles.get("texto_grey_wrapped"));
                cell.setCellValue("Empresa");
                cell = row.createCell(1);
                cell.setCellStyle(styles.get("texto_grey_wrapped"));
                cell.setCellValue("Rubro");
                cell = row.createCell(2);
                cell.setCellStyle(styles.get("texto_grey_wrapped"));
                cell.setCellValue("Cuenta de Rut");
                cell = row.createCell(3);
                cell.setCellStyle(styles.get("texto_grey_wrapped"));
                cell.setCellValue("Suma de Monto prima final");
                cell = row.createCell(4);
                cell.setCellStyle(styles.get("texto_grey_wrapped"));
                cell.setCellValue("Suma de PRIMA NETA");
                cell = row.createCell(5);
                cell.setCellStyle(styles.get("texto_grey_wrapped"));
                cell.setCellValue("Com. Int. Neta");
                cell = row.createCell(6);
                cell.setCellStyle(styles.get("texto_grey_wrapped"));
                cell.setCellValue("Com. Int. Bruta");
                cell = row.createCell(7);
                cell.setCellStyle(styles.get("texto_grey_wrapped"));
                cell.setCellValue("Com. Cob");
	        }
	        else if (this.companyId == 4) { //BICE
	        	row = sheetCalcCom.createRow(++rowCount);
		        cell = row.createCell(0);
		        cell.setCellStyle(styles.get("texto_grey_wrapped"));
	            cell.setCellValue("Empresa");
	            cell = row.createCell(1);
	            cell.setCellStyle(styles.get("texto_grey_wrapped"));
	            cell.setCellValue("Rubro");
	            cell = row.createCell(2);
	            cell.setCellStyle(styles.get("texto_grey_wrapped"));
	            cell.setCellValue("Cuenta de Rut");
	            cell = row.createCell(3);
	            cell.setCellStyle(styles.get("texto_grey_wrapped"));
	            cell.setCellValue("Suma de Monto");
	            cell = row.createCell(4);
	            cell.setCellStyle(styles.get("texto_grey_wrapped"));
	            cell.setCellValue("Suma de Monto prima pagada neta (Neto)");
	            cell = row.createCell(5);
	            cell.setCellStyle(styles.get("texto_grey_wrapped"));
	            cell.setCellValue("Suma de Monto prima NETA UF");
	            cell = row.createCell(6);
	            cell.setCellStyle(styles.get("texto_grey_wrapped"));
	            cell.setCellValue("Suma de Monto prima BRUTA UF");
	            cell = row.createCell(7);
	            cell.setCellStyle(styles.get("texto_grey_wrapped"));
	            cell.setCellValue("Prima NETA en pesos");
	            cell = row.createCell(8);
	            cell.setCellStyle(styles.get("texto_grey_wrapped"));
	            cell.setCellValue("Comisión de intermediación sobre la recaudación NETO");
	            cell = row.createCell(9);
	            cell.setCellStyle(styles.get("texto_grey_wrapped"));
	            cell.setCellValue("Comisión de intermediación sobre la recaudación BRUTO");
	            cell = row.createCell(10);
	            cell.setCellStyle(styles.get("texto_grey_wrapped"));
	            cell.setCellValue("Comisión de recaudación sobre la recaudación NETO");
	        }
	        else {
	        	row = sheetCalcCom.createRow(++rowCount);
		        cell = row.createCell(0);
		        cell.setCellStyle(styles.get("texto_grey_wrapped"));
	            cell.setCellValue("Empresa");
	            cell = row.createCell(1);
	            cell.setCellStyle(styles.get("texto_grey_wrapped"));
	            cell.setCellValue("Rubro");
	            cell = row.createCell(2);
	            cell.setCellStyle(styles.get("texto_grey_wrapped"));
	            cell.setCellValue("Cuenta de Rut");
	            cell = row.createCell(3);
	            cell.setCellStyle(styles.get("texto_grey_wrapped"));
	            cell.setCellValue("Suma de Monto");
	            cell = row.createCell(4);
	            cell.setCellStyle(styles.get("texto_grey_wrapped"));
	            cell.setCellValue("Suma de Monto prima neta (Neto)");
	            cell = row.createCell(5);
	            cell.setCellStyle(styles.get("texto_grey_wrapped"));
	            cell.setCellValue("Comisión de intermediación sobre la recaudación NETO");
	            cell = row.createCell(6);
	            cell.setCellStyle(styles.get("texto_grey_wrapped"));
	            cell.setCellValue("Comisión de intermediación sobre la recaudación BRUTO");
	            cell = row.createCell(7);
	            cell.setCellStyle(styles.get("texto_grey_wrapped"));
	            cell.setCellValue("Comisión de recaudación sobre la recaudación");
	            if (this.companyId == 5) {
	        		cell = row.createCell(8);
		        	cell.setCellStyle(styles.get("texto_grey_wrapped"));
		        	cell.setCellValue("Comisión de recaudación sobre la recaudación BRUTO");
	        	}
	        }
        	
        	//ABC *****************************************************
	        //LISTADO DE CODIGOS DE PRODUCTOS PARA EL PERIODO
	        List<Integer> colRubros = collectionRepository.findRubroByPeriodCompanyEnterprise(this.month, this.year, this.companyId, 1);
	        Empresa = "ABC";
            
	        if (colRubros.size() > 0) {
	            //FILAS SUMADAS POR RUBRO
		        for (Integer rubro: colRubros)
		        {
		        	row = sheetCalcCom.createRow(++rowCount);
		            
		        	cell = row.createCell(0);
		            cell.setCellValue(Empresa);
		            
		            Integer sumaMontoAbc = 0;
		        	List<Collection> colCollectionsRubro = collectionRepository.findByPeriodCompanyEnterpriseRubro(this.month, this.year, this.companyId, 1, rubro);
		        	
		        	for (Collection col : colCollectionsRubro)
		        		sumaMontoAbc += col.getMonto();
		        	
		        	//El cálculo de comisiones se hace diferente para OHIO
		        	if (this.companyId == 2) {
		        		colCommisionCodProducto = commissionRepository.findByCompanyEnterpriseRubro(this.companyId, 1, rubro);
		        		if (rubro == 58) {
		        			sumaMontoPrimaNetaAbc = (sumaMontoAbc / 2) * (colCommisionCodProducto.size() > 0 ? colCommisionCodProducto.get(0).getPorcNeta() : 0d);
		        			sumaMontoAbc = Math.round(sumaMontoAbc / 2);
		        		}
		        		else
		        			sumaMontoPrimaNetaAbc = sumaMontoAbc * (colCommisionCodProducto.size() > 0 ? colCommisionCodProducto.get(0).getPorcNeta() : 0d);
    			        comisionIntNetoAbc = sumaMontoPrimaNetaAbc * (colCommisionCodProducto.size() > 0 ? colCommisionCodProducto.get(0).getComCorredora() : 0d);
    			        comisionIntBrutoAbc = comisionIntNetoAbc * 1.19;
    			        comisionRecaudaAbc = sumaMontoPrimaNetaAbc * (colCommisionCodProducto.size() > 0 ? colCommisionCodProducto.get(0).getComRecCobranza() : 0d);
		        	}
		        	else if (this.companyId == 6) { //El cálculo de comisiones se hace diferente para METLIFE
		        		colCommisionCodProducto = commissionRepository.findByCompanyRubro(this.companyId, rubro);
    			        sumaMontoPrimaNetaAbc = sumaMontoAbc / 1.19;
    			        comisionIntNetoAbc = sumaMontoPrimaNetaAbc * (colCommisionCodProducto.size() > 0 ? colCommisionCodProducto.get(0).getComCorredora() : 0d);
    			        comisionIntBrutoAbc = comisionIntNetoAbc * 1.19;
    			        comisionRecaudaAbc = sumaMontoAbc * (colCommisionCodProducto.size() > 0 ? colCommisionCodProducto.get(0).getComRecCobranza() : 0d);
		        	}
		        	else if (this.companyId == 4) { //Calculo diferente para BICE
		        		colCommisionCodProducto = commissionRepository.findByCompanyRubro(this.companyId, rubro);
		        		sumaMontoPrimaNetaAbc = sumaMontoAbc / 1.19;
		        		sumaMontoPrimaNetaUFAbc = colCollectionsRubro.size() * colCommisionCodProducto.get(0).getPrimaNeta();
		        		sumaMontoPrimaBrutaUFAbc = colCollectionsRubro.size() * colCommisionCodProducto.get(0).getPrimaBruta();
		        		sumaPrimaNetaPesosBICEAbc = sumaMontoPrimaNetaUFAbc * ufDoubleValue;
    			        comisionIntNetoAbc = sumaPrimaNetaPesosBICEAbc * (colCommisionCodProducto.size() > 0 ? colCommisionCodProducto.get(0).getComCorredora() : 0d);
    			        comisionIntBrutoAbc = comisionIntNetoAbc * 1.19;
    			        comisionRecaudaAbc = sumaPrimaNetaPesosBICEAbc * (colCommisionCodProducto.size() > 0 ? colCommisionCodProducto.get(0).getComRecCobranza() : 0d);
		        	}
		        	else {
		        		colCommisionCodProducto = commissionRepository.findByCompanyRubro(this.companyId, rubro);
    			        sumaMontoPrimaNetaAbc = sumaMontoAbc / 1.19;
    			        comisionIntNetoAbc = sumaMontoPrimaNetaAbc * (colCommisionCodProducto.size() > 0 ? colCommisionCodProducto.get(0).getComCorredora() : 0d);
    			        comisionIntBrutoAbc = comisionIntNetoAbc * 1.19;
    			        comisionRecaudaAbc = sumaMontoPrimaNetaAbc * (colCommisionCodProducto.size() > 0 ? colCommisionCodProducto.get(0).getComRecCobranza() : 0d);
    			        comisionRecaudaBrutoAbc = sumaMontoAbc * (colCommisionCodProducto.size() > 0 ? colCommisionCodProducto.get(0).getComRecCobranza() : 0d);
		        	}
		        	
		        	if (this.companyId == 4) { //BICE
			        	cell = row.createCell(1);
			        	cell.setCellValue(rubro);
			        	cell = row.createCell(2);
			        	cell.setCellValue(colCollectionsRubro.size());
			        	cell = row.createCell(3);
			        	cell.setCellStyle(styles.get("pesos_white"));
			        	cell.setCellValue(sumaMontoAbc);
			        	cell = row.createCell(4);
			        	cell.setCellStyle(styles.get("pesos_white"));
			        	cell.setCellValue(sumaMontoPrimaNetaAbc);
			        	cell = row.createCell(5);
			        	cell.setCellStyle(styles.get("decimal_white"));
			        	cell.setCellValue(sumaMontoPrimaNetaUFAbc);
			        	cell = row.createCell(6);
			        	cell.setCellStyle(styles.get("decimal_white"));
			        	cell.setCellValue(sumaMontoPrimaBrutaUFAbc);
			        	cell = row.createCell(7);
			        	cell.setCellStyle(styles.get("pesos_white"));
			        	cell.setCellValue(sumaPrimaNetaPesosBICEAbc);
			        	cell = row.createCell(8);
			        	cell.setCellStyle(styles.get("pesos_white"));
			        	cell.setCellValue(comisionIntNetoAbc);
			        	cell = row.createCell(9);
			        	cell.setCellStyle(styles.get("pesos_white"));
			        	cell.setCellValue(comisionIntBrutoAbc);
			        	cell = row.createCell(10);
			        	cell.setCellStyle(styles.get("pesos_white"));
			        	cell.setCellValue(comisionRecaudaAbc);
		        	}
		        	else {
		        		cell = row.createCell(1);
			        	cell.setCellValue(rubro);
			        	cell = row.createCell(2);
			        	cell.setCellValue(colCollectionsRubro.size());
			        	cell = row.createCell(3);
			        	cell.setCellStyle(styles.get("pesos_white"));
			        	cell.setCellValue(sumaMontoAbc);
			        	cell = row.createCell(4);
			        	cell.setCellStyle(styles.get("pesos_white"));
			        	cell.setCellValue(sumaMontoPrimaNetaAbc);
			        	cell = row.createCell(5);
			        	cell.setCellStyle(styles.get("pesos_white"));
			        	cell.setCellValue(comisionIntNetoAbc);
			        	cell = row.createCell(6);
			        	cell.setCellStyle(styles.get("pesos_white"));
			        	cell.setCellValue(comisionIntBrutoAbc);
			        	cell = row.createCell(7);
			        	cell.setCellStyle(styles.get("pesos_white"));
			        	cell.setCellValue(comisionRecaudaAbc);
			        	if (this.companyId == 5) {
			        		cell = row.createCell(8);
				        	cell.setCellStyle(styles.get("pesos_white"));
				        	cell.setCellValue(comisionRecaudaBrutoAbc);
			        	}
		        	}
		        	
		        	totalCantRecAbc += colCollectionsRubro.size();
		        	totalSumaMontoAbc += sumaMontoAbc;
		        	totalSumaMontoPrimaNetaAbc += sumaMontoPrimaNetaAbc;
		        	totalComisionIntNetoAbc += comisionIntNetoAbc;
		        	totalComisionIntBrutoAbc += comisionIntBrutoAbc;
		        	totalComisionRecaudaAbc += comisionRecaudaAbc;
		        	totalComisionRecaudaBrutoAbc += comisionRecaudaBrutoAbc;
		        	totalSumaMontoPrimaNetaUFAbc += sumaMontoPrimaNetaUFAbc;
			        totalSumaMontoPrimaBrutaUFAbc += sumaMontoPrimaBrutaUFAbc;
			        totalSumaPrimaNetaPesosBICEAbc += sumaPrimaNetaPesosBICEAbc;
		        	
		        	if (rubro == 70 || rubro == 71 || rubro == 90 || rubro == 91) {
		        		totalCantRecTele += colCollectionsRubro.size();
			        	totalSumaMontoTele += sumaMontoAbc;
			        	totalSumaMontoPrimaNetaTele += sumaMontoPrimaNetaAbc;
			        	totalComisionIntNetoTele += comisionIntNetoAbc;
			        	totalComisionIntBrutoTele += comisionIntBrutoAbc;
			        	totalComisionRecaudaTele += comisionRecaudaAbc;
		        	}
		        	else {
		        		totalCantRecPres += colCollectionsRubro.size();
			        	totalSumaMontoPres += sumaMontoAbc;
			        	totalSumaMontoPrimaNetaPres += sumaMontoPrimaNetaAbc;
			        	totalComisionIntNetoPres += comisionIntNetoAbc;
			        	totalComisionIntBrutoPres += comisionIntBrutoAbc;
			        	totalComisionRecaudaPres += comisionRecaudaAbc;
		        	}
		        	
		        	Empresa = "";
		        }
		    }
	        else
	        {
	        	//NO HAY REGISTROS
		        row = sheetCalcCom.createRow(++rowCount);
		        cell = row.createCell(0);
	        	cell.setCellValue(Empresa);
	        	cell = row.createCell(1);
	        	cell.setCellValue(0);
	        	cell = row.createCell(2);
	        	cell.setCellValue(0);
	        	cell = row.createCell(3);
	        	cell.setCellValue(0);
	        	cell = row.createCell(4);
	        	cell.setCellValue(0d);
	        	cell = row.createCell(5);
	        	cell.setCellValue(0d);
	        	cell = row.createCell(6);
	        	cell.setCellValue(0d);
	        	cell = row.createCell(7);
	        	cell.setCellValue(0d);
	        	Empresa = "";
	        }
	        
	        //TOTAL ABC
	        
	        if (this.companyId == 4) { //BICE
	        	row = sheetCalcCom.createRow(++rowCount);
		        cell = row.createCell(0);
		        cell.setCellStyle(styles.get("texto_black"));
	        	cell.setCellValue("Total ABC");
	        	cell = row.createCell(1);
	        	cell.setCellStyle(styles.get("texto_black"));
	        	cell = row.createCell(2);
	        	cell.setCellStyle(styles.get("entero_black"));
	        	cell.setCellValue(totalCantRecAbc);
	        	cell = row.createCell(3);
	        	cell.setCellStyle(styles.get("pesos_black"));
	        	cell.setCellValue(totalSumaMontoAbc);
	        	cell = row.createCell(4);
	        	cell.setCellStyle(styles.get("pesos_black"));
	        	cell.setCellValue(totalSumaMontoPrimaNetaAbc);
	        	cell = row.createCell(5);
	        	cell.setCellStyle(styles.get("decimal_black"));
	        	cell.setCellValue(totalSumaMontoPrimaNetaUFAbc);
	        	cell = row.createCell(6);
	        	cell.setCellStyle(styles.get("decimal_black"));
	        	cell.setCellValue(totalSumaMontoPrimaBrutaUFAbc);
	        	cell = row.createCell(7);
	        	cell.setCellStyle(styles.get("pesos_black"));
	        	cell.setCellValue(totalSumaPrimaNetaPesosBICEAbc);
	        	cell = row.createCell(8);
	        	cell.setCellStyle(styles.get("pesos_black"));
	        	cell.setCellValue(totalComisionIntNetoAbc);
	        	cell = row.createCell(9);
	        	cell.setCellStyle(styles.get("pesos_black"));
	        	cell.setCellValue(totalComisionIntBrutoAbc);
	        	cell = row.createCell(10);
	        	cell.setCellStyle(styles.get("pesos_black"));
	        	cell.setCellValue(totalComisionRecaudaAbc);
	        }
	        else {
		        row = sheetCalcCom.createRow(++rowCount);
		        cell = row.createCell(0);
		        cell.setCellStyle(styles.get("texto_black"));
	        	cell.setCellValue("Total ABC");
	        	cell = row.createCell(1);
	        	cell.setCellStyle(styles.get("texto_black"));
	        	cell = row.createCell(2);
	        	cell.setCellStyle(styles.get("entero_black"));
	        	cell.setCellValue(totalCantRecAbc);
	        	cell = row.createCell(3);
	        	cell.setCellStyle(styles.get("pesos_black"));
	        	cell.setCellValue(totalSumaMontoAbc);
	        	cell = row.createCell(4);
	        	cell.setCellStyle(styles.get("pesos_black"));
	        	cell.setCellValue(totalSumaMontoPrimaNetaAbc);
	        	cell = row.createCell(5);
	        	cell.setCellStyle(styles.get("pesos_black"));
	        	cell.setCellValue(totalComisionIntNetoAbc);
	        	cell = row.createCell(6);
	        	cell.setCellStyle(styles.get("pesos_black"));
	        	cell.setCellValue(totalComisionIntBrutoAbc);
	        	cell = row.createCell(7);
	        	cell.setCellStyle(styles.get("pesos_black"));
	        	cell.setCellValue(totalComisionRecaudaAbc);
	        	if (this.companyId == 5) {
	        		cell = row.createCell(8);
		        	cell.setCellStyle(styles.get("pesos_black"));
		        	cell.setCellValue(totalComisionRecaudaBrutoAbc);
	        	}
	        }
	        
	        //ABC *****************************************************	
	        
        	//DIN *****************************************************
	        //LISTADO DE CODIGOS DE PRODUCTOS PARA EL PERIODO
	        colRubros = collectionRepository.findRubroByPeriodCompanyEnterprise(this.month, this.year, this.companyId, 2);
	        Empresa = "DIN";
            
	        if (colRubros.size() > 0) {
	            //FILAS SUMADAS POR RUBRO
		        for (Integer rubro: colRubros)
		        {
		        	row = sheetCalcCom.createRow(++rowCount);
		            
		        	cell = row.createCell(0);
		            cell.setCellValue(Empresa);
		            
		            Integer sumaMontoDin = 0;
		        	List<Collection> colCollectionsRubro = collectionRepository.findByPeriodCompanyEnterpriseRubro(this.month, this.year, this.companyId, 2	, rubro);
		        	
		        	for (Collection col : colCollectionsRubro)
		        		sumaMontoDin += col.getMonto();
		        	
		        	//El cálculo de comisiones se hace diferente para OHIO
		        	if (this.companyId == 2) {
		        		colCommisionCodProducto = commissionRepository.findByCompanyEnterpriseRubro(this.companyId, 2, rubro);
		        		if (rubro == 58) {
		        			sumaMontoPrimaNetaDin = (sumaMontoDin / 2) * (colCommisionCodProducto.size() > 0 ? colCommisionCodProducto.get(0).getPorcNeta() : 0d);
		        			sumaMontoDin = Math.round(sumaMontoDin / 2);
		        		}
		        		else
		        			sumaMontoPrimaNetaDin = sumaMontoDin * (colCommisionCodProducto.size() > 0 ? colCommisionCodProducto.get(0).getPorcNeta() : 0d);
    			        comisionIntNetoDin = sumaMontoPrimaNetaDin * (colCommisionCodProducto.size() > 0 ? colCommisionCodProducto.get(0).getComCorredora() : 0d);
    			        comisionIntBrutoDin = comisionIntNetoDin * 1.19;
    			        comisionRecaudaDin = sumaMontoPrimaNetaDin * (colCommisionCodProducto.size() > 0 ? colCommisionCodProducto.get(0).getComRecCobranza() : 0d);
		        	}
		        	else if (this.companyId == 6){ //El cálculo de comisiones se hace diferente para METLIFE
		        		colCommisionCodProducto = commissionRepository.findByCompanyRubro(this.companyId, rubro);
    			        sumaMontoPrimaNetaDin = sumaMontoDin / 1.19;
    			        comisionIntNetoDin = sumaMontoPrimaNetaDin * (colCommisionCodProducto.size() > 0 ? colCommisionCodProducto.get(0).getComCorredora() : 0d);
    			        comisionIntBrutoDin = comisionIntNetoDin * 1.19;
    			        comisionRecaudaDin = sumaMontoDin * (colCommisionCodProducto.size() > 0 ? colCommisionCodProducto.get(0).getComRecCobranza() : 0d);
		        	}
		        	else if (this.companyId == 4) { //Calculo diferente para BICE
		        		colCommisionCodProducto = commissionRepository.findByCompanyRubro(this.companyId, rubro);
		        		sumaMontoPrimaNetaDin = sumaMontoDin / 1.19;
		        		sumaMontoPrimaNetaUFDin = colCollectionsRubro.size() * colCommisionCodProducto.get(0).getPrimaNeta();
		        		sumaMontoPrimaBrutaUFDin = colCollectionsRubro.size() * colCommisionCodProducto.get(0).getPrimaBruta();
		        		sumaPrimaNetaPesosBICEDin = sumaMontoPrimaNetaUFDin * ufDoubleValue;
    			        comisionIntNetoDin = sumaPrimaNetaPesosBICEDin * (colCommisionCodProducto.size() > 0 ? colCommisionCodProducto.get(0).getComCorredora() : 0d);
    			        comisionIntBrutoDin = comisionIntNetoDin * 1.19;
    			        comisionRecaudaDin = sumaPrimaNetaPesosBICEDin * (colCommisionCodProducto.size() > 0 ? colCommisionCodProducto.get(0).getComRecCobranza() : 0d);
		        	}
		        	else {
		        		colCommisionCodProducto = commissionRepository.findByCompanyRubro(this.companyId, rubro);
    			        sumaMontoPrimaNetaDin = sumaMontoDin / 1.19;
    			        comisionIntNetoDin = sumaMontoPrimaNetaDin * (colCommisionCodProducto.size() > 0 ? colCommisionCodProducto.get(0).getComCorredora() : 0d);
    			        comisionIntBrutoDin = comisionIntNetoDin * 1.19;
    			        comisionRecaudaDin = sumaMontoPrimaNetaDin * (colCommisionCodProducto.size() > 0 ? colCommisionCodProducto.get(0).getComRecCobranza() : 0d);
    			        comisionRecaudaBrutoDin = sumaMontoDin * (colCommisionCodProducto.size() > 0 ? colCommisionCodProducto.get(0).getComRecCobranza() : 0d);
		        	}
		        	
		        	if (this.companyId == 4) { //BICE
			        	cell = row.createCell(1);
			        	cell.setCellValue(rubro);
			        	cell = row.createCell(2);
			        	cell.setCellValue(colCollectionsRubro.size());
			        	cell = row.createCell(3);
			        	cell.setCellStyle(styles.get("pesos_white"));
			        	cell.setCellValue(sumaMontoDin);
			        	cell = row.createCell(4);
			        	cell.setCellStyle(styles.get("pesos_white"));
			        	cell.setCellValue(sumaMontoPrimaNetaDin);
			        	cell = row.createCell(5);
			        	cell.setCellStyle(styles.get("decimal_white"));
			        	cell.setCellValue(sumaMontoPrimaNetaUFDin);
			        	cell = row.createCell(6);
			        	cell.setCellStyle(styles.get("decimal_white"));
			        	cell.setCellValue(sumaMontoPrimaBrutaUFDin);
			        	cell = row.createCell(7);
			        	cell.setCellStyle(styles.get("pesos_white"));
			        	cell.setCellValue(sumaPrimaNetaPesosBICEDin);
			        	cell = row.createCell(8);
			        	cell.setCellStyle(styles.get("pesos_white"));
			        	cell.setCellValue(comisionIntNetoDin);
			        	cell = row.createCell(9);
			        	cell.setCellStyle(styles.get("pesos_white"));
			        	cell.setCellValue(comisionIntBrutoDin);
			        	cell = row.createCell(10);
			        	cell.setCellStyle(styles.get("pesos_white"));
			        	cell.setCellValue(comisionRecaudaDin);
		        	}
		        	else {
		        		cell = row.createCell(1);
			        	cell.setCellValue(rubro);
			        	cell = row.createCell(2);
			        	cell.setCellValue(colCollectionsRubro.size());
			        	cell = row.createCell(3);
			        	cell.setCellStyle(styles.get("pesos_white"));
			        	cell.setCellValue(sumaMontoDin);
			        	cell = row.createCell(4);
			        	cell.setCellStyle(styles.get("pesos_white"));
			        	cell.setCellValue(sumaMontoPrimaNetaDin);
			        	cell = row.createCell(5);
			        	cell.setCellStyle(styles.get("pesos_white"));
			        	cell.setCellValue(comisionIntNetoDin);
			        	cell = row.createCell(6);
			        	cell.setCellStyle(styles.get("pesos_white"));
			        	cell.setCellValue(comisionIntBrutoDin);
			        	cell = row.createCell(7);
			        	cell.setCellStyle(styles.get("pesos_white"));
			        	cell.setCellValue(comisionRecaudaDin);
			        	if (this.companyId == 5) {
			        		cell = row.createCell(8);
				        	cell.setCellStyle(styles.get("pesos_white"));
				        	cell.setCellValue(comisionRecaudaBrutoDin);
			        	}
		        	}
		        	
		        	totalCantRecDin += colCollectionsRubro.size();
		        	totalSumaMontoDin += sumaMontoDin;
		        	totalSumaMontoPrimaNetaDin += sumaMontoPrimaNetaDin;
		        	totalComisionIntNetoDin += comisionIntNetoDin;
		        	totalComisionIntBrutoDin += comisionIntBrutoDin;
		        	totalComisionRecaudaDin += comisionRecaudaDin;
		        	totalComisionRecaudaBrutoDin += comisionRecaudaBrutoDin;
		        	totalSumaMontoPrimaNetaUFDin += sumaMontoPrimaNetaUFDin;
			        totalSumaMontoPrimaBrutaUFDin += sumaMontoPrimaBrutaUFDin;
			        totalSumaPrimaNetaPesosBICEDin += sumaPrimaNetaPesosBICEDin;
		        	
		        	if (rubro == 70 || rubro == 71 || rubro == 90 || rubro == 91) {
		        		totalCantRecTele += colCollectionsRubro.size();
			        	totalSumaMontoTele += sumaMontoDin;
			        	totalSumaMontoPrimaNetaTele += sumaMontoPrimaNetaDin;
			        	totalComisionIntNetoTele += comisionIntNetoDin;
			        	totalComisionIntBrutoTele += comisionIntBrutoDin;
			        	totalComisionRecaudaTele += comisionRecaudaDin;
		        	}
		        	else {
		        		totalCantRecPres += colCollectionsRubro.size();
			        	totalSumaMontoPres += sumaMontoDin;
			        	totalSumaMontoPrimaNetaPres += sumaMontoPrimaNetaDin;
			        	totalComisionIntNetoPres += comisionIntNetoDin;
			        	totalComisionIntBrutoPres += comisionIntBrutoDin;
			        	totalComisionRecaudaPres += comisionRecaudaDin;
		        	}
		        	
		        	Empresa = "";
		        }
	        }
	        else
	        {
	        	//NO HAY REGISTROS
		        row = sheetCalcCom.createRow(++rowCount);
		        cell = row.createCell(0);
	        	cell.setCellValue(Empresa);
	        	cell = row.createCell(1);
	        	cell.setCellValue(0);
	        	cell = row.createCell(2);
	        	cell.setCellValue(0);
	        	cell = row.createCell(3);
	        	cell.setCellValue(0);
	        	cell = row.createCell(4);
	        	cell.setCellValue(0d);
	        	cell = row.createCell(5);
	        	cell.setCellValue(0d);
	        	cell = row.createCell(6);
	        	cell.setCellValue(0d);
	        	cell = row.createCell(7);
	        	cell.setCellValue(0d);
	        	Empresa = "";
	        }
	        
	        //TOTAL DIN
	        
	        if (this.companyId == 4) { //BICE
		        row = sheetCalcCom.createRow(++rowCount);
		        cell = row.createCell(0);
		        cell.setCellStyle(styles.get("texto_black"));
	        	cell.setCellValue("Total DIN");
	        	cell = row.createCell(1);
	        	cell.setCellStyle(styles.get("texto_black"));
	        	cell = row.createCell(2);
	        	cell.setCellStyle(styles.get("entero_black"));
	        	cell.setCellValue(totalCantRecDin);
	        	cell = row.createCell(3);
	        	cell.setCellStyle(styles.get("pesos_black"));
	        	cell.setCellValue(totalSumaMontoDin);
	        	cell = row.createCell(4);
	        	cell.setCellStyle(styles.get("pesos_black"));
	        	cell.setCellValue(totalSumaMontoPrimaNetaDin);
	        	cell = row.createCell(5);
	        	cell.setCellStyle(styles.get("decimal_black"));
	        	cell.setCellValue(totalSumaMontoPrimaNetaUFDin);
	        	cell = row.createCell(6);
	        	cell.setCellStyle(styles.get("decimal_black"));
	        	cell.setCellValue(totalSumaMontoPrimaBrutaUFDin);
	        	cell = row.createCell(7);
	        	cell.setCellStyle(styles.get("pesos_black"));
	        	cell.setCellValue(totalSumaPrimaNetaPesosBICEDin);
	        	cell = row.createCell(8);
	        	cell.setCellStyle(styles.get("pesos_black"));
	        	cell.setCellValue(totalComisionIntNetoDin);
	        	cell = row.createCell(9);
	        	cell.setCellStyle(styles.get("pesos_black"));
	        	cell.setCellValue(totalComisionIntBrutoDin);
	        	cell = row.createCell(10);
	        	cell.setCellStyle(styles.get("pesos_black"));
	        	cell.setCellValue(totalComisionRecaudaDin);
	        }
	        else {
	        	row = sheetCalcCom.createRow(++rowCount);
		        cell = row.createCell(0);
		        cell.setCellStyle(styles.get("texto_black"));
	        	cell.setCellValue("Total DIN");
	        	cell = row.createCell(1);
	        	cell.setCellStyle(styles.get("texto_black"));
	        	cell = row.createCell(2);
	        	cell.setCellStyle(styles.get("entero_black"));
	        	cell.setCellValue(totalCantRecDin);
	        	cell = row.createCell(3);
	        	cell.setCellStyle(styles.get("pesos_black"));
	        	cell.setCellValue(totalSumaMontoDin);
	        	cell = row.createCell(4);
	        	cell.setCellStyle(styles.get("pesos_black"));
	        	cell.setCellValue(totalSumaMontoPrimaNetaDin);
	        	cell = row.createCell(5);
	        	cell.setCellStyle(styles.get("pesos_black"));
	        	cell.setCellValue(totalComisionIntNetoDin);
	        	cell = row.createCell(6);
	        	cell.setCellStyle(styles.get("pesos_black"));
	        	cell.setCellValue(totalComisionIntBrutoDin);
	        	cell = row.createCell(7);
	        	cell.setCellStyle(styles.get("pesos_black"));
	        	cell.setCellValue(totalComisionRecaudaDin);
	        	if (this.companyId == 5) {
	        		cell = row.createCell(8);
		        	cell.setCellStyle(styles.get("pesos_black"));
		        	cell.setCellValue(totalComisionRecaudaBrutoDin);
	        	}
	        }
	        
	        //DIN *****************************************************	
	        
	        //TOTAL GENERAL
        	
	        if (this.companyId == 4) { //BICE
	        	row = sheetCalcCom.createRow(++rowCount);
		        cell = row.createCell(0);
		        cell.setCellStyle(styles.get("texto_black"));
	        	cell.setCellValue("Total general");
	        	cell = row.createCell(1);
		        cell.setCellStyle(styles.get("texto_black"));
	        	cell = row.createCell(2);
	        	cell.setCellStyle(styles.get("entero_black"));
	        	cell.setCellValue(totalCantRecDin + totalCantRecAbc);
	        	cell = row.createCell(3);
	        	cell.setCellStyle(styles.get("pesos_black"));
	        	cell.setCellValue(totalSumaMontoDin + totalSumaMontoAbc);
	        	cell = row.createCell(4);
	        	cell.setCellStyle(styles.get("pesos_black"));
	        	cell.setCellValue(totalSumaMontoPrimaNetaDin + totalSumaMontoPrimaNetaAbc);
	        	cell = row.createCell(5);
	        	cell.setCellStyle(styles.get("decimal_black"));
	        	cell.setCellValue(totalSumaMontoPrimaNetaUFDin + totalSumaMontoPrimaNetaUFAbc);
	        	cell = row.createCell(6);
	        	cell.setCellStyle(styles.get("decimal_black"));
	        	cell.setCellValue(totalSumaMontoPrimaBrutaUFDin + totalSumaMontoPrimaBrutaUFAbc);
	        	cell = row.createCell(7);
	        	cell.setCellStyle(styles.get("pesos_black"));
	        	cell.setCellValue(totalSumaPrimaNetaPesosBICEDin + totalSumaPrimaNetaPesosBICEAbc);
	        	cell = row.createCell(8);
	        	cell.setCellStyle(styles.get("pesos_black"));
	        	cell.setCellValue(totalComisionIntNetoDin + totalComisionIntNetoAbc);
	        	cell = row.createCell(9);
	        	cell.setCellStyle(styles.get("pesos_black"));
	        	cell.setCellValue(totalComisionIntBrutoDin + totalComisionIntBrutoAbc);
	        	cell = row.createCell(10);
	        	cell.setCellStyle(styles.get("pesos_black"));
	        	cell.setCellValue(totalComisionRecaudaDin + totalComisionRecaudaAbc);
	        }
	        else {
	        	row = sheetCalcCom.createRow(++rowCount);
		        cell = row.createCell(0);
		        cell.setCellStyle(styles.get("texto_black"));
	        	cell.setCellValue("Total general");
	        	cell = row.createCell(1);
		        cell.setCellStyle(styles.get("texto_black"));
	        	cell = row.createCell(2);
	        	cell.setCellStyle(styles.get("entero_black"));
	        	cell.setCellValue(totalCantRecDin + totalCantRecAbc);
	        	cell = row.createCell(3);
	        	cell.setCellStyle(styles.get("pesos_black"));
	        	cell.setCellValue(totalSumaMontoDin + totalSumaMontoAbc);
	        	cell = row.createCell(4);
	        	cell.setCellStyle(styles.get("pesos_black"));
	        	cell.setCellValue(totalSumaMontoPrimaNetaDin + totalSumaMontoPrimaNetaAbc);
	        	cell = row.createCell(5);
	        	cell.setCellStyle(styles.get("pesos_black"));
	        	cell.setCellValue(totalComisionIntNetoDin + totalComisionIntNetoAbc);
	        	cell = row.createCell(6);
	        	cell.setCellStyle(styles.get("pesos_black"));
	        	cell.setCellValue(totalComisionIntBrutoDin + totalComisionIntBrutoAbc);
	        	cell = row.createCell(7);
	        	cell.setCellStyle(styles.get("pesos_black"));
	        	cell.setCellValue(totalComisionRecaudaDin + totalComisionRecaudaAbc);
	        	if (this.companyId == 5) {
	        		cell = row.createCell(8);
		        	cell.setCellStyle(styles.get("pesos_black"));
		        	cell.setCellValue(totalComisionRecaudaBrutoDin + totalComisionRecaudaBrutoAbc);
	        	}
	        }
        	
        	
        	rowCount = rowCount + 3;
        	
        	//RECAUDACION HDI (ESPECIAL PARA OHIO)
        	if (this.companyId == 2)
	        {
	        	//HEADER
		        row = sheetCalcCom.createRow(++rowCount);
		        cell = row.createCell(0);
		        cell.setCellStyle(styles.get("texto_black"));
		        cell.setCellValue("Comisión sobre la Prima Recaudada " + this.colMonths.get(month) + " " + this.year + " - Antifraude HDI");
		        cell = row.createCell(1);
		        cell.setCellStyle(styles.get("texto_black"));
		        cell = row.createCell(2);
		        cell.setCellStyle(styles.get("texto_black"));
		        cell = row.createCell(3);
		        cell.setCellStyle(styles.get("texto_black"));
		        cell = row.createCell(4);
		        cell.setCellStyle(styles.get("texto_black"));
		        cell = row.createCell(5);
		        cell.setCellStyle(styles.get("texto_black"));
		        cell = row.createCell(6);
		        cell.setCellStyle(styles.get("texto_black"));
		        cell = row.createCell(7);
		        cell.setCellStyle(styles.get("texto_black"));
		        
		        
	        	row = sheetCalcCom.createRow(++rowCount);
		        cell = row.createCell(0);
		        cell.setCellStyle(styles.get("texto_grey_wrapped"));
	            cell.setCellValue("Empresa");
	            cell = row.createCell(1);
	            cell.setCellStyle(styles.get("texto_grey_wrapped"));
	            cell.setCellValue("Rubro");
	            cell = row.createCell(2);
	            cell.setCellStyle(styles.get("texto_grey_wrapped"));
	            cell.setCellValue("Cuenta de Rut");
	            cell = row.createCell(3);
	            cell.setCellStyle(styles.get("texto_grey_wrapped"));
	            cell.setCellValue("Suma de Monto");
	            cell = row.createCell(4);
	            cell.setCellStyle(styles.get("texto_grey_wrapped"));
	            cell.setCellValue("Suma de Monto prima neta (Neto)");
	            cell = row.createCell(5);
	            cell.setCellStyle(styles.get("texto_grey_wrapped"));
	            cell.setCellValue("Comisión de intermediación sobre la recaudación NETO");
	            cell = row.createCell(6);
	            cell.setCellStyle(styles.get("texto_grey_wrapped"));
	            cell.setCellValue("Comisión de intermediación  sobre la recaudación BRUTO");
	            cell = row.createCell(7);
	            cell.setCellStyle(styles.get("texto_grey_wrapped"));
	            cell.setCellValue("Comisión de recaudación sobre la recaudación");
	        	
	        	//ABC *****************************************************
		        //LISTADO DE CODIGOS DE PRODUCTOS PARA EL PERIODO SE BUSCA PARA HDI
		        Empresa = "ABC";
		        Integer rubro = 68; //Solo rubro 68 para HDI
	            
	        	row = sheetCalcCom.createRow(++rowCount);
	            
	        	cell = row.createCell(0);
	            cell.setCellValue(Empresa);
	            
	            Integer sumaMontoAbc = 0;
	        	List<Collection> colCollectionsRubro = collectionRepository.findByPeriodCompanyEnterpriseRubro(this.month, this.year, 3, 1, rubro); //HDI
	        	
	        	for (Collection col : colCollectionsRubro)
	        		sumaMontoAbc += col.getMonto();
	        	
	        	colCommisionCodProducto = commissionRepository.findByCompanyEnterpriseRubro(this.companyId, 1, rubro); //TRAEMOS LOS DATOS DE REMESAS DE HDI, PERO TOMAMOS LAS COMISIONES DE OHIO
		        sumaMontoPrimaNetaAbc = sumaMontoAbc / 1.19;
		        comisionIntNetoAbc = sumaMontoAbc * (colCommisionCodProducto.size() > 0 ? colCommisionCodProducto.get(0).getComCorredora() : 0d);
		        comisionIntBrutoAbc = comisionIntNetoAbc * 1.19;
		        comisionRecaudaAbc = sumaMontoAbc * (colCommisionCodProducto.size() > 0 ? colCommisionCodProducto.get(0).getComRecCobranza() : 0d);
	        	
	        	cell = row.createCell(1);
	        	cell.setCellValue(rubro);
	        	cell = row.createCell(2);
	        	cell.setCellValue(colCollectionsRubro.size());
	        	cell = row.createCell(3);
	        	cell.setCellStyle(styles.get("pesos_white"));
	        	cell.setCellValue(sumaMontoAbc);
	        	cell = row.createCell(4);
	        	cell.setCellStyle(styles.get("pesos_white"));
	        	cell.setCellValue(sumaMontoPrimaNetaAbc);
	        	cell = row.createCell(5);
	        	cell.setCellStyle(styles.get("pesos_white"));
	        	cell.setCellValue(comisionIntNetoAbc);
	        	cell = row.createCell(6);
	        	cell.setCellStyle(styles.get("pesos_white"));
	        	cell.setCellValue(comisionIntBrutoAbc);
	        	cell = row.createCell(7);
	        	cell.setCellStyle(styles.get("pesos_white"));
	        	cell.setCellValue(comisionRecaudaAbc);
	        	
	        	totalCantRecHDIAbc += colCollectionsRubro.size();
	        	totalSumaMontoHDIAbc += sumaMontoAbc;
	        	totalSumaMontoPrimaNetaHDIAbc += sumaMontoPrimaNetaAbc;
	        	totalComisionIntNetoHDIAbc += comisionIntNetoAbc;
	        	totalComisionIntBrutoHDIAbc += comisionIntBrutoAbc;
	        	totalComisionRecaudaHDIAbc += comisionRecaudaAbc;
		        //ABC *****************************************************	
		        
	        	//DIN *****************************************************
		        //LISTADO DE CODIGOS DE PRODUCTOS PARA EL PERIODO
		        Empresa = "DIN";
	        	row = sheetCalcCom.createRow(++rowCount);
	            
	        	cell = row.createCell(0);
	            cell.setCellValue(Empresa);
	            
	            Integer sumaMontoDin = 0;
	        	colCollectionsRubro = collectionRepository.findByPeriodCompanyEnterpriseRubro(this.month, this.year, 3, 2, rubro);
	        	
	        	for (Collection col : colCollectionsRubro)
	        		sumaMontoDin += col.getMonto();
	        	
	        	colCommisionCodProducto = commissionRepository.findByCompanyEnterpriseRubro(this.companyId, 1, rubro); //TRAEMOS LOS DATOS DE REMESAS DE HDI, PERO TOMAMOS LAS COMISIONES DE OHIO
		        sumaMontoPrimaNetaDin = sumaMontoDin / 1.19;
		        comisionIntNetoDin = sumaMontoDin * (colCommisionCodProducto.size() > 0 ? colCommisionCodProducto.get(0).getComCorredora() : 0d);
		        comisionIntBrutoDin = comisionIntNetoDin * 1.19;
		        comisionRecaudaDin = sumaMontoDin * (colCommisionCodProducto.size() > 0 ? colCommisionCodProducto.get(0).getComRecCobranza() : 0d);
	        	
	        	cell = row.createCell(1);
	        	cell.setCellValue(rubro);
	        	cell = row.createCell(2);
	        	cell.setCellValue(colCollectionsRubro.size());
	        	cell = row.createCell(3);
	        	cell.setCellStyle(styles.get("pesos_white"));
	        	cell.setCellValue(sumaMontoDin);
	        	cell = row.createCell(4);
	        	cell.setCellStyle(styles.get("pesos_white"));
	        	cell.setCellValue(sumaMontoPrimaNetaDin);
	        	cell = row.createCell(5);
	        	cell.setCellStyle(styles.get("pesos_white"));
	        	cell.setCellValue(comisionIntNetoDin);
	        	cell = row.createCell(6);
	        	cell.setCellStyle(styles.get("pesos_white"));
	        	cell.setCellValue(comisionIntBrutoDin);
	        	cell = row.createCell(7);
	        	cell.setCellStyle(styles.get("pesos_white"));
	        	cell.setCellValue(comisionRecaudaDin);
	        	
	        	totalCantRecHDIDin += colCollectionsRubro.size();
	        	totalSumaMontoHDIDin += sumaMontoDin;
	        	totalSumaMontoPrimaNetaHDIDin += sumaMontoPrimaNetaDin;
	        	totalComisionIntNetoHDIDin += comisionIntNetoDin;
	        	totalComisionIntBrutoHDIDin += comisionIntBrutoDin;
	        	totalComisionRecaudaHDIDin += comisionRecaudaDin;
	        	    
		        //DIN *****************************************************	
		        
		        //TOTAL GENERAL
	        	
	        	row = sheetCalcCom.createRow(++rowCount);
		        cell = row.createCell(0);
		        cell.setCellStyle(styles.get("texto_black"));
	        	cell.setCellValue("Total general");
	        	cell = row.createCell(1);
		        cell.setCellStyle(styles.get("texto_black"));
	        	cell = row.createCell(2);
	        	cell.setCellStyle(styles.get("entero_black"));
	        	cell.setCellValue(totalCantRecHDIDin + totalCantRecHDIAbc);
	        	cell = row.createCell(3);
	        	cell.setCellStyle(styles.get("pesos_black"));
	        	cell.setCellValue(totalSumaMontoHDIDin + totalSumaMontoHDIAbc);
	        	cell = row.createCell(4);
	        	cell.setCellStyle(styles.get("pesos_black"));
	        	cell.setCellValue(totalSumaMontoPrimaNetaHDIDin + totalSumaMontoPrimaNetaHDIAbc);
	        	cell = row.createCell(5);
	        	cell.setCellStyle(styles.get("pesos_black"));
	        	cell.setCellValue(totalComisionIntNetoHDIDin + totalComisionIntNetoHDIAbc);
	        	cell = row.createCell(6);
	        	cell.setCellStyle(styles.get("pesos_black"));
	        	cell.setCellValue(totalComisionIntBrutoHDIDin + totalComisionIntBrutoHDIAbc);
	        	cell = row.createCell(7);
	        	cell.setCellStyle(styles.get("pesos_black"));
	        	cell.setCellValue(totalComisionRecaudaHDIDin + totalComisionRecaudaHDIAbc);
	        }        	
        	
        	
        	
        	//SI ES BICE, ENTONCES SE AGREGA UN VALOR UF
        	if (this.companyId == 4)
	        {
	        	rowCount = rowCount + 2;
	        	
	        	//UF
	        	row = sheetCalcCom.createRow(rowCount);
	        	cell = row.createCell(0);
	        	cell.setCellValue("UF al " + maxMonthDay + " de " + this.colMonths.get(month));
	        	cell = row.createCell(1);
	        	cell.setCellValue(this.UFValue);
	        }
	        
			//HOJA RESUMEN ***************************************************************       	
	        row = sheetResumen.createRow(2);
	        
	        //CARDIF TIENE UN RESUMEN DISTINTO
	        if (this.companyId == 5) {
		        //HEADER
	        	sheetResumen.setColumnWidth(0, 13*256);
	        	sheetResumen.setColumnWidth(1, 35*256);
	        	sheetResumen.setColumnWidth(2, 18*256);
	        	sheetResumen.setColumnWidth(3, 21*256);

	        	cell = row.createCell(1);
		        cell.setCellStyle(styles.get("texto_grey_wrapped"));
	            cell.setCellValue("Canal");
	            cell = row.createCell(2);
	            cell.setCellStyle(styles.get("texto_grey_wrapped"));
	            cell.setCellValue("Comisión de intermediación  sobre la recaudación");
	            cell = row.createCell(3);
	            cell.setCellStyle(styles.get("texto_grey_wrapped"));
	            cell.setCellValue("Comisión de recaudación sobre la recaudación");
	            
	            row = sheetResumen.createRow(3);
	            cell = row.createCell(1);
	            cell.setCellValue("Telemarketing - Rubros 70, 71, 90, 91");
	            cell = row.createCell(2);
	            cell.setCellStyle(styles.get("pesos_white"));
	            cell.setCellValue(totalComisionIntBrutoTele);
	            cell = row.createCell(3);
	            cell.setCellStyle(styles.get("pesos_white"));
	            cell.setCellValue(totalComisionIntBrutoTele);
	            
	            row = sheetResumen.createRow(4);
	            cell = row.createCell(1);
	            cell.setCellValue("Presencial");
	            cell = row.createCell(2);
	            cell.setCellStyle(styles.get("pesos_white"));
	            cell.setCellValue(totalComisionIntBrutoPres);
	            cell = row.createCell(3);
	            cell.setCellStyle(styles.get("pesos_white"));
	            cell.setCellValue(totalComisionIntBrutoPres);
	            
	            row = sheetResumen.createRow(5);
	            cell = row.createCell(1);
	            cell.setCellStyle(styles.get("texto_black"));
	            cell.setCellValue("TOTAL");
	            cell = row.createCell(2);
	            cell.setCellStyle(styles.get("pesos_black"));
	            cell.setCellValue(totalComisionIntBrutoTele + totalComisionIntBrutoPres);
	            cell = row.createCell(3);
	            cell.setCellStyle(styles.get("pesos_black"));
	            cell.setCellValue(totalComisionIntBrutoTele + totalComisionIntBrutoPres);
	        }
	        else if (this.companyId == 2) { //OHIO TIENE UN RESUMEN DISTINTO
	        	//HEADER
	        	sheetResumen.setColumnWidth(0, 13*256);
	        	sheetResumen.setColumnWidth(1, 13*256);
	        	sheetResumen.setColumnWidth(2, 18*256);
	        	sheetResumen.setColumnWidth(3, 21*256);
	        	sheetResumen.setColumnWidth(4, 21*256);
	        	sheetResumen.setColumnWidth(5, 21*256);

		        cell = row.createCell(1);
		        cell.setCellStyle(styles.get("texto_grey_wrapped"));
	            cell.setCellValue("Empresa");
	            cell = row.createCell(2);
	            cell.setCellStyle(styles.get("texto_grey_wrapped"));
	            cell.setCellValue("Comisión Garantizada fija sobre ventas TOTAL");
	            cell = row.createCell(3);
	            cell.setCellStyle(styles.get("texto_grey_wrapped"));
	            cell.setCellValue("Comisión de intermediación  sobre la recaudación NETO");
	            cell = row.createCell(4);
	            cell.setCellStyle(styles.get("texto_grey_wrapped"));
	            cell.setCellValue("Comisión de intermediación  sobre la recaudación BRUTO");
	            cell = row.createCell(5);
	            cell.setCellStyle(styles.get("texto_grey_wrapped"));
	            cell.setCellValue("Comisión de recaudación sobre la recaudación BRUTO");
	            
	            row = sheetResumen.createRow(3);
	            cell = row.createCell(1);
	            cell.setCellValue("ABC");
	            cell = row.createCell(2);
	            cell.setCellStyle(styles.get("pesos_white"));
	            cell.setCellValue((int) Math.round(totalComisionDecimalAbc));
	            cell = row.createCell(3);
	            cell.setCellStyle(styles.get("pesos_white"));
	            cell.setCellValue(totalComisionIntNetoAbc + totalComisionIntNetoHDIAbc);
	            cell = row.createCell(4);
	            cell.setCellStyle(styles.get("pesos_white"));
	            cell.setCellValue(totalComisionIntBrutoAbc + totalComisionIntBrutoHDIAbc);
	            cell = row.createCell(5);
	            cell.setCellStyle(styles.get("pesos_white"));
	            cell.setCellValue(totalComisionRecaudaAbc + totalComisionRecaudaHDIAbc);
	            
	            row = sheetResumen.createRow(4);
	            cell = row.createCell(1);
	            cell.setCellValue("DIN");
	            cell = row.createCell(2);
	            cell.setCellStyle(styles.get("pesos_white"));
	            cell.setCellValue((int) Math.round(totalComisionDecimalDin));
	            cell = row.createCell(3);
	            cell.setCellStyle(styles.get("pesos_white"));
	            cell.setCellValue(totalComisionIntNetoDin + totalComisionIntNetoHDIDin);
	            cell = row.createCell(4);
	            cell.setCellStyle(styles.get("pesos_white"));
	            cell.setCellValue(totalComisionIntBrutoDin + totalComisionIntBrutoHDIDin);
	            cell = row.createCell(5);
	            cell.setCellStyle(styles.get("pesos_white"));
	            cell.setCellValue(totalComisionRecaudaDin + totalComisionRecaudaHDIDin);
	            
	            row = sheetResumen.createRow(5);
	            cell = row.createCell(1);
	            cell.setCellStyle(styles.get("texto_black"));
	            cell.setCellValue("TOTAL");
	            cell = row.createCell(2);
	            cell.setCellStyle(styles.get("pesos_black"));
	            cell.setCellValue((int) Math.round(totalComisionDecimalAbc + totalComisionDecimalDin));
	            cell = row.createCell(3);
	            cell.setCellStyle(styles.get("pesos_black"));
	            cell.setCellValue(totalComisionIntNetoAbc + totalComisionIntNetoHDIAbc + totalComisionIntNetoDin + totalComisionIntNetoHDIDin);
	            cell = row.createCell(4);
	            cell.setCellStyle(styles.get("pesos_black"));
	            cell.setCellValue(totalComisionIntBrutoAbc + totalComisionIntBrutoHDIAbc + totalComisionIntBrutoDin + totalComisionIntBrutoHDIDin);
	            cell = row.createCell(5);
	            cell.setCellStyle(styles.get("pesos_black"));
	            cell.setCellValue(totalComisionRecaudaAbc + totalComisionRecaudaHDIAbc + totalComisionRecaudaDin + totalComisionRecaudaHDIDin);
	        }
	        else {
		        //HEADER
	        	sheetResumen.setColumnWidth(0, 13*256);
	        	sheetResumen.setColumnWidth(1, 13*256);
	        	sheetResumen.setColumnWidth(2, 18*256);
	        	sheetResumen.setColumnWidth(3, 21*256);
	        	sheetResumen.setColumnWidth(4, 17*256);

		        cell = row.createCell(1);
		        cell.setCellStyle(styles.get("texto_grey_wrapped"));
	            cell.setCellValue("Empresa");
	            cell = row.createCell(2);
	            cell.setCellStyle(styles.get("texto_grey_wrapped"));
	            cell.setCellValue("Comisión Anticipada Acceso Preferente TOTAL");
	            cell = row.createCell(3);
	            cell.setCellStyle(styles.get("texto_grey_wrapped"));
	            cell.setCellValue("Comisión de intermediación  sobre la recaudación");
	            cell = row.createCell(4);
	            cell.setCellStyle(styles.get("texto_grey_wrapped"));
	            cell.setCellValue("Comisión de recaudación sobre la recaudación");
	            
	            row = sheetResumen.createRow(3);
	            cell = row.createCell(1);
	            cell.setCellValue("ABC");
	            cell = row.createCell(2);
	            cell.setCellStyle(styles.get("pesos_white"));
	            cell.setCellValue((int) Math.round(totalComisionDecimalAbc));
	            cell = row.createCell(3);
	            cell.setCellStyle(styles.get("pesos_white"));
	            if (this.companyId == 3 || this.companyId == 4) //BICE y HDI muestran en el resumen el total comision NETO
	            	cell.setCellValue(totalComisionIntNetoAbc);
	            else
	            	cell.setCellValue(totalComisionIntBrutoAbc);
	            cell = row.createCell(4);
	            cell.setCellStyle(styles.get("pesos_white"));
	            cell.setCellValue(totalComisionRecaudaAbc);
	            
	            row = sheetResumen.createRow(4);
	            cell = row.createCell(1);
	            cell.setCellValue("DIN");
	            cell = row.createCell(2);
	            cell.setCellStyle(styles.get("pesos_white"));
	            cell.setCellValue((int) Math.round(totalComisionDecimalDin));
	            cell = row.createCell(3);
	            cell.setCellStyle(styles.get("pesos_white"));
	            if (this.companyId == 3 || this.companyId == 4) //BICE y HDI muestran en el resumen el total comision NETO
	            	cell.setCellValue(totalComisionIntNetoDin);
	            else
	            	cell.setCellValue(totalComisionIntBrutoDin);
	            cell = row.createCell(4);
	            cell.setCellStyle(styles.get("pesos_white"));
	            cell.setCellValue(totalComisionRecaudaDin);
	            
	            row = sheetResumen.createRow(5);
	            cell = row.createCell(1);
	            cell.setCellStyle(styles.get("texto_black"));
	            cell.setCellValue("TOTAL");
	            cell = row.createCell(2);
	            cell.setCellStyle(styles.get("pesos_black"));
	            cell.setCellValue((int) Math.round(totalComisionDecimalAbc + totalComisionDecimalDin));
	            cell = row.createCell(3);
	            cell.setCellStyle(styles.get("pesos_black"));
	            if (this.companyId == 3 || this.companyId == 4) //BICE y HDI muestran en el resumen el total comision NETO
	            	cell.setCellValue(totalComisionIntNetoAbc + totalComisionIntNetoDin);
	            else
	            	cell.setCellValue(totalComisionIntBrutoAbc + totalComisionIntBrutoDin);
	            cell = row.createCell(4);
	            cell.setCellStyle(styles.get("pesos_black"));
	            cell.setCellValue(totalComisionRecaudaAbc + totalComisionRecaudaDin);
	        }

	        //INFORMACION DE VENTAS SOLO PARA BCI Y OHIO
            if (this.companyId == 1 || this.companyId == 2)
			{
            	//HOJA Ventas Totales*************************************************************************
	            rowCount = 0;
	            
	            sheetVT.setColumnWidth(1, 13*256);
	            
		        //HEADER
		        row = sheetVT.createRow(0);
		        
		        cell = row.createCell(0);
		        cell.setCellStyle(styles.get("texto_white_bold"));
	            cell.setCellValue("Empresa");
	            cell = row.createCell(1);
	            cell.setCellStyle(styles.get("texto_white_bold"));
	            cell.setCellValue("Certificado");
	            cell = row.createCell(2);
	            cell.setCellStyle(styles.get("texto_white_bold"));
	            cell.setCellValue("Sucursal");
	            cell = row.createCell(3);
	            cell.setCellStyle(styles.get("texto_white_bold"));
	            cell.setCellValue("Rut Vendedor");
	            cell = row.createCell(4);
	            cell.setCellStyle(styles.get("texto_white_bold"));
	            cell.setCellValue("Nombre Vendedor");
	            cell = row.createCell(5);
	            cell.setCellStyle(styles.get("texto_white_bold"));
	            cell.setCellValue("Cargo Vendedor");
	            cell = row.createCell(6);
	            cell.setCellStyle(styles.get("texto_white_bold"));
	            cell.setCellValue("Rut Referente");
	            cell = row.createCell(7);
	            cell.setCellStyle(styles.get("texto_white_bold"));
	            cell.setCellValue("Nombre Referente");
	            cell = row.createCell(8);
	            cell.setCellStyle(styles.get("texto_white_bold"));
	            cell.setCellValue("Cargo Referente");
	            cell = row.createCell(9);
	            cell.setCellStyle(styles.get("texto_white_bold"));
	            cell.setCellValue("Ind Cruce");
	            cell = row.createCell(10);
	            cell.setCellStyle(styles.get("texto_white_bold"));
	            cell.setCellValue("Nro. Remes");
	            cell = row.createCell(11);
	            cell.setCellStyle(styles.get("texto_white_bold"));
	            cell.setCellValue("Código Producto");
	            cell = row.createCell(12);
	            cell.setCellStyle(styles.get("texto_white_bold"));
	            cell.setCellValue("Puntos por Venta");
	            cell = row.createCell(13);
	            cell.setCellStyle(styles.get("texto_white_bold"));
	            cell.setCellValue("Fecha Emisión");
	            cell = row.createCell(14);
	            cell.setCellStyle(styles.get("texto_white_bold"));
	            cell.setCellValue("Tarjeta");
	            cell = row.createCell(15);
	            cell.setCellStyle(styles.get("texto_white_bold"));
	            cell.setCellValue("Prima Anualizada");
	            cell = row.createCell(16);
	            cell.setCellStyle(styles.get("texto_white_bold"));
	            cell.setCellValue("Comisión Recaudación");
	            cell = row.createCell(17);
	            cell.setCellStyle(styles.get("texto_white_bold"));
	            cell.setCellValue("Comisión Recaudación");
	            cell = row.createCell(18);
	            cell.setCellStyle(styles.get("texto_white_bold"));
	            cell.setCellValue("Rut Asegurado");
	            cell = row.createCell(19);
	            cell.setCellStyle(styles.get("texto_white_bold"));
	            cell.setCellValue("Nombre Asegurado");
	            cell = row.createCell(20);
	            cell.setCellStyle(styles.get("texto_white_bold"));
	            cell.setCellValue("Fecha Nacimiento");
	            cell = row.createCell(21);
	            cell.setCellStyle(styles.get("texto_white_bold"));
	            cell.setCellValue("Dirección Particular");
	            cell = row.createCell(22);
	            cell.setCellStyle(styles.get("texto_white_bold"));
	            cell.setCellValue("Comuna");
	            cell = row.createCell(23);
	            cell.setCellStyle(styles.get("texto_white_bold"));
	            cell.setCellValue("Teléfono Particular");
	            cell = row.createCell(24);
	            cell.setCellStyle(styles.get("texto_white_bold"));
	            cell.setCellValue("Teléfono Laboral");
	            cell = row.createCell(25);
	            cell.setCellStyle(styles.get("texto_white_bold"));
	            cell.setCellValue("Teléfono Celular");
	            cell = row.createCell(26);
	            cell.setCellStyle(styles.get("texto_white_bold"));
	            cell.setCellValue("Estado");
	            cell = row.createCell(27);
	            cell.setCellStyle(styles.get("texto_white_bold"));
	            cell.setCellValue("Usuario Desafiliación");
	            cell = row.createCell(28);
	            cell.setCellStyle(styles.get("texto_white_bold"));
	            cell.setCellValue("Fecha Desafiliación");
	            cell = row.createCell(29);
	            cell.setCellStyle(styles.get("texto_white_bold"));
	            cell.setCellValue("Motivo Desafiliación");
	            cell = row.createCell(30);
	            cell.setCellStyle(styles.get("texto_white_bold"));
	            cell.setCellValue("Identificador Grabación");
	            /*cell = row.createCell(31);
	            cell.setCellValue("Fecha Recepción");
	            cell = row.createCell(32);
	            cell.setCellValue("Sin Fecha Vigencia");
	            cell = row.createCell(33);
	            cell.setCellValue("Sin Firma Cliente");
	            cell = row.createCell(34);
	            cell.setCellValue("Sin Firma DPS");
	            cell = row.createCell(35);
	            cell.setCellValue("Fechas Distintas");
	            cell = row.createCell(36);
	            cell.setCellValue("Sin Timbre Fecha");
	            cell = row.createCell(37);
	            cell.setCellValue("Estado Recepción");
	            cell = row.createCell(38);
	            cell.setCellValue("Beneficiarios");
	            cell = row.createCell(39);
	            cell.setCellValue("Departamento");
	            cell = row.createCell(40);
	            cell.setCellValue("Casa");
	            cell = row.createCell(41);
	            cell.setCellValue("Chapas de Seguridad");
	            cell = row.createCell(42);
	            cell.setCellValue("Proteccion en todas las ventanas");
	            cell = row.createCell(43);
	            cell.setCellValue("Alarma");
	            cell = row.createCell(44);
	            cell.setCellValue("Sistema de Vigilancia");
	            cell = row.createCell(45);
	            cell.setCellValue("Condominio Cerrado");
	            cell = row.createCell(46);
	            cell.setCellValue("Depto. en tercer piso o superior");
	            cell = row.createCell(47);
	            cell.setCellValue("Direccion");
	            cell = row.createCell(48);
	            cell.setCellValue("Villa o Poblacion");
	            cell = row.createCell(49);
	            cell.setCellValue("Block");
	            cell = row.createCell(50);
	            cell.setCellValue("Piso");
	            cell = row.createCell(51);
	            cell.setCellValue("Grupo Familiar");
	            cell = row.createCell(52);
	            cell.setCellValue("Rut asegurado Adicional");
	            cell = row.createCell(53);
	            cell.setCellValue("Nombres");
	            cell = row.createCell(54);
	            cell.setCellValue("Apellido Paterno");
	            cell = row.createCell(55);
	            cell.setCellValue("Apellido Materno");
	            cell = row.createCell(56);
	            cell.setCellValue("Fecha de Nacimiento");*/
		        
		        for (Sale item : colTotalSales) {
		            row = sheetVT.createRow(++rowCount);
		            
		            cell = row.createCell(0);
		            cell.setCellValue(item.getEnterprise().getName());
		            cell = row.createCell(1);
		            cell.setCellValue(item.getCertificado() == null ? 0 : item.getCertificado());
		            cell = row.createCell(2);
		            cell.setCellValue(item.getSucursal() == null ? 0 : item.getSucursal());
		            cell = row.createCell(3);
		            cell.setCellValue(item.getRutVendedor() == null ? "" : item.getRutVendedor());
		            cell = row.createCell(4);
		            cell.setCellValue(item.getNombreVendedor() == null ? "" : item.getNombreVendedor());
		            cell = row.createCell(5);
		            cell.setCellValue(item.getCargoVendedor() == null ? "" : item.getNombreVendedor());
		            cell = row.createCell(6);
		            cell.setCellValue(item.getRutReferente() == null ? "" : item.getRutReferente());
		            cell = row.createCell(7);
		            cell.setCellValue(item.getNombreReferente() == null ? "" : item.getNombreReferente());
		            cell = row.createCell(8);
		            cell.setCellValue(item.getCargoReferente() == null ? "" : item.getCargoReferente());
		            cell = row.createCell(9);
		            cell.setCellValue(item.getIndCruce() == null ? "" : item.getIndCruce());
		            cell = row.createCell(10);
		            cell.setCellValue(item.getNroRemesa() == null ? 0 : item.getNroRemesa());
		            cell = row.createCell(11);
		            cell.setCellValue(item.getCodProducto() == null ? 0 : item.getCodProducto());
		            cell = row.createCell(12);
		            cell.setCellValue(item.getPuntosVenta() == null ? 0 : item.getPuntosVenta());
		            cell = row.createCell(13);
		            cell.setCellValue(item.getFecEmision() == null ? cal.getTime() : item.getFecEmision());
		            cell = row.createCell(14);
		            cell.setCellValue(item.getTarjeta() == null ? 0 : item.getTarjeta());
		            cell = row.createCell(15);
		            cell.setCellValue(item.getPrimaAnualizada() == null ? 0d : item.getPrimaAnualizada());
		            cell = row.createCell(16);
		            cell.setCellValue(item.getComisionRecaudacion() == null ? 0d : item.getComisionRecaudacion());
		            cell = row.createCell(17);
		            cell.setCellValue(item.getComisionRecaudacion2() == null ? 0d : item.getComisionRecaudacion2());
		            cell = row.createCell(18);
		            cell.setCellValue(item.getRutAsegurado() == null ? "" : item.getRutAsegurado());
		            cell = row.createCell(19);
		            cell.setCellValue(item.getNombreAsegurado() == null ? "" : item.getNombreAsegurado());
		            cell = row.createCell(20);
		            cell.setCellValue(item.getFecNacimiento() == null ? cal.getTime() : item.getFecNacimiento());
		            cell = row.createCell(21);
		            cell.setCellValue(item.getDireccionParticular() == null ? "" : item.getDireccionParticular());
		            cell = row.createCell(22);
		            cell.setCellValue(item.getComuna() == null ? "" : item.getComuna());
		            cell = row.createCell(23);
		            cell.setCellValue(item.getTelefonoParticular() == null ? "" : item.getTelefonoParticular());
		            cell = row.createCell(24);
		            cell.setCellValue(item.getTelefonoLaboral() == null ? "" : item.getTelefonoLaboral());
		            cell = row.createCell(25);
		            cell.setCellValue(item.getTelefonoCelular() == null ? "" : item.getTelefonoCelular());
		            cell = row.createCell(26);
		            cell.setCellValue(item.getEstado() == null ? "" : item.getEstado());
		            cell = row.createCell(27);
		            cell.setCellValue(item.getUsuarioDesafiliacion() == null ? "" : item.getUsuarioDesafiliacion());
		            cell = row.createCell(28);
		            cell.setCellValue(item.getFechaDesafiliacion() == null ? cal.getTime()  : item.getFechaDesafiliacion());
		            cell = row.createCell(29);
		            cell.setCellValue(item.getMotivoDesafiliacion() == null ? "" : item.getMotivoDesafiliacion());
		            cell = row.createCell(30);
		            cell.setCellValue(item.getIdentificadorGrabacion() == null ? 0 : item.getIdentificadorGrabacion());
		        }
		        
		        //HOJA Ventas sin desafil y prima 0*************************************************
		        rowCount = 0;
		        
		        sheetVSin.setColumnWidth(1, 13*256);
		      	        	        
		        //HEADER
		        row = sheetVSin.createRow(0);
		        
		        cell = row.createCell(0);
		        cell.setCellStyle(styles.get("texto_white_bold"));
	            cell.setCellValue("Empresa");
	            cell = row.createCell(1);
	            cell.setCellStyle(styles.get("texto_white_bold"));
	            cell.setCellValue("Certificado");
	            cell = row.createCell(2);
	            cell.setCellStyle(styles.get("texto_white_bold"));
	            cell.setCellValue("Sucursal");
	            cell = row.createCell(3);
	            cell.setCellStyle(styles.get("texto_white_bold"));
	            cell.setCellValue("Rut Vendedor");
	            cell = row.createCell(4);
	            cell.setCellStyle(styles.get("texto_white_bold"));
	            cell.setCellValue("Nombre Vendedor");
	            cell = row.createCell(5);
	            cell.setCellStyle(styles.get("texto_white_bold"));
	            cell.setCellValue("Cargo Vendedor");
	            cell = row.createCell(6);
	            cell.setCellStyle(styles.get("texto_white_bold"));
	            cell.setCellValue("Rut Referente");
	            cell = row.createCell(7);
	            cell.setCellStyle(styles.get("texto_white_bold"));
	            cell.setCellValue("Nombre Referente");
	            cell = row.createCell(8);
	            cell.setCellStyle(styles.get("texto_white_bold"));
	            cell.setCellValue("Cargo Referente");
	            cell = row.createCell(9);
	            cell.setCellStyle(styles.get("texto_white_bold"));
	            cell.setCellValue("Ind Cruce");
	            cell = row.createCell(10);
	            cell.setCellStyle(styles.get("texto_white_bold"));
	            cell.setCellValue("Nro. Remes");
	            cell = row.createCell(11);
	            cell.setCellStyle(styles.get("texto_white_bold"));
	            cell.setCellValue("Código Producto");
	            cell = row.createCell(12);
	            cell.setCellStyle(styles.get("texto_white_bold"));
	            cell.setCellValue("Puntos por Venta");
	            cell = row.createCell(13);
	            cell.setCellStyle(styles.get("texto_white_bold"));
	            cell.setCellValue("Fecha Emisión");
	            cell = row.createCell(14);
	            cell.setCellStyle(styles.get("texto_white_bold"));
	            cell.setCellValue("Tarjeta");
	            cell = row.createCell(15);
	            cell.setCellStyle(styles.get("texto_white_bold"));
	            cell.setCellValue("Prima Anualizada");
	            cell = row.createCell(16);
	            cell.setCellStyle(styles.get("texto_white_bold"));
	            cell.setCellValue("Comisión Recaudación");
	            cell = row.createCell(17);
	            cell.setCellStyle(styles.get("texto_white_bold"));
	            cell.setCellValue("Comisión Recaudación");
	            cell = row.createCell(18);
	            cell.setCellStyle(styles.get("texto_white_bold"));
	            cell.setCellValue("Rut Asegurado");
	            cell = row.createCell(19);
	            cell.setCellStyle(styles.get("texto_white_bold"));
	            cell.setCellValue("Nombre Asegurado");
	            cell = row.createCell(20);
	            cell.setCellStyle(styles.get("texto_white_bold"));
	            cell.setCellValue("Fecha Nacimiento");
	            cell = row.createCell(21);
	            cell.setCellStyle(styles.get("texto_white_bold"));
	            cell.setCellValue("Dirección Particular");
	            cell = row.createCell(22);
	            cell.setCellStyle(styles.get("texto_white_bold"));
	            cell.setCellValue("Comuna");
	            cell = row.createCell(23);
	            cell.setCellStyle(styles.get("texto_white_bold"));
	            cell.setCellValue("Teléfono Particular");
	            cell = row.createCell(24);
	            cell.setCellStyle(styles.get("texto_white_bold"));
	            cell.setCellValue("Teléfono Laboral");
	            cell = row.createCell(25);
	            cell.setCellStyle(styles.get("texto_white_bold"));
	            cell.setCellValue("Teléfono Celular");
	            cell = row.createCell(26);
	            cell.setCellStyle(styles.get("texto_white_bold"));
	            cell.setCellValue("Estado");
	            cell = row.createCell(27);
	            cell.setCellStyle(styles.get("texto_white_bold"));
	            cell.setCellValue("Usuario Desafiliación");
	            cell = row.createCell(28);
	            cell.setCellStyle(styles.get("texto_white_bold"));
	            cell.setCellValue("Fecha Desafiliación");
	            cell = row.createCell(29);
	            cell.setCellStyle(styles.get("texto_white_bold"));
	            cell.setCellValue("Motivo Desafiliación");
	            cell = row.createCell(30);
	            cell.setCellStyle(styles.get("texto_white_bold"));
	            cell.setCellValue("Identificador Grabación");
	            /*cell = row.createCell(31);
	            cell.setCellValue("Fecha Recepción");
	            cell = row.createCell(32);
	            cell.setCellValue("Sin Fecha Vigencia");
	            cell = row.createCell(33);
	            cell.setCellValue("Sin Firma Cliente");
	            cell = row.createCell(34);
	            cell.setCellValue("Sin Firma DPS");
	            cell = row.createCell(35);
	            cell.setCellValue("Fechas Distintas");
	            cell = row.createCell(36);
	            cell.setCellValue("Sin Timbre Fecha");
	            cell = row.createCell(37);
	            cell.setCellValue("Estado Recepción");
	            cell = row.createCell(38);
	            cell.setCellValue("Beneficiarios");
	            cell = row.createCell(39);
	            cell.setCellValue("Departamento");
	            cell = row.createCell(40);
	            cell.setCellValue("Casa");
	            cell = row.createCell(41);
	            cell.setCellValue("Chapas de Seguridad");
	            cell = row.createCell(42);
	            cell.setCellValue("Proteccion en todas las ventanas");
	            cell = row.createCell(43);
	            cell.setCellValue("Alarma");
	            cell = row.createCell(44);
	            cell.setCellValue("Sistema de Vigilancia");
	            cell = row.createCell(45);
	            cell.setCellValue("Condominio Cerrado");
	            cell = row.createCell(46);
	            cell.setCellValue("Depto. en tercer piso o superior");
	            cell = row.createCell(47);
	            cell.setCellValue("Direccion");
	            cell = row.createCell(48);
	            cell.setCellValue("Villa o Poblacion");
	            cell = row.createCell(49);
	            cell.setCellValue("Block");
	            cell = row.createCell(50);
	            cell.setCellValue("Piso");
	            cell = row.createCell(51);
	            cell.setCellValue("Grupo Familiar");
	            cell = row.createCell(52);
	            cell.setCellValue("Rut asegurado Adicional");
	            cell = row.createCell(53);
	            cell.setCellValue("Nombres");
	            cell = row.createCell(54);
	            cell.setCellValue("Apellido Paterno");
	            cell = row.createCell(55);
	            cell.setCellValue("Apellido Materno");
	            cell = row.createCell(56);
	            cell.setCellValue("Fecha de Nacimiento");*/
		        
	            for (Sale item : colSalesSinDesa) {
		            row = sheetVSin.createRow(++rowCount);
		            
		            cell = row.createCell(0);
		            cell.setCellValue(item.getEnterprise().getName());
		            cell = row.createCell(1);
		            cell.setCellValue(item.getCertificado() == null ? 0 : item.getCertificado());
		            cell = row.createCell(2);
		            cell.setCellValue(item.getSucursal() == null ? 0 : item.getSucursal());
		            cell = row.createCell(3);
		            cell.setCellValue(item.getRutVendedor() == null ? "" : item.getRutVendedor());
		            cell = row.createCell(4);
		            cell.setCellValue(item.getNombreVendedor() == null ? "" : item.getNombreVendedor());
		            cell = row.createCell(5);
		            cell.setCellValue(item.getCargoVendedor() == null ? "" : item.getNombreVendedor());
		            cell = row.createCell(6);
		            cell.setCellValue(item.getRutReferente() == null ? "" : item.getRutReferente());
		            cell = row.createCell(7);
		            cell.setCellValue(item.getNombreReferente() == null ? "" : item.getNombreReferente());
		            cell = row.createCell(8);
		            cell.setCellValue(item.getCargoReferente() == null ? "" : item.getCargoReferente());
		            cell = row.createCell(9);
		            cell.setCellValue(item.getIndCruce() == null ? "" : item.getIndCruce());
		            cell = row.createCell(10);
		            cell.setCellValue(item.getNroRemesa() == null ? 0 : item.getNroRemesa());
		            cell = row.createCell(11);
		            cell.setCellValue(item.getCodProducto() == null ? 0 : item.getCodProducto());
		            cell = row.createCell(12);
		            cell.setCellValue(item.getPuntosVenta() == null ? 0 : item.getPuntosVenta());
		            cell = row.createCell(13);
		            cell.setCellValue(item.getFecEmision() == null ? cal.getTime() : item.getFecEmision());
		            cell = row.createCell(14);
		            cell.setCellValue(item.getTarjeta() == null ? 0 : item.getTarjeta());
		            cell = row.createCell(15);
		            cell.setCellValue(item.getPrimaAnualizada() == null ? 0d : item.getPrimaAnualizada());
		            cell = row.createCell(16);
		            cell.setCellValue(item.getComisionRecaudacion() == null ? 0d : item.getComisionRecaudacion());
		            cell = row.createCell(17);
		            cell.setCellValue(item.getComisionRecaudacion2() == null ? 0d : item.getComisionRecaudacion2());
		            cell = row.createCell(18);
		            cell.setCellValue(item.getRutAsegurado() == null ? "" : item.getRutAsegurado());
		            cell = row.createCell(19);
		            cell.setCellValue(item.getNombreAsegurado() == null ? "" : item.getNombreAsegurado());
		            cell = row.createCell(20);
		            cell.setCellValue(item.getFecNacimiento() == null ? cal.getTime() : item.getFecNacimiento());
		            cell = row.createCell(21);
		            cell.setCellValue(item.getDireccionParticular() == null ? "" : item.getDireccionParticular());
		            cell = row.createCell(22);
		            cell.setCellValue(item.getComuna() == null ? "" : item.getComuna());
		            cell = row.createCell(23);
		            cell.setCellValue(item.getTelefonoParticular() == null ? "" : item.getTelefonoParticular());
		            cell = row.createCell(24);
		            cell.setCellValue(item.getTelefonoLaboral() == null ? "" : item.getTelefonoLaboral());
		            cell = row.createCell(25);
		            cell.setCellValue(item.getTelefonoCelular() == null ? "" : item.getTelefonoCelular());
		            cell = row.createCell(26);
		            cell.setCellValue(item.getEstado() == null ? "" : item.getEstado());
		            cell = row.createCell(27);
		            cell.setCellValue(item.getUsuarioDesafiliacion() == null ? "" : item.getUsuarioDesafiliacion());
		            cell = row.createCell(28);
		            cell.setCellValue(item.getFechaDesafiliacion() == null ? cal.getTime()  : item.getFechaDesafiliacion());
		            cell = row.createCell(29);
		            cell.setCellValue(item.getMotivoDesafiliacion() == null ? "" : item.getMotivoDesafiliacion());
		            cell = row.createCell(30);
		            cell.setCellValue(item.getIdentificadorGrabacion() == null ? 0 : item.getIdentificadorGrabacion());
		        }
			}
	        
	        //HOJA Remesas*******************************************************************************
            rowCount = 0;
	        
	        //HEADER
	        row = sheetRemesa.createRow(0);
	        
	        cell = row.createCell(0);
	        cell.setCellStyle(styles.get("texto_white_bold"));
            cell.setCellValue("Rut Cía");
            cell = row.createCell(1);
            cell.setCellStyle(styles.get("texto_white_bold"));
            cell.setCellValue("Rubro");
            cell = row.createCell(2);
            cell.setCellStyle(styles.get("texto_white_bold"));
            cell.setCellValue("Rut");
            cell = row.createCell(3);
            cell.setCellStyle(styles.get("texto_white_bold"));
            cell.setCellValue("Fecha Apli");
            cell = row.createCell(4);
            cell.setCellStyle(styles.get("texto_white_bold"));
            cell.setCellValue("Monto");
            cell = row.createCell(5);
            cell.setCellStyle(styles.get("texto_white_bold"));
            cell.setCellValue("Empresa");
            
            if (this.companyId == 4) //BICE
            {
            	cell = row.createCell(6);
                cell.setCellStyle(styles.get("texto_white_bold"));
                cell.setCellValue("Monto prima pagada neta (Neto)");
	            cell = row.createCell(7);
	            cell.setCellStyle(styles.get("texto_white_bold"));
	            cell.setCellValue("Monto prima NETA UF");
	            cell = row.createCell(8);
	            cell.setCellStyle(styles.get("texto_white_bold"));
	            cell.setCellValue("Monto prima BRUTA UF");
            }
            else if (this.companyId == 2) //OHIO
            {
            	cell = row.createCell(6);
                cell.setCellStyle(styles.get("texto_white_bold"));
                cell.setCellValue("Monto prima final");
	            cell = row.createCell(7);
	            cell.setCellStyle(styles.get("texto_white_bold"));
	            cell.setCellValue("EXENTA");
	            cell = row.createCell(8);
	            cell.setCellStyle(styles.get("texto_white_bold"));
	            cell.setCellValue("AFECTA");
	            cell = row.createCell(9);
	            cell.setCellStyle(styles.get("texto_white_bold"));
	            cell.setCellValue("PRIMA NETA");
	            cell = row.createCell(10);
	            cell.setCellStyle(styles.get("texto_white_bold"));
	            cell.setCellValue("IVA");
	            cell = row.createCell(11);
	            cell.setCellStyle(styles.get("texto_white_bold"));
	            cell.setCellValue("BRUTA");
            }
            else {
            	cell = row.createCell(6);
                cell.setCellStyle(styles.get("texto_white_bold"));
                cell.setCellValue("Monto prima neta (Neto)");
            }
	        
            for (Collection item : colRecaudacion) {
	            row = sheetRemesa.createRow(++rowCount);
	            
	            cell = row.createCell(0);
	            cell.setCellValue(item.getCompany() == null ? "" : item.getCompany().getRut());
	            cell = row.createCell(1);
	            cell.setCellValue(item.getRubro() == null ? 0 : item.getRubro());
	            cell = row.createCell(2);
	            cell.setCellValue(item.getRutCli() == null ? "" : item.getRutCli());
	            cell = row.createCell(3);
	            cell.setCellValue(item.getFechaAplicacion() == null ? cal.getTime() : item.getFechaAplicacion());
	            cell = row.createCell(4);
	            cell.setCellValue(item.getMonto() == null ? 0 : item.getMonto());
	            cell = row.createCell(5);
	            cell.setCellValue(item.getEnterprise() == null ? "" : item.getEnterprise().getName());
	            
	            if (this.companyId == 4) //BICE
	            {
	            	List<Commission> colCom = commissionRepository.findByCompanyRubro(this.companyId, item.getRubro());
	            	Double monto = item.getMonto() == null ? 0 : item.getMonto() / 1.19;
	            	cell = row.createCell(6);
	            	cell.setCellStyle(styles.get("pesos_white"));
		            cell.setCellValue((int) Math.round(monto));
	            	cell = row.createCell(7);
	            	cell.setCellValue(colCom.size() > 0 ? colCom.get(0).getPrimaNeta() : 0);
		            cell = row.createCell(8);
		            cell.setCellValue(colCom.size() > 0 ? colCom.get(0).getPrimaBruta() : 0);
	            }
	            else if (this.companyId == 2) //OHIO
	            {
	            	List<Commission> colCom = commissionRepository.findByCompanyEnterpriseRubro(this.companyId, item.getEnterprise().getId(), item.getRubro());
	            	Double monto = item.getMonto() == null ? 0d : item.getRubro() == 58 ? item.getMonto() / 2 : item.getMonto();
	            	cell = row.createCell(6);
	            	cell.setCellStyle(styles.get("pesos_white"));
		            cell.setCellValue((int) Math.round(monto));
	            	cell = row.createCell(7);
		            cell.setCellValue(colCom.size() > 0 ? monto * colCom.get(0).getPorcExenta() : 0);
		            cell = row.createCell(8);
		            cell.setCellValue(colCom.size() > 0 ? monto * colCom.get(0).getPorcAfecta() : 0);
		            cell = row.createCell(9);
		            cell.setCellValue(colCom.size() > 0 ? monto * colCom.get(0).getPorcNeta() : 0);
		            cell = row.createCell(10);
		            cell.setCellValue(colCom.size() > 0 ? monto * colCom.get(0).getPorcIva() : 0);
		            cell = row.createCell(11);
		            cell.setCellValue(colCom.size() > 0 ? monto * colCom.get(0).getPorcBruta() : 0);
	            }
	            else {
	            	cell = row.createCell(6);
	            	cell.setCellStyle(styles.get("pesos_white"));
		            cell.setCellValue(item.getMonto() == null ? 0 : (int) Math.round(item.getMonto() / 1.19));
	            }
	        }
            
            //OHIO
            if (this.companyId == 2) {
            	//HOJAS Cuadro Comisiones OHIO************************************************************************************
            	
            	XSSFSheet sheetCuadroComDin = workbook.createSheet("Cuadro Comisiones DIN");
            	XSSFSheet sheetCuadroComAbc = workbook.createSheet("Cuadro Comisiones ABC");
            	
            	//HOJA ABC
	            rowCount = 0;
		        
		        //HEADER
	            sheetCuadroComAbc.setColumnWidth(0, 10*256);
	            sheetCuadroComAbc.setColumnWidth(1, 11*256);
	            sheetCuadroComAbc.setColumnWidth(2, 13*256);
	            sheetCuadroComAbc.setColumnWidth(3, 12*256);
	            sheetCuadroComAbc.setColumnWidth(4, 12*256);
	            sheetCuadroComAbc.setColumnWidth(5, 11*256);
	            sheetCuadroComAbc.setColumnWidth(6, 11*256);
		        
	            row = sheetCuadroComAbc.createRow(0);
		        
		        cell = row.createCell(0);
		        cell.setCellStyle(styles.get("texto_white_wrapped"));
	            cell.setCellValue("Compañía");
	            cell = row.createCell(1);
	            cell.setCellStyle(styles.get("texto_white_wrapped"));
	            cell.setCellValue("Cod. Producto");
	            cell = row.createCell(2);
	            cell.setCellStyle(styles.get("texto_white_wrapped"));
	            cell.setCellValue("Rubro");
	            cell = row.createCell(3);
	            cell.setCellStyle(styles.get("texto_grey_wrapped"));
	            cell.setCellValue("Comisión Fija anticioada por Acceso Preferente UF");
	            cell = row.createCell(4);
	            cell.setCellStyle(styles.get("texto_black_wrapped"));
	            cell.setCellValue("Comisión Corredora (sobre la recaudación)");
	            cell = row.createCell(5);
	            cell.setCellStyle(styles.get("texto_black_wrapped"));
	            cell.setCellValue("Comisión de recaudación y Cobranza Variable");
	            cell = row.createCell(6);
	            cell.setCellStyle(styles.get("texto_lavender_wrapped"));
	            cell.setCellValue("% EXENTA");
	            cell = row.createCell(7);
	            cell.setCellStyle(styles.get("texto_lavender_wrapped"));
	            cell.setCellValue("% AFECTA");
	            cell = row.createCell(8);
	            cell.setCellStyle(styles.get("texto_lavender_wrapped"));
	            cell.setCellValue("% NETA");
	            cell = row.createCell(9);
	            cell.setCellStyle(styles.get("texto_lavender_wrapped"));
	            cell.setCellValue("% IVA");
	            cell = row.createCell(10);
	            cell.setCellStyle(styles.get("texto_lavender_wrapped"));
	            cell.setCellValue("% BRUTA");
		        
	            for (Commission item : colComisionABC) {
		            row = sheetCuadroComAbc.createRow(++rowCount);
		            
		            cell = row.createCell(0);
		            cell.setCellValue(item.getCompany().getName());
		            cell = row.createCell(1);
		            cell.setCellValue(item.getCodProducto() == null ? 0 : item.getCodProducto());
		            cell = row.createCell(2);
		            cell.setCellValue(item.getRubro());
		            cell = row.createCell(3);
		            cell.setCellValue(item.getComFijaAnticipada() == null ? 0 : item.getComFijaAnticipada());
		            cell = row.createCell(4);
		            cell.setCellValue(item.getComCorredora() == null ? 0 : item.getComCorredora());
		            cell = row.createCell(5);
		            cell.setCellValue(item.getComRecCobranza() == null ? 0 : item.getComRecCobranza());
		            cell = row.createCell(6);
		            cell.setCellValue(item.getPorcExenta() == null ? 0 : item.getPorcExenta());
		            cell = row.createCell(7);
		            cell.setCellValue(item.getPorcAfecta() == null ? 0 : item.getPorcAfecta());
		            cell = row.createCell(8);
		            cell.setCellValue(item.getPorcNeta() == null ? 0 : item.getPorcNeta());
		            cell = row.createCell(9);
		            cell.setCellValue(item.getPorcIva() == null ? 0 : item.getPorcIva());
		            cell = row.createCell(10);
		            cell.setCellValue(item.getPrimaBruta() == null ? 0 : item.getPrimaBruta());
		        }
	            
	            
	            //HOJA DIN
	            rowCount = 0;
		        
		        //HEADER
	            sheetCuadroComDin.setColumnWidth(0, 10*256);
	            sheetCuadroComDin.setColumnWidth(1, 11*256);
	            sheetCuadroComDin.setColumnWidth(2, 13*256);
	            sheetCuadroComDin.setColumnWidth(3, 12*256);
	            sheetCuadroComDin.setColumnWidth(4, 12*256);
	            sheetCuadroComDin.setColumnWidth(5, 11*256);
	            sheetCuadroComDin.setColumnWidth(6, 11*256);
	            sheetCuadroComDin.setColumnWidth(7, 11*256);
	            sheetCuadroComDin.setColumnWidth(8, 11*256);
	            sheetCuadroComDin.setColumnWidth(9, 11*256);
	            sheetCuadroComDin.setColumnWidth(10, 11*256);
		        
	            row = sheetCuadroComDin.createRow(0);
		        
		        cell = row.createCell(0);
		        cell.setCellStyle(styles.get("texto_white_wrapped"));
	            cell.setCellValue("Compañía");
	            cell = row.createCell(1);
	            cell.setCellStyle(styles.get("texto_white_wrapped"));
	            cell.setCellValue("Cod. Producto");
	            cell = row.createCell(2);
	            cell.setCellStyle(styles.get("texto_white_wrapped"));
	            cell.setCellValue("Rubro");
	            cell = row.createCell(3);
	            cell.setCellStyle(styles.get("texto_grey_wrapped"));
	            cell.setCellValue("Comisión Fija anticioada por Acceso Preferente UF");
	            cell = row.createCell(4);
	            cell.setCellStyle(styles.get("texto_black_wrapped"));
	            cell.setCellValue("Comisión Corredora (sobre la recaudación)");
	            cell = row.createCell(5);
	            cell.setCellStyle(styles.get("texto_black_wrapped"));
	            cell.setCellValue("Comisión de recaudación y Cobranza Variable");
	            cell = row.createCell(6);
	            cell.setCellStyle(styles.get("texto_lavender_wrapped"));
	            cell.setCellValue("% EXENTA");
	            cell = row.createCell(7);
	            cell.setCellStyle(styles.get("texto_lavender_wrapped"));
	            cell.setCellValue("% AFECTA");
	            cell = row.createCell(8);
	            cell.setCellStyle(styles.get("texto_lavender_wrapped"));
	            cell.setCellValue("% NETA");
	            cell = row.createCell(9);
	            cell.setCellStyle(styles.get("texto_lavender_wrapped"));
	            cell.setCellValue("% IVA");
	            cell = row.createCell(10);
	            cell.setCellStyle(styles.get("texto_lavender_wrapped"));
	            cell.setCellValue("% BRUTA");
		        
	            for (Commission item : colComisionDIN) {
		            row = sheetCuadroComDin.createRow(++rowCount);
		            
		            cell = row.createCell(0);
		            cell.setCellValue(item.getCompany().getName());
		            cell = row.createCell(1);
		            cell.setCellValue(item.getCodProducto() == null ? 0 : item.getCodProducto());
		            cell = row.createCell(2);
		            cell.setCellValue(item.getRubro());
		            cell = row.createCell(3);
		            cell.setCellValue(item.getComFijaAnticipada() == null ? 0 : item.getComFijaAnticipada());
		            cell = row.createCell(4);
		            cell.setCellValue(item.getComCorredora() == null ? 0 : item.getComCorredora());
		            cell = row.createCell(5);
		            cell.setCellValue(item.getComRecCobranza() == null ? 0 : item.getComRecCobranza());
		            cell = row.createCell(6);
		            cell.setCellValue(item.getPorcExenta() == null ? 0 : item.getPorcExenta());
		            cell = row.createCell(7);
		            cell.setCellValue(item.getPorcAfecta() == null ? 0 : item.getPorcAfecta());
		            cell = row.createCell(8);
		            cell.setCellValue(item.getPorcNeta() == null ? 0 : item.getPorcNeta());
		            cell = row.createCell(9);
		            cell.setCellValue(item.getPorcIva() == null ? 0 : item.getPorcIva());
		            cell = row.createCell(10);
		            cell.setCellValue(item.getPrimaBruta() == null ? 0 : item.getPrimaBruta());
		        }
            }
            else {
		        //HOJA Cuadro Comisiones DISTINTAS DE OHIO************************************************************************************
            	
            	XSSFSheet sheetCuadroCom = workbook.createSheet("Cuadro Comisiones");
            	
	            rowCount = 0;
		        
		        //HEADER
	            sheetCuadroCom.setColumnWidth(0, 10*256);
	            sheetCuadroCom.setColumnWidth(1, 11*256);
	            sheetCuadroCom.setColumnWidth(2, 13*256);
	            sheetCuadroCom.setColumnWidth(3, 12*256);
	            sheetCuadroCom.setColumnWidth(4, 12*256);
	            sheetCuadroCom.setColumnWidth(5, 11*256);
	            sheetCuadroCom.setColumnWidth(6, 11*256);
		        
	            row = sheetCuadroCom.createRow(0);
		        
		        cell = row.createCell(0);
		        cell.setCellStyle(styles.get("texto_white_wrapped"));
	            cell.setCellValue("Compañía");
	            cell = row.createCell(1);
	            cell.setCellStyle(styles.get("texto_white_wrapped"));
	            cell.setCellValue("Cod. Producto");
	            cell = row.createCell(2);
	            cell.setCellStyle(styles.get("texto_white_wrapped"));
	            cell.setCellValue("Rubro");
	            cell = row.createCell(3);
	            cell.setCellStyle(styles.get("texto_grey_wrapped"));
	            cell.setCellValue("Comisión Fija anticioada por Acceso Preferente UF");
	            cell = row.createCell(4);
	            cell.setCellStyle(styles.get("texto_black_wrapped"));
	            cell.setCellValue("Comisión Corredora (sobre la recaudación)");
	            cell = row.createCell(5);
	            cell.setCellStyle(styles.get("texto_black_wrapped"));
	            cell.setCellValue("Comisión de recaudación y Cobranza Variable");
	            
	            //SOLO PARA COMPAÑÍA BICE
	            if (companyId == 4)
	            {
		            cell = row.createCell(6);
		            cell.setCellStyle(styles.get("texto_white_wrapped"));
		            cell.setCellValue("PRIMA NETA");
		            cell = row.createCell(7);
		            cell.setCellStyle(styles.get("texto_white_wrapped"));
		            cell.setCellValue("PRIMA BRUTA");
	            }
		        
	            for (Commission item : colComision) {
		            row = sheetCuadroCom.createRow(++rowCount);
		            
		            cell = row.createCell(0);
		            cell.setCellValue(item.getCompany().getName());
		            cell = row.createCell(1);
		            cell.setCellValue(item.getCodProducto() == null ? 0 : item.getCodProducto());
		            cell = row.createCell(2);
		            cell.setCellValue(item.getRubro());
		            cell = row.createCell(3);
		            cell.setCellValue(item.getComFijaAnticipada() == null ? 0 : item.getComFijaAnticipada());
		            cell = row.createCell(4);
		            cell.setCellValue(item.getComCorredora() == null ? 0 : item.getComCorredora());
		            cell = row.createCell(5);
		            cell.setCellValue(item.getComRecCobranza() == null ? 0 : item.getComRecCobranza());
		            
		            //SOLO PARA COMPAÑÍA BICE
		            if (companyId == 4)
		            {
			            cell = row.createCell(6);
			            cell.setCellValue(item.getPrimaNeta() == null ? 0 : item.getPrimaNeta());
			            cell = row.createCell(7);
			            cell.setCellValue(item.getPrimaBruta() == null ? 0 : item.getPrimaBruta());
		            }
		        }
            }
	        
	        workbook.write(returnStream);
	        
	        workbook.close();
		}
		catch(Exception ex)
		{
			CommonMessages.ShowErrorMessage("Error", "No fue posible generar el reporte.");
		}
		
		return returnStream;
	}
	
	public void CloseDialogReport(){
		this.UFMessage = "";
		this.UFValue = "";
		this.validUF = true;
		RequestContext context = RequestContext.getCurrentInstance();
		context.execute("$('#dialogConfirmReport').hide();");
	}
	
	public Boolean ValidateForm()
	{
		if (this.year <= 0)	{
			CommonMessages.ShowWarningMessage("Datos Incompletos", "Debe seleccionar un año.");
			return false;
		}
		
		if (this.month <= 0) {
			CommonMessages.ShowWarningMessage("Datos Incompletos", "Debe seleccionar un mes.");
			return false;
		}
		
		if (this.companyId <= 0) {
			CommonMessages.ShowWarningMessage("Datos Incompletos", "Debe seleccionar una compañia.");
			return false;
		}
		
		if (!ValidateGenerationPermissions()) {
			CommonMessages.ShowWarningMessage("No Autorizado:", "Aún no se ha generado el reporte de cierre del periodo actual y usted no tiene los permisos necesarios para generarlo.");
			return false;
		}
		
		return true;
	}
	
	public Boolean ValidateGenerationPermissions() {
		Boolean canGenerate = false;
		
		if (this.companyId == 1 || this.companyId == 2 || this.companyId == 4) {
			if (this.loggedUser != null)
			{
				List<Authority> authorities = authorityRepository.findByUserId(this.loggedUser.getId());
				
				for (Authority auth : authorities) {
					//Role rol = roleRepository.findOne(auth.getId());
					//System.out.println("************************************* ROL: " + auth.getId());
					if (auth.getRole().getName().equals("ROLE_ADMIN")){
						canGenerate = true;
					}
				}
				
				ReadBinnacle();
				
				if (!canGenerate)
					canGenerate = this.binnacles.size() > 0;
			}
		}
		else {
			return true;
		}
		
		return canGenerate;
	}
	
	public Boolean ValidateUF()
	{
		this.UFMessage = "";
		this.validUF = true;
		
		if (this.UFValue.contains(".") || this.UFValue.contains("-")){
			this.validUF = false;
			this.UFMessage  = "El formato del valor U.F. es 000000,000000. Ejemplo: 25345,334345";
			return false;
		}
		
		try {
			Double numberUF = Double.parseDouble(this.UFValue.replace(",", "."));
		} catch (NumberFormatException e) {
			this.validUF = false;
			this.UFMessage  = "El formato del valor U.F. es 000000,000000. Ejemplo: 25345,334345";
			return false;
		}
		
		return true;
	}

	public Boolean ValidateClosedPeriod()
	{
		List<Period> colPeriods = periodRepository.findByYearMonthType(this.year, this.month, fileTypesVentas.get(0).getId());
		
		if (colPeriods.size() <= 0)
		{
			CommonMessages.ShowWarningMessage("Periodo Inválido", "El periodo seleccionado no es válido. No existe información cargada en el sistema.");
			return false;
		}
		
		for (Period per : colPeriods)
			if (!per.getCerrado()) {
				CommonMessages.ShowWarningMessage("Periodo Inválido", "El periodo seleccionado no está cerrado. Debe cerrarse para generar el reporte.");
				return false;
			}
		
		return true;
	}

	public void InsertBinnacle(FileType fileType, Integer binType, String fileName, String valorUF){
		if (fileTypesVentas.size() > 0)
		{
			List<Period> colPeriods = periodRepository.findByYearMonthType(this.year, this.month, fileTypesVentas.get(0).getId());
			
			if (colPeriods.size() > 0)
			{
				Binnacle binnacle = new Binnacle();
				java.util.Date fecha = new Date();
				binnacle.setBinDate(fecha);
				binnacle.setFileType(fileType);
				binnacle.setBinType(binType);
				User userSession = (User) getCurrentInstance().getExternalContext().getSessionMap().get("user"); 
				binnacle.setUser(userSession);
				binnacle.setPeriod(colPeriods.get(0));
				binnacle.setDetails(valorUF);
				binnacle.setDetails2(fileName);
				binnacleRepository.save(binnacle);
			}
		}
		else
		{
			CommonMessages.ShowErrorMessage("Error:", "No se cargaron los tipos de archivo de Ventas.");
		}
	}
	
    public void ReadBinnacle(){
    	try{
    		List<FileTypeCompany> ftCollReporte = fileTypeCompanyRepository.findByCompanyCategory(this.companyId, "reporte");
    		
    		if (ftCollReporte.size() > 0)
    		{
	    		if (binnacles == null)
	    			binnacles = new ArrayList<Binnacle>();
	    		
	    		binnacles.clear();
	    		
	    		List<Binnacle> colBin = binnacleRepository.findLastClosedByTypeAndPeriod(this.year, this.month, ftCollReporte.get(0).getFileType().getId(), 2);
	    		
	    		if (colBin.size() > 0)
	    		{
	        		binnacles.add(colBin.get(0));
	        		this.UFValue = colBin.get(0).getDetails();
	    		}
    		}
			
    	}catch(Exception e){
    		e.printStackTrace();
    	}
	}
}
