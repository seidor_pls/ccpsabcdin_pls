package com.ccps.web.bean;

import static javax.faces.context.FacesContext.getCurrentInstance;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.event.AjaxBehaviorEvent;

import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.ccps.web.common.CommonMessages;
import com.ccps.web.model.BaseType;
import com.ccps.web.model.Binnacle;
import com.ccps.web.model.FileType;
import com.ccps.web.model.Massive;
import com.ccps.web.model.Period;
import com.ccps.web.model.User;
import com.ccps.web.model.repository.BaseTypeRepository;
import com.ccps.web.model.repository.BinnacleRepository;
import com.ccps.web.model.repository.FileTypeRepository;
import com.ccps.web.model.repository.MassiveRepository;
import com.ccps.web.model.repository.PeriodRepository;

/**
 * author
 * plopezs
 */

@ManagedBean(name="massiveBean")
@SessionScoped
@Component(value="massiveBean")
@Scope("session")
public class MassiveBean implements Serializable{
	private static final long serialVersionUID = 6650231242030770594L;
	
	private Boolean error = false;
	
	private UploadedFile file;
	private String cantRegistros = "999";
	private String totalMontoAsegurado = "999";
	private String totalValorPrima = "999";
	private int closingYear = 0;
	private int closingMonth = 0;
	private int year = 0;
	private int month = 0;
	private String strMonth = "";
	private String tipoSeguro = "";
	private int baseTypeId = 0;
	private List<BaseType> colBaseType;
	private List<BaseType> colSelectedBaseType;
	private Map<Integer, String> colYears;
	private Map<Integer, String> colMonths;
	private List<Massive> currentPeriodFiles;
	List<FileType> fileTypesMassive;
	private List<Binnacle> binnacles;
	
	@Autowired
	private BaseTypeRepository baseTypeRepository;
	
	@Autowired
	private FileTypeRepository fileTypeRepository;
	
	@Autowired
	private MassiveRepository massiveRepository;
	
	@Autowired
	private PeriodRepository periodRepository;
	
	@Autowired
	private BinnacleRepository binnacleRepository;
	
	//PROPERTIES
	public Boolean getError() {
		return error;
	}

	public void setError(Boolean error) {
		this.error = error;
	}

	public UploadedFile getFile() {
		return file;
	}

	public void setFile(UploadedFile file) {
		this.file = file;
	}

	public String getCantRegistros() {
		return cantRegistros;
	}

	public void setCantRegistros(String cantRegistros) {
		this.cantRegistros = cantRegistros;
	}

	public String getTotalMontoAsegurado() {
		return totalMontoAsegurado;
	}

	public void setTotalMontoAsegurado(String totalMontoAsegurado) {
		this.totalMontoAsegurado = totalMontoAsegurado;
	}

	public String getTotalValorPrima() {
		return totalValorPrima;
	}

	public void setTotalValorPrima(String totalValorPrima) {
		this.totalValorPrima = totalValorPrima;
	}

	public int getClosingYear() {
		return closingYear;
	}

	public void setClosingYear(int closingYear) {
		this.closingYear = closingYear;
	}

	public int getClosingMonth() {
		return closingMonth;
	}

	public void setClosingMonth(int closingMonth) {
		this.closingMonth = closingMonth;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		this.month = month;
	}

	public String getTipoSeguro() {
		return tipoSeguro;
	}

	public void setTipoSeguro(String tipoSeguro) {
		this.tipoSeguro = tipoSeguro;
	}

	public int getBaseTypeId() {
		return baseTypeId;
	}

	public void setBaseTypeId(int baseTypeId) {
		this.baseTypeId = baseTypeId;
	}
	
	public List<BaseType> getColBaseType() {
		return colBaseType;
	}

	public void setColBaseType(List<BaseType> colBaseType) {
		this.colBaseType = colBaseType;
	}

	public Map<Integer, String> getColYears() {
		return colYears;
	}

	public void setColYears(Map<Integer, String> colYears) {
		this.colYears = colYears;
	}

	public Map<Integer, String> getColMonths() {
		return colMonths;
	}

	public void setColMonths(Map<Integer, String> colMonths) {
		this.colMonths = colMonths;
	}

	public String getStrMonth() {
		return strMonth;
	}

	public void setStrMonth(String strMonth) {
		this.strMonth = strMonth;
	}

	public List<Massive> getCurrentPeriodFiles() {
		return currentPeriodFiles;
	}

	public void setCurrentPeriodFiles(List<Massive> currentPeriodFiles) {
		this.currentPeriodFiles = currentPeriodFiles;
	}
	
	public List<FileType> getFileTypesMassive() {
		return fileTypesMassive;
	}

	public void setFileTypesMassive(List<FileType> fileTypesMassive) {
		this.fileTypesMassive = fileTypesMassive;
	}
	
	public List<Binnacle> getBinnacles() {
		return binnacles;
	}

	public void setBinnacles(List<Binnacle> binnacles) {
		this.binnacles = binnacles;
	}
	
	public Boolean getCanClosePeriod() {
		return this.currentPeriodFiles.size() == this.colBaseType.size();
	}

	public List<BaseType> getColSelectedBaseType() {
		return colSelectedBaseType;
	}

	public void setColSelectedBaseType(List<BaseType> colSelectedBaseType) {
		this.colSelectedBaseType = colSelectedBaseType;
	}
	
	public Boolean getCanRollbackPeriod() {
		if (this.binnacles.size() > 0)
				return false;
		
		return true;
	}

	//METHODS
	@PostConstruct
	public void PageLoad(){
		this.colBaseType = baseTypeRepository.findAll();
		
		InitYearsMonths();
		InitPeriod();
		ReadBinnacle();
		this.currentPeriodFiles = GetCurrentPeriodMassiveFiles();
	}
	
	public void InitYearsMonths()
	{
		try
		{
			Date today = new Date();
			
			this.colYears = new HashMap<Integer, String>();
			this.colMonths = new HashMap<Integer, String>();
			
			//Agrega el año actual y los 4 siguientes.
			for (Integer i = today.getYear(); i < today.getYear() + 5; i++)
			{
				this.colYears.put(i, i.toString());
			}
			
			this.colMonths.put(1, "Enero");
			this.colMonths.put(2, "Febrero");
			this.colMonths.put(3, "Marzo");
			this.colMonths.put(4, "Abril");
			this.colMonths.put(5, "Mayo");
			this.colMonths.put(6, "Junio");
			this.colMonths.put(7, "Julio");
			this.colMonths.put(8, "Agosto");
			this.colMonths.put(9, "Septiembre");
			this.colMonths.put(10, "Octubre");
			this.colMonths.put(11, "Noviembre");
			this.colMonths.put(12, "Diciembre");
		}
		catch (Exception e) 
		{
			CommonMessages.ShowErrorMessage("Error:", "Error al inicializar los meses y años. " + e.getMessage());
	    }
	}
	
	public void InitPeriod(){
		try
		{
			this.fileTypesMassive = fileTypeRepository.findByCategory("masivos");
			
			if (this.fileTypesMassive.size() > 0) {
				int maxYear = periodRepository.findMaxYearClosedPeriodsByType(this.fileTypesMassive.get(0).getId());
				int maxMonth = 0;
				
				List<Period> colPeriods = periodRepository.findAllClosedPeriodsByYearType(maxYear, this.fileTypesMassive.get(0).getId());
				
				for (Period period:colPeriods){
					if (period.getMes() > maxMonth)
						maxMonth = period.getMes();
				}
				
				if (maxMonth < 12) {
					maxMonth++;
				}
				else {
					maxYear++;
					maxMonth = 1;
				}
				
				colPeriods = periodRepository.findByYearMonthType(maxYear, maxMonth, this.fileTypesMassive.get(0).getId());
				
				//Si no existe creo un nuevo periodo
				if (colPeriods.size() <= 0)
				{
					for (FileType ft : fileTypesMassive)
					{
						Period periodo = new Period(ft, maxMonth, maxYear, false);
						periodRepository.save(periodo);
					}
				}
				
				this.month = maxMonth;
				this.year = maxYear;
				this.strMonth = this.colMonths.get(maxMonth);
			}
		}
		catch (Exception e)
		{
			CommonMessages.ShowErrorMessage("Error:", "Error al tratar de inicializar el periodo. " + e.getMessage());
	    }
	}
	
	public void ChangeTipoSeguro(AjaxBehaviorEvent event)
	{
		if (this.tipoSeguro.isEmpty())
		{
			this.colSelectedBaseType = null;
			return;
		}
		
		this.colSelectedBaseType = baseTypeRepository.findByTipo(this.tipoSeguro);
	}
	
	public void HandleFileUpload(FileUploadEvent event) {
		this.file = event.getFile();
	}
	
	public void DeleteCarga(int fileTypeId, String nombreInterno) {
		/*BaseType bt = baseTypeRepository.findByNombreInterno(nombreInterno);
		
		List<Massive> colMassive = massiveRepository.findByFileTypeBasePeriod(fileTypeId, bt.getId(), this.month, this.year);
		List<Binnacle> colBinnacle = binnacleRepository.findLastByTypeAndPeriod(this.year, this.month, fileTypeId, 1);
		
		for (Massive m : colMassive) {
			massiveRepository.delete(m);
		}
		
		for (Binnacle b : colBinnacle) {
			binnacleRepository.delete(b);
		}*/
		
		CommonMessages.ShowInfoMessage("Info:", "Carga eliminada correctamente.");
	}
	
	public void Upload() {
		try
		{
			FileType fileType = new FileType();
			BaseType baseType = new BaseType();
			RequestContext context = RequestContext.getCurrentInstance();
			
			if (!ValidateForm())
			{
				context.execute("PF('statusDialog').hide();");
				return;
			}
			
			for (FileType ft : this.fileTypesMassive)
			{
				if (this.tipoSeguro.equals(ft.getType()))
				{
					fileType = ft;
					break;
				}
			}
			
			baseType = baseTypeRepository.findOne(this.baseTypeId);
			
			//VALIDA SI YA SE HA CARGADO UN ARCHIVO
			List<Massive> colMassive = massiveRepository.findByFileTypeBasePeriod(fileType.getId(), baseType.getId(), this.month, this.year);
			
			//Existen datos ya cargados para el periodo de fechas.
			if (colMassive.size() > 0)
			{
				context.execute("PF('statusDialog').hide();");
				context.execute("$('#dialogConfirmRecarga').show();");
				return;
			}
			
			List<Period> colPeriods = periodRepository.findAllOpenPeriodsByMonthYearType(this.month, this.year, fileType.getId());
			
			Massive objMassive = GetMassiveFromBase(this.file.getInputstream(), fileType, baseType);
			objMassive.setPeriod(colPeriods.get(0));
			
			SaveMassive(objMassive, fileType, this.file.getFileName(), baseType.getNombreInterno());
			
			Integer cantReg = objMassive.getCantRegistros();
			Long totalMonAse = objMassive.getTotalMontoAsegurado();
			Long totalValPrima = objMassive.getTotalValorPrima();
			
			this.cantRegistros = cantReg.toString();
			this.totalMontoAsegurado = totalMonAse.toString();
			this.totalValorPrima = totalValPrima.toString();
		
			CommonMessages.ShowInfoMessage("", "Archivo procesado correctamente.");
			CommonMessages.ShowInfoMessage("", "Cantidad de Registros Procesados:" + String.format("%,d", objMassive.getCantRegistros()).replace(",", ".") + ".");
			CommonMessages.ShowInfoMessage("", "Total Monto Asegurado: $" + String.format("%,d", objMassive.getTotalMontoAsegurado()).replace(",", ".") + ".");
			CommonMessages.ShowInfoMessage("", "Total Valor Prima: $" + String.format("%,d", objMassive.getTotalValorPrima()).replace(",", ".") + ".");
			
			context.execute("$('#dialogConfirmRecarga').hide();");
			context.execute("PF('statusDialog').hide();");
			
			this.file = null;
		}
		catch (IOException e)
		{
			CommonMessages.ShowErrorMessage("Error", "No fue posible leer el archivo. Es probable que se encuentre bloqueado por otro proceso, o fue eliminado.");
	    }
		catch (Exception e)
		{
			CommonMessages.ShowErrorMessage("Error", "No se pudo procesar archivo. " + e.getMessage());
	    }
	}
	
	public void AcceptReload() {
		try
		{
			FileType fileType = new FileType();
			BaseType baseType = new BaseType();
			
			if (!ValidateForm())
				return;
			
			for (FileType ft : this.fileTypesMassive)
			{
				if (this.tipoSeguro.equals(ft.getType()))
				{
					fileType = ft;
					break;
				}
			}
			
			baseType = baseTypeRepository.findOne(this.baseTypeId);
			
			//OBTIENE EL ARCHIVO CARGADO Y BORRA SUS REGISTROS
			List<Massive> colMassive = massiveRepository.findByFileTypeBasePeriod(fileType.getId(), baseType.getId(), this.month, this.year);
			for (Massive mas:colMassive)
			{
				massiveRepository.delete(mas);
			}
			
			List<Period> colPeriods = periodRepository.findAllOpenPeriodsByMonthYearType(this.month, this.year, fileType.getId());
			
			Massive objMassive = GetMassiveFromBase(this.file.getInputstream(), fileType, baseType);
			objMassive.setPeriod(colPeriods.get(0));
			
			SaveMassive(objMassive, fileType, this.file.getFileName(), baseType.getNombreInterno());
			
			Integer cantReg = objMassive.getCantRegistros();
			Long totalMonAse = objMassive.getTotalMontoAsegurado();
			Long totalValPrima = objMassive.getTotalValorPrima();
			
			this.cantRegistros = cantReg.toString();
			this.totalMontoAsegurado = totalMonAse.toString();
			this.totalValorPrima = totalValPrima.toString();
		
			CommonMessages.ShowInfoMessage("", "Archivo procesado correctamente.");
			CommonMessages.ShowInfoMessage("", "Cantidad de Registros Procesados:" + String.format("%,d", objMassive.getCantRegistros()).replace(",", ".") + ".");
			CommonMessages.ShowInfoMessage("", "Total Monto Asegurado: $" + String.format("%,d", objMassive.getTotalMontoAsegurado()).replace(",", ".") + ".");
			CommonMessages.ShowInfoMessage("", "Total Valor Prima: $" + String.format("%,d", objMassive.getTotalValorPrima()).replace(",", ".") + ".");
			
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("$('#dialogConfirmRecarga').hide();");
			context.execute("PF('statusDialog').hide();");
			
			this.file = null;
		}
		catch (IOException e) 
		{
			CommonMessages.ShowErrorMessage("Error:", "No fue posible leer el archivo. Es probable que se encuentre bloqueado por otro proceso, o fue eliminado.");
	    }
		catch (Exception e) 
		{
			CommonMessages.ShowErrorMessage("Error:", "No fue posible leer el archivo. " + e.getMessage());
	    }
	}
	
	public Massive GetMassiveFromBase(InputStream input, FileType fileType, BaseType baseType) throws Exception {
		
		Massive objReturn = new Massive();
		
		try
		{
	    	Scanner scanner = new Scanner(input);
	    	
	    	Integer lineNum = 0;
	    	String line = scanner.nextLine();
	    	long totalMontoAsegurado = 0l;
	    	long totalValorPrima = 0l;
	    	
	    	while (scanner.hasNextLine()) {
	    		lineNum++;
	    		
	    		line = scanner.nextLine();
	    		
	    		String[] attributes = line.split(";");
				
				try
				{
					if (attributes.length > 0)
					{
						int montoAsegurado = attributes[2].trim().equals("") ? 0 : Integer.parseInt(attributes[2]);
						int valorPrima = attributes[6].trim().equals("") ? 0 : Integer.parseInt(attributes[6]);
						
						totalMontoAsegurado += montoAsegurado;
						totalValorPrima += valorPrima;
					}
				}
				catch (Exception e)
				{
					scanner.close();
					throw new Exception("Columna o tipo no esperado en linea " + lineNum + ": " + e.getMessage() + ".");
				}
	    	}
	    	
	    	scanner.close();
	    	
	    	objReturn.setBaseType(baseType);
	    	objReturn.setCantRegistros(lineNum);
	    	objReturn.setFileType(fileType);
	    	objReturn.setTotalMontoAsegurado(totalMontoAsegurado);
	    	objReturn.setTotalValorPrima(totalValorPrima);
	    }
		catch (Exception e) {
			throw new Exception("El formato del archivo es incorrecto.");
	    }
		
		return objReturn;
	}
	
	public void CloseMonth() {
		try
		{
			for (FileType ft : fileTypesMassive)
			{
				List<Period> colPeriods = periodRepository.findByYearMonthType(this.year, this.month, ft.getId());
				
				for (Period per: colPeriods){
					per.setCerrado(true);
					periodRepository.save(per);
					InsertBinnacle(ft, 4, "Masivos: Cierre de Mes", "");
				}
			}

			InitPeriod();
			ReadBinnacle();
			this.currentPeriodFiles = GetCurrentPeriodMassiveFiles();
			
			CommonMessages.ShowInfoMessage("Info:", "Mes cerrado correctamente.");
			
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("$('#dialogConfirmCierre').hide();");
		}
		catch (Exception e) 
		{
			CommonMessages.ShowErrorMessage("Error:", "Error al intentar cerrar el mes. " + e.getMessage());
	    }
	}

	public void SaveMassive(Massive objMassive, FileType ft, String fileName, String poliza){
		massiveRepository.save(objMassive);
		InsertBinnacle(ft, 1, fileName, poliza);
		ReadBinnacle();
		this.currentPeriodFiles = GetCurrentPeriodMassiveFiles();
	}
	
	public List<Massive> GetCurrentPeriodMassiveFiles()
	{
		List<Massive> colReturn = new ArrayList<Massive>();
		
		for (FileType ft : this.fileTypesMassive)
		{
			List <Massive> colFiles = massiveRepository.findByFileTypePeriod(ft.getId(), this.month, this.year);
			colReturn.addAll(colFiles);
		}

		return colReturn;
	}

	public Boolean ValidateForm()
	{
		if (this.tipoSeguro.trim().equals(""))
		{
			CommonMessages.ShowWarningMessage("Datos Incompletos:", "Debe seleccionar un tipo de seguro.");
			return false;
		}
		
		if (this.baseTypeId == 0)
		{
			CommonMessages.ShowWarningMessage("Datos Incompletos:", "Debe seleccionar una base.");
			return false;
		}
		
		if (this.file == null)
		{
			CommonMessages.ShowWarningMessage("Datos Incompletos:", "Debe seleccionar un archivo a cargar.");
			return false;
		}
		
		return true;
	}

	public void InsertBinnacle(FileType fileType, Integer binType, String fileName, String poliza){
		
		List<Period> colPeriods = periodRepository.findByYearMonthType(this.year, this.month, fileType.getId());
		
		if (colPeriods.size() > 0)
		{
			Binnacle binnacle = new Binnacle(); 
			java.util.Date fecha = new Date();
			binnacle.setBinDate(fecha);
			binnacle.setFileType(fileType);
			binnacle.setBinType(binType);
			User userSession = (User) getCurrentInstance().getExternalContext().getSessionMap().get("user"); 
			binnacle.setUser(userSession);
			binnacle.setPeriod(colPeriods.get(0));
			binnacle.setDetails(poliza);
			binnacle.setDetails2(fileName);
			binnacleRepository.save(binnacle);
		}
	}
	
	public void ReadBinnacle(){
    	try{
    		if (binnacles == null)
    			binnacles = new ArrayList<Binnacle>();
    		
    		binnacles.clear();
    		
    		for (BaseType bt : this.colBaseType)
			{
    			List<Binnacle> colBin = binnacleRepository.findLastByDetailsAndPeriod(this.year, this.month, bt.getNombreInterno(), 1);
    			if (colBin.size() > 0)
        			binnacles.add(colBin.get(0));
			}
    	}catch(Exception e){
    		e.printStackTrace();
    	}
	}
	
	public void RollBackClosePeriod() {
		
		//Elimino los periodos actuales
		for (FileType ft : fileTypesMassive) {
			List<Period> colActualPeriods = periodRepository.findByYearMonthType(this.year, this.month, ft.getId());
			
			for (Period p : colActualPeriods) {
				periodRepository.delete(p);
			}
		}
		
		//Cambio estado de cierre de periodo anterior
		for (FileType ft : fileTypesMassive) {
			int maxYear = periodRepository.findMaxYearClosedPeriodsByType(ft.getId());
			int maxMonth = 0;
			
			List<Period> colPeriods = periodRepository.findAllClosedPeriodsByYearType(maxYear, ft.getId());
			
			for (Period period:colPeriods){
				if (period.getMes() > maxMonth)
					maxMonth = period.getMes();
			}
			
			colPeriods = periodRepository.findByYearMonthType(maxYear, maxMonth, ft.getId());
			
			for (Period p : colPeriods) {
				p.setCerrado(false);
				periodRepository.save(p);
			}
			
			List<Binnacle> colBinFT = binnacleRepository.findByTypeAndAllPeriods(maxYear, maxMonth, ft.getId(), 4);
			
			for (Binnacle b : colBinFT) {
				binnacleRepository.delete(b);
			}
			
			FileType ftRep = fileTypeRepository.findByType("ReporteMasivo");
			
			List<Binnacle> colBinFTRep = binnacleRepository.findByTypeAndAllPeriods(maxYear, maxMonth, ftRep.getId(), 4);
			
			for (Binnacle b : colBinFTRep) {
				binnacleRepository.delete(b);
			}
			
			colBinFTRep = binnacleRepository.findByTypeAndAllPeriods(maxYear, maxMonth, ftRep.getId(), 2);
			
			for (Binnacle b : colBinFTRep) {
				binnacleRepository.delete(b);
			}
		}
		
		InitPeriod();
		ReadBinnacle();
		this.currentPeriodFiles = GetCurrentPeriodMassiveFiles();
		getCanClosePeriod();
		getCanRollbackPeriod();
		
		CommonMessages.ShowInfoMessage("Info:", "Se ha realizado el Rollback del cierre de periodo correctamente.");
		
		RequestContext context = RequestContext.getCurrentInstance();
		context.execute("$('#dialogConfirmRollback').hide();");
	}
}