package com.ccps.web.bean;


import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.primefaces.model.UploadedFile;
import org.springframework.stereotype.Component;

/**
 * @author jDuchens
 *
 */


@ManagedBean(name="uploadFileBean")
@Component("uploadFileBean")
@SessionScoped
public class UploadFileBean{
    private UploadedFile file;
    public UploadFileBean(){
    }
    public UploadedFile getFile() {
        return file;
    }

    public void setFile(UploadedFile file) {
    	FacesContext fc = FacesContext.getCurrentInstance();
        Map<String,String> params = fc.getExternalContext().getRequestParameterMap();
        int userId = Integer.parseInt(params.get("userid"));
    	try {
			InputStream inputStream = file.getInputstream();
			OutputStream outputStream = null;
			File folder = new File("/home/images/"+userId);
			if (!folder.exists()){
				folder.mkdirs();
			}
			File newFile = new File("/home/images/"+userId + "/" + file.getFileName());
			if (!newFile.exists()) {
				newFile.createNewFile();
			}
			outputStream = new FileOutputStream(newFile);
			int read = 0;
			byte[] bytes = new byte[1024];

			while ((read = inputStream.read(bytes)) != -1) {
				outputStream.write(bytes, 0, read);
			}
			outputStream.close();
			inputStream.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
    	
    	
            this.file = file;
        }
}