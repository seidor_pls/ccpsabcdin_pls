package com.ccps.web.bean;

import java.io.IOException;
import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
/**
 * @author jDuchens
 *
 */


@ManagedBean
@SessionScoped
@Component(value="basicBean")
@Scope("session")
public class BasicBean implements Serializable{
	

	private static final long serialVersionUID = 1L;
	private static final Logger logger = LoggerFactory.getLogger(BasicBean.class);
	
	@PostConstruct
    public void init() {
	}
	
	public void Navigate(String pageId)
	{
		try
		{
			if (pageId.contains("cargaInd")) {
				FacesContext.getCurrentInstance().getExternalContext().redirect(FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath() + "/pages/sales/create.jsf");
			}
			if (pageId.contains("cierreInd")) {
				FacesContext.getCurrentInstance().getExternalContext().redirect(FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath() + "/pages/reportes/cierreIndividual.jsf");
			}	
			if (pageId.contains("cargaCP")) {
				FacesContext.getCurrentInstance().getExternalContext().redirect(FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath() + "/pages/cargaGarantia/carga.jsf");
			}
			if (pageId.contains("validaCP")) {
				FacesContext.getCurrentInstance().getExternalContext().redirect(FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath() + "/pages/cargaGarantia/valida.jsf");
			}
			if (pageId.contains("cierreCP")) {
				FacesContext.getCurrentInstance().getExternalContext().redirect(FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath() + "/pages/reportes/cierreGarantias.jsf");
			}
			if (pageId.contains("cargaMas")) {
				FacesContext.getCurrentInstance().getExternalContext().redirect(FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath() + "/pages/cargaMasivo/carga.jsf");
			}
			if (pageId.contains("cierreMas")) {
				FacesContext.getCurrentInstance().getExternalContext().redirect(FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath() + "/pages/reportes/cierreMasivo.jsf");
			}
			if (pageId.contains("comisiones")) {
				FacesContext.getCurrentInstance().getExternalContext().redirect(FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath() + "/pages/comisiones/list.jsf");
			}
			if (pageId.contains("usuarios")) {
				FacesContext.getCurrentInstance().getExternalContext().redirect(FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath() + "/pages/user/list.jsf");
			}
		}
		catch (IOException ex)
		{
			//TODO: Excepcion
		}
	}
}
