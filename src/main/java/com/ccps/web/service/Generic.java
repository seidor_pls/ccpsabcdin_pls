package com.ccps.web.service;

import java.io.File;
import java.io.FileInputStream;

import javax.faces.context.FacesContext;

import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import com.ccps.web.model.User;

/**
 * @author jDuchens
 *
 */


public class Generic {

	
	public static StreamedContent getProfileImage(User userImage){
		
		StreamedContent profileImage = null;
		String contentType = null;
		String destination="/home/images/";
		try{
			FacesContext context = FacesContext.getCurrentInstance();
	    	String fileString = destination+"profile/"+userImage.getId()+"/"+userImage.getPhoto();
			File file = new File(fileString);
			
			if(file.isFile()){
				profileImage = new DefaultStreamedContent(new FileInputStream(fileString), contentType);
			}
			else{
				String filePath = context.getExternalContext().getRealPath("/resources/images/avatar_default.jpg");
				profileImage = new DefaultStreamedContent(new FileInputStream(filePath), contentType);
			}
		}catch(Exception e){ e.printStackTrace(); }
		
		return profileImage;
        
    }
}