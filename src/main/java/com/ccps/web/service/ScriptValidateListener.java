package com.ccps.web.service;

import java.util.List;

import javax.faces.component.UIComponent;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.SystemEvent;
import javax.faces.event.SystemEventListener;
import javax.servlet.http.HttpServletRequest;

/**
 * @author jDuchens
 *
 */


public class ScriptValidateListener implements SystemEventListener {

    @Override
    public void processEvent(SystemEvent event) throws AbortProcessingException {
        UIViewRoot root = (UIViewRoot) event.getSource();
        FacesContext ctx = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest)ctx.getExternalContext().getRequest();
        
        List<UIComponent> resources = root.getComponentResources(ctx, "HEAD");
        if(request.getRequestURL().toString().contains("create") || request.getRequestURL().toString().contains("update") || request.getRequestURL().toString().contains("show")){
        	for (UIComponent r : resources) {
                String name = (String) r.getAttributes().get("name");
                if (name == null) {
                    continue;
                }
                if (name.contains(".js")) {

                }
            }
        }

        resources = root.getComponentResources(ctx, "HEAD");
        if(request.getRequestURL().toString().contains("create") || request.getRequestURL().toString().contains("update") || request.getRequestURL().toString().contains("show")){
        	for (UIComponent r : resources) {
                String name = (String) r.getAttributes().get("name");
                if (name == null) {
                    continue;
                }
                
                if (name.contains(".js")) {

                }
            }
        }
        
        resources = root.getComponentResources(ctx, "HEAD");
        if(request.getRequestURL().toString().contains("create") || request.getRequestURL().toString().contains("update") || request.getRequestURL().toString().contains("show")){
        	for (UIComponent r : resources) {
                String name = (String) r.getAttributes().get("name");
                if (name == null) {
                    continue;
                }
                
                if (name.contains(".js")) {

                }
            }
        }

    }

    @Override
    public boolean isListenerForSource(Object source) {
        return (source instanceof UIViewRoot);
    }
}