package com.ccps.web.converters;
import java.lang.reflect.Field;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.persistence.EntityManager;
import javax.persistence.Id;

import org.springframework.beans.factory.annotation.Autowired;
 
/**
 * @author jDuchens
 *
 */


public class EntityConverter implements Converter {
 
	@Autowired
	private EntityManager em;
 
	@Override
	public Object getAsObject(FacesContext fc, UIComponent component, String string) {
		try {
			String[] split = string.split(":");
			return em.find(Class.forName(split[0]), Long.valueOf(split[1]));
		} catch (ClassNotFoundException e) {
			return null;
		}
	}

	@Override
	public String getAsString(FacesContext fc, UIComponent component, Object object) {
		try {
			Class<? extends Object> clazz = object.getClass();
			for (Field f : clazz.getDeclaredFields()) {
				if (f.isAnnotationPresent(Id.class)) {
					f.setAccessible(true);
					Long id = (Long) f.get(object);
					return clazz.getCanonicalName() + ":" + id.toString();
				}
			}
		} catch (IllegalAccessException e) { }
		return null;
	}
}