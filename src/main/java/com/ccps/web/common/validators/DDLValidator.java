package com.ccps.web.common.validators;

import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import org.primefaces.component.selectonemenu.SelectOneMenu;

@FacesValidator("ddlValidator")
public class DDLValidator implements Validator{

	 @Override
	    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
		 	SelectOneMenu objDDL = (SelectOneMenu)component;
	    	if (value == null) {
	    		throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,objDDL.getValidatorMessage(),null));
	        }
	    	
	    	if (value.toString().equals("0")) {
	    		throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,objDDL.getValidatorMessage(),null));
	        }
	    }
}
