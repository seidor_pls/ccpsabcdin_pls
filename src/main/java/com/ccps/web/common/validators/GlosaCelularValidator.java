package com.ccps.web.common.validators;

import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

@FacesValidator("glosaCelularValidator")
public class GlosaCelularValidator implements Validator{

    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
    	if (value == null) {
            return;
        }

        String strGlosa = (String) value;

        if (strGlosa.trim().isEmpty()) {
            return;
        }
		
		if (strGlosa.trim().length() < 2) {
			throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,"Ingrese una glosa válida.",null));
		}
		
		if (strGlosa.trim().toUpperCase().equals("NO DEFINIDO")) {
			throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,"Ingrese una glosa válida.",null));
		}
		
		List<String> glosasCelulares = new ArrayList<String>();
		glosasCelulares.add("CELULAR");
		glosasCelulares.add("LG L1 E4");
		glosasCelulares.add("CEL SAM");
		glosasCelulares.add("CONTR MO");
		glosasCelulares.add("CONTR ENT");
		glosasCelulares.add("CONTRATO");
		glosasCelulares.add("CEL ALCA");
		glosasCelulares.add("TELEFONI");
		glosasCelulares.add("CELUL SA");
		glosasCelulares.add("TELEFONO");
		glosasCelulares.add("CONT MOV");
		glosasCelulares.add("CEL SON");
		glosasCelulares.add("CEL SAMS");
		glosasCelulares.add("APPLE IPH");
		glosasCelulares.add("ALCATEL");
		glosasCelulares.add("PLAN ENT");
		glosasCelulares.add("PLAN MOV");
		glosasCelulares.add("MI PRIME");
		
		for (String glosa : glosasCelulares)
		{
			if (strGlosa.trim().toUpperCase().contains(glosa))
			{
				return;
			}
		}

        throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,"Ingrese una glosa válida.",null));
    }

}