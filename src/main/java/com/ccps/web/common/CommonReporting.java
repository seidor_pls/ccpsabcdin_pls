package com.ccps.web.common;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

import org.primefaces.model.DefaultStreamedContent;

public final class CommonReporting {
	public static DefaultStreamedContent StreamExcelReport(ByteArrayOutputStream stream, String fileName){
		if (stream != null)
		{
			String contentExcel2007 = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
			String contentExcel97 = "application/vnd.ms-excel";
			String currentContent = "";
			
			if (fileName.toLowerCase().contains(".xlsx"))
				currentContent = contentExcel2007;
			else
				currentContent = contentExcel97;
			
			byte [] outArray = stream.toByteArray();
			
			ByteArrayInputStream bis = new ByteArrayInputStream(outArray);
	        
			return new DefaultStreamedContent(bis, currentContent, fileName);
		}
		else
		{
			return null;
		}
	}
}