package com.ccps.web.common;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

public final class CommonMessages {
	public static void ShowErrorMessage(String summary, String message) {
		FacesMessage msg = new FacesMessage();
		msg.setSeverity(FacesMessage.SEVERITY_ERROR);
		msg.setSummary(summary);
		msg.setDetail(message);
		FacesContext.getCurrentInstance().addMessage(null, msg);
	}
	
	public static void ShowWarningMessage(String summary, String message) {
		FacesMessage msg = new FacesMessage();
		msg.setSeverity(FacesMessage.SEVERITY_WARN);
		msg.setSummary(summary);
		msg.setDetail(message);
		FacesContext.getCurrentInstance().addMessage(null, msg);
	}
	
	public static void ShowInfoMessage(String summary, String message) {
		FacesMessage msg = new FacesMessage();
		msg.setSeverity(FacesMessage.SEVERITY_INFO);
		msg.setSummary(summary);
		msg.setDetail(message);
		FacesContext.getCurrentInstance().addMessage(null, msg);
	}
}