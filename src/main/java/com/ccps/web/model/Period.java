package com.ccps.web.model;

import java.io.Serializable;

import javax.persistence.*;

/**
 * @author plopezs
 *
 */
@Entity
@Table(name="ccps_period")
public class Period implements Serializable {
	private static final long serialVersionUID = 5873684400064992288L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(nullable=false)
	private int id;
	
	//bi-directional many-to-one association to ccpsFileType
	@ManyToOne
	@JoinColumn(name="id_type")
	private FileType type;
	
	@Column(nullable=false)
	private int mes;
	
	@Column(nullable=false)
	private int ano;
	
	@Column(nullable=false)
	private Boolean cerrado;
	
	public Period() {
		
	}
	
	public Period(FileType type, int mes, int ano, Boolean cerrado) {
		this.type = type;
		this.mes = mes;
		this.ano = ano;
		this.cerrado = cerrado;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public FileType getType() {
		return type;
	}

	public void setType(FileType type) {
		this.type = type;
	}

	public int getMes() {
		return mes;
	}

	public void setMes(int mes) {
		this.mes = mes;
	}

	public int getAno() {
		return ano;
	}

	public void setAno(int ano) {
		this.ano = ano;
	}

	public Boolean getCerrado() {
		return cerrado;
	}

	public void setCerrado(Boolean cerrado) {
		this.cerrado = cerrado;
	}
	
}
