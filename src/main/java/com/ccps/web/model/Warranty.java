package com.ccps.web.model;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * author
 * plopezs
 */

@Entity
@Table(name="ccps_warranty")
public class Warranty implements Serializable{
	private static final long serialVersionUID = 6669156456591022392L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(nullable=false)
	private int id;
	
	//uni-directional many-to-one association to ccpsFileType
	@ManyToOne
	@JoinColumn(name="id_type")
	private FileType fileType;
	
	//uni-directional many-to-one association to ccpsPeriod
	@ManyToOne
	@JoinColumn(name="id_period")
	private Period period;
	
	//uni-directional many-to-one association to ccpsEnterprise
	@ManyToOne
	@JoinColumn(name="id_enterprise")
	private Enterprise enterprise;
	
	//uni-directional many-to-one association to ccpsCompany
	@ManyToOne
	@JoinColumn(name="id_company")
	private Company company;
	
	@Column(nullable=true)
	private Integer folio;
	
	@Column(nullable=true)
	private Integer correlativo;
	
	@Column(nullable=true)
	private String rutCliente;
	
	@Column(nullable=true)
	private String apPaterno;
	
	@Column(nullable=true)
	private String apMaterno;
	
	@Column(nullable=true)
	private String nombres;
	
	@Column(nullable=true)
	private String direccion;
	
	@Column(nullable=true)
	private String comuna;
	
	@Column(nullable=true)
	private String ciudad;
	
	@Column(nullable=true)
	private String telefono;
	
	@Column(nullable=true)
	private Integer codSucursal;
	
	@Column(nullable=true)
	private String glosaSucursal;
	
	@Column(nullable=true)
	private String codGarantiaExt;
	
	@Column(nullable=true)
	private String glosaGarantiaExt;
	
	@Column(nullable=true)
	private Date fecVentaGarantia;
	
	@Column(nullable=true)
	private Integer precioVenta;
	
	@Column(nullable=true)
	private String vendedor;
	
	@Column(nullable=true)
	private Integer precioVentaGarantia;
	
	@Column(nullable=true)
	private String articulo;
	
	@Column(nullable=true)
	private String glosaArticulo;
	
	@Column(nullable=true)
	private Date fecInicio;
	
	@Column(nullable=true)
	private Date fecFin;
	
	@Column(nullable=false)
	private String status;
	
	public Warranty(){
		
	}
	
	public Warranty(FileType type, Enterprise enterprise, Period period, Integer folio, Integer correlativo, String rutCliente, String apPaterno,
			String apMaterno, String nombres, String direccion, String comuna, String ciudad, String telefono,
			Integer codSucursal, String glosaSucursal, String codGarantiaExt, String glosaGarantiaExt,
			Date fecVentaGarantia, Integer precioVenta, String vendedor, Integer precioVentaGarantia, String articulo,
			String glosaArticulo, Date fecIni, Date fecFin, String status) {
		super();
		
		this.enterprise = enterprise;
		this.fileType = type;
		this.period = period;
		this.folio = folio;
		this.correlativo = correlativo;
		this.rutCliente = rutCliente;
		this.apPaterno = apPaterno;
		this.apMaterno = apMaterno;
		this.nombres = nombres;
		this.direccion = direccion;
		this.comuna = comuna;
		this.ciudad = ciudad;
		this.telefono = telefono;
		this.codSucursal = codSucursal;
		this.glosaSucursal = glosaSucursal;
		this.codGarantiaExt = codGarantiaExt;
		this.glosaGarantiaExt = glosaGarantiaExt;
		this.fecVentaGarantia = fecVentaGarantia;
		this.precioVenta = precioVenta;
		this.vendedor = vendedor;
		this.precioVentaGarantia = precioVentaGarantia;
		this.articulo = articulo;
		this.glosaArticulo = glosaArticulo;
		this.fecInicio = fecIni;
		this.fecFin = fecFin;
		this.status = status;
	}
	
	public Warranty(String[] attributes, Date fecIni, Date fecFin, FileType type, Period period, Enterprise enterprise, String status) throws Exception{
		super();
		
		try
		{
			SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy");
			
			this.fileType = type;
			this.enterprise = enterprise;
			this.folio = Integer.parseInt(attributes[2]);
			this.correlativo = Integer.parseInt(attributes[3]);
			this.rutCliente = attributes[4];
			this.apPaterno = attributes[5];
			this.apMaterno = attributes[6];
			this.nombres = attributes[7];
			this.direccion = attributes[8];
			this.comuna = attributes[9];
			this.ciudad = attributes[10];
			this.telefono = attributes[11];
			this.codSucursal = Integer.parseInt(attributes[12]);
			this.glosaSucursal = attributes[13];
			this.codGarantiaExt = attributes[14];
			this.glosaGarantiaExt = attributes[15];
			this.fecVentaGarantia = dateFormatter.parse(attributes[16]);
			this.precioVenta = Integer.parseInt(attributes[17].replace(".", ""));
			this.vendedor = attributes[18];
			this.precioVentaGarantia = Integer.parseInt(attributes[19].replace(".", ""));
			this.articulo = attributes[20];
			this.glosaArticulo = attributes[21];
			this.fecInicio = fecIni;
			this.fecFin = fecFin;
			this.period = period;
			this.status = status;
		}
		catch (ParseException ex)
		{
			throw new Exception("Error al convertir fecha a string.");
		}
		catch (NumberFormatException ex)
		{
			throw new Exception("Error al convertir entero a string.");
		}
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Integer getFolio() {
		return folio;
	}

	public void setFolio(Integer folio) {
		this.folio = folio;
	}

	public Integer getCorrelativo() {
		return correlativo;
	}

	public void setCorrelativo(Integer correlativo) {
		this.correlativo = correlativo;
	}

	public String getRutCliente() {
		return rutCliente;
	}

	public void setRutCliente(String rutCliente) {
		this.rutCliente = rutCliente;
	}

	public String getApPaterno() {
		return apPaterno;
	}

	public void setApPaterno(String apPaterno) {
		this.apPaterno = apPaterno;
	}

	public String getApMaterno() {
		return apMaterno;
	}

	public void setApMaterno(String apMaterno) {
		this.apMaterno = apMaterno;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getComuna() {
		return comuna;
	}

	public void setComuna(String comuna) {
		this.comuna = comuna;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public Integer getCodSucursal() {
		return codSucursal;
	}

	public void setCodSucursal(Integer codSucursal) {
		this.codSucursal = codSucursal;
	}

	public String getGlosaSucursal() {
		return glosaSucursal;
	}

	public void setGlosaSucursal(String glosaSucursal) {
		this.glosaSucursal = glosaSucursal;
	}

	public String getCodGarantiaExt() {
		return codGarantiaExt;
	}

	public void setCodGarantiaExt(String codGarantiaExt) {
		this.codGarantiaExt = codGarantiaExt;
	}

	public String getGlosaGarantiaExt() {
		return glosaGarantiaExt;
	}

	public void setGlosaGarantiaExt(String glosaGarantiaExt) {
		this.glosaGarantiaExt = glosaGarantiaExt;
	}

	public Date getFecVentaGarantia() {
		return fecVentaGarantia;
	}

	public void setFecVentaGarantia(Date fecVentaGarantia) {
		this.fecVentaGarantia = fecVentaGarantia;
	}

	public Integer getPrecioVenta() {
		return precioVenta;
	}

	public void setPrecioVenta(Integer precioVenta) {
		this.precioVenta = precioVenta;
	}

	public String getVendedor() {
		return vendedor;
	}

	public void setVendedor(String vendedor) {
		this.vendedor = vendedor;
	}

	public Integer getPrecioVentaGarantia() {
		return precioVentaGarantia;
	}

	public void setPrecioVentaGarantia(Integer precioVentaGarantia) {
		this.precioVentaGarantia = precioVentaGarantia;
	}

	public String getArticulo() {
		return articulo;
	}

	public void setArticulo(String articulo) {
		this.articulo = articulo;
	}

	public String getGlosaArticulo() {
		return glosaArticulo;
	}

	public void setGlosaArticulo(String glosaArticulo) {
		this.glosaArticulo = glosaArticulo;
	}

	public FileType getFileType() {
		return fileType;
	}

	public void setFileType(FileType fileType) {
		this.fileType = fileType;
	}

	public Date getFecInicio() {
		return fecInicio;
	}

	public void setFecInicio(Date fecInicio) {
		this.fecInicio = fecInicio;
	}

	public Date getFecFin() {
		return fecFin;
	}

	public void setFecFin(Date fecFin) {
		this.fecFin = fecFin;
	}

	public Period getPeriod() {
		return period;
	}

	public void setPeriod(Period period) {
		this.period = period;
	}

	public Enterprise getEnterprise() {
		return enterprise;
	}

	public void setEnterprise(Enterprise enterprise) {
		this.enterprise = enterprise;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}
}
