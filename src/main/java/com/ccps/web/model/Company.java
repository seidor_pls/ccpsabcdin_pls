package com.ccps.web.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.*;

/**
 * @author plopezs
 *
 */
@Entity
@Table(name="ccps_company")
public class Company implements Serializable {
	private static final long serialVersionUID = -8514043949806536571L;

	@Id
	@Column(nullable=false)
	private int id;
	
	@Column(nullable=false)
	private String name;
	
	@Column(nullable=false)
	private String rut;
	
	@Column(nullable=false)
	private Boolean enabled;
	
	@Column(nullable=false)
	private Date creaDate;
	
	public Company() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public Date getCreaDate() {
		return creaDate;
	}

	public void setCreaDate(Date creaDate) {
		this.creaDate = creaDate;
	}

	public String getRut() {
		return rut;
	}

	public void setRut(String rut) {
		this.rut = rut;
	}	
}