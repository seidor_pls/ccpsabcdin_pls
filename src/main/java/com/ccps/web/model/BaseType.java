package com.ccps.web.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * author
 * plopezs
 */

@Entity
@Table(name="ccps_base_type")
public class BaseType implements Serializable{
	private static final long serialVersionUID = 425764920071691727L;

	@Id
	@Column(nullable=false)
	private int id;
	
	@Column(nullable=false)
	private String poliza;
	
	@Column(nullable=false)
	private String nombrePoliza;
	
	@Column(nullable=false)
	private String tipo;
	
	@Column(nullable=false)
	private String nombreInterno;
	
	@Column(nullable=false)
	private Double comIntermediacion;
	
	@Column(nullable=false)
	private Double tasaTecnica;
	
	@Column(nullable=false)
	private Boolean enabled;

	public BaseType() {
		super();
		// TODO Auto-generated constructor stub
	}

	public BaseType(int id, String poliza, String nombrePoliza, String tipo, String nombreInterno, Double comIntermediacion,
			Double tasaTecnica) {
		super();
		this.id = id;
		this.poliza = poliza;
		this.nombrePoliza = nombrePoliza;
		this.tipo = tipo;
		this.nombreInterno = nombreInterno;
		this.comIntermediacion = comIntermediacion;
		this.tasaTecnica = tasaTecnica;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPoliza() {
		return poliza;
	}

	public void setPoliza(String poliza) {
		this.poliza = poliza;
	}

	public String getNombrePoliza() {
		return nombrePoliza;
	}

	public void setNombrePoliza(String nombrePoliza) {
		this.nombrePoliza = nombrePoliza;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getNombreInterno() {
		return nombreInterno;
	}

	public void setNombreInterno(String nombreInterno) {
		this.nombreInterno = nombreInterno;
	}

	public Double getComIntermediacion() {
		return comIntermediacion;
	}

	public void setComIntermediacion(Double comIntermediacion) {
		this.comIntermediacion = comIntermediacion;
	}

	public Double getTasaTecnica() {
		return tasaTecnica;
	}

	public void setTasaTecnica(Double tasaTecnica) {
		this.tasaTecnica = tasaTecnica;
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}
	
	
}