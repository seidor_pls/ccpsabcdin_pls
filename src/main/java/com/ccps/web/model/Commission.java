package com.ccps.web.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * @author plopezs
 *
 */
@Entity
@Table(name="ccps_commission")
public class Commission implements Serializable {
	private static final long serialVersionUID = -1431117625851452192L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(nullable=false)
	private int id;
	
	//uni-directional many-to-one association to ccpsFileType
	@ManyToOne
	@JoinColumn(name="id_company")
	private Company company;
	
	//uni-directional many-to-one association to ccpsEnterprise
	@ManyToOne
	@JoinColumn(name="id_enterprise")
	private Enterprise enterprise;
	
	@Column(nullable=true)
	private Integer codProducto;
	
	@Column(nullable=false)
	private int rubro;
	
	@Column(nullable=false)
	private Double comFijaAnticipada;
	
	@Column(nullable=false)
	private Double comCorredora;
	
	@Column(nullable=false)
	private Double comRecCobranza;
	
	@Column(nullable=true)
	private Double primaNeta;
	
	@Column(nullable=true)
	private Double primaBruta;
	
	@Column(nullable=true)
	private Double porcExenta;

	@Column(nullable=true)
	private Double porcAfecta;
	
	@Column(nullable=true)
	private Double porcNeta;
	
	@Column(nullable=true)
	private Double porcIva;
	
	@Column(nullable=true)
	private Double porcBruta;
	
	@Column(nullable=true)
	private Boolean enabled;
	
	public Commission(){
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public Enterprise getEnterprise() {
		return enterprise;
	}

	public void setEnterprise(Enterprise enterprise) {
		this.enterprise = enterprise;
	}

	public Integer getCodProducto() {
		return codProducto;
	}

	public void setCodProducto(Integer codProducto) {
		this.codProducto = codProducto;
	}

	public int getRubro() {
		return rubro;
	}

	public void setRubro(int rubro) {
		this.rubro = rubro;
	}

	public Double getComFijaAnticipada() {
		return comFijaAnticipada;
	}

	public void setComFijaAnticipada(Double comFijaAncipada) {
		this.comFijaAnticipada = comFijaAncipada;
	}

	public Double getComCorredora() {
		return comCorredora;
	}

	public void setComCorredora(Double comFijaCorredora) {
		this.comCorredora = comFijaCorredora;
	}

	public Double getComRecCobranza() {
		return comRecCobranza;
	}

	public void setComRecCobranza(Double comRecCobranza) {
		this.comRecCobranza = comRecCobranza;
	}

	public Double getPrimaNeta() {
		return primaNeta;
	}

	public void setPrimaNeta(Double primaNeta) {
		this.primaNeta = primaNeta;
	}

	public Double getPrimaBruta() {
		return primaBruta;
	}

	public void setPrimaBruta(Double primaBruta) {
		this.primaBruta = primaBruta;
	}

	public Double getPorcExenta() {
		return porcExenta;
	}

	public void setPorcExenta(Double porcExenta) {
		this.porcExenta = porcExenta;
	}

	public Double getPorcAfecta() {
		return porcAfecta;
	}

	public void setPorcAfecta(Double porcAfecta) {
		this.porcAfecta = porcAfecta;
	}

	public Double getPorcNeta() {
		return porcNeta;
	}

	public void setPorcNeta(Double porcNeta) {
		this.porcNeta = porcNeta;
	}

	public Double getPorcIva() {
		return porcIva;
	}

	public void setPorcIva(Double porcIva) {
		this.porcIva = porcIva;
	}

	public Double getPorcBruta() {
		return porcBruta;
	}

	public void setPorcBruta(Double porcBruta) {
		this.porcBruta = porcBruta;
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}
}