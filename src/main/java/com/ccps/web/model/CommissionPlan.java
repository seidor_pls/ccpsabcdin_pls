package com.ccps.web.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author plopezs
 *
 */
@Entity
@Table(name="ccps_commission_plan")
public class CommissionPlan implements Serializable {

	private static final long serialVersionUID = -8968118690264072786L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(nullable=false)
	private int id;
	
	@Column(nullable=false)
	private String tipoPlan;
	
	@Column(nullable=true)
	private Double primaFinalSeguroRobo;
	
	@Column(nullable=true)
	private Double comInterPrimaNeta;
	
	@Column(nullable=true)
	private Integer rangoInferior;
	
	@Column(nullable=true)
	private Integer rangoSuperior;
	
	@Column(nullable=true)
	private Boolean enabled;
	
	public CommissionPlan(){
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTipoPlan() {
		return tipoPlan;
	}

	public void setTipoPlan(String tipoPlan) {
		this.tipoPlan = tipoPlan;
	}

	public Double getPrimaFinalSeguroRobo() {
		return primaFinalSeguroRobo;
	}

	public void setPrimaFinalSeguroRobo(Double primaFinalSeguroRobo) {
		this.primaFinalSeguroRobo = primaFinalSeguroRobo;
	}

	public Double getComInterPrimaNeta() {
		return comInterPrimaNeta;
	}

	public void setComInterPrimaNeta(Double comInterPrimaNeta) {
		this.comInterPrimaNeta = comInterPrimaNeta;
	}

	public Integer getRangoInferior() {
		return rangoInferior;
	}

	public void setRangoInferior(Integer rangoInferior) {
		this.rangoInferior = rangoInferior;
	}

	public Integer getRangoSuperior() {
		return rangoSuperior;
	}

	public void setRangoSuperior(Integer rangoSuperior) {
		this.rangoSuperior = rangoSuperior;
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}
}
