package com.ccps.web.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * @author jDuchens
 *
 */


@Entity
@Table(name="ccps_role")
public class Role implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(nullable=false)
	private int id;

	@Column(length=24)
	private String name;

	//bi-directional many-to-one association to Authority
	@OneToMany(mappedBy="role")
	private List<Authority> authorities;

	public Role() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Authority> getAuthorities() {
		return this.authorities;
	}

	public void setAuthorities(List<Authority> authorities) {
		this.authorities = authorities;
	}

	public Authority addAuthority(Authority authority) {
		getAuthorities().add(authority);
		authority.setRole(this);

		return authority;
	}

	public Authority removeAuthority(Authority authority) {
		getAuthorities().remove(authority);
		authority.setRole(null);

		return authority;
	}

}