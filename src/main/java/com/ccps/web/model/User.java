package com.ccps.web.model;

import java.io.File;
import java.io.FileInputStream;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.faces.context.FacesContext;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;


/**
 * @author jDuchens
 *
 */


@Entity
@Table(name="ccps_user")
public class User implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(nullable=false)
	private int id;

	@Column(name="additional_info", length=512)
	private String additionalInfo;

	@Temporal(TemporalType.DATE)
	private Date birthday;

	@Column(length=60)
	private String degree;
	
	@NotEmpty
	@Column(nullable=false, length=128)
	private String email;

	@Column(length=256)
	private String firstname;

	@Column(length=256)
	private String lastname;
	
	@Column(length=256)
	private String photo;
	
	private boolean enabled;

	@NotEmpty
	@Size(min=4,max=256)
	@Column(nullable=false, length=256)
	private String password;
	
	@Column(name="personal_address", length=256)
	private String personalAddress;

	@Column(name="personal_phone", length=30)
	private String personalPhone;

	@Column(name="work_address", length=256)
	private String workAddress;

	@Column(name="work_phone", length=30)
	private String workPhone;

	//bi-directional many-to-one association to Authority
	@OneToMany(mappedBy="user")
	private List<Authority> authorities;
	
	//bi-directional many-to-one association to Binnacle
	@OneToMany(mappedBy="user")
	private List<Binnacle> binnacle;
	
	@Transient
	private StreamedContent userImage;
	
	public User() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public String getAdditionalInfo() {
		return this.additionalInfo;
	}

	public void setAdditionalInfo(String additionalInfo) {
		this.additionalInfo = additionalInfo;
	}

	public Date getBirthday() {
		return this.birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public String getDegree() {
		return this.degree;
	}

	public void setDegree(String degree) {
		this.degree = degree;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstname() {
		return this.firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return this.lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	
	public boolean getEnabled() {
		return this.enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPersonalAddress() {
		return this.personalAddress;
	}

	public void setPersonalAddress(String personalAddress) {
		this.personalAddress = personalAddress;
	}

	public String getPersonalPhone() {
		return this.personalPhone;
	}

	public void setPersonalPhone(String personalPhone) {
		this.personalPhone = personalPhone;
	}

	public String getWorkAddress() {
		return this.workAddress;
	}

	public void setWorkAddress(String workAddress) {
		this.workAddress = workAddress;
	}

	public String getWorkPhone() {
		return this.workPhone;
	}

	public void setWorkPhone(String workPhone) {
		this.workPhone = workPhone;
	}
	
	public String getPhoto() {
		return this.photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}
	
	public String getMessage() {
		return "Task message";
	}
	
	public List<Binnacle> getBinnacle() {
		return binnacle;
	}

	public void setBinnacle(List<Binnacle> binnacle) {
		this.binnacle = binnacle;
	}

	public StreamedContent getUserImage() {
		StreamedContent profileImage = null;
		String contentType = null;
		String destination="/home/images/";
		try{
			FacesContext context = FacesContext.getCurrentInstance();
	    	String fileString = destination+"profile/"+this.getId()+"/"+this.getPhoto();
			File file = new File(fileString);
			
			if(file.isFile()){
				profileImage = new DefaultStreamedContent(new FileInputStream(fileString), contentType);
			}
			else{
				String filePath = context.getExternalContext().getRealPath("/resources/images/avatar_default.jpg");
				profileImage = new DefaultStreamedContent(new FileInputStream(filePath), contentType);
			}
		}catch(Exception e){ e.printStackTrace(); }
		
		return profileImage;
	}

	public void setUserImage(StreamedContent userImage) {
		this.userImage = userImage;
	}
	
	public List<Authority> getAuthorities() {
		// TODO Auto-generated method stub
		return this.authorities;
	}

	public boolean equals(Object o) {
		if(o == null)
	    	return false;

		return true;
	}


}