package com.ccps.web.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

/**
 * @author plopezs
 *
 */
@Entity
@Table(name="ccps_file_type")
public class FileType implements Serializable {
	private static final long serialVersionUID = -4111418024229577184L;

	@Id
	@Column(nullable=false)
	private int id;
	
	//bi-directional one-to-many association to ccpsWarranty
	@OneToMany(mappedBy="fileType")
	private List<Warranty> warrantyRows;
	
	//bi-directional one-to-many association to ccpsWarranty
	@OneToMany(mappedBy="fileType")
	private List<Sale> saleRows;
	
	//bi-directional one-to-many association to ccpsWarranty
	@OneToMany(mappedBy="fileType")
	private List<Collection> collectionRows;
	
	//bi-directional many-to-one association to Binnacle
	@OneToMany(mappedBy="fileType")
	private List<Binnacle> binnacle;
	
	@Column(nullable=false)
	private Date creaDate;
	
	@Column(nullable=false)
	private String type;
	
	@Column(nullable=false)
	private String category;
	
	@Column(nullable=false)
	private Boolean enabled;
	
	public FileType() {
	}
	
	public FileType(Date date, String type) {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getCreaDate() {
		return creaDate;
	}

	public void setCreaDate(Date creaDate) {
		this.creaDate = creaDate;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public List<Binnacle> getBinnacle() {
		return binnacle;
	}

	public void setBinnacle(List<Binnacle> binnacle) {
		this.binnacle = binnacle;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public List<Warranty> getWarrantyRows() {
		return warrantyRows;
	}

	public void setWarrantyRows(List<Warranty> warrantyRows) {
		this.warrantyRows = warrantyRows;
	}

	public List<Sale> getSaleRows() {
		return saleRows;
	}

	public void setSaleRows(List<Sale> saleRows) {
		this.saleRows = saleRows;
	}

	public List<Collection> getCollectionRows() {
		return collectionRows;
	}

	public void setCollectionRows(List<Collection> collectionRows) {
		this.collectionRows = collectionRows;
	}
}
