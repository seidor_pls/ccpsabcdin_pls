package com.ccps.web.model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * @author plopezs
 *
 */
@Entity
@Table(name="ccps_file_type_company")
public class FileTypeCompany implements Serializable {
	private static final long serialVersionUID = 7319998720576005764L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(nullable=false)
	private int id;
	
	//bi-directional many-to-one association to ccpsFileType
	@ManyToOne
	@JoinColumn(name="id_file_type")
	private FileType fileType;
	
	//bi-directional many-to-one association to ccpsCompany
	@ManyToOne
	@JoinColumn(name="id_company")
	private Company company;
	
	@Column(nullable=false)
	private Boolean enabled;
	
	public FileTypeCompany() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public FileType getFileType() {
		return fileType;
	}

	public void setFileType(FileType fileType) {
		this.fileType = fileType;
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}
}