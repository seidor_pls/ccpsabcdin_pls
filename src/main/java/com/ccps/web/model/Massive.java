package com.ccps.web.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * author
 * plopezs
 */

@Entity
@Table(name="ccps_massive")
public class Massive implements Serializable{
	private static final long serialVersionUID = 950285017325260140L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(nullable=false)
	private int id;
	
	//uni-directional many-to-one association to ccpsFileType
	@ManyToOne
	@JoinColumn(name="id_type")
	private FileType fileType;
	
	//uni-directional many-to-one association to ccpsBase
	@ManyToOne
	@JoinColumn(name="id_base")
	private BaseType baseType;
	
	//uni-directional many-to-one association to ccpsPeriod
	@ManyToOne
	@JoinColumn(name="id_period")
	private Period period;
	
	@Column(nullable=false)
	private int cantRegistros;
	
	@Column(nullable=false)
	private long totalMontoAsegurado;
	
	@Column(nullable=false)
	private long totalValorPrima;

	public Massive() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Massive(int id, FileType fileType, BaseType baseType, int cantRegistros, long totalMontoAsegurado,
			long totalValorPrima) {
		super();
		this.id = id;
		this.fileType = fileType;
		this.baseType = baseType;
		this.cantRegistros = cantRegistros;
		this.totalMontoAsegurado = totalMontoAsegurado;
		this.totalValorPrima = totalValorPrima;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public FileType getFileType() {
		return fileType;
	}

	public void setFileType(FileType fileType) {
		this.fileType = fileType;
	}

	public BaseType getBaseType() {
		return baseType;
	}

	public void setBaseType(BaseType baseType) {
		this.baseType = baseType;
	}

	public Period getPeriod() {
		return period;
	}

	public void setPeriod(Period period) {
		this.period = period;
	}

	public int getCantRegistros() {
		return cantRegistros;
	}

	public void setCantRegistros(int cantRegistros) {
		this.cantRegistros = cantRegistros;
	}

	public long getTotalMontoAsegurado() {
		return totalMontoAsegurado;
	}

	public void setTotalMontoAsegurado(long totalMontoAsegurado) {
		this.totalMontoAsegurado = totalMontoAsegurado;
	}

	public long getTotalValorPrima() {
		return totalValorPrima;
	}

	public void setTotalValorPrima(long totalValorPrima) {
		this.totalValorPrima = totalValorPrima;
	}
}
