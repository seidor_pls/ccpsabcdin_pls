package com.ccps.web.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author jDuchens
 *
 */

@Entity
@Table(name="ccps_collection")
public class Collection implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(nullable=false)
	private Integer id;
	
	//bi-directional many-to-one association to ccpsFileType
	@ManyToOne
	@JoinColumn(name="id_type")
	private FileType fileType;
	
	//uni-directional many-to-one association to ccpsPeriod
	@ManyToOne
	@JoinColumn(name="id_period")
	private Period period;
	
	//uni-directional many-to-one association to ccpsCompany
	@ManyToOne
	@JoinColumn(name="id_company")
	private Company company;
	
	//uni-directional many-to-one association to ccpsEnterprise
	@ManyToOne
	@JoinColumn(name="id_enterprise")
	private Enterprise enterprise;
	
	@Column(nullable=true)
	private String rutEmp;
	
	@Column(nullable=true)
	private Integer rubro;

	@Column(nullable=true)
	private Integer correlativo;
	
	@Column(nullable=true)
	private String rutCli;
	
	@Column(nullable=true)
	private Date fechaAplicacion;
	
	@Column(nullable=true)
	private Integer numeroRemesa;

	@Column(nullable=true)
	private Date fechaProceso;

	@Column(nullable=true)
	private Integer monto;

	public Collection() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Collection(FileType fileType, Period period, Company company, Enterprise enterprise,
			String rutEmp, Integer rubro, Integer correlativo, String rutCli, Date fechaAplicacion,
			Integer numeroRemesa, Date fechaProceso, Integer monto) {
		super();
		this.fileType = fileType;
		this.period = period;
		this.company = company;
		this.enterprise = enterprise;
		this.rutEmp = rutEmp;
		this.rubro = rubro;
		this.correlativo = correlativo;
		this.rutCli = rutCli;
		this.fechaAplicacion = fechaAplicacion;
		this.numeroRemesa = numeroRemesa;
		this.fechaProceso = fechaProceso;
		this.monto = monto;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public FileType getFileType() {
		return fileType;
	}

	public void setFileType(FileType fileType) {
		this.fileType = fileType;
	}

	public Period getPeriod() {
		return period;
	}

	public void setPeriod(Period period) {
		this.period = period;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public Enterprise getEnterprise() {
		return enterprise;
	}

	public void setEnterprise(Enterprise enterprise) {
		this.enterprise = enterprise;
	}

	public String getRutEmp() {
		return rutEmp;
	}

	public void setRutEmp(String rutEmp) {
		this.rutEmp = rutEmp;
	}

	public Integer getRubro() {
		return rubro;
	}

	public void setRubro(Integer rubro) {
		this.rubro = rubro;
	}

	public Integer getCorrelativo() {
		return correlativo;
	}

	public void setCorrelativo(Integer correlativo) {
		this.correlativo = correlativo;
	}

	public String getRutCli() {
		return rutCli;
	}

	public void setRutCli(String rutCli) {
		this.rutCli = rutCli;
	}

	public Date getFechaAplicacion() {
		return fechaAplicacion;
	}

	public void setFechaAplicacion(Date fechaAplicacion) {
		this.fechaAplicacion = fechaAplicacion;
	}

	public Integer getNumeroRemesa() {
		return numeroRemesa;
	}

	public void setNumeroRemesa(Integer numeroRemesa) {
		this.numeroRemesa = numeroRemesa;
	}

	public Date getFechaProceso() {
		return fechaProceso;
	}

	public void setFechaProceso(Date fechaProceso) {
		this.fechaProceso = fechaProceso;
	}

	public Integer getMonto() {
		return monto;
	}

	public void setMonto(Integer monto) {
		this.monto = monto;
	}
}
