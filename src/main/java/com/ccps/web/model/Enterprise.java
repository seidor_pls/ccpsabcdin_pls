package com.ccps.web.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.*;

/**
 * @author plopezs
 *
 */
@Entity
@Table(name="ccps_enterprise")
public class Enterprise implements Serializable {
	private static final long serialVersionUID = 3657732141893368499L;

	@Id
	@Column(nullable=false)
	private int id;
	
	@Column(nullable=false)
	private String name;
	
	@Column(nullable=false)
	private Boolean enabled;
	
	@Column(nullable=false)
	private Date creaDate;
	
	public Enterprise() {
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public Date getCreaDate() {
		return creaDate;
	}

	public void setDate(Date creaDate) {
		this.creaDate = creaDate;
	}
}