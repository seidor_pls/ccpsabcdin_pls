package com.ccps.web.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author jDuchens
 *
 */

@Entity
@Table(name="ccps_sale")
public class Sale implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(nullable=false)
	private Integer id;
	
	//bi-directional many-to-one association to ccpsFileType
	@ManyToOne
	@JoinColumn(name="id_type")
	private FileType fileType;
	
	//uni-directional many-to-one association to ccpsPeriod
	@ManyToOne
	@JoinColumn(name="id_period")
	private Period period;
	
	//uni-directional many-to-one association to ccpsCompany
	@ManyToOne
	@JoinColumn(name="id_company")
	private Company company;
	
	//uni-directional many-to-one association to ccpsEnterprise
	@ManyToOne
	@JoinColumn(name="id_enterprise")
	private Enterprise enterprise;
	
	@Column(nullable=true)
	private Integer certificado;

	@Column(nullable=true)
	private Integer sucursal;
	
	@Column(nullable=true)
	private String rutVendedor;
	
	@Column(nullable=true)
	private String nombreVendedor;
	
	@Column(nullable=true)
	private String cargoVendedor;
	
	@Column(nullable=true)
	private String rutReferente;
	
	@Column(nullable=true)
	private String nombreReferente;
	
	@Column(nullable=true)
	private String cargoReferente;
	
	@Column(nullable=true)
	private String indCruce;
	
	@Column(nullable=true)
	private Integer nroRemesa;
	
	@Column(nullable=true)
	private Integer codProducto;
	
	@Column(nullable=true)
	private Integer puntosVenta;
	
	@Column(nullable=true)
	private Date fecEmision;
	
	@Column(nullable=true)
	private Integer tarjeta;
	
	@Column(nullable=true)
	private Double primaAnualizada;
	
	@Column(nullable=true)
	private Double comisionRecaudacion;
	
	@Column(nullable=true)
	private Double comisionRecaudacion2;
	
	@Column(nullable=true)
	private String rutAsegurado;
	
	@Column(nullable=true)
	private String nombreAsegurado;
	
	@Column(nullable=true)
	private Date fecNacimiento;
	
	@Column(nullable=true)
	private String direccionParticular;
	
	@Column(nullable=true)
	private String comuna;
	
	@Column(nullable=true)
	private String telefonoParticular;
	
	@Column(nullable=true)
	private String telefonoLaboral;
	
	@Column(nullable=true)
	private String telefonoCelular;
	
	@Column(nullable=true)
	private String estado;
	
	@Column(nullable=true)
	private String usuarioDesafiliacion;
	
	@Column(nullable=true)
	private Date fechaDesafiliacion;
	
	@Column(nullable=true)
	private String motivoDesafiliacion;
	
	@Column(nullable=true)
	private Integer identificadorGrabacion;

	public Sale() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Sale(FileType fileType, Period period, Company company, Enterprise enterprise, Integer certificado, Integer sucursal, String rutVendedor, String nombreVendedor,
			String cargoVendedor, String rutReferente, String nombreReferente, String cargoReferente, String indCruce,
			Integer nroRemesa, Integer codProducto, Integer puntosVenta, Date fecEmision, Integer tarjeta,
			Double primaAnualizada, Double comisionRecaudacion, Double comisionRecaudacion2, String rutAsegurado,
			String nombreAsegurado, Date fecNacimiento, String direccionParticular, String comuna,
			String telefonoParticular, String telefonoLaboral, String telefonoCelular, String estado,
			String usuarioDesafiliacion, Date fechaDesafiliacion, String motivoDesafiliacion,
			Integer identificadorGrabacion) {
		super();
		this.fileType = fileType;
		this.period = period;
		this.company = company;
		this.enterprise = enterprise;
		this.certificado = certificado;
		this.sucursal = sucursal;
		this.rutVendedor = rutVendedor;
		this.nombreVendedor = nombreVendedor;
		this.cargoVendedor = cargoVendedor;
		this.rutReferente = rutReferente;
		this.nombreReferente = nombreReferente;
		this.cargoReferente = cargoReferente;
		this.indCruce = indCruce;
		this.nroRemesa = nroRemesa;
		this.codProducto = codProducto;
		this.puntosVenta = puntosVenta;
		this.fecEmision = fecEmision;
		this.tarjeta = tarjeta;
		this.primaAnualizada = primaAnualizada;
		this.comisionRecaudacion = comisionRecaudacion;
		this.comisionRecaudacion2 = comisionRecaudacion2;
		this.rutAsegurado = rutAsegurado;
		this.nombreAsegurado = nombreAsegurado;
		this.fecNacimiento = fecNacimiento;
		this.direccionParticular = direccionParticular;
		this.comuna = comuna;
		this.telefonoParticular = telefonoParticular;
		this.telefonoLaboral = telefonoLaboral;
		this.telefonoCelular = telefonoCelular;
		this.estado = estado;
		this.usuarioDesafiliacion = usuarioDesafiliacion;
		this.fechaDesafiliacion = fechaDesafiliacion;
		this.motivoDesafiliacion = motivoDesafiliacion;
		this.identificadorGrabacion = identificadorGrabacion;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public FileType getFileType() {
		return fileType;
	}

	public void setFileType(FileType fileType) {
		this.fileType = fileType;
	}

	public Period getPeriod() {
		return period;
	}

	public void setPeriod(Period period) {
		this.period = period;
	}
	
	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public Enterprise getEnterprise() {
		return enterprise;
	}

	public void setEnterprise(Enterprise enterprise) {
		this.enterprise = enterprise;
	}

	public Integer getCertificado() {
		return certificado;
	}

	public void setCertificado(Integer certificado) {
		this.certificado = certificado;
	}

	public Integer getSucursal() {
		return sucursal;
	}

	public void setSucursal(Integer sucursal) {
		this.sucursal = sucursal;
	}

	public String getRutVendedor() {
		return rutVendedor;
	}

	public void setRutVendedor(String rutVendedor) {
		this.rutVendedor = rutVendedor;
	}

	public String getNombreVendedor() {
		return nombreVendedor;
	}

	public void setNombreVendedor(String nombreVendedor) {
		this.nombreVendedor = nombreVendedor;
	}

	public String getCargoVendedor() {
		return cargoVendedor;
	}

	public void setCargoVendedor(String cargoVendedor) {
		this.cargoVendedor = cargoVendedor;
	}

	public String getRutReferente() {
		return rutReferente;
	}

	public void setRutReferente(String rutReferente) {
		this.rutReferente = rutReferente;
	}

	public String getNombreReferente() {
		return nombreReferente;
	}

	public void setNombreReferente(String nombreReferente) {
		this.nombreReferente = nombreReferente;
	}

	public String getCargoReferente() {
		return cargoReferente;
	}

	public void setCargoReferente(String cargoReferente) {
		this.cargoReferente = cargoReferente;
	}

	public String getIndCruce() {
		return indCruce;
	}

	public void setIndCruce(String indCruce) {
		this.indCruce = indCruce;
	}

	public Integer getNroRemesa() {
		return nroRemesa;
	}

	public void setNroRemesa(Integer nroRemesa) {
		this.nroRemesa = nroRemesa;
	}

	public Integer getCodProducto() {
		return codProducto;
	}

	public void setCodProducto(Integer codProducto) {
		this.codProducto = codProducto;
	}

	public Integer getPuntosVenta() {
		return puntosVenta;
	}

	public void setPuntosVenta(Integer puntosVenta) {
		this.puntosVenta = puntosVenta;
	}

	public Date getFecEmision() {
		return fecEmision;
	}

	public void setFecEmision(Date fecEmision) {
		this.fecEmision = fecEmision;
	}

	public Integer getTarjeta() {
		return tarjeta;
	}

	public void setTarjeta(Integer tarjeta) {
		this.tarjeta = tarjeta;
	}

	public Double getPrimaAnualizada() {
		return primaAnualizada;
	}

	public void setPrimaAnualizada(Double primaAnualizada) {
		this.primaAnualizada = primaAnualizada;
	}

	public Double getComisionRecaudacion() {
		return comisionRecaudacion;
	}

	public void setComisionRecaudacion(Double comisionRecaudacion) {
		this.comisionRecaudacion = comisionRecaudacion;
	}

	public Double getComisionRecaudacion2() {
		return comisionRecaudacion2;
	}

	public void setComisionRecaudacion2(Double comisionRecaudacion2) {
		this.comisionRecaudacion2 = comisionRecaudacion2;
	}

	public String getRutAsegurado() {
		return rutAsegurado;
	}

	public void setRutAsegurado(String rutAsegurado) {
		this.rutAsegurado = rutAsegurado;
	}

	public String getNombreAsegurado() {
		return nombreAsegurado;
	}

	public void setNombreAsegurado(String nombreAsegurado) {
		this.nombreAsegurado = nombreAsegurado;
	}

	public Date getFecNacimiento() {
		return fecNacimiento;
	}

	public void setFecNacimiento(Date fecNacimiento) {
		this.fecNacimiento = fecNacimiento;
	}

	public String getDireccionParticular() {
		return direccionParticular;
	}

	public void setDireccionParticular(String direccionParticular) {
		this.direccionParticular = direccionParticular;
	}

	public String getComuna() {
		return comuna;
	}

	public void setComuna(String comuna) {
		this.comuna = comuna;
	}

	public String getTelefonoParticular() {
		return telefonoParticular;
	}

	public void setTelefonoParticular(String telefonoParticular) {
		this.telefonoParticular = telefonoParticular;
	}

	public String getTelefonoLaboral() {
		return telefonoLaboral;
	}

	public void setTelefonoLaboral(String telefonoLaboral) {
		this.telefonoLaboral = telefonoLaboral;
	}

	public String getTelefonoCelular() {
		return telefonoCelular;
	}

	public void setTelefonoCelular(String telefonoCelular) {
		this.telefonoCelular = telefonoCelular;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getUsuarioDesafiliacion() {
		return usuarioDesafiliacion;
	}

	public void setUsuarioDesafiliacion(String usuarioDesafiliacion) {
		this.usuarioDesafiliacion = usuarioDesafiliacion;
	}

	public Date getFechaDesafiliacion() {
		return fechaDesafiliacion;
	}

	public void setFechaDesafiliacion(Date fechaDesafiliacion) {
		this.fechaDesafiliacion = fechaDesafiliacion;
	}

	public String getMotivoDesafiliacion() {
		return motivoDesafiliacion;
	}

	public void setMotivoDesafiliacion(String motivoDesafiliacion) {
		this.motivoDesafiliacion = motivoDesafiliacion;
	}

	public Integer getIdentificadorGrabacion() {
		return identificadorGrabacion;
	}

	public void setIdentificadorGrabacion(Integer identificadorGrabacion) {
		this.identificadorGrabacion = identificadorGrabacion;
	}
}
