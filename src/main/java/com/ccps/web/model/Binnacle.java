package com.ccps.web.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author jDuchens
 *
 */

@Entity
@Table(name="ccps_binnacle")
public class Binnacle implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(nullable=false)
	private int id;
	
	@Column(nullable=false)
	private Date binDate;
	
	//bi-directional many-to-one association to ccpsFileType
	@ManyToOne
	@JoinColumn(name="id_type", nullable=false)
	private FileType fileType;

	//bi-directional many-to-one association to ccpsUser
	@ManyToOne
	@JoinColumn(name="user_id", nullable=false)
	private User user;
	
	//uni-directional many-to-one association to ccpsPeriod
	@ManyToOne
	@JoinColumn(name="id_period")
	private Period period;
	
	@Column(nullable=true)
	private String details;
	
	@Column(nullable=true)
	private String details2;
	
	//BIN TYPE: Tipo de inserción en la bitacora. 1 = upload de archivo, 2 = generacion de archivo, 3 = update o modificación de información, 4 = cierre de mes
	@Column(nullable=false)
	private Integer binType;

	public Binnacle() {
		super();
	}
	
	public Binnacle(Date binDate, FileType fileType, User user, Period period, String details, String details2) {
		super();
		this.binDate = binDate;
		this.fileType = fileType;
		this.user = user;
		this.period = period;
		this.details = details;
		this.details2 = details2;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getBinDate() {
		return binDate;
	}

	public void setBinDate(Date binDate) {
		this.binDate = binDate;
	}

	public FileType getFileType() {
		return fileType;
	}

	public void setFileType(FileType fileType) {
		this.fileType = fileType;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Period getPeriod() {
		return period;
	}

	public void setPeriod(Period period) {
		this.period = period;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	public String getDetails2() {
		return details2;
	}

	public void setDetails2(String details2) {
		this.details2 = details2;
	}

	public Integer getBinType() {
		return binType;
	}

	public void setBinType(Integer binType) {
		this.binType = binType;
	}
}
