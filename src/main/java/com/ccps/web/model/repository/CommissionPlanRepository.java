package com.ccps.web.model.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.ccps.web.model.CommissionPlan;

/**
 * @author plopezs
 *
 */
public interface CommissionPlanRepository extends JpaRepository<CommissionPlan, Integer>{

	public final static String FIND_ALL = "SELECT c FROM CommissionPlan c WHERE enabled = true";
	public final static String FIND_BY_RANGE = "SELECT c FROM CommissionPlan c WHERE :range BETWEEN c.rangoInferior AND c.rangoSuperior AND enabled = true";
	public final static String FIND_BY_TIPO_PLAN = "SELECT c FROM CommissionPlan c WHERE c.tipoPlan = :tipo AND enabled = true";

	@Query(FIND_ALL)
	public List<CommissionPlan> findAll();
	
	@Query(FIND_BY_RANGE)
	public CommissionPlan findByRange(@Param("range") Integer range);
	
	@Query(FIND_BY_TIPO_PLAN)
	public CommissionPlan findByTipoPlan(@Param("tipo") String tipo);
}