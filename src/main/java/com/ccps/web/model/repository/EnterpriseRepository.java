package com.ccps.web.model.repository;

import java.util.List;

import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.ccps.web.model.Enterprise;

/**
 * @author jDuchens, plopezs
 *
 */
public interface EnterpriseRepository extends JpaRepository<Enterprise, Integer> {
	
	public final static String FIND_ALL  = "SELECT u FROM Enterprise u";
	public final static String FIND_BY_NAME_ENABLE  = "SELECT e FROM Enterprise e WHERE e.name = :name and e.enabled = true";

	@Query(FIND_ALL)
	public List<Enterprise> findAll();
	
	@Query(FIND_BY_NAME_ENABLE)
	public Enterprise findByName(@Param("name") String name);
}