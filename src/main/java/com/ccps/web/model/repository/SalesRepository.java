package com.ccps.web.model.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.ccps.web.model.Sale;

/**
 * @author jDuchens
 *
 */

public interface SalesRepository extends JpaRepository<Sale, Integer>{
	
	public final static String FIND_BY_ID_QUERY = "SELECT s FROM Sale s WHERE s.id = :id ORDER BY s.id DESC";
	public final static String FIND_BY_PERIOD_COMPANY = "SELECT s FROM Sale s WHERE s.period.mes = :month AND s.period.ano = :year AND s.company.id = :companyId";
	public final static String FIND_BY_PERIOD_COMPANY_SIN_ESTADO = "SELECT s FROM Sale s WHERE s.period.mes = :month AND s.period.ano = :year AND s.company.id = :companyId AND s.estado != :estado AND s.primaAnualizada > 0";
	public final static String FIND_BY_PERIOD_COMPANY_ENTERPRISE = "SELECT s FROM Sale s WHERE s.period.mes = :month AND s.period.ano = :year AND s.company.id = :companyId AND s.enterprise.id = :enterpriseId";
	public final static String FIND_BY_PERIOD_COMPANY_ENTERPRISE_COD_PRODUCTO = "SELECT s FROM Sale s WHERE s.period.mes = :month AND s.period.ano = :year AND s.company.id = :companyId AND s.enterprise.id = :enterpriseId AND s.codProducto = :codProducto AND s.estado != :estado AND s.primaAnualizada > 0";
	public final static String FIND_COLPRODUCTO_BY_PERIOD_COMPANY_ENTERPRISE = "SELECT DISTINCT s.codProducto FROM Sale s WHERE s.period.mes = :month AND s.period.ano = :year AND s.company.id = :companyId AND s.enterprise.id = :enterpriseId";
	
	@Query(FIND_BY_ID_QUERY)
	public Sale findById(@Param("id") Integer saleId);
	
	@Query(FIND_BY_PERIOD_COMPANY)
	public List<Sale> findByPeriodCompany(@Param("month") int month, @Param("year") int year, @Param("companyId") int companyId);
	
	@Query(FIND_BY_PERIOD_COMPANY_SIN_ESTADO)
	public List<Sale> findByPeriodCompanySinEstado(@Param("month") int month, @Param("year") int year, @Param("companyId") int companyId, @Param("estado") String estado);
	
	@Query(FIND_BY_PERIOD_COMPANY_ENTERPRISE)
	public List<Sale> findByPeriodCompanyEnterprise(@Param("month") int month, @Param("year") int year, @Param("companyId") int companyId, @Param("enterpriseId") int enterpriseId);
	
	@Query(FIND_BY_PERIOD_COMPANY_ENTERPRISE_COD_PRODUCTO)
	public List<Sale> findByPeriodCompanyEnterpriseCodProducto(@Param("month") int month, @Param("year") int year, @Param("companyId") int companyId, @Param("enterpriseId") int enterpriseId, @Param("codProducto") int codProducto, @Param("estado") String estado);
	
	@Query(FIND_COLPRODUCTO_BY_PERIOD_COMPANY_ENTERPRISE)
	public List<Integer> findColProductoByPeriodCompanyEnterprise(@Param("month") int month, @Param("year") int year, @Param("companyId") int companyId, @Param("enterpriseId") int enterpriseId);
}
