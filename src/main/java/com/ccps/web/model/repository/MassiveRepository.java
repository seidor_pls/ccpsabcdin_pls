package com.ccps.web.model.repository;

import java.util.List;

import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ccps.web.model.Massive;

/**
 * @author plopezs
 *
 */
public interface MassiveRepository extends JpaRepository<Massive, Integer> {
	
	public final static String FIND_ALL = "SELECT u FROM Massive u";
	public final static String FIND_BY_FILE_TYPE_PERIOD = "SELECT u FROM Massive u WHERE u.fileType.id = ?1 AND u.period.mes = ?2 AND u.period.ano = ?3";
	public final static String FIND_BY_FILE_TYPE_BASE_PERIOD = "SELECT u FROM Massive u WHERE u.fileType.id = ?1 AND u.baseType.id = ?2 AND u.period.mes = ?3 AND u.period.ano = ?4";
	
	@Query(FIND_ALL)
	public List<Massive> findAll();
	
	@Query(FIND_BY_FILE_TYPE_PERIOD)
	public List<Massive> findByFileTypePeriod(int fileTypeId, int month, int year);
	
	@Query(FIND_BY_FILE_TYPE_BASE_PERIOD)
	public List<Massive> findByFileTypeBasePeriod(int fileTypeId, int baseTypeId, int month, int year);
}
