package com.ccps.web.model.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.ccps.web.model.BaseType;

/**
 * @author plopezs
 *
 */
public interface BaseTypeRepository extends JpaRepository<BaseType, Integer> {
	
	public final static String FIND_ALL = "SELECT b FROM BaseType b";
	public final static String FIND_BY_TIPO = "SELECT b FROM BaseType b WHERE b.tipo = :tipo";
	public final static String FIND_BY_NAME = "SELECT b FROM BaseType b WHERE b.nombreInterno = :name";

	@Query(FIND_ALL)
	public List<BaseType> findAll();
	
	@Query(FIND_BY_TIPO)
	public List<BaseType> findByTipo(@Param("tipo") String tipo);
	
	@Query(FIND_BY_NAME)
	public BaseType findByNombreInterno(@Param("name") String name);
}