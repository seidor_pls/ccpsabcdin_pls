package com.ccps.web.model.repository;

import java.util.List;

import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ccps.web.model.User;

/**
 * @author jDuchens
 *
 */

public interface UserRepository extends JpaRepository<User, Integer> {

	public final static String FIND_ALL = "SELECT u FROM User u";
	public final static String FIND_BY_EMAIL = "SELECT u FROM User u WHERE u.email = ?1";
	
	@Query(FIND_ALL)
	public List<User> findAll();
	
	@Query(FIND_BY_EMAIL)
	public User findByEmail(String email);
	
}