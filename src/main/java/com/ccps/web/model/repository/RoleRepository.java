package com.ccps.web.model.repository;

import java.util.List;

import javax.persistence.NamedQuery;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ccps.web.model.Role;

/**
 * @author jDuchens
 *
 */


public interface RoleRepository extends JpaRepository<Role, Integer> {
	public final static String FIND_ALL = "SELECT r FROM Role r";
	
	@Query(FIND_ALL)
	public List<Role> findAll();
}