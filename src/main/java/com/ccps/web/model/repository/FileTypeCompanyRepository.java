package com.ccps.web.model.repository;

import java.util.List;

import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.ccps.web.model.FileType;
import com.ccps.web.model.FileTypeCompany;

/**
 * @author jDuchens, plopezs
 *
 */
public interface FileTypeCompanyRepository extends JpaRepository<FileTypeCompany, Integer> {
	
	public final static String FIND_ALL = "SELECT u FROM FileTypeCompany u";
	public final static String FIND_BY_COMPANY = "SELECT u FROM FileTypeCompany u WHERE u.company.id = ?1 and u.enabled = true";
	public final static String FIND_BY_FILE_TYPE = "SELECT u FROM FileTypeCompany u WHERE u.fileType.id = ?1 and u.enabled = true";	
	public final static String FIND_BY_COMPANY_CATEGORY = "SELECT f "+
														  "FROM FileTypeCompany f "+
														  "WHERE f.fileType.category=:category "+
														  "AND f.company.id=:companyId";
	
	@Query(FIND_ALL)
	public List<FileTypeCompany> findAll();
	
	@Query(FIND_BY_COMPANY)
	public List<FileTypeCompany> findByCompany(int companyId);
	
	@Query(FIND_BY_FILE_TYPE)
	public List<FileTypeCompany> findByFileType(int fileTypeId);
	
	@Query(FIND_BY_COMPANY_CATEGORY)
	public List<FileTypeCompany> findByCompanyCategory(@Param("companyId") int companyId, @Param("category") String category);
}
