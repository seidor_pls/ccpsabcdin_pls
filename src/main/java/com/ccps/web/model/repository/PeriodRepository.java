package com.ccps.web.model.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.ccps.web.model.Period;

/**
 * @author plopezs, jDuchens
 *
 */

public interface PeriodRepository extends JpaRepository<Period, Integer>{

	public final static String FIND_BY_YEAR_MONTH_TYPE_QUERY        = "SELECT p FROM Period p WHERE p.ano = :year and p.mes = :month and p.type.id = :typeId";
	public final static String FIND_ALL_BY_TYPE_QUERY               = "SELECT P FROM Period p WHERE p.type.id = :typeId";
	public final static String FIND_ALL_CLOSED_BY_YEAR_TYPE_QUERY   = "SELECT P FROM Period p WHERE p.ano = :year AND p.type.id = :typeId AND p.cerrado = true";
	public final static String FIND_MAX_YEAR_CLOSED_BY_TYPE_QUERY   = "SELECT MAX(p.ano) FROM Period p WHERE p.type.id = :typeId AND p.cerrado = true";
	public final static String FIND_ALL_OPEN_YEAR_MONTH_TYPE_QUERY  = "SELECT p FROM Period p WHERE p.mes = :month AND p.ano = :year AND p.type.id = :typeId AND p.cerrado = false";
	
	@Query(FIND_BY_YEAR_MONTH_TYPE_QUERY)
	public List<Period> findByYearMonthType(@Param("year") Integer year, @Param("month") Integer month, @Param("typeId") Integer typeId);
	
	@Query(FIND_ALL_BY_TYPE_QUERY)
	public List<Period> findAllPeriodsByType(@Param("typeId") Integer typeId);
	
	@Query(FIND_ALL_CLOSED_BY_YEAR_TYPE_QUERY)
	public List<Period> findAllClosedPeriodsByYearType(@Param("year") Integer year, @Param("typeId") Integer typeId);
	
	@Query(FIND_MAX_YEAR_CLOSED_BY_TYPE_QUERY)
	public Integer findMaxYearClosedPeriodsByType(@Param("typeId") Integer typeId);
	
	@Query(FIND_ALL_OPEN_YEAR_MONTH_TYPE_QUERY)
	public List<Period> findAllOpenPeriodsByMonthYearType(@Param("month") Integer month, @Param("year") Integer year, @Param("typeId") Integer typeId);
}
