package com.ccps.web.model.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ccps.web.model.Commission;

/**
 * @author plopezs
 *
 */
public interface CommissionRepository extends JpaRepository<Commission, Integer> {
	
	public final static String FIND_ALL = "SELECT u FROM Commission ORDER BY u.id ASC";
	public final static String FIND_BY_COMPANY = "SELECT u FROM Commission u where u.company.id = ?1 ORDER BY u.rubro ASC";
	public final static String FIND_BY_COMPANY_COD_PRODUCTO = "SELECT u FROM Commission u where u.company.id = ?1 AND u.codProducto = ?2";
	public final static String FIND_BY_COMPANY_RUBRO = "SELECT u FROM Commission u where u.company.id = ?1 AND u.rubro = ?2";
	public final static String FIND_BY_COMPANY_ENTERPRISE_RUBRO = "SELECT u FROM Commission u where u.company.id = ?1 AND u.enterprise.id = ?2 AND u.rubro = ?3";
	public final static String FIND_BY_COMPANY_ENTERPRISE = "SELECT u FROM Commission u where u.company.id = ?1 AND u.enterprise.id = ?2";
	
	@Query(FIND_ALL)
	public List<Commission> findAll();
	
	@Query(FIND_BY_COMPANY)
	public List<Commission> findByCompany(int companyId);
	
	@Query(FIND_BY_COMPANY_COD_PRODUCTO)
	public List<Commission> findByCompanyCodProducto(int companyId, int codProducto);
	
	@Query(FIND_BY_COMPANY_RUBRO)
	public List<Commission> findByCompanyRubro(int companyId, int rubro);
	
	@Query(FIND_BY_COMPANY_ENTERPRISE_RUBRO)
	public List<Commission> findByCompanyEnterpriseRubro(int companyId, int enterpriseId, int rubro);
	
	@Query(FIND_BY_COMPANY_ENTERPRISE)
	public List<Commission> findByCompanyEnterprise(int companyId, int enterpriseId);
}