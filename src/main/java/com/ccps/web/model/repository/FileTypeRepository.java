package com.ccps.web.model.repository;

import java.util.List;

import javax.persistence.NamedQuery;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.ccps.web.model.FileType;

/**
 * @author jDuchens, plopezs
 *
 */
public interface FileTypeRepository extends JpaRepository<FileType, Integer> {
	
	public final static String FIND_ALL = "SELECT f FROM FileType f WHERE f.enabled = true";
	public final static String FIND_BY_ID_TYPE = "SELECT f FROM FileType f WHERE f.type = :type and f.enabled = true";
	public final static String FIND_BY_CATEGORY = "SELECT f FROM FileType f WHERE f.category = :category and f.enabled = true";

	@Query(FIND_BY_ID_TYPE)
	public FileType findByType(@Param("type") String type);
	
	@Query(FIND_BY_CATEGORY)
	public List<FileType> findByCategory(@Param("category") String category);
	
	@Query(FIND_ALL)
	public List<FileType> findAll();
}