package com.ccps.web.model.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.ccps.web.model.Authority;

/**
 * @author jDuchens, plopezs
 *
 */


public interface AuthorityRepository extends JpaRepository<Authority, Integer> {
	public final static String FIND_ALL = "SELECT a FROM Authority a";
	public final static String FIND_BY_USER = "SELECT a FROM Authority a WHERE a.user.id = :userId";

	@Query(FIND_ALL)
	public List<Authority> findAll();
	
	@Query(FIND_BY_USER)
	public List<Authority> findByUserId(@Param("userId") int userId);
}