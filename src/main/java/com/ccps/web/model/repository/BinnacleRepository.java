package com.ccps.web.model.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.ccps.web.model.Binnacle;

/**
 * @author jDuchens
 *
 */

public interface BinnacleRepository extends JpaRepository<Binnacle, Integer>{
	
	public final static String FIND_BY_TYPE_PERIOD_QUERY        = "SELECT b "+
																  "FROM Binnacle b "+
																  "LEFT JOIN b.period p "+
																  "WHERE p.ano=:year "+
																  "AND p.mes=:month " +
																  "AND p.cerrado = false " +
																  "AND p.type.id IN (:p1,:p2,:p3)" + 
																  "AND b.binType = :binType";
	
	public final static String FIND_LAST_BY_TYPE_PERIOD_QUERY     =   "SELECT b "+
																	  "FROM Binnacle b "+
																	  "LEFT JOIN b.period p "+
																	  "WHERE p.ano=:year "+
																	  "AND p.mes=:month " +
																	  "AND p.cerrado = false " +
																	  "AND p.type.id = :type " + 
																	  "AND b.binType = :binType " + 
																	  "ORDER BY b.id DESC";
	
	public final static String FIND_LAST_BY_DETAILS_PERIOD_QUERY     =   "SELECT b "+
																		"FROM Binnacle b "+
																		"LEFT JOIN b.period p "+
																		"WHERE p.ano=:year "+
																		"AND p.mes=:month " +
																		"AND p.cerrado = false " +
																		"AND b.details = :details " + 
																		"AND b.binType = :binType " +
																		"ORDER BY b.id DESC";
	
	public final static String FIND_LAST_CLOSED_BY_TYPE_PERIOD_QUERY     =   "SELECT b "+
																	  "FROM Binnacle b "+
																	  "LEFT JOIN b.period p "+
																	  "WHERE p.ano=:year "+
																	  "AND p.mes=:month " +
																	  "AND p.cerrado = true " +
																	  "AND b.fileType.id = :type " + 
																	  "AND b.binType = :binType " +
																	  "ORDER BY b.id DESC";
	public final static String FIND_BY_TYPE_PERIOD_QUERY_ALL    = "SELECT b FROM Binnacle b WHERE b.period.ano=:year AND b.period.mes=:month AND b.fileType.id = :fileType AND b.binType = :binType";
	
	
	@Query(FIND_BY_TYPE_PERIOD_QUERY)
	public List<Binnacle> findByTypeAndPeriod(@Param("year") Integer year, @Param("month") Integer month, @Param("p1") Integer p1, @Param("p2") Integer p2, @Param("p3") Integer p3, @Param("binType") Integer binType);
	
	@Query(FIND_LAST_BY_TYPE_PERIOD_QUERY)
	public List<Binnacle> findLastByTypeAndPeriod(@Param("year") Integer year, @Param("month") Integer month, @Param("type") Integer type, @Param("binType") Integer binType);
	
	@Query(FIND_LAST_BY_DETAILS_PERIOD_QUERY)
	public List<Binnacle> findLastByDetailsAndPeriod(@Param("year") Integer year, @Param("month") Integer month, @Param("details") String details, @Param("binType") Integer binType);
	
	@Query(FIND_LAST_CLOSED_BY_TYPE_PERIOD_QUERY)
	public List<Binnacle> findLastClosedByTypeAndPeriod(@Param("year") Integer year, @Param("month") Integer month, @Param("type") Integer type, @Param("binType") Integer binType);
	
	@Query(FIND_BY_TYPE_PERIOD_QUERY_ALL)
	public List<Binnacle> findByTypeAndAllPeriods(@Param("year") Integer year, @Param("month") Integer month, @Param("fileType") Integer fileType, @Param("binType") Integer binType);
}
