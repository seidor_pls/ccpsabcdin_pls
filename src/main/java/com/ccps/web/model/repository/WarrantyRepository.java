package com.ccps.web.model.repository;

import java.util.Date;
import java.util.List;

import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ccps.web.model.Warranty;

/**
 * @author plopezs
 *
 */

public interface WarrantyRepository extends JpaRepository<Warranty, Integer> {
	
	public final static String FIND_ALL = "SELECT u FROM Warranty u";
	public final static String FIND_BY_PERIOD = "SELECT u FROM Warranty u WHERE u.period.mes = ?1 and u.period.ano = ?2 and u.fileType.id = ?3";
	public final static String FIND_BY_DATE_RANGE = "SELECT u FROM Warranty u WHERE ((?1 >= u.fecInicio and ?1 <= u.fecFin) or (?2 >= u.fecInicio and ?1 <= u.fecFin)) and u.fileType.id = ?3";
	public final static String FIND_BY_PERIOD_STATUS = "SELECT u FROM Warranty u WHERE u.period.mes = ?1 and u.period.ano = ?2 and u.fileType.id = ?3 and u.status = ?4";
	public final static String FIND_DAYS_INI_FIN_BY_PERIOD = "SELECT DISTINCT CONCAT(CONCAT(u.fecInicio, '&'),u.fecFin) FROM Warranty u WHERE u.period.mes = ?1 and u.period.ano = ?2 and u.fileType.id = ?3";
	
	@Query(FIND_ALL)
	public List<Warranty> findAll(int month, int year, int fileTypeId);

	@Query(FIND_BY_PERIOD)
	public List<Warranty> findByPeriod(int month, int year, int fileTypeId);
	
	@Query(FIND_BY_DATE_RANGE)
	public List<Warranty> findByDateRange(Date fecIni, Date fecFin, int fileTypeId);
	
	@Query(FIND_BY_PERIOD_STATUS)
	public List<Warranty> findByPeriodStatus(int month, int year, int fileTypeId, String status);
	
	@Query(FIND_DAYS_INI_FIN_BY_PERIOD)
	public List<String> findDaysIniFinByPeriod(int month, int year, int fileTypeId);
}