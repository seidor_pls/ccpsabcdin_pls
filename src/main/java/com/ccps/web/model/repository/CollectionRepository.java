package com.ccps.web.model.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.ccps.web.model.Collection;

/**
 * @author jDuchens
 *
 */

public interface CollectionRepository extends JpaRepository<Collection, Integer> {
	
	public final static String FIND_ALL = "SELECT s FROM Collection s";
	public final static String FIND_BY_PERIOD = "SELECT s FROM Collection s WHERE s.period.mes = :month AND s.period.ano = :year";
	public final static String FIND_BY_PERIOD_COMPANY = "SELECT s FROM Collection s WHERE s.period.mes = :month AND s.period.ano = :year AND s.company.id = :companyId";
	public final static String FIND_BY_PERIOD_COMPANY_ENTERPRISE_RUBRO = "SELECT s FROM Collection s WHERE s.period.mes = :month AND s.period.ano = :year AND s.company.id = :companyId AND s.enterprise.id = :enterpriseId AND s.rubro = :rubro";
	public final static String FIND_RUBRO_BY_PERIOD_COMPANY_ENTERPRISE = "SELECT DISTINCT s.rubro FROM Collection s WHERE s.period.mes = :month AND s.period.ano = :year AND s.company.id = :companyId AND s.enterprise.id = :enterpriseId";

	@Query(FIND_ALL)
	public List<Collection> findAll();
	
	@Query(FIND_BY_PERIOD)
	public List<Collection> findByPeriod(@Param("month") int month, @Param("year") int year);
	
	@Query(FIND_BY_PERIOD_COMPANY)
	public List<Collection> findByPeriodCompany(@Param("month") int month, @Param("year") int year, @Param("companyId") int companyId);
	
	@Query(FIND_BY_PERIOD_COMPANY_ENTERPRISE_RUBRO)
	public List<Collection> findByPeriodCompanyEnterpriseRubro(@Param("month") int month, @Param("year") int year, @Param("companyId") int companyId, @Param("enterpriseId") int enterpriseId, @Param("rubro") int rubro);
	
	@Query(FIND_RUBRO_BY_PERIOD_COMPANY_ENTERPRISE)
	public List<Integer> findRubroByPeriodCompanyEnterprise(@Param("month") int month, @Param("year") int year, @Param("companyId") int companyId, @Param("enterpriseId") int enterpriseId);
}
