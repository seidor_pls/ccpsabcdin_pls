package com.ccps.web.model.repository;

import java.util.List;

import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.ccps.web.model.Company;

/**
 * @author jDuchens, plopezs
 *
 */
public interface CompanyRepository extends JpaRepository<Company, Integer> {
	
	public final static String FIND_ALL = "SELECT u FROM Company u";
	public final static String FIND_BY_RUT = "SELECT u FROM Company u WHERE u.rut = ?1";
	public final static String FIND_BY_NAME_ENABLE  = "SELECT c FROM Company c WHERE c.name = :name ";

	@Query(FIND_ALL)
	public List<Company> findAll();
	
	@Query(FIND_BY_RUT)
	public Company findByRut();
	
	@Query(FIND_BY_NAME_ENABLE)
	public Company findByName(@Param("name") String name);
}